using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TmManager.Tsdr;

namespace TmManager.Tests
{
    [TestClass]
    public class GivenTrademarkSerialNumber
    {
        [TestMethod]
        public async Task ShouldAbleToFetchDetailsFromLogicApp()
        {
            var results = await Tsdr.TsdrApiService.GetTrademarkDetails1("sn", true, "88910043");
            foreach (var each in results.transactionList[0].trademarks[0].parties.ownerGroups)
            {
                var ownergroup = each.Value.ToObject<List<GetTrademarkDetails_10>>();
            }
            //var service-new TradeMarkService(DbContext);
            //var saveResult = service.SyncTrademark(results);
        }

        [TestMethod]
        public async Task ShouldAbleToFetchTrademarkDocsFromTempString()
        {
            var results = await Tsdr.TsdrApiService.GetTrademarkDocumentDetails("sn", false, "88910043");
        }
    }
}
