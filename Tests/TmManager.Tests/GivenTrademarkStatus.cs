using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Data.Entity;
using TmManager.Trademarks;

namespace TmManager.Tests
{
    [TestClass]
    public class GivenTrademarkStatus
    {
        [TestMethod]
        public async Task ShouldBeAbleToDetermineStatusOfApplication()
        {
            const long tmNumber = 87486841;
            using IEntityContext dbcontext = new EntityContext();
            var tm = await dbcontext.Select<Trademark>(c => c.IsActive && c.SerialNumber == tmNumber).FirstOrDefaultAsync();
            var statusChecker = new TrademarkStatusChecker(tm);
            Assert.AreEqual(statusChecker.CurrentStatus, TrademarkProgressStatus.NineA);
        }
        [TestMethod]
        public async Task ShouldBeAbleToDetermineRegisteredStatus()
        {
            const long tmNumber = 77514654;//Abr
            using IEntityContext dbcontext = new EntityContext();
            var tm = await dbcontext.Select<Trademark>(c => c.IsActive && c.SerialNumber == tmNumber).FirstOrDefaultAsync();
            var statusChecker = new TrademarkStatusChecker(tm);
            Assert.AreEqual(statusChecker.RegisteredStatusColor, RegisteredTrademarkStatusColor.Red);
        }
        [TestMethod]
        public async Task ShouldBeAbleToDetermineDeadTrademark()
        {
            try
            {
                const long tmNumber = 87275324;//dead Nike
                using IEntityContext dbcontext = new EntityContext();
                var tm = await dbcontext.Select<Trademark>(c => c.IsActive && c.SerialNumber == tmNumber).FirstOrDefaultAsync();
                var statusChecker = new TrademarkStatusChecker(tm);
                Assert.AreEqual(statusChecker.CurrentStatus, TrademarkProgressStatus.Dead);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
            }
        }
        [TestMethod]
        public async Task ShouldBeAbleToDetermineStatusOfThreeShip()
        {
            const long tmNumber = 88910043;//Three Ship
            using IEntityContext dbcontext = new EntityContext();
            var tm = await dbcontext.Select<Trademark>(c => c.IsActive && c.SerialNumber == tmNumber).FirstOrDefaultAsync();
            var statusChecker = new TrademarkStatusChecker(tm);
            Assert.AreEqual(statusChecker.CurrentStatus, TrademarkProgressStatus.Registered);
            Assert.AreEqual(statusChecker.RegisteredStatusColor, RegisteredTrademarkStatusColor.Green);
        }
    }
}
