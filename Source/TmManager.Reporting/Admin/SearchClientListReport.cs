﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Identity;

namespace TmManager.Admin
{
    public class SearchClientListReport : ReportBase<SearchClientListRequest, SearchClientListResponse>
    {
        private readonly UserManager _userManager;
        public SearchClientListReport(UserManager userManager)
        {
            _userManager = userManager;
        }
        protected override async Task<ReportResponse> ExecuteAsync(SearchClientListRequest request)
        {
            var response = new SearchClientListResponse();
            try
            {
                using (_userManager)
                {
                    var query = _userManager.Users.Where(x => x.UserType == UserType.AppUser);
                    if (request.StartDate.HasValue && request.EndDate.HasValue)
                        query = query.Where(x => x.Created >= request.StartDate.Value && x.Created <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                    else
                    {
                        request.StartDate = DateTimeHelper.Now().AddDays(-7).Date;
                        request.EndDate = DateTimeHelper.Now().Date;
                        query = query.Where(x => x.Created >= request.StartDate.Value && x.Created <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                    }

                    if (!string.IsNullOrEmpty(request.Username))
                        query = query.Where(x => x.UserName.Contains(request.Username.Trim()));
                    if (!string.IsNullOrEmpty(request.Name))
                        query = query.Where(x => x.ClientName.Contains(request.Name.Trim()));

                    var count = await query.CountAsync();

                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Username":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.UserName) : query.OrderByDescending(x => x.UserName);
                                break;
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.ClientName) : query.OrderByDescending(x => x.ClientName);
                                break;
                            case "Registered":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.EmailConfirmed) : query.OrderByDescending(x => x.EmailConfirmed);
                                break;
                            case "Created":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);
                    response.Data = query.Select(x => new SearchClientListReportModel()
                    {
                        Id = x.Id,
                        Name = x.ClientName,
                        Email = x.Email,
                        Username = x.UserName,
                        Registered = x.EmailConfirmed ? "Yes" : "No",
                        Created = x.Created.ToString("yyyy.MM.dd")
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class SearchClientListRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
    }
    public class SearchClientListResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class SearchClientListReportModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Created { get; set; }
        public string Registered { get; set; }
        public string Name { get; set; }
    }
}
