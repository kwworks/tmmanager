﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Identity;

namespace TmManager.Admin
{
    public class SearchAdminReport : ReportBase<SearchAdminRequest,SearchAdminResponse>
    {
        private readonly UserManager _userManager;
        public SearchAdminReport(UserManager userManager)
        {
            _userManager = userManager;
        }
        protected override async Task<ReportResponse> ExecuteAsync(SearchAdminRequest request)
        {
            var response = new SearchAdminResponse();
            try
            {
                using (_userManager)
                {
                    var query = _userManager.Users.Where(x => x.UserType == UserType.Admin && x.UserName != "webmaster@kwworks.com");
                    var count = await query.CountAsync();

                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Username":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.FullName) : query.OrderByDescending(x => x.FullName);
                                break;
                            case "Email":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Email) : query.OrderByDescending(x => x.Email);
                                break;
                            case "Created":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);

                    response.Data = query.Select(x => new SearchAdminReportModel()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        Username = x.UserName,
                        Created = x.Created.ToString("yyyy.MM.dd")
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class SearchAdminRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
    }
    public class SearchAdminResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class SearchAdminReportModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Created { get; set; }
    }
}
