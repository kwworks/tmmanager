﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkRetrievedReport : ReportBase<TrademarkRetrievedRequest, TrademarkRetrievedResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkRetrievedReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkRetrievedRequest request)
        {
            var response = new TrademarkRetrievedResponse();
            try
            {
                using (_entityContext)
                {
                    var dateNow = DateTimeHelper.Now();
                    var last180Days = dateNow.AddDays(-180);
                    var query = _entityContext.Select<TrademarkImportFile>(x => x.IsActive && x.Created >= last180Days && x.Created <= dateNow.AddDays(1).AddTicks(-1));
                    var count = await query.CountAsync();
                    
                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
                                break;
                            case "Count":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkImportItems.Count()) : query.OrderByDescending(x => x.TrademarkImportItems.Count());
                                break;
                            case "Created":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);

                    response.Data = query.Select(x => new TrademarkRetrievedReportModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Count = x.TrademarkImportItems.Count(),
                        Processed = x.TrademarkImportItems.Count(x=> x.Status != TrademarkStatus.Processing),
                        UnProcessed = x.TrademarkImportItems.Count(x=> x.Status == TrademarkStatus.Processing),
                        Created = x.Created,
                        ShowButton = x.TrademarkImportItems.Any(x=> x.Status == TrademarkStatus.TrademarkNotFound),
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class TrademarkRetrievedResponse : ReportResponse
    {
        public TrademarkRetrievedResponse()
        {
            Data = new List<TrademarkRetrievedReportModel>();
        }
        public IEnumerable<TrademarkRetrievedReportModel> Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkRetrievedRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
    }

    public class TrademarkRetrievedReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public DateTime Created { get; set; }
        public int Processed { get; set; }
        public int UnProcessed { get; set; }
        public bool ShowButton { get; internal set; }
    }
}
