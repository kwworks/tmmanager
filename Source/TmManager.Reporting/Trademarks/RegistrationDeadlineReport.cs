﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class RegistrationDeadlineReport : ReportBase<RegistrationDeadlineRequest, RegistrationDeadlineResponse>
    {
        private readonly IEntityContext _entityContext;
        public RegistrationDeadlineReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(RegistrationDeadlineRequest request)
        {
            var response = new RegistrationDeadlineResponse();
            if (!string.IsNullOrEmpty(request.RegistrationDateType))
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<Trademark>(x => x.IsActive);

                        if (request.RegistrationDateType == "earliest")
                        {
                            if (request.StartDate.HasValue && request.EndDate.HasValue)
                                query = query.Where(x => x.NextDeadline >= request.StartDate.Value && x.NextDeadline <= request.EndDate.Value);
                        }
                        else
                        {
                            if (request.StartDate.HasValue && request.EndDate.HasValue)
                                query = query.Where(x => x.MaxNextDeadline >= request.StartDate.Value && x.MaxNextDeadline <= request.EndDate.Value);
                        }
                        
                        var count = await query.CountAsync();

                        if (!string.IsNullOrEmpty(request.SortMember))
                        {
                            switch (request.SortMember)
                            {
                                case "TrademarkName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkElement) : query.OrderByDescending(x => x.TrademarkElement);
                                    break;
                                case "RegistrationNumber":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.UsRegistrationNumber) : query.OrderByDescending(x => x.UsRegistrationNumber);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                case "RegistrationDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.UsRegistrationDate) : query.OrderByDescending(x => x.UsRegistrationDate);
                                    break;
                                case "EarliestFileDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.NextDeadline) : query.OrderByDescending(x => x.NextDeadline);
                                    break;
                                case "LatestFileDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.MaxNextDeadline) : query.OrderByDescending(x => x.MaxNextDeadline);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.FiliingDate);
                        
                        query = query.Skip(request.Skip).Take(request.Take);

                        response.Data = query.Select(x => new RegistrationDeadlineReportModel()
                        {
                            Id = x.Id,
                            TrademarkName = x.TrademarkElement,
                            SerialNumber = x.SerialNumber,
                            RegistrationNumber = x.UsRegistrationNumber,
                            RegistrationDate = x.UsRegistrationDate,
                            EarliestFileDate = x.NextDeadline,
                            LatestFileDate = x.MaxNextDeadline,
                            Status = x.NextDeadline.HasValue && DateTime.Now.Date > x.NextDeadline.Value ? "Red" : (x.NextDeadline.HasValue && (DateTime.Now.Date >= x.NextDeadline.Value && DateTime.Now.Date <= x.MaxNextDeadline.Value) ? "Yellow" : "Green"),
                            ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                        }).ToList();
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {
                response.Data = new List<TrademarkSearchReportModel>();
                response.Total = 0;
            }
            return response;
        }
    }

    public class RegistrationDeadlineResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class RegistrationDeadlineRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string RegistrationDateType { get; set; }
    }

    public class RegistrationDeadlineReportModel
    {
        public Guid Id { get; set; }
        public string TrademarkName { get; set; }
        public long SerialNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string ClientName { get; set; }
        public string Status { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime? EarliestFileDate { get; set; }
        public DateTime? LatestFileDate { get; set; }
    }
}
