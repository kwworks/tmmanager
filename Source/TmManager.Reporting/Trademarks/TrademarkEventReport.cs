﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkEventReport : ReportBase<TrademarkEventRequest, TrademarkEventResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkEventReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkEventRequest request)
        {
            var response = new TrademarkEventResponse();
            if (request.StartDate.HasValue && request.EndDate.HasValue)
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<ProsecutionHistory>(x => x.IsActive && x.EntryDate >= request.StartDate && x.EntryDate <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                        if (!string.IsNullOrEmpty(request.SortDirection))
                        {
                            switch (request.SortMember)
                            {
                                case "EventDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.EntryDate) : query.OrderByDescending(x => x.EntryDate);
                                    break;
                                case "EventName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Description) : query.OrderByDescending(x => x.Description);
                                    break;
                                case "TrademarkName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Trademark.Name) : query.OrderByDescending(x => x.Trademark.Name);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.EntryDate);

                        query = query.Skip(request.Skip).Take(request.Take);
                        var count = await _entityContext.Select<ProsecutionHistory>(x => x.IsActive && x.EntryDate >= request.StartDate && x.EntryDate <= request.EndDate).CountAsync();

                        response.Data = (query)
                            .Select(x => new ProsecutionHistoryReportModel()
                            {
                                Id = x.TrademarkId,
                                SerialNumber = x.Trademark.SerialNumber,
                                TrademarkName = x.Trademark.TrademarkElement,
                                ClientName = x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                                EventDate = x.EntryDate.HasValue ? x.EntryDate.Value.ToString("yyyy.MM.dd") : string.Empty,
                                EventName = x.Description
                            }).ToList();
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {
                response.Data = new List<ProsecutionHistoryReportModel>();
                response.Total = 0;
            }
            return response;
        }
    }

    public class TrademarkEventRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class TrademarkEventResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class ProsecutionHistoryReportModel
    {
        public Guid Id { get; set; }
        public string EventDate { get; set; }
        public long SerialNumber { get; set; }
        public string EventName { get; set; }
        public string TrademarkName { get; set; }
        public string ClientName { get; set; }
    }
}
