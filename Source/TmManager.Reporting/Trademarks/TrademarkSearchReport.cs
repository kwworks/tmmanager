﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkSearchReport : ReportBase<TrademarkSearchRequest, TrademarkSearchResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkSearchReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkSearchRequest request)
        {
            var response = new TrademarkSearchResponse();
            if (request.SerialNumber > 0 || !string.IsNullOrEmpty(request.ClientEmail) || !string.IsNullOrEmpty(request.ClientName) || !string.IsNullOrEmpty(request.RegistrationNumber))
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<Trademark>(x => x.IsActive);

                        if (request.SerialNumber != null && request.SerialNumber > 0)
                            query = query.Where(x => x.SerialNumber == request.SerialNumber);
                        else if (!string.IsNullOrEmpty(request.RegistrationNumber))
                            query = query.Where(x => x.UsRegistrationNumber == request.RegistrationNumber);
                        else if (!string.IsNullOrEmpty(request.ClientEmail))
                            query = query.Where(x => x.OwnerGroups.Any(x => x.Email == request.ClientEmail.Trim()));
                        else if (!string.IsNullOrEmpty(request.ClientName))
                            query = query.Where(x => x.OwnerGroups.Any(x => x.Name.Contains(request.ClientName.Trim())));

                        if (!string.IsNullOrEmpty(request.SortMember))
                        {
                            switch (request.SortMember)
                            {
                                case "Name":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkElement) : query.OrderByDescending(x => x.TrademarkElement);
                                    break;
                                case "SerialNumber":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.SerialNumber) : query.OrderByDescending(x => x.SerialNumber);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                case "PrimaryEmail":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.OrderBy(x => x.Type).First().Email) : query.OrderByDescending(x => x.OwnerGroups.OrderByDescending(x => x.Type).First().Email);
                                    break;
                                case "ImportedDateString":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.FiliingDate);

                        query = query.Skip(request.Skip).Take(request.Take);
                        var count = await query.CountAsync();

                        response.Data = query.Select(x => new TrademarkSearchReportModel()
                        {
                            //Id = x.OwnerGroups.OrderByDescending(x => x.Type).First().Id,
                            Id = x.Id,
                            ClientId = x.User.Id,
                            Name = x.TrademarkElement,
                            ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                            ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                            FilingDate = x.FiliingDate,
                            ImportedDate = x.Created,
                            PrimaryEmail = x.User.Email,
                            StatusDescription = x.Tm5StatusDescription,
                            SerialNumber = (int)x.SerialNumber,
                            AttorneyName = x.Attorney.Name ?? "N/A",
                        }).ToList();
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {
                response.Data = new List<TrademarkSearchReportModel>();
                response.Total = 0;
                response.Errors.Add("At least one field is required");
            }
            return response;
        }
    }

    public class TrademarkSearchResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkSearchRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public string Name { get; set; }
        public long? SerialNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string PrimaryEmail { get; set; }
        public string ImportedDate { get; set; }
    }

    public class TrademarkSearchReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public DateTime ImportedDate { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string StatusDescription { get; set; }
        public string AttorneyName { get; set; }
        public DateTime? FilingDate { get; set; }
        public string ClientId { get; set; }
    }
}
