﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkArchievedReport : ReportBase<TrademarkArchievedRequest, TrademarkArchievedResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkArchievedReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkArchievedRequest request)
        {
            var response = new TrademarkArchievedResponse();
            try
            {
                using (_entityContext)
                {
                    var query = _entityContext.Select<TrademarkImportFile>(x => x.IsActive);
                    var last90Days = DateTimeHelper.Now().AddDays(90);

                    if (request.StartDate.HasValue && request.EndDate.HasValue)
                        query = query.Where(x => x.Created >= request.StartDate.Value && x.Created <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                    else
                        query = query.Where(x => x.Created >= last90Days.AddDays(-90) && x.Created <= last90Days);

                    var count = await query.CountAsync();
                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
                                break;
                            case "Count":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkImportItems.Count()) : query.OrderByDescending(x => x.TrademarkImportItems.Count());
                                break;
                            case "Created":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);

                    response.Data = query.Select(x => new TrademarkArchievedReportModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Count = x.TrademarkImportItems.Count(),
                        Created = x.Created
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class TrademarkArchievedResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkArchievedRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class TrademarkArchievedReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public DateTime Created { get; set; }
    }
}
