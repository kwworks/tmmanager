﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkDeadReport : ReportBase<TrademarkDeadRequest, TrademarkDeadResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkDeadReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkDeadRequest request)
        {
            var response = new TrademarkDeadResponse();
            try
            {
                using (_entityContext)
                {
                    var query = _entityContext.Select<Trademark>(x => x.Tm5StatusDescription.StartsWith("Dead"));
                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkElement) : query.OrderByDescending(x => x.TrademarkElement);
                                break;
                            case "SerialNumber":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.SerialNumber) : query.OrderByDescending(x => x.SerialNumber);
                                break;
                            case "ClientName":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                break;
                            case "PrimaryEmail":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.User.Email) : query.OrderByDescending(x => x.User.Email);
                                break;
                            case "ImportedDateString":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.FiliingDate);

                    query = query.Skip(request.Skip).Take(request.Take);
                    var count = await _entityContext.Select<Trademark>(x => x.Tm5StatusDescription.StartsWith("Dead")).CountAsync();

                    response.Data = query.Select(x => new TrademarkDeadReportModel()
                    {
                        Id = x.Id,
                        Name = x.TrademarkElement,
                        ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                        ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                        FilingDate = x.FiliingDate,
                        ImportedDate = x.Created,
                        PrimaryEmail = x.User.Email,
                        StatusDescription = x.Tm5StatusDescription,
                        SerialNumber = (int)x.SerialNumber,
                        AttorneyName = x.Attorney.Name ?? "N/A",
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class TrademarkDeadResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkDeadRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string PrimaryEmail { get; set; }
        public string ImportedDate { get; set; }
    }

    public class TrademarkDeadReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public DateTime ImportedDate { get; set; }
        public string PrimaryEmail { get; set; }
        public string StatusDescription { get; set; }
        public string AttorneyName { get; set; }
        public DateTime? FilingDate { get; set; }
    }
}
