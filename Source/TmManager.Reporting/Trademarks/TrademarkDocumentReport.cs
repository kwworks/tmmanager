﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkDocumentReport : ReportBase<TrademarkDocumentRequest, TrademarkDocumentResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkDocumentReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkDocumentRequest request)
        {
            var response = new TrademarkDocumentResponse();
            if(request.StartDate.HasValue && request.EndDate.HasValue)
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<Document>(x => x.IsActive && x.MailRoomDate >= request.StartDate && x.MailRoomDate <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                        if (!string.IsNullOrEmpty(request.SortDirection))
                        {
                            switch (request.SortMember)
                            {
                                case "DocumentDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.MailRoomDate) : query.OrderByDescending(x => x.MailRoomDate);
                                    break;
                                case "DocumentName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.DocumentType.Description) : query.OrderByDescending(x => x.DocumentType.Description);
                                    break;
                                case "TrademarkName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Trademark.Name) : query.OrderByDescending(x => x.Trademark.Name);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.MailRoomDate);

                        query = query.Skip(request.Skip).Take(request.Take);
                        var count = await _entityContext.Select<Document>(x => x.IsActive && x.MailRoomDate >= request.StartDate && x.MailRoomDate <= request.EndDate).CountAsync();

                        response.Data = (query)
                            .Select(x => new TrademarkDocumentReportModel()
                            {
                                Id = x.TrademarkId,
                                TrademarkName = x.Trademark.TrademarkElement,
                                SerialNumber = x.Trademark.SerialNumber,
                                ClientName = x.Trademark.OwnerGroups.Count() > 0 ? x.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                                DocumentDate = x.MailRoomDate.HasValue ? x.MailRoomDate.Value.ToString("yyyy.MM.dd") : string.Empty,
                                DocumentName = x.DocumentType.Description
                            }).ToList();
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {
                response.Data = new List<TrademarkDocumentReportModel>();
                response.Total = 0;
            }
            return response;
        }
    }

    public class TrademarkDocumentRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class TrademarkDocumentResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkDocumentReportModel
    {
        public Guid Id { get; set; }
        public string DocumentDate { get; set; }
        public long SerialNumber { get; set; }
        public string DocumentName { get; set; }
        public string TrademarkName { get; set; }
        public string ClientName { get; set; }
    }
}