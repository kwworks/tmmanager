﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkCreatedReport : ReportBase<TrademarkCreatedRequest, TrademarkCreatedResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkCreatedReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkCreatedRequest request)
        {
            var response = new TrademarkCreatedResponse();
            if (request.StartDate != null)
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<Trademark>(x => x.Created >= request.StartDate && x.Created <= request.EndDate.Value.AddDays(1).AddTicks(-1));
                        var attornies = await _entityContext.Select<Attorneys.RecognizedAttorney>(x => x.IsActive).Select(s => new { s.Name, s.Email }).ToArrayAsync();
                        var attorniesNames = attornies.Select(x => x.Name).ToArray();
                        var attorniesEmails = attornies.Select(x => x.Email).ToArray();
                        if (!string.IsNullOrEmpty(request.TrademarkStatus))
                        {
                            if (request.TrademarkStatus == "dead")
                                query = query.Where(x => x.Tm5StatusDescription.StartsWith(request.TrademarkStatus));

                            else if (request.TrademarkStatus == "attorneyissue")
                                query = query.Where(x => x.AttorneyId.HasValue == false || (attorniesNames.Contains(x.Attorney.Name) == false || attorniesEmails.Contains(x.Attorney.Email) == false));

                            else if (request.TrademarkStatus == "registered")
                                query = query.Where(x => x.CurrentStatus == "R");

                            if (request.TrademarkStatus == "notregistered")
                                query = query.Where(x => x.CurrentStatus != "R" && x.CurrentStatus != "D");
                        }
                        var count = await query.CountAsync();

                        if (!string.IsNullOrEmpty(request.SortMember))
                        {
                            switch (request.SortMember)
                            {
                                case "Name":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkElement) : query.OrderByDescending(x => x.TrademarkElement);
                                    break;
                                case "SerialNumber":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.SerialNumber) : query.OrderByDescending(x => x.SerialNumber);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                case "ClientEmail":
                                    //query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.OrderBy(x => x.Type).First().Email) : query.OrderByDescending(x => x.OwnerGroups.OrderByDescending(x => x.Type).First().Email);
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty);
                                    break;
                                case "ImportedDateString":
                                case "ImportedDate":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.FiliingDate);

                        query = query.Skip(request.Skip).Take(request.Take);

                        response.Data = query.Select(x => new TrademarkSearchReportModel()
                        {
                            Id = x.Id,
                            Name = x.TrademarkElement,
                            ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                            ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                            FilingDate = x.FiliingDate,
                            ImportedDate = x.Created,
                            PrimaryEmail = x.User != null ? x.User.Email : string.Empty,
                            StatusDescription = x.Tm5StatusDescription,
                            SerialNumber = (int)x.SerialNumber,
                            AttorneyName = x.Attorney.Name ?? "N/A",
                        }).ToList();
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {

            }
            return response;
        }
    }

    public class TrademarkCreatedResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class TrademarkCreatedRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public string Name { get; set; }
        public long? SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string PrimaryEmail { get; set; }
        public string ImportedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TrademarkStatus { get; set; }
    }

    public class TrademarkCreatedReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public DateTime ImportedDate { get; set; }
        public string PrimaryEmail { get; set; }
        public string StatusDescription { get; set; }
        public string AttorneyName { get; set; }
        public DateTime? FilingDate { get; set; }
    }
}
