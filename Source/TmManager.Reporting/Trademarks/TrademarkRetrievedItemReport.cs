﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Trademarks
{
    public class TrademarkRetrievedItemReport : ReportBase<TrademarkRetrievedItemRequest, TrademarkRetrievedItemResponse>
    {
        private readonly IEntityContext _entityContext;
        public TrademarkRetrievedItemReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(TrademarkRetrievedItemRequest request)
        {
            var response = new TrademarkRetrievedItemResponse();
            if (request.Id.HasValue)
            {
                try
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<TrademarkImportItem>(x => x.TrademarkImportFileId == request.Id);
                        var count = await query.CountAsync();

                        if (!string.IsNullOrEmpty(request.SortMember))
                        {
                            switch (request.SortMember)
                            {
                                case "SerialNumber":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.SerialNumber) : query.OrderByDescending(x => x.SerialNumber);
                                    break;
                                case "ClientName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.ClientName) : query.OrderByDescending(x => x.ClientName);
                                    break;
                                case "Email":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Email) : query.OrderByDescending(x => x.Email);
                                    break;
                                case "TrademarkName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkName) : query.OrderByDescending(x => x.TrademarkName);
                                    break;
                                case "Created":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.ClientName);

                        query = query.Skip(request.Skip).Take(request.Take);

                        var trademarkData = new List<TrademarkRetrievedItemReportModel>();
                        var status = new [] { TrademarkStatus.TrademarkAccountCreated, TrademarkStatus.Accepted,TrademarkStatus.TrademarkSuccess, TrademarkStatus.TrademarkLinked, };
                        foreach (var item in query.ToList())
                        {
                            var tempData = GetItem(item.Status);
                            trademarkData.Add(new TrademarkRetrievedItemReportModel()
                            {
                                Id = item.Id,
                                TrademarkId = item.TrademarkId,
                                SerialNumber = item.SerialNumber,
                                ClientName = item.ClientName,
                                Email = item.Email,
                                TrademarkName = item.TrademarkName,
                                Created = item.Created,
                                IsActive = tempData.IsActive,
                                StatusString = tempData.StatusString,
                                ImagePath = tempData.ImagePath,
                                ShowDetails = status.Contains(item.Status)
                            });
                        }
                        
                        response.Data = trademarkData;
                        response.Total = count;
                    }
                }
                catch (Exception ex)
                {
                    response.Errors.Add(ex.Message);
                }
            }
            else
            {
                response.Data = new List<TrademarkRetrievedItemReportModel>();
                response.Total = 0;
            }
            return response;
        }

        private TempTrademarkItem GetItem(TrademarkStatus status)
        {
            var result = new TempTrademarkItem();
            switch (status)
            {
                case TrademarkStatus.Pending:
                    result.StatusString = "Pending";
                    result.ImagePath = "";
                    result.IsActive = true;
                    break;
                case TrademarkStatus.TrademarkSuccess:
                    result.StatusString = "Accepted";
                    result.ImagePath = "/images/status-accepted.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkInSystem:
                    result.StatusString = "In System";
                    result.ImagePath = "/images/status-accepted.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.DifferentAssociate:
                    result.StatusString = "Different Associate";
                    result.ImagePath = "/images/redalert.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkDead:
                    result.StatusString = "Dead TM";
                    result.ImagePath = "/images/cross.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkNotFound:
                    result.StatusString = "Not Found";
                    result.ImagePath = "/images/status-unavailable.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkLinked:
                    result.StatusString = "Accepted";
                    result.ImagePath = "/images/status-accepted.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkAccountCreated:
                    result.StatusString = "Accepted";
                    result.ImagePath = "/images/status-accepted.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkAttorneyIssue:
                    result.StatusString = "Attorney Issue";
                    result.ImagePath = "/images/redalert.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkEmailNotFound:
                    result.StatusString = "Email Not Found";
                    result.ImagePath = "/images/status-unavailable.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.TrademarkFailed:
                    result.StatusString = "Failed";
                    result.ImagePath = "/images/status-rejected.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.Accepted:
                    result.StatusString = "Accepted";
                    result.ImagePath = "/images/status-accepted.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.Rejected:
                    result.StatusString = "Rejected";
                    result.ImagePath = "/images/status-rejected.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.Error:
                    result.StatusString = "Error";
                    result.ImagePath = "/images/status-rejected.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.SystemError:
                    result.StatusString = "System Error";
                    result.ImagePath = "/images/status-rejected.png";
                    result.IsActive = false;
                    break;
                case TrademarkStatus.Processing:
                    result.StatusString = "Processing";
                    result.ImagePath = "/images/status-unavailable.png";
                    result.IsActive = false;
                    break;
                default:
                    break;
            }
            return result;
        }
    }

    public class TrademarkRetrievedItemResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }
    public class TempTrademarkItem
    {
        public string StatusString { get; set; }
        public string ImagePath { get; set; }
        public bool IsActive { get; set; }
    }
    public class TrademarkRetrievedItemRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public Guid? Id { get; set; }
    }

    public class TrademarkRetrievedItemReportModel
    {
        public Guid Id { get; set; }
        public long SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string Email { get; set; }
        public string TrademarkName { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public string StatusString { get; set; }
        public string ImagePath { get; set; }
        public Guid? TrademarkId { get; set; }
        public bool ShowDetails { get; internal set; }
    }
}
