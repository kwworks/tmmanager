﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;
using TmManager.Data;

namespace TmManager.Attorney
{
    public class SearchRecognizedAttorneyReport : ReportBase<SearchRecognizedAttorneyRequest, SearchRecognizedAttorneyResponse>
    {
        private readonly IEntityContext _entityContext;
        public SearchRecognizedAttorneyReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected async override Task<ReportResponse> ExecuteAsync(SearchRecognizedAttorneyRequest request)
        {
            var response = new SearchRecognizedAttorneyResponse();
            try
            {
                using (_entityContext)
                {
                    var query = _entityContext.Select<RecognizedAttorney>(x => x.IsActive);
                    var count = await query.CountAsync();

                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
                                break;
                            case "Email":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Email) : query.OrderByDescending(x => x.Email);
                                break;
                            case "Active":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.IsActive) : query.OrderByDescending(x => x.IsActive);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);

                    response.Data = query.Select(x => new SearchRecognizedAttorneyReportModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Email = x.Email,
                        Active = x.IsActive
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class SearchRecognizedAttorneyRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
    }
    public class SearchRecognizedAttorneyResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class SearchRecognizedAttorneyReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
    }
}
