﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager
{
    public class ReportResponse
    {
        public ReportResponse()
        {
            Errors = new HashSet<string>();
        }
        public HashSet<string> Errors { get; set; }
    }
}
