﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TmManager
{
    public interface IReport:IDisposable {
        bool CanExecuteReport(ReportRequest request, HashSet<string> errros);
        Task<ReportResponse> ExecuteReportAsync(ReportRequest request);
    }
}
