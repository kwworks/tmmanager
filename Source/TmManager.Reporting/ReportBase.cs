﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TmManager
{
    public abstract class ReportBase<TRequest, TResponse> : IReport
        where TRequest : ReportRequest
        where TResponse : ReportResponse
    {
        protected virtual async Task<ReportResponse> ExecuteAsync(TRequest request)
        {
            return await Task.Run(() => { return default(TResponse); });
        }
        public virtual bool CanExecuteReport(ReportRequest request, HashSet<string> errros)
        {
            return true;
        }
        public virtual void Dispose()
        {
        }

        public async Task<ReportResponse> ExecuteReportAsync(ReportRequest request)
        {
            return await ExecuteAsync((TRequest)request);
        }
    }
}
