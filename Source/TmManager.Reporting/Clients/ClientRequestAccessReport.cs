﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.AccountRequests;
using TmManager.Data;

namespace TmManager.Clients
{
    public class ClientRequestAccessReport : ReportBase<ClientRequestAccessRequest, ClientRequestAccessResponse>
    {
        private readonly IEntityContext _entityContext;
        public ClientRequestAccessReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        protected override async Task<ReportResponse> ExecuteAsync(ClientRequestAccessRequest request)
        {
            var response = new ClientRequestAccessResponse();
            try
            {
                using (_entityContext)
                {
                    var query = _entityContext.Select<AccountRequest>(x => x.IsActive);
                    if (!string.IsNullOrEmpty(request.SortMember))
                    {
                        switch (request.SortMember)
                        {
                            case "Name":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
                                break;
                            //case "Email":
                            //    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Email) : query.OrderByDescending(x => x.Email);
                            //    break;
                            case "CompanyName":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Company) : query.OrderByDescending(x => x.Company);
                                break;
                            case "TrademarkName":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkName) : query.OrderByDescending(x => x.TrademarkName);
                                break;
                            case "IsAllowed":
                                query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.IsAllowed) : query.OrderByDescending(x => x.IsAllowed);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        query = query.OrderByDescending(x => x.Created);

                    query = query.Skip(request.Skip).Take(request.Take);
                    var count = await _entityContext.Select<AccountRequest>(x => x.IsActive).CountAsync();

                    response.Data = query.Select(x => new ClientRequestAccessReportModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Created = x.Created,
                        Company = x.Company,
                        Email = x.Email,
                        IsAllowed = x.IsAllowed,
                        TrademarkName = x.TrademarkName,
                    }).ToList();
                    response.Total = count;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class ClientRequestAccessRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
    }

    public class ClientRequestAccessResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class ClientRequestAccessReportModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string TrademarkName { get; set; }
        public bool IsAllowed { get; set; }
        public DateTime Created { get; set; }
    }
}
