﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Trademarks;

namespace TmManager.Clients
{
    public class SearchClientsReport : ReportBase<SearchClientsRequest, SearchClientsResponse>
    {
        private readonly IEntityContext _entityContext;
        public SearchClientsReport(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        protected override async Task<ReportResponse> ExecuteAsync(SearchClientsRequest request)
        {
            var response = new SearchClientsResponse();
            try
            {
                if (request.SerialNumber > 0 || !string.IsNullOrEmpty(request.ClientEmail) || !string.IsNullOrEmpty(request.ClientName) || !string.IsNullOrEmpty(request.PrimaryName))
                {
                    using (_entityContext)
                    {
                        var query = _entityContext.Select<Trademark>(x => x.IsActive && x.User != null);

                        if (request.SerialNumber != null && request.SerialNumber > 0)
                            query = query.Where(x => x.SerialNumber == request.SerialNumber);
                        else if (!string.IsNullOrEmpty(request.ClientEmail))
                            query = query.Where(x => x.OwnerGroups.Any(x => x.Email == request.ClientEmail.Trim()));
                        else if (!string.IsNullOrEmpty(request.ClientName))
                            query = query.Where(x => x.OwnerGroups.Any(x => x.Name.StartsWith(request.ClientName.Trim())));
                        else if (!string.IsNullOrWhiteSpace(request.PrimaryName))
                        {
                            var split = request.PrimaryName.Split(',');
                            if(split.Count() > 1)
                            {
                                query = query.Where(x => x.User.LastName == split[0] && x.User.FirstName == split[1]);
                            }
                            else
                            {
                                query = query.Where(x => x.User.LastName == request.PrimaryName);
                            }
                        }
                        if (!string.IsNullOrEmpty(request.SortMember))
                        {
                            switch (request.SortMember)
                            {
                                case "TrademarkName":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.TrademarkElement) : query.OrderByDescending(x => x.TrademarkElement);
                                    break;
                                case "PrimaryEmail":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.User.Email) : query.OrderByDescending(x => x.User.Email);
                                    break;
                                case "Name":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : query.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                                    break;
                                case "Created":
                                    query = request.SortDirection == "Ascending" ? query.OrderBy(x => x.Created) : query.OrderByDescending(x => x.Created);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                            query = query.OrderByDescending(x => x.FiliingDate);

                        query = query.Skip(request.Skip).Take(request.Take);
                        var count = await query.CountAsync();

                        response.Data = query.Select(x => new TrademarkSearchReportModel()
                        {
                            Id = x.Id,
                            ClientId = x.User != null ? x.User.Id : string.Empty,
                            Name = x.TrademarkElement,
                            ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                            ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                            FilingDate = x.FiliingDate,
                            ImportedDate = x.Created,
                            PrimaryEmail = x.User != null ? x.User.Email : string.Empty,
                            StatusDescription = x.Tm5StatusDescription,
                            SerialNumber = (int)x.SerialNumber,
                            PrimaryPhone = x.User != null ? x.User.PhoneNumber : string.Empty,
                            AttorneyName = x.Attorney.Name ?? "N/A",
                        }).Where(c => !string.IsNullOrEmpty(c.ClientId)).ToList();
                        response.Total = count;
                    }
                }
                else
                {
                    response.Data = new List<SearchClientsReportModel>();
                    response.Total = 0;
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
            return response;
        }
    }

    public class SearchClientsResponse : ReportResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }

    public class SearchClientsRequest : ReportRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public string SortDirection { get; set; }
        public string SortMember { get; set; }
        public string Name { get; set; }
        public long? SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string PrimaryName { get; set; }
    }

    public class SearchClientsReportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string ImportedDateString { get; set; }
        public string PrimaryEmail { get; set; }
        public string StatusDescription { get; set; }
        public string AttorneyName { get; set; }
        public DateTime? FilingDate { get; set; }
    }
}