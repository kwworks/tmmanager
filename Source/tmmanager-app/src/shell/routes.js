import React, { lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import SecureRoute from './SecureRoute';
import FallbackLoader from "../elements/FallbackLoader";

// Lazy Components
const Login = lazy(() => import('../components/Access/Login'));
const ForgotPassword = lazy(() => import('../components/Access/ForgotPassword'));
const ResetPassword = lazy(() => import('../components/Access/ResetPassword'));

const Registration = lazy(() => import('../components/Registration/Registration'));
const Request = lazy(() => import('../components/Registration/Request'));
const AccountReview = lazy(() => import('../components/Profile/Wizard'));
const ReviewComplete = lazy(() => import('../components/Profile/Complete'));
const Profile = lazy(() => import('../components/Profile/Profile'));
const AlertLists = lazy(() => import('../components/Alert/AlertsList'));
const AlertDetails = lazy(() => import('../components/Alert/AlertDetails'));

const Treadmarks = lazy(() => import('../components/Treadmarks'));
const Details = lazy(() => import('../components/Details'));

export default function Routes(props) {
    return (
        <Suspense fallback={<FallbackLoader allowimage={false} allowloader={true} />}>
            <Switch>
                <Route exact sensitive path="/" component={Login} />
                <Route exact sensitive path="/forgotpassword" component={ForgotPassword} />
                <Route exact sensitive path="/resetpassword" component={ResetPassword} />
                <Route exact sensitive path="/register" component={Registration} />
                <Route exact sensitive path="/register/:email" component={Registration} />
                <Route exact sensitive path="/request" component={Request} />
                <Route exact sensitive path="/review/:email" component={AccountReview} />
                <Route exact sensitive path="/complete/:email" component={ReviewComplete} />
                <SecureRoute exact sensitive path="/profile" component={Profile} />
                <SecureRoute exact sensitive path="/alerts" component={AlertLists} />
                <SecureRoute exact sensitive path="/alerts/:id" component={AlertDetails} />
                <SecureRoute exact sensitive path="/trademarks" component={Treadmarks} />
                <SecureRoute exact sensitive path="/trademarks/:id" component={Details} />
                <Route path=""><NotFound /></Route>
            </Switch>
        </Suspense>
    );
}

function NotFound() {
    return (
        <div>
            <h2>Not Found</h2>
            
        </div>
    );
}

