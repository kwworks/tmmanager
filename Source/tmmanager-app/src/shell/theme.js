import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
	palette: {
		primary: {
			light: '#F2ECE7',
			main: '#842828',
			medium: '#D9C9BA'
		},
		secondary: {
			light: '#707070',
			main: '#222222',
			dark: '#515151'
		},
		others: {
			light: '#68733F',
			main: '#004CFF',
			medium: '#FF00D6',
		},
		light: {
			light: '#E4EFF7',
			main: '#ffffff',
			dark: '#FCFEE2'
		},
		danger: {
			main: '#FF0000',
		},
		success: {
			main: '#24A649',
		},
	},
	typography: {
		h1: {
			fontSize: 32,
			fontWeight: 600,
			fontFamily: 'Adobe-Jenson-Pro',
			marginBottom: 8,
		},
		h2: {
			fontSize: 24,
			fontWeight: 400,
			'& strong':{
				fontWeight: 500,
			}
		},
		h3: {
			fontSize: 22,
			fontWeight: 600,
		},
		h4: {
			fontSize: 20,
			fontWeight: 400,
		},
		h5: {
			fontSize: 18,
			fontWeight: 400,
		},
		h6: {
			fontSize: 16,
			fontWeight: 600,
		},
		subtitle1: {
			fontSize: 16,
			fontWeight: 500,
			letterSpacing: '-0.1px',
			lineHeight: 1,
		},
		subtitle2: {
			fontSize: 16,
			fontWeight: 400,
			letterSpacing: 0,
			lineHeight: 1.25,
		},
		body1: {
			fontSize: 14,
		},
		body2: {
			fontSize: 12
		},
	},
	overrides: {
		MuiCssBaseline: {
			'@global': {
				html: {
					'& *, & .MuiInputBase-input': {
						fontFamily: 'Runda',
					},
					'& ul': {
						paddingLeft: 16
					},
					'& *[gutterbottomdouble="true"]':{
						marginBottom: 16,
					},
					'& table':{
						width: '100%',
						textAlign:'left',
						fontSize: 16,
						'& a':{
							color: '#004CFF',
						},
					},
					'& svg.danger .a':{
						stroke: '#FF0000',
					},
					'& [data-error]':{
						color: '#f44336',
					},
					'& .MuiList-padding[role="listbox"]':{
						padding: 0,
					}
				},
			},
		},
		MuiMenu: {
			list: {
				'& section': {
					'& .MuiListItem-root': {
						padding: '0 10px',
					}
				}
			},
		},
		MuiTypography: {
			root: {
				color: '#222222',
				wordBreak: 'break-word',
				'&:not(.MuiTypography-h1)': {
					fontFamily: 'Runda',
				},
			}
		},
		MuiTextField: {
			root: {
				width: '100%',
				padding: '18px 0 24px 0',
				'& .MuiInputLabel-outlined:not(.Mui-focused), & .MuiInputLabel-outlined.Mui-error': {
					color: '#222222',
				},
				'& .MuiInputLabel-outlined': {
					fontSize: 14,
					transform: 'translate(0, 0) scale(1)!important',
					'& .MuiFormLabel-asterisk':{
						color: '#FF0000',
					}
				},
				'& .MuiInputBase-input': {
					padding: '16px'
				},
				'& fieldset': {
					top: 0,
					borderWidth: 2,
					'& legend': {
						display: 'none',
					},
				},
				'& .MuiOutlinedInput-root.Mui-error .MuiOutlinedInput-notchedOutline':{
					borderColor: 'rgba(0, 0, 0, 0.23)',
				},
				'& .MuiFormHelperText-root': {
					position: 'absolute',
					margin: 0,
					bottom: 5,
					left: 0,
					fontFamily: 'Runda',
				}
			}
		},
		MuiFormControl:{
			root:{
				width: '100%',
				padding: '18px 0 24px 0',
				'& .MuiInputLabel-formControl:not(.Mui-focused), & .MuiInputLabel-formControl.Mui-error': {
					color: '#222222',
				},
				'& .MuiInputLabel-shrink, & .MuiInputLabel-formControl': {
					fontSize: 14,
					transform: 'translate(0, 0) scale(1)!important',
				},
				'& .MuiInputBase-input': {
					padding: '16px'
				},
				'& fieldset': {
					top: 0,
					borderWidth: 2,
					'& legend': {
						display: 'none',
					},
				},
				'& .MuiOutlinedInput-root.Mui-error .MuiOutlinedInput-notchedOutline':{
					borderColor: 'rgba(0, 0, 0, 0.23)',
				},
				'& .MuiInputLabel-formControl + div + .MuiTypography-body2': {
					position: 'absolute',
					margin: 0,
					bottom: 5,
					left: 0,
				},
				'& .MuiAutocomplete-clearIndicatorDirty':{
					display: 'none',
				}
			}
		},
		MuiButton: {
			root: {
				padding: '12px!important',
				textTransform: 'normal',
				boxShadow: '0 0 1px',
				lineHeight: 1.5,
				transition: '0.5s',
				'&.MuiButton-outlined':{
					borderWidth: 2,
					lineHeight: 1.25,
				},
				'&.MuiButton-contained':{
					'&.success':{
						backgroundColor: '#24A649',
					},
					'&.danger':{
						backgroundColor: '#FF0000',
					},
					'&.darked':{
						backgroundColor: '#707070',
					},
					'@media (min-width: 1024px)':{
						'&:hover':{
							opacity: 0.75,
							transition: '0.5s',
						}
					},
				},
				'& .MuiButton-label': {
					fontSize: 16,
					fontWeight: 400,
				},
			}
		},
	},
});

export default theme;