import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import AuthService from '../services/authService';
//con
const SecureRoute = ({ component: Component, ...rest }) => (
    <Route {...rest}
        render={(props) => (
            AuthService.IsAuthenticated() === true ? <Component {...props} /> :
            <Redirect to={{pathname: '/'}}/>
        )}
    />
);
export default SecureRoute;