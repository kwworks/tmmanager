import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import AccessLayout from '../../layouts/AccessLayout';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import FallbackLoader from '../../elements/FallbackLoader';
import AlertBox from '../../elements/AlertBox';
import ClearIcon from '@material-ui/icons/Clear';
import CommonService from '../../services/CommonService';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg";
import { ReactComponent as InfoIcon } from '../../assets/infoIcon.svg';

const useStyles = makeStyles((theme) => ({
  AccessBox: {
    width: '90%',
    maxWidth: 1050,
    margin: '24px auto',
    borderRadius: 25,
  },
  Logo: {
    textAlign: 'center',
    '& img': {
      maxWidth: '100%',
    },
    [theme.breakpoints.up('md')]: {
      '& img': {
        maxWidth: '350px',
        float: 'left',
        marginBottom: theme.spacing(2),
      },
    }
  },
  RedBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'stretch',
    flex: 1,
    height: 'calc(100vh - 125px)',
    marginTop: theme.spacing(2),
    backgroundColor: theme.palette.primary.main,
    borderRadius: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      height: 'calc(100vh - 160px)',
    },
  },
  Titlebar: {
    flex: 0,
    padding: theme.spacing(1.5),
    backgroundColor: '#2976A6',
    margin: theme.spacing(1.5, 1.5, 0),
    borderRadius: theme.spacing(1.5, 1.5, 0, 0),
    position: 'relative',
    [theme.breakpoints.up('md')]: {
      margin: theme.spacing(3, 3, 0),
    },
    '& .MuiTypography-h2': {
      color: theme.palette.light.main,
      fontSize: 30,
      letterSpacing: 1,
      '& + svg': {
        position: 'absolute',
        right: 5,
        top: 15,
        width: '1.5em',
        height: '1.5em',
        fill: theme.palette.light.main,
      }
    }
  },
  Access: {
    padding: theme.spacing(1.5),
    backgroundColor: theme.palette.light.main,
    margin: theme.spacing(0, 1.5, 1.5),
    overflowY: 'auto',
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(3, 9),
      margin: theme.spacing(0, 3, 3),
    },
    '& .MuiTypography-h2': {
      fontWeight: 500,
    },
    '& button': {
      marginBottom: theme.spacing(3),
    },
  }
}));

export default function Request(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);
  const [state, setState] = React.useState([]);
  const history = useHistory();

  const handleOnChange = (event, name) => {
    setState({
      ...state,
      [name]: event.target.value,
    })
  };

  const GotoLogin = () => {
    history.push('/')
  };

  const GoBack = () => {
    history.goBack()
  }

  const handleRegister = () => {
    setLoader(true);
    if (state.name === '' || state.name === undefined || state.coName === '' || state.coName === undefined || (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0) || state.email === '' || state.email === undefined || state.tmName === '' || state.tmName === undefined) {
      setState({
        ...state,
        error: true,
      })
      setLoader(false);
    }
    else {
      var request = {
        Email: state.email,
        Name: state.name,
        Company: state.coName,
        TrademarkName: state.tmName
      };

      CommonService.ExecutePost({ Url: "requestaccess" }, request, (res) => {
        setLoader(true);
        if (res.isSuccess) {
          setState({
            ...state,
            error: false,
            currentView: 'requestComplete'
          });

        } else {
          setState({
            ...state,
            error: true,
            message: res.Errors[0]
          });
        }
        setLoader(false);
      });
      setLoader(false);
    }
  };

  const CloseDialog = (name) => {
    setState({
      ...state,
      [name]: false,
    })
  }

  const OpenDialog = (name) => {
    setState({
      ...state,
      [name]: true,
    })
  }

  return (
    <AccessLayout>
      {loader ? <FallbackLoader allowimage={true} allowloader={true} /> : ''}
      <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
        </Grid>
        <div className={classes.RedBox}>
          <Grid item xs={12} className={classes.Titlebar}>
            <Typography variant="h2"><strong>TM Manager Account Sign Up</strong></Typography>
            <ClearIcon onClick={() => OpenDialog('cancelSetup')} />
          </Grid>
          {(state.currentView === undefined || state.currentView === "requestAccess") ?
            <Grid item xs={12} className={classes.Access}>
              <Typography variant="h2" gutterBottom>Request Account Access</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">Fill out the form below to request account access. All fields are mandatory.</Typography>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="name"
                  label="Name"
                  variant="outlined"
                  placeholder=""
                  value={state.name || ""}
                  onChange={(event) => handleOnChange(event, 'name')}
                  error={state?.error || false}
                  helperText={(state.name === undefined || state.name === "") && state.error === true ? "Name is required" : ""}
                  fullWidth
                  required
                />

                <TextField
                  id="coName"
                  label="Company"
                  variant="outlined"
                  placeholder=""
                  value={state.coName || ""}
                  onChange={(event) => handleOnChange(event, 'coName')}
                  error={state?.error || false}
                  helperText={(state.coName === undefined || state.coName === "") && state.error === true ? "Company Name is required" : ""}
                  fullWidth
                  required
                />

                <TextField
                  id="email"
                  label="Email"
                  variant="outlined"
                  placeholder=""
                  value={state.email || ""}
                  onChange={(event) => handleOnChange(event, 'email')}
                  error={state?.error || false}
                  helperText={(state.email === undefined || state.email === "") && state.error === true ? "Email is required" : (state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
                  fullWidth
                  required
                />

                <TextField
                  id="tmName"
                  label="Trademark Name"
                  variant="outlined"
                  placeholder=""
                  value={state.tmName || ""}
                  onChange={(event) => handleOnChange(event, 'tmName')}
                  error={state?.error || false}
                  helperText={(state.tmName === undefined || state.tmName === "") && state.error === true ? "Trademark is required" : ""}
                  fullWidth
                  required
                />
                <Button onClick={() => handleRegister()} variant="contained" color="primary" disableElevation fullWidth size="large">Request Account Access</Button>
                <Typography variant="body1" data-error gutterBottom>{state?.message || ""}</Typography>
              </Grid>
            </Grid> :
            <Grid item xs={12} className={classes.Access}>
              <Typography variant="h2" gutterBottom>Request Sent</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">Your request for account access has been sent. A system administrator will review your material and be in touch with you shortly.</Typography>
              <Grid item xs={12} sm={6}>
                <Button onClick={() => GotoLogin()} variant="contained" color="primary" disableElevation fullWidth size="large">Back to Login Screen</Button>
              </Grid>
            </Grid>}
        </div>
      </Grid>
      <AlertBox
        open={state.cancelSetup === undefined ? false : state.cancelSetup}
        content={
          <>
            <div style={{ textAlign: 'center' }}>
              <InfoIcon className={'danger'} />
              <Typography variant="h2" gutterBottom><strong>Are you sure you want to stop?</strong></Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">All your changes will be lost. Select yes to proceed, no to cancel this action.</Typography>
            </div>
          </>
        }
        cancelButtonText="Yes, do it!"
        cancelButtonClass="danger"
        proceedButtonText="No, Cancel"
        proceedButtonClass="darked"
        onClickCancel={() => GoBack()}
        onClickProceed={() => CloseDialog('cancelSetup')}
      />
    </AccessLayout>
  );
}