import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useParams, useHistory } from "react-router-dom";
import AccessLayout from '../../layouts/AccessLayout';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import FallbackLoader from '../../elements/FallbackLoader';
import AlertBox from '../../elements/AlertBox';
import ClearIcon from '@material-ui/icons/Clear';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg";
import { ReactComponent as InfoIcon } from '../../assets/infoIcon.svg';

// Queries
import CommonService from '../../services/CommonService';

const useStyles = makeStyles((theme) => ({
  AccessBox: {
    width: '90%',
    maxWidth: 1050,
    margin: '24px auto',
    borderRadius: 25,
  },
  Logo: {
    textAlign: 'center',
    '& img': {
      maxWidth: '100%',
    },
    [theme.breakpoints.up('md')]: {
      '& img': {
        maxWidth: '350px',
        float: 'left',
        marginBottom: theme.spacing(2),
      },
    }
  },
  RedBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'stretch',
    flex: 1,
    height: 'calc(100vh - 125px)',
    marginTop: theme.spacing(2),
    backgroundColor: theme.palette.primary.main,
    borderRadius: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      height: 'calc(100vh - 160px)',
    },
  },
  Titlebar: {
    flex: 0,
    padding: theme.spacing(1.5),
    backgroundColor: '#2976A6',
    margin: theme.spacing(1.5, 1.5, 0),
    borderRadius: theme.spacing(1.5, 1.5, 0, 0),
    position: 'relative',
    [theme.breakpoints.up('md')]: {
      margin: theme.spacing(3, 3, 0),
    },
    '& .MuiTypography-h2': {
      color: theme.palette.light.main,
      fontSize: 30,
      letterSpacing: 1,
      '& + svg': {
        position: 'absolute',
        right: 5,
        top: 15,
        width: '1.5em',
        height: '1.5em',
        fill: theme.palette.light.main,
      }
    }
  },
  Access: {
    padding: theme.spacing(1.5),
    backgroundColor: theme.palette.light.main,
    margin: theme.spacing(0, 1.5, 1.5),
    overflowY: 'auto',
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(3, 9),
      margin: theme.spacing(0, 3, 3),
      '& .MuiButton-fullWidth, & .MuiFormControl-fullWidth': {
        display: 'table',
        width: '50%',
      }
    },
    '& .MuiTypography-h2': {
      fontWeight: 500,
    },
    '& button': {
      marginBottom: theme.spacing(3),
    },
  }
}));

export default function Registration(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);
  const [state, setState] = React.useState({
    currentView: useParams().email !== undefined ? "enterCode" : undefined,
    email: useParams().email
  });

  const history = useHistory();

  const handleOnChange = (event, name) => {
    setState({
      ...state,
      [name]: event.target.value,
    })
  };

  const GotoURL = (url) => {
    if (url === "/review") {
      url = url + "/" + state.email;
    }
    history.push(url)
  }

  const GoBack = () => {
    history.goBack()
  }

  const handleRegister = () => {
    setLoader(true);
    if (
      (state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ||
      state.email === '' ||
      state.email === undefined) {
      setState({
        ...state,
        error: true,
      })
      setLoader(false);
    }
    else {
      CommonService.ExecuteGet({ Url: "verifyemail?email=" + state.email }, (res) => {
        if (res.Errors.length > 0) {
          var error = res.Errors[0];
          if (error === "Email found. Account not found!") {
            setState({ ...state, currentView: 'enterCode', error: false })
            setLoader(false);
          } else {
            setState({ ...state, currentView: 'requestAccess', error: false })
            setLoader(false);
          }

        }
        else if (res.IsSuccess === true) {
          setState({ ...state, currentView: 'accountAlreadyCreated', error: false })
          setLoader(false);
        }

      });
    }
  };

  const handleAccessCode = () => {
    setLoader(true);
    if (state.code === '' || state.code === undefined) {
      setState({
        ...state,
        error: true,
      })
      setLoader(false);
    }
    else {
      CommonService.ExecuteGet({ Url: "verifyaccesscode?email=" + state.email + "&code=" + state.code }, (res) => {
        if (res.IsSuccess === true) {
          setState({ ...state, currentView: 'createProfile', codeIncorrect: false, error: false })
          setLoader(false);
        } else if (res.Errors.length > 0) {
          setLoader(false);
          setState({
            ...state,
            error: false,
            //currentView: 'createProfile',
            codeIncorrect: true,
          })
        }

      });

      setLoader(false);
    }
  };

  const handleResendAccessCode = () => {
    setLoader(true);
    CommonService.ExecuteGet({ Url: "resendaccesscode?email=" + state.email }, (res) => {
      if (res.IsSuccess === true) {
        setLoader(false);
      }
      else {
        setLoader(false);
        console.log(res)
      }
    });
  };

  const CloseDialog = (name) => {
    setState({
      ...state,
      [name]: false,
    })
  }

  const OpenDialog = (name) => {
    setState({
      ...state,
      [name]: true,
    })
  }

  return (
    <AccessLayout>
      {loader ? <FallbackLoader allowloader={true} /> : ''}
      <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
        </Grid>
        <div className={classes.RedBox}>
          <Grid item xs={12} className={classes.Titlebar}>
            <Typography variant="h2"><strong>TM Manager Account Sign Up</strong></Typography>
            <ClearIcon onClick={() => OpenDialog('cancelSetup')} />
          </Grid>
          {(state.currentView === undefined || state.currentView === "submitEmail") ?
            <Grid item xs={12} className={classes.Access}>
              <Typography variant="h2" gutterBottom>Verify your email.</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">Please enter your email. If your email is enabled for account access, you will need your access code to continue to your profile setup.</Typography>
              <div>
                <TextField
                  id="email"
                  label="Email"
                  variant="outlined"
                  placeholder=""
                  value={state.email || ""}
                  onChange={(event) => handleOnChange(event, 'email')}
                  error={state?.error || false}
                  helperText={(state.email === undefined || state.email === "") && state.error === true ? "Email is required" : (state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
                  fullWidth
                  required
                />
              </div>
              <Button onClick={() => handleRegister()} variant="contained" color="primary" disableElevation fullWidth size="large">Verify Email</Button>
              <Button onClick={() => GotoURL('/')} variant="outlined" color="primary" disableElevation fullWidth size="large">Cancel</Button>
            </Grid>
            : (state.currentView !== undefined && state.currentView === "enterCode") ?
              <Grid item xs={12} className={classes.Access}>
                <Typography variant="h2" gutterBottom>Success</Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">The email: <strong>{state.email}</strong> has been found!<br />Please enter the access code you received and then select the button below to start creating your profile.<br /> Once your profile is set up you will be able to login to the system and access your dashboard.
              </Typography>
                <div>
                  <TextField
                    id="code"
                    label="Access Code"
                    variant="outlined"
                    placeholder=""
                    value={state.code || ""}
                    onChange={(event) => handleOnChange(event, 'code')}
                    error={state?.error || false}
                    helperText={(state.code === undefined || state.code === "") && state.error === true ? "Access Code is required" : ""}
                    fullWidth
                    required
                  />
                </div>
                <Button onClick={() => handleAccessCode()} variant="contained" color="primary" disableElevation fullWidth size="large">Create Profile</Button>
                <Button onClick={() => handleResendAccessCode()} variant="outlined" color="primary" disableElevation fullWidth size="large">Resend Code</Button>
              </Grid>
              : (state.currentView !== undefined && state.currentView === "accountAlreadyCreated") ?
                <Grid item xs={12} className={classes.Access}>
                  <Typography variant="h2" gutterBottom>Account already created.</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">An account for: <strong>{state.email}</strong> has already been created. Please login to your account using your credentials to access system functionality.</Typography>
                  <Button onClick={() => GotoURL('/')} variant="contained" color="primary" disableElevation fullWidth size="large">Login to System</Button>
                </Grid>
                : (state.currentView !== undefined && state.currentView === "createProfile") ?
                  <Grid item xs={12} className={classes.Access}>
                    <Typography variant="h2" gutterBottom>Success!</Typography>
                    <Typography variant="subtitle2" gutterbottomdouble="true">Your access code is valid! Select continue to proceed with your account registration.</Typography>
                    <Button onClick={() => GotoURL("/review")} variant="contained" color="primary" disableElevation fullWidth size="large">Continue</Button>
                    <Button onClick={() => GotoURL('/')} variant="outlined" color="primary" disableElevation fullWidth size="large">Cancel</Button>
                  </Grid>
                  : (state.currentView !== undefined && state.currentView === "requestAccess") ?
                    <Grid item xs={12} className={classes.Access}>
                      <Typography variant="h2" gutterBottom>Oh no!</Typography>
                      <Typography variant="subtitle2" gutterbottomdouble="true">The email: <strong>{state.email}</strong> was not found. Please contact a system administrator to set up your email for account access or select the button below to fill out the online form.</Typography>
                      <Button onClick={() => GotoURL('/request')} variant="contained" color="primary" disableElevation fullWidth size="large">Request Account Access</Button>
                    </Grid>
                    : ""
          }
        </div>
      </Grid>
      <AlertBox
        open={state.codeIncorrect === undefined ? false : state.codeIncorrect}
        content={
          <>
            <div style={{ textAlign: 'center' }}>
              <InfoIcon className={'danger'} />
              <Typography variant="h2" gutterBottom><strong>Oh no!</strong></Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">Your access code appears to be incorrect. Please try again or contact the system administrator.</Typography>
            </div>
          </>
        }
        cancelButtonText="No Cancel"
        proceedButtonText="Yes, try again"
        onClickCancel={() => GoBack()}
        onClickProceed={() => CloseDialog('codeIncorrect')}
      />
      <AlertBox
        open={state.cancelSetup === undefined ? false : state.cancelSetup}
        content={
          <>
            <div style={{ textAlign: 'center' }}>
              <InfoIcon className={'danger'} />
              <Typography variant="h2" gutterBottom><strong>Are you sure you want to stop?</strong></Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">All your changes will be lost. Select yes to proceed, no to cancel this action.</Typography>
            </div>
          </>
        }
        cancelButtonClass="danger"
        cancelButtonText="Yes, do it!"
        proceedButtonText="No, Cancel"
        proceedButtonClass="darked"
        onClickCancel={() => GoBack()}
        onClickProceed={() => CloseDialog('cancelSetup')}
      />
    </AccessLayout>
  );
}