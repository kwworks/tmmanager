import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useParams, useHistory } from "react-router-dom";
import AccessLayout from '../../layouts/AccessLayout';
import { Grid, Typography, Button } from '@material-ui/core';
import AlertBox from '../../elements/AlertBox';
import ClearIcon from '@material-ui/icons/Clear';
import MobileStepper from '@material-ui/core/MobileStepper';
import CommonService from '../../services/CommonService';
// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg";
import { ReactComponent as InfoIcon } from '../../assets/infoIcon.svg';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';

// Steps
import StepOne from './Steps/StepOne'
import StepTwo from './Steps/StepTwo';
import StepThree from './Steps/StepThree';
import StepFour from './Steps/StepFour';
import StepFive from './Steps/StepFive';

const useStyles = makeStyles((theme) => ({
    AccessBox: {
        width: '90%',
        maxWidth: 1050,
        margin: '24px auto',
        borderRadius: 25,
    },
    Logo: {
        textAlign: 'center',
        '& img': {
            maxWidth: '100%',
        },
        [theme.breakpoints.up('md')]: {
            '& img': {
                maxWidth: '350px',
                float: 'left',
                marginBottom: theme.spacing(2),
            },
        }
    },
    RedBox: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'stretch',
        flex: 1,
        height: 'calc(100vh - 125px)',
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.primary.main,
        borderRadius: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            height: 'calc(100vh - 160px)',
        },
    },
    Titlebar: {
        flex: 0,
        padding: theme.spacing(1.5),
        backgroundColor: '#2976A6',
        margin: theme.spacing(1.5, 1.5, 0),
        borderRadius: theme.spacing(1.5, 1.5, 0, 0),
        position: 'relative',
        '& .MuiTypography-h2': {
            color: theme.palette.light.main,
            fontSize: 30,
            letterSpacing: 1,
            maxWidth: 'calc(100% - 30px)',
            '& + svg': {
                position: 'absolute',
                right: 5,
                top: 15,
                width: '1.5em',
                height: '1.5em',
                fill: theme.palette.light.main,
            }
        },
        [theme.breakpoints.up('md')]: {
            margin: theme.spacing(3, 3, 0),
        },
      
    },
    Access: {
        padding: theme.spacing(1.5),
        backgroundColor: theme.palette.light.main,
        margin: theme.spacing(0, 1.5, 1.5),
        overflowY: 'auto',
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(3, 9),
            margin: theme.spacing(0, 3, 3),
        },
        '& .wizard-head': {
            '& .MuiMobileStepper-root': {
                padding: 0,
                '& .MuiMobileStepper-progress': {
                    backgroundColor: '#E6E6E6',
                    width: '100%',
                    height: 20,
                    borderRadius: theme.spacing(0.5),
                    '& .MuiLinearProgress-barColorPrimary': {
                        backgroundColor: '#2976A6',
                    },
                },
                '& + p': {
                    textAlign: 'center',
                    margin: theme.spacing(0.25, 0, 1),
                    paddingBottom: theme.spacing(0.25),
                    borderBottom: '1px solid #9C9CA6',
                    [theme.breakpoints.up('sm')]: {
                        marginBottom: theme.spacing(2),
                    }
                }
            },
        },
        '& .wizard-body': {
            height: 'calc(100vh - 365px)',
            marginBottom: 10,
            overflowY: 'auto',
            [theme.breakpoints.up('md')]: {
                height: 'calc(100vh - 420px)',
            },
        },
        '& .wizard-footer': {
            display: 'flex',
            justifyContent: 'space-between',
            '& .MuiButton-root': {
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: '#F49393',
                minWidth: 100,
                padding: '0!important',
                minHeight: 36,
                '& svg': {
                    position: 'absolute',
                    height: '100%',
                    backgroundColor: theme.palette.primary.main,
                    boxSizing: 'content-box',
                    borderLeft: '3px solid ' + theme.palette.primary.main,
                    borderRight: '3px solid ' + theme.palette.primary.main,
                },
                '&:first-child .MuiButton-label': {
                    paddingLeft: theme.spacing(5),
                    '& svg': {
                        left: 0,
                        borderRadius: '4px 0 0 4px',
                    }
                },
                '&:last-child .MuiButton-label': {
                    paddingLeft: theme.spacing(2),
                    '& svg': {
                        top: 0,
                        right: 0,
                        borderRadius: '0 4px 4px 0',
                    }
                }
            }
        },
    }
}));

export default function Request(props) {
    const classes = useStyles();
    const theme = useTheme();
    const history = useHistory();
    const [state, setState] = React.useState([]);
    const [dataState, setDataState] = React.useState({
        email: useParams().email,
        error: false,
        primaryUser: { firstName: '', lastName: '', phone: '', phoneType: 'Office', email: useParams().email, address: '', city: '', state: '', zip: '', country: 'USA' },
        passwords: { password: '', confirmPassword: '' },
        contacts: [],
    });
    const [activeStep, setActiveStep] = React.useState(1);

    const handleNext = () => {
        if (activeStep === 3 && (
            dataState.primaryUser.firstName === '' || dataState.primaryUser.firstName === undefined ||
            dataState.primaryUser.lastName === '' || dataState.primaryUser.lastName === undefined ||
            dataState.primaryUser.phone === '' || dataState.primaryUser.phone === undefined ||
            (dataState.primaryUser.email === undefined || dataState.primaryUser.email === '') || (dataState.primaryUser.email.lastIndexOf('.') < 0 || dataState.primaryUser.email.lastIndexOf('@') < 0))) {
            //var data = GetSelected(activeStep);
            setDataState({ ...dataState, error: true })
        }
        else {
            setDataState({ ...dataState, error: false })
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
    };
    const completeRegistration = () => {
        if (
            dataState.passwords?.password === undefined || dataState.passwords?.password === "" || dataState.passwords.confirmpassword === "" || dataState.passwords.confirmpassword === undefined || dataState.passwords.password !== dataState.passwords.confirmpassword) {
            setDataState({ ...dataState, error: true })
        }
        else {
            var contacts = dataState.contacts.filter(function(f){
                return f.Id != null
            });
            var model = {
                FirstName: dataState.primaryUser.firstName,
                LastName: dataState.primaryUser.lastName,
                ClientName: dataState.primaryUser.clientName,
                LegalEntityType: dataState.primaryUser.legalEntityType,
                Phone: dataState.primaryUser.phone,
                Office: dataState.primaryUser.Office,
                Email: dataState.primaryUser.email,
                Address: dataState.primaryUser.address,
                City: dataState.primaryUser.city,
                State: dataState.primaryUser.state,
                ZipCode: dataState.primaryUser.zip,
                Country: dataState.primaryUser.country,
                Password: dataState.passwords.password,
                PhoneType: dataState.primaryUser.phoneType,
                UserContacts: contacts,
                UserName: dataState.email
            };
            setDataState({ ...dataState, error: false })
            CommonService.ExecutePost({ Url: "updateuserinfo" }, model, (res) => {
                if (res?.IsSuccess === true) {
                    GotoURL('/complete/' + dataState.email)
                } else {
                    setState({
                        ...state,
                        networkError: true,
                    })
                }
            });
        };
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const showData = () => {
        if (activeStep === 1) {
            return <StepOne data={dataState} onChange={handleChangeStepOneData} />
        }
        else if (activeStep === 2) {
            return <StepTwo data={dataState} />
        }
        else if (activeStep === 3) {
            return <StepThree data={dataState} selected={GetSelected(activeStep)} onChange={handleUserDataChange} error={dataState.error} />
        }
        else if (activeStep === 4) {
            return <StepFour data={dataState} selected={GetSelected(activeStep)} onChange={handleContactsDataChange} error={dataState.error} />
        }
        else if (activeStep === 5) {
            return <StepFive data={dataState} onChange={handlePasswordDataChange} error={dataState.error} />
        }
        else {
            return null
        }
    }

    const GotoURL = (url) => {
        history.push(url)
    }

    const CloseDialog = (name) => {
        setState({
            ...state,
            [name]: false,
        })
    }
    const OpenDialog = (name) => {
        setState({
            ...state,
            [name]: true,
        })
    }
    function GetSelected(step) {
        switch (step) {
            case 3:
                return dataState.primaryUser
            default:
                return null;
        }
    }
    function handleChangeStepOneData(data) {
        setDataState({
            ...dataState,
            primaryUser: { clientName: data.ClientDetail.ClientName, address: data.ClientDetail.Address, legalEntityType: data.ClientDetail.LegalEntity, phone: data.ClientDetail.Phone, email: data.ClientDetail.Email, city: data.ClientDetail.City, state: data.ClientDetail.State, zip: data.ClientDetail.ZipCode, country: data.ClientDetail.Country },
            contacts: data.ClientContacts,
        });
    }
    function handleUserDataChange(data) {
        setDataState({
            ...dataState,
            primaryUser: data
        });
    }
    function handleContactsDataChange(data) {
        var contacts = dataState.contacts;
        contacts.push(data);
        setDataState({
            ...dataState,
            contacts: contacts
        });
    }
    function handlePasswordDataChange(data) {
        setDataState({
            ...dataState,
            passwords: data
        });
    }
    return (
        <AccessLayout>
            <Grid className={classes.AccessBox} container>
                <Grid item xs={12} className={classes.Logo}>
                    <img src={LogoImage} alt="" />
                </Grid>
                <div className={classes.RedBox}>
                    <Grid item xs={12} className={classes.Titlebar}>
                        <Typography variant="h2"><strong>TM Manager Account Sign Up</strong></Typography>
                        <ClearIcon onClick={() => OpenDialog('cancelSetup')} />
                    </Grid>
                    <Grid item xs={12} className={classes.Access}>
                        <div className={'wizard-head'}>
                            <MobileStepper variant="progress" steps={6} position="static" activeStep={activeStep} className={classes.root} />
                            <Typography variant="body2">Step {activeStep} of 5</Typography>
                        </div>
                        <div className={'wizard-body'}>
                            {showData()}
                        </div>
                        <div className={'wizard-footer'}>
                            <Button size="small" variant="contained" color="primary" onClick={handleBack} disabled={activeStep === 1}> {theme.direction === 'rtl' ? <ArrowBack /> : <ArrowBack />} Back </Button>
                            {activeStep <= 4 ?
                                <Button size="small" variant="contained" color="primary" onClick={handleNext} > Next {theme.direction === 'rtl' ? <ArrowForward /> : <ArrowForward />} </Button>
                                :
                                <Button size="small" variant="contained" color="primary" onClick={completeRegistration}> Next {theme.direction === 'rtl' ? <ArrowForward /> : <ArrowForward />} </Button>
                            }
                        </div>
                    </Grid>
                </div>
            </Grid>
            <AlertBox
                open={state.cancelSetup === undefined ? false : state.cancelSetup}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Are you sure you want to stop?</strong></Typography>
                            <Typography variant="subtitle2" gutterbottomdouble="true">All your changes will be lost. Select yes to proceed, no to cancel this action.</Typography>
                        </div>
                    </>
                }
                cancelButtonText="No, Cancel"
                proceedButtonText="Yes, do it!"
                proceedButtonClass="danger"
                onClickCancel={() => CloseDialog('cancelSetup')}
                onClickProceed={() => GotoURL('/')}
            />

            <AlertBox
                open={state.networkError === undefined ? false : state.networkError}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Something went wrong!</strong></Typography>
                            <Typography variant="subtitle2">Something went wrong with our server. Please contact with administrator.</Typography>
                        </div>
                    </>
                }
                showCancelButton={false}
                proceedButtonText="Close"
                proceedButtonClass="danger"
                onClickProceed={() => CloseDialog('networkError')}
            />
        </AccessLayout>
    );
}