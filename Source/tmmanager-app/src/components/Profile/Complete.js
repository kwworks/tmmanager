import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useParams,useHistory } from "react-router-dom";
import AccessLayout from '../../layouts/AccessLayout';
import { Grid, Typography, Button } from '@material-ui/core';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg";


const useStyles = makeStyles((theme) => ({
    AccessBox: {
        width: '90%',
        maxWidth: 1050,
        margin: '24px auto',
        borderRadius: 25,
    },
    Logo: {
        textAlign: 'center',
        '& img': {
            maxWidth: '100%',
        },
        [theme.breakpoints.up('md')]: {
            '& img': {
                maxWidth: '350px',
                float: 'left',
                marginBottom: theme.spacing(2),
            },
        }
    },
    RedBox: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'stretch',
        flex: 1,
        height: 'calc(100vh - 115px)',
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.primary.main,
        borderRadius: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            height: 'calc(100vh - 160px)',
        },
    },
    Titlebar: {
        flex: 0,
        padding: theme.spacing(1.5),
        backgroundColor: '#2976A6',
        margin: theme.spacing(1.5, 1.5, 0),
        borderRadius: theme.spacing(1.5, 1.5, 0, 0),
        position: 'relative',
        [theme.breakpoints.up('md')]: {
            margin: theme.spacing(3, 3, 0),
        },
        '& .MuiTypography-h2': {
            color: theme.palette.light.main,
            fontSize: 30,
            letterSpacing: 1,
        }
    },
    Access: {
        padding: theme.spacing(1.25),
        backgroundColor: theme.palette.light.main,
        margin: theme.spacing(0, 1.5, 1.5),
        overflowY: 'auto',
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(3, 7),
            margin: theme.spacing(0, 3, 3),
        },
    },
}));

export default function ReviewComplete(props) {
    const classes = useStyles();
    const history = useHistory();

    const GotoLogin = () => {
        history.push('/')
    }

    return (
        <AccessLayout>
            {/* {loader ? <FallbackLoader allowimage={true} allowloader={true} /> : ''} */}
            <Grid className={classes.AccessBox} container>
                <Grid item xs={12} className={classes.Logo}>
                    <img src={LogoImage} alt="" />
                </Grid>
                <div className={classes.RedBox}>
                    <Grid item xs={12} className={classes.Titlebar}>
                        <Typography variant="h2"><strong>TM Manager Account Sign Up</strong></Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.Access}>
                        <Typography variant="h2" gutterBottom><strong>Your account has been set up!</strong></Typography>
                        <Typography variant="subtitle2" gutterbottomdouble="true">Congratulations! Your account in TM Manager has now been set up. You can now use your credentials to login to the system.</Typography>
                        <Grid item xs={12} sm={6} gutterbottomdouble="true">
                            <table>
                                <tbody>
                                    <tr>
                                        <td width="50%"><strong>Email</strong></td>
                                        <td>{useParams().email}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Password</strong></td>
                                        <td>*********</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Grid>
                        <Grid item xs={12}></Grid>
                        <Grid item xs={12} sm={6}>
                            <Button color="primary" fullWidth onClick={() => GotoLogin()} variant="contained">Login to the System</Button>
                        </Grid>

                    </Grid>
                </div>
            </Grid>
        </AccessLayout>
    );
}