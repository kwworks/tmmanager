import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ACCountry from '../../../elements/ACCountry';
import AuthService from '../../../services/authService';
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 0,
        '& .MuiTypography-h2': {
            lineHeight: 1,
        },
        '& table': {
            width: '100%',
            overflowX: 'hidden',
            '& td': {
                padding: theme.spacing(0.5, 0)
            }
        },
        [theme.breakpoints.down('xs')]: {
            '& .MuiGrid-grid-xs-12': {
                padding: 0,
            }
        },
        [theme.breakpoints.up('sm')]: {
            '& .childs-wrap': {
                display: 'flex',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                '& > *': {
                    width: 'calc(50% - ' + theme.spacing(1) + 'px)',
                }
            }
        },
    }
}));

export default function StepThree(props) {
    const classes = useStyles();
    const [state, setState] = React.useState(props?.selected !== undefined ? props?.selected : { firstName: '', lastName: '', phone: '', phoneType: '', email: props?.data.email, address: '', city: '', state: '', zip: '', country: '', error: props?.error });
    const handleOnChange = (event, name) => {
        if (event.currentTarget.className === 'MuiAutocomplete-option'){
            setState({
                ...state,
                [name]: event.currentTarget.innerText,
            })
        }
        else {
            setState({
                ...state,
                [name]: event.target.value,
            })
        }
    };
    React.useEffect(() => {
        props?.onChange(state);
    }, [state]);
    return (
        <Grid container spacing={1} className={classes.root}>
            <Grid item xs={12}>
                <Typography variant="h2" gutterBottom><strong>Primary Contact</strong></Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">Enter the name and contact information for the primary contact person for this account.</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    id="firstName"
                    label="First Name"
                    variant="outlined"
                    placeholder=""
                    value={state.firstName || ""}
                    onChange={(event) => handleOnChange(event, 'firstName')}
                    error={props?.error || false}
                    helperText={(state.firstName === undefined || state.firstName === "") && props?.error === true ? "First Name is required" : ""}
                    fullWidth
                    required
                />
                <TextField
                    id="lastName"
                    label="Last Name"
                    variant="outlined"
                    placeholder=""
                    value={state.lastName || ""}
                    onChange={(event) => handleOnChange(event, 'lastName')}
                    error={props?.error || false}
                    helperText={(state.lastName === undefined || state.lastName === "") && props?.error === true ? "Last Name is required" : ""}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={12} className={'childs-wrap'}>
                <TextField
                    id="phone"
                    label="Phone"
                    variant="outlined"
                    placeholder=""
                    value={state.phone || ""}
                    error={props?.error || false}
                    helperText={(state.phone === undefined || state.phone === "") && props?.error === true ? "Phone Number is required" : ""}
                    onChange={(event) => handleOnChange(event, 'phone')}
                    type="number"
                    required
                    fullWidth
                />
                <FormControl className={classes.formControl} >
                    <InputLabel htmlFor="phoneType">Phone Type</InputLabel>
                    <Select onChange={(event) => handleOnChange(event, 'phoneType')}
                        value={state.phoneType !== '' ? state.phoneType : "Office"}
                        defaultValue="Company"
                        id="phoneType"
                        fullWidth
                        required
                        error={false}
                        variant="outlined">
                        <MenuItem value="Company">Company</MenuItem>
                        <MenuItem value="Fax">Fax</MenuItem>
                        <MenuItem value="Home">Home</MenuItem>
                        <MenuItem value="Mobile">Mobile</MenuItem>
                        <MenuItem value="Office">Office</MenuItem>
                    </Select>
                    <Typography data-error variant="body2"></Typography>
                </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    id="email"
                    label="Email"
                    variant="outlined"
                    placeholder=""
                    value={state.email || ""}
                    onChange={(event) => handleOnChange(event, 'email')}
                    error={props?.error || false}
                    helperText={(state.email === undefined || state.email === "") && props?.error === true ? "Email is required" : (state.email !== undefined && props?.error === true && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
                    fullWidth
                    required
                    InputProps={{
                        readOnly: true,
                    }}
                />
                <TextField
                    id="address"
                    label="Street Address"
                    variant="outlined"
                    placeholder=""
                    value={state.address !== null ? state.address : ""}
                    onChange={(event) => handleOnChange(event, 'address')}
                    fullWidth
                />
                <TextField
                    id="city"
                    label="City"
                    variant="outlined"
                    placeholder=""
                    value={state.city || ""}
                    onChange={(event) => handleOnChange(event, 'city')}
                    fullWidth
                />
                <TextField
                    id="state"
                    label="State/Province"
                    variant="outlined"
                    placeholder=""
                    value={state.state || ""}
                    onChange={(event) => handleOnChange(event, 'state')}
                    fullWidth
                />
                <TextField
                    id="zip"
                    label="Zip Code"
                    variant="outlined"
                    placeholder=""
                    value={state.zip !== null ? state.zip : ""}
                    onChange={(event) => handleOnChange(event, 'zip')}
                    type="number"
                    fullWidth
                />
                <ACCountry onchange={(event) => handleOnChange(event, 'country')} value={AuthService.getCountryName(state?.country) || 'UNITED STATES'} />
            </Grid>
        </Grid>
    );
}