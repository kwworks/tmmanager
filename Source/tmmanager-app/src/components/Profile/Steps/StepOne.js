import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CommonService from '../../../services/CommonService';
import FallbackLoader from '../../../elements/FallbackLoader';
import AlertBox from '../../../elements/AlertBox';
import { ReactComponent as InfoIcon } from '../../../assets/infoIcon.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 0,
        '& .MuiTypography-h2': {
            lineHeight: 1,
        },
        '& table': {
            width: '100%',
            overflowX: 'hidden',
            '& td': {
                padding: theme.spacing(0.5, 0),
                verticalAlign: 'top',
                '&:first-child': {
                    minWidth: 140
                }
            }
        },
        [theme.breakpoints.down('xs')]: {
            '& .MuiGrid-grid-xs-12': {
                padding: 0,
            }
        }
    }
}));

export default function StepOne(props) {
    const classes = useStyles();
    const [loader, setLoader] = React.useState(false);
    const [state, setState] = React.useState([]);
    React.useEffect(() => {
        setLoader(true);
        CommonService.ExecuteGet({ Url: "getclientdetail?email=" + props?.data.email }, (res) => {
            if (res.ClientDetail?.ClientName !== undefined) {
                props?.onChange(res);
                setState(res);
                setLoader(false);
            } else {
                setLoader(false);
                setState({
                    ...state,
                    networkError:true,
                })
            }
        });
    }, [props?.data.email]);

    const CloseDialog = (name) => {
        setState({
            ...state,
            [name]: false,
        })
    }

    return (
        <Grid container spacing={1} className={classes.root}>
            {loader ? <FallbackLoader allowloader={true} /> : ''}
            <Grid item xs={12}>
                <Typography variant="h2" gutterBottom><strong>Review your account information.</strong></Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">Below is account information based on data from one or more of your current trademarks. Please review for accuracy. If changes are necessary, contact us.</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
                <table>
                    <tbody>
                        <tr>
                            <td><strong>USPTO Name</strong></td>
                            <td>{state?.ClientDetail?.ClientName}</td>
                        </tr>
                        <tr>
                            <td><strong>USPTO Email</strong></td>
                            <td><span style={{ wordBreak: 'break-all' }}>{state?.ClientDetail?.Email}</span></td>
                        </tr>
                        <tr>
                            <td><strong>USPTO Phone</strong></td>
                            <td>{state?.ClientDetail?.Phone || ""}</td>
                        </tr>
                        <tr>
                            <td><strong>Street Address</strong></td>
                            <td>{state?.ClientDetail?.Address}</td>
                        </tr>
                        <tr>
                            <td><strong>City</strong></td>
                            <td>{state?.ClientDetail?.City}</td>
                        </tr>
                        <tr>
                            <td><strong>State/Province</strong></td>
                            <td>{state?.ClientDetail?.State}</td>
                        </tr>
                        <tr>
                            <td><strong>Zip Code</strong></td>
                            <td>{state?.ClientDetail?.ZipCode || ""}</td>
                        </tr>
                        <tr>
                            <td><strong>Country</strong></td>
                            <td>{state?.ClientDetail?.Country}</td>
                        </tr>
                    </tbody>
                </table>
            </Grid>
            <AlertBox
                open={state.networkError === undefined ? false : state.networkError}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Fetching Failed!</strong></Typography>
                            <Typography variant="subtitle2" >Error getting details from server.</Typography>
                        </div>
                    </>
                }
                showCancelButton={false}
                proceedButtonText="Close"
                proceedButtonClass="danger"
                onClickProceed={() => CloseDialog('networkError')}
            />
        </Grid>
    );
}