import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
//import FallbackLoader from '../../../elements/FallbackLoader';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 0,
        '& .MuiTypography-h2': {
            lineHeight: 1,
        },
        [theme.breakpoints.down('xs')]: {
            '& .MuiGrid-grid-xs-12': {
                padding: 0,
            }
        }
    }
}));

export default function StepOne(props) {
    const classes = useStyles();
    const [state, setState] = React.useState([]);
    //const [loader, setLoader] = React.useState(false);

    const handleOnChange = (event, name) => {
        setState({
            ...state,
            [name]: event.target.value,
        });
    }
    React.useEffect(() => {
        props.onChange(state);
    }, [state]);
    return (
        <Grid container spacing={1} className={classes.root}>
            {/* {loader ? <FallbackLoader allowimage={true} allowloader={true} /> : ''} */}

            <Grid item xs={12}>
                <Typography variant="h2" gutterBottom><strong>Create account password</strong></Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">Create a password for the account.</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Typography variant="subtitle1">Email</Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">{props?.data.email}</Typography>
            </Grid>
            <Grid item xs={12}></Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    id="password"
                    label="Password"
                    type="password"
                    variant="outlined"
                    placeholder=""
                    value={state.password || ""}
                    onChange={(event) => handleOnChange(event, 'password')}
                    error={props?.error || false}
                    helperText={(state.password === undefined || state.password === "") && props?.error ? "Password is required" : ""}
                    fullWidth
                    required
                />
                <TextField
                    id="confirmpassword"
                    label="Confirm Password"
                    type="password"
                    variant="outlined"
                    placeholder=""
                    value={state.confirmpassword || ""}
                    onChange={(event) => handleOnChange(event, 'confirmpassword')}
                    error={props?.error || false}
                    helperText={(state.confirmpassword === undefined || state.confirmpassword === "") && props?.error ? "Password is required" : (state.password !== state.confirmpassword) && props?.error ? "Password did not matched" : ""}
                    fullWidth
                    required
                />
            </Grid>
        </Grid>
    );
}