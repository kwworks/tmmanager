import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CommonService from '../../../services/CommonService';
import FallbackLoader from '../../../elements/FallbackLoader';
import AlertBox from '../../../elements/AlertBox';
import { ReactComponent as InfoIcon } from '../../../assets/infoIcon.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 0,
        '& .MuiTypography-h2': {
            lineHeight: 1,
        },
        '& table': {
            width: '100%',
            overflowX: 'hidden',
            '& td': {
                padding: theme.spacing(0.5, 0)
            }
        },
        [theme.breakpoints.down('xs')]: {
            '& .MuiGrid-grid-xs-12': {
                padding: 0,
            }
        }
    }
}));

export default function StepTwo(props) {
    const classes = useStyles();
    const [loader, setLoader] = React.useState(false);
    const [state, setState] = React.useState([]);
    React.useEffect(() => {
        setLoader(true);
        CommonService.ExecuteGet({ Url: "getclientdetail?email=" + props?.data.email}, (res) => {
            if (res?.ClientTrademarkList.length > 0) {
                setState(res);
                setLoader(false);
            } else {
                setLoader(false);
                setState({
                    ...state,
                    networkError:true,
                })
            }
        });
    }, [props?.data.email]);

    const CloseDialog = (name) => {
        setState({
            ...state,
            [name]: false,
        })
    }

    return (
        <Grid container spacing={1} className={classes.root}>
            {loader ? <FallbackLoader allowloader={true} /> : ''}
            <Grid item xs={12}>
                <Typography variant="h2" gutterBottom><strong>Current Trademarks</strong></Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">Below are trademarks currently associated with your account:</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
                <table>
                    <thead>
                        <tr>
                            <th>Trademark Name</th>
                            <th>Serial Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {state?.ClientTrademarkList?.length > 0 ? state?.ClientTrademarkList?.map((each) => (
                            <tr key={each?.Id}>
                                <td>{each?.TrademarkName}</td>
                                <td>{each?.SerialNumber}</td>
                            </tr>
                        )) : <tr></tr>}

                    </tbody>
                </table>
            </Grid>

            <AlertBox
                open={state.networkError === undefined ? false : state.networkError}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Fetching Failed!</strong></Typography>
                            <Typography variant="subtitle2" >Error getting details from server.</Typography>
                        </div>
                    </>
                }
                showCancelButton={false}
                proceedButtonText="Close"
                proceedButtonClass="danger"
                onClickProceed={() => CloseDialog('networkError')}
            />
        </Grid>
    );
}