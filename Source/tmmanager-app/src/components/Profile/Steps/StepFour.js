import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import AlertBox from '../../../elements/AlertBox';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import AuthService from '../../../services/authService';
import ACCountry from '../../../elements/ACCountry';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: 0,
        '& .MuiTypography-h2': {
            lineHeight: 1,
        },
        '& table': {
            width: '100%',
            overflowX: 'hidden',
            position: 'relative',
            '& td': {
                padding: theme.spacing(0.5, 0),
                '& span[data-error]':{
                    position: 'absolute',
                    right: 0,
                    cursor: 'pointer',
                }
            },
            [theme.breakpoints.up('sm')]: {
                '&:nth-child(2) ~ table tr > td:first-child': {
                    display: 'none',
                },
                '& tbody': {
                    display: 'flex',
                    '& > tr': {
                        display: 'inline-flex',
                        flex: '0 0 25%',
                        maxWidth: '25%',
                        flexDirection: 'column',
                    }
                }
            }
        },
        [theme.breakpoints.down('xs')]: {
            '& .MuiGrid-grid-xs-12': {
                padding: 0,
            },
            '& table': {
                borderTop: '1px solid',
                '&:last-child': {
                    borderBottom: '1px solid',
                }
            },
        },
        [theme.breakpoints.up('sm')]: {
            '& div:nth-child(2) button': {
                float: 'right',
                marginBottom: 0,
            }
        }
    },

    Modal: {
        [theme.breakpoints.up('sm')]: {
            '& .childs-wrap': {
                display: 'flex',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                '& > *': {
                    width: 'calc(50% - ' + theme.spacing(1.5) + 'px)',
                }
            }
        },
    }
}));

export default function StepFour(props) {
    const classes = useStyles();
    const defaultState = { status: '', firstName: '', lastName: '', phone: '', contactType: 'Office', email: '', streetAddress: '', city: '', state: '', zip: '', country: 'USA', error: false };
    const [state, setState] = React.useState(defaultState);
    const [contactState, setContactState] = React.useState({ contacts: props?.data?.contacts.length > 0 ? props?.data.contacts : [] });
    const handleOnChange = (event, name) => {
        setState({
            ...state,
            [name]: event.target.value,
        })
    };
    React.useEffect(() => {
        //props?.onChange(state);
    }, [state]);
    const CloseDialog = (name) => {
        setState({
            ...state,
            [name]: false,
        })
    }
    const OpenDialog = (name) => {
        setState({
            ...state,
            [name]: true,
        })
    }

    const AddContact = (name) => {
        if ((state.firstName === '' || state.firstName === undefined ||
            state.lastName === '' || state.lastName === undefined ||
            state.phone === '' || state.phone === undefined ||
            (state.email === undefined || state.email === '') ||
            (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0))) {
            setState({
                ...state,
                error: true,
            })
        }

        else {
            var model = {
                Id: null,
                AppId : contactState.contacts.length,
                Status: 'Secondary',
                FirstName: state.firstName,
                LastName: state.lastName,
                Phone: state.phone,
                Office: state.Office,
                Email: state.email,
                Address: state.streetAddress,
                City: state.city,
                State: state.state,
                ZipCode: state.zip,
                Country: state.country,
                Password: ''
            };
            props?.onChange(model);
            var allcontacts = contactState.contacts;
            allcontacts.push(model);
            setContactState({
                contacts: allcontacts
            });
            CloseDialog('addContact');
            setState(defaultState);

        }
    }
    const RemoveContact = (contact)=> {
        if(contact.Id != null){

        }else {
            var contacts = contactState.contacts.filter(function(f){
                return f.AppId != contact.AppId
            });
            setContactState({
                contacts: contacts
            });
        }
    }
    return (
        <Grid container spacing={1} className={classes.root}>
            <Grid item xs={12} sm={6}>
                <Typography variant="h2" gutterBottom><strong>Add Additional Contacts</strong></Typography>
                <Typography variant="subtitle2" gutterbottomdouble="true">Enter the name and contact information for additional person for this account.</Typography>
            </Grid>
            <Grid item xs={12} sm={6} style={{ textAlign: 'center' }}>
                <Button onClick={() => OpenDialog('addContact')} variant="contained" gutterbottomdouble="true" className={'success'} color="primary"><AddIcon /> Add Contact</Button>
            </Grid>
            <Grid item xs={12}>
                <table>
                    <tbody>
                        <tr>
                            <td style={{ width: 70 }}><strong>Status</strong></td>
                            <td>Primary</td>
                        </tr>
                        <tr>
                            <td><strong>Name</strong></td>
                            <td>{props?.data.primaryUser.lastName} {props?.data.primaryUser.firstName}</td>
                        </tr>
                        <tr>
                            <td><strong>Email</strong></td>
                            <td><span style={{ wordBreak: 'break-all' }}>{props?.data.email}</span></td>
                        </tr>
                        <tr>
                            <td><strong>Phone</strong></td>
                            <td>{props?.data.primaryUser.phone}</td>
                        </tr>
                        {/* MapContactsHere */}
                    </tbody>
                </table>
                {contactState.contacts?.length > 0 ? contactState.contacts?.map((each) => (
                    <table key={Math.random()}>
                        <tbody>
                            <tr>
                                <td style={{ width: 70 }}><strong>Status</strong></td>
                                <td>Secondary <span onClick={()=>RemoveContact(each)} data-error>Remove</span></td>
                            </tr>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{each.LastName} {each.FirstName}</td>
                            </tr>
                            <tr>
                                <td><strong>Email</strong></td>
                                <td><span style={{ wordBreak: 'break-all' }}></span>{each.Email}</td>
                            </tr>
                            <tr>
                                <td><strong>Phone</strong></td>
                                <td>{each.Phone}</td>
                            </tr>
                        </tbody>
                    </table>
                )) : ''}
            </Grid>
            <AlertBox
                open={state.addContact === undefined ? false : state.addContact}
                content={
                    <div className={classes.Modal}>
                        <Typography variant="h2" gutterBottom><strong>Add Contact</strong></Typography>
                        <Typography variant="subtitle2" gutterbottomdouble="true">Enter additional account contacts</Typography>
                        <Grid item xs={12} className={'childs-wrap'}>
                            <TextField
                                id="firstName"
                                label="First Name"
                                variant="outlined"
                                placeholder=""
                                value={state.firstName || ""}
                                onChange={(event) => handleOnChange(event, 'firstName')}
                                error={state?.error || false}
                                helperText={(state.firstName === undefined || state.firstName === "") && state.error === true ? "First Name is required" : ""}
                                fullWidth
                                required
                            />
                            <TextField
                                id="lastName"
                                label="Last Name"
                                variant="outlined"
                                placeholder=""
                                value={state.lastName || ""}
                                onChange={(event) => handleOnChange(event, 'lastName')}
                                error={state?.error || false}
                                helperText={(state.lastName === undefined || state.lastName === "") && state.error === true ? "Last Name is required" : ""}
                                fullWidth
                                required
                            />
                            <TextField
                                id="phone"
                                label="Phone"
                                variant="outlined"
                                placeholder=""
                                value={state.phone || ""}
                                error={state?.error || false}
                                helperText={(state.phone === undefined || state.phone === "") && state?.error === true ? "Phone Number is required" : ""}
                                onChange={(event) => handleOnChange(event, 'phone')}
                                type="number"
                                fullWidth
                                required
                            />
                            <FormControl className={classes.formControl} >
                                <InputLabel htmlFor="contactType">Phone Type</InputLabel>
                                <Select onChange={(event) => handleOnChange(event, 'contactType')}
                                    value={state.contactType !== undefined ? state.contactType : "Office"}
                                    id="contactType"
                                    fullWidth
                                    required
                                    error={false}
                                    variant="outlined">
                                    <MenuItem value="Company">Company</MenuItem>
                                    <MenuItem value="Fax">Fax</MenuItem>
                                    <MenuItem value="Home">Home</MenuItem>
                                    <MenuItem value="Mobile">Mobile</MenuItem>
                                    <MenuItem value="Office">Office</MenuItem>
                                </Select>
                                <Typography data-error variant="body2"></Typography>
                            </FormControl>
                            <TextField
                                id="email"
                                label="Email"
                                variant="outlined"
                                placeholder=""
                                value={state.email || ""}
                                onChange={(event) => handleOnChange(event, 'email')}
                                error={state?.error || false}
                                helperText={(state.email === undefined || state.email === "") && state?.error === true ? "Email is required" : (state.email !== undefined && state?.error === true && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
                                fullWidth
                                required

                            />
                            <TextField
                                id="streetAddress"
                                label="Street Address"
                                variant="outlined"
                                placeholder=""
                                value={state.streetAddress || ""}
                                onChange={(event) => handleOnChange(event, 'streetAddress')}
                                fullWidth
                            />
                            <TextField
                                id="city"
                                label="City"
                                variant="outlined"
                                placeholder=""
                                value={state.city || ""}
                                onChange={(event) => handleOnChange(event, 'city')}
                                fullWidth
                            />
                            <TextField
                                id="state"
                                label="State"
                                variant="outlined"
                                placeholder=""
                                value={state.state || ""}
                                onChange={(event) => handleOnChange(event, 'state')}
                                fullWidth
                            />
                            <TextField
                                id="zip"
                                label="Zip"
                                variant="outlined"
                                placeholder=""
                                value={state.zip || ""}
                                onChange={(event) => handleOnChange(event, 'zip')}
                                type="number"
                                fullWidth
                            />
                            <ACCountry onchange={(event) => handleOnChange(event, 'country')} value={AuthService.getCountryName(state?.country) || 'UNITED STATES'} />
                        </Grid>
                    </div>
                }
                cancelButtonText="Cancel"
                proceedButtonText="Add Contact"
                proceedButtonClass="success"
                onClickCancel={() => CloseDialog('addContact')}
                onClickProceed={() => AddContact()}
                cancelButtonClass="danger"
                disableOverflow={false}
            />
        </Grid>
    );
}