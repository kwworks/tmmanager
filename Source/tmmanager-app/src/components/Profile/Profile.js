import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BasicLayout from '../../layouts/BasicLayout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FallbackLoader from '../../elements/FallbackLoader';
import AuthService from '../../services/authService';
import DialogBox from '../../elements/DialogBox';
import AlertBox from '../../elements/AlertBox';
import CTMPhone from '../../elements/CTMPhone';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CommonService from '../../services/CommonService';
import { ReactComponent as InfoIcon } from '../../assets/infoIcon.svg';
import { ReactComponent as ThumbsUpIcon } from '../../assets/ThumbsUp.svg';
import ACCountry from '../../elements/ACCountry';

const useStyles = makeStyles((theme) => ({

    ProfileBox: {
        '& .MuiTypography-h5': {
            display: 'flex',
            justifyContent: 'space-between',
            '&.MuiTypography-gutterBottom': {
                marginTop: theme.spacing(1.5),
                '& ~ .MuiTypography-h5:not(.MuiTypography-gutterBottom)': {
                    paddingLeft: theme.spacing(3),
                }
            },
            '& strong': {
                display: 'inline-block',
                cursor: 'pointer',
                fontSize: 16,
                color: theme.palette.primary.main,
            }
        },
        '& ul': {
            paddingLeft: theme.spacing(3),
            margin: 0,
            listStyle: 'none',
        }
    }
}));

export default function Profile(props) {
    const classes = useStyles();
    const initialState = {
        Address: '',
        City: '',
        ClientName: '',
        Country: 'UNITED STATES',
        Email: '',
        FirstName: '',
        LastName: '',
        LegalEntityType: '',
        Office: null,
        Password: null,
        Phone: '',
        PhoneType: 'Office',
        State: '',
        UserId: AuthService.getUserId(),
        UserName: AuthService.getUserName(),
        UserContacts: [],
        ZipCode: '',
    }
    const [loader, setLoader] = React.useState(true);
    const [response, setResponse] = React.useState([]);
    const [state, setState] = React.useState(initialState);
    const [error, setError] = React.useState(false);
    const [modals, setModals] = React.useState([])

    const OpenDialog = (name, target, id) => {
        if (target === 'updatePrimaryContact') {
            setState(response);
            setModals({ ...modals, currentModal: target, [name]: true, })
        }
        else if (target === 'updateSecondaryContact') {
            var secondaryContact = response.UserContacts.filter(x => x.Id === id);
            setState(secondaryContact[0]);
            setModals({ ...modals, currentModal: target, [name]: true, })
        }
        else if (target === 'addSecondaryContact') {
            setState(initialState);
            setModals({ ...modals, currentModal: target, [name]: true, })
        }
        else {
            setModals({ ...modals, [name]: true, })
        }
    }
    const CloseDialog = (name) => {
        setModals({
            ...modals,
            [name]: false,
        })
    }
    const handleOnChange = (event, name) => {
        if (event.currentTarget.className === 'MuiAutocomplete-option') {
            setState({
                ...state,
                [name]: event.currentTarget.innerText,
            })
        }
        else {
            setState({
                ...state,
                [name]: event.target.value,
            })
        }
    };
    const FormErrors = () => {
        if ((state.FirstName === '' || state.FirstName === undefined ||
            state.LastName === '' || state.LastName === undefined ||
            state.Phone === '' || state.Phone === undefined ||
            (state.Email === undefined || state.Email === '') ||
            (state.Email.lastIndexOf('.') < 0 || state.Email.lastIndexOf('@') < 0))) {
            return true
        }
        else {
            return false
        }
    }
    const UpdatePrimaryContact = () => {
        if (FormErrors()) {
            setError(true);
        }

        else {
            setError(false);
            var model = state;
            CommonService.ExecutePost({ Url: "updateuserprofile" }, model, (res) => {
                if (res?.IsSuccess === true) {
                    setModals({ ...modals, currentModal: 'contactUpdated', editcontact: false, alertsuccess: true, })
                }
                else {
                    setState({
                        ...state,
                        networkError: true,
                    })
                }
            });
        }
    }
    const AddSecondaryContact = () => {

        if (FormErrors()) {
            setError(true);
        }

        else {
            setError(false);
            var model = state;
            model.UserName = AuthService.getUserName();
            model.UserId = AuthService.getUserId();
            CommonService.ExecutePost({ Url: "updatesecondarycontact" }, model, (res) => {
                if (res?.IsSuccess === true) {
                    setModals({ ...modals, currentModal: 'contactadded', editcontact: false, alertsuccess: true, })
                }
                else {
                    setState({
                        ...state,
                        networkError: true,
                    })
                }
            });
        }
    }
    const UpdateSecondaryContacts = () => {
        if (FormErrors()) {
            setError(true);
        }

        else {
            setError(false);
            var model = state;
            model.UserName = AuthService.getUserName();
            model.UserId = AuthService.getUserId();
            CommonService.ExecutePost({ Url: "updatesecondarycontact" }, model, (res) => {
                if (res?.IsSuccess === true) {
                    setModals({ ...modals, currentModal: 'contactUpdated', editcontact: false, alertsuccess: true, })
                }
                else {
                    setState({
                        ...state,
                        networkError: true,
                    })
                }
            });
        };
    }
    const DeleteSecondaryContact = () => {
        var id = state.Id
        CommonService.ExecuteGet({ Url: "removesecondarycontact?id=" + id }, (res) => {
            if (res?.IsSuccess === true) {
                setModals({
                    ...modals,
                    alertdanger: false,
                    editcontact: false
                })
            }
            else {
                setState({
                    ...state,
                    alertdanger: false,
                    networkError: true,
                })
            }
        });
    }
    const ChangePassword = () => {
        if (state.newPassword === undefined || state.newPassword === "" || state.confirmPassword === undefined || state.confirmPassword === "" || state.confirmPassword !== state.newPassword 
        || state.newPassword.length < 6) {
            setError(true);
        }

        else {
            var model = {
                UserName: AuthService.getUserName(),
                Password: state.newPassword
            }
            CommonService.ExecutePost({ Url: "changepassword" }, model, (res) => {
                if (res?.IsSuccess === true) {
                    setModals({ ...modals, currentModal: '', changePassword: false, alertsuccess: true, })
                } 
                else {
                    setState({
                        ...state,
                        networkError: true,
                    })
                }
            });
            setError(false);
        }
    }
    React.useEffect(() => {
        setLoader(true);
        CommonService.ExecuteGet({ Url: "getprofiledetails?email=" + AuthService.getUserName() }, (res) => {
            if (res.message === undefined) {
                setResponse(res);
                setLoader(false);
            } else {
                setLoader(false);
                setModals({
                    ...modals,
                    networkError: true,
                })
            }
        });

    }, [modals]);

    return (
        <BasicLayout>
            {loader ? <FallbackLoader allowloader={true} /> : ''}
            <Grid container className={classes.ProfileBox}>
                <Grid item xs={12}>
                    <Typography variant="h1" color="primary" gutterbottomdouble="true">Profile</Typography>
                    <Typography variant="h5" gutterBottom>Password: <strong color="primary" onClick={() => OpenDialog('changePassword')}>Change</strong></Typography>
                    <Typography variant="h5" gutterBottom>Primary Contact: <strong onClick={() => OpenDialog('editcontact', 'updatePrimaryContact')}>Edit</strong></Typography>
                    <Typography variant="h5">{response.LastName + " " + response.FirstName}</Typography>
                    <Typography variant="h5">{response.Phone}</Typography>
                    <Typography variant="h5">{response.Email}</Typography>
                    <Typography variant="h5">{response.Address + " " + response.City + " " + response.State + " " + response.ZipCode + " " + response.Country}</Typography>
                    <Typography variant="h5" gutterBottom>Secondary Contact:</Typography>
                    <ul>
                        {response?.UserContacts?.length > 0 ? response?.UserContacts?.map((each) => (
                            <li key={Math.random()} gutterbottomdouble="true">
                                <Typography variant="h5">{each.LastName + " " + each.FirstName} <strong onClick={() => OpenDialog('editcontact', 'updateSecondaryContact', each.Id)}>Edit</strong></Typography>
                                <Typography variant="h5">{each.Phone}</Typography>
                            </li>
                        )) : ''}
                    </ul>
                    <Typography variant="h5" gutterBottom>Add Contact: <strong onClick={() => OpenDialog('editcontact', 'addSecondaryContact')}>Add</strong></Typography>
                </Grid>
            </Grid>

            {/* Popups */}
            <DialogBox
                open={modals.changePassword === undefined ? false : modals.changePassword}
                title="Change Password"
                showFooter={true}
                content={
                    <>
                        <Typography variant="subtitle2" gutterbottomdouble="true">Update account password.</Typography>
                        <Typography variant="subtitle1" gutterBottom>Email</Typography>
                        <Typography variant="subtitle2" gutterbottomdouble="true">smith@acme.com</Typography>
                        <Grid container spacing={2} style={{ marginTop: 16 }}>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="newPassword"
                                    label="New Password"
                                    variant="outlined"
                                    placeholder=""
                                    type="password"
                                    value={state.newPassword || ""}
                                    onChange={(event) => handleOnChange(event, 'newPassword')}
                                    error={error || false}
                                    helperText={(state.newPassword === undefined || state.newPassword === "") && error === true ? "New Password is required" : state?.newPassword?.length < 6 && error === true ? "Minimum six characters required" : ""}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="confirmPassword"
                                    label="Confirm Password"
                                    variant="outlined"
                                    placeholder=""
                                    type="password"
                                    value={state.confirmPassword || ""}
                                    onChange={(event) => handleOnChange(event, 'confirmPassword')}
                                    error={error || false}
                                    helperText={(state.confirmPassword === undefined || state.confirmPassword === "") && error === true ? "Confirm Password is required" : (state.newPassword !== state.confirmPassword) && error === true ? "Passwords did not mached" : ""}
                                    fullWidth
                                    required
                                />
                            </Grid>
                        </Grid>
                    </>
                }
                cancelButtonText="Update Password"
                cancelButtonClass="success"
                onClickCancel={() => ChangePassword()}
                showProceed={false}
                fullWidthActions={true}
                ModalClose={() => CloseDialog('changePassword')}
            />

            <DialogBox
                open={modals.editcontact === undefined ? false : modals.editcontact}
                title={modals.currentModal === 'addSecondaryContact' ? "Add Secondary Contact" :
                    modals.currentModal === 'updateSecondaryContact' ? "Update Secondary Contact" : "Edit Primary Contact"}
                showFooter={true}
                content={
                    <>
                        <Grid container spacing={2} style={{ marginTop: 8 }}>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="FirstName"
                                    label="First Name"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.FirstName || ""}
                                    onChange={(event) => handleOnChange(event, 'FirstName')}
                                    error={error || false}
                                    helperText={(state.FirstName === undefined || state.FirstName === "") && error === true ? "First Name is required" : ""}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="LastName"
                                    label="Last Name"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.LastName || ""}
                                    onChange={(event) => handleOnChange(event, 'LastName')}
                                    error={error || false}
                                    helperText={(state.LastName === undefined || state.LastName === "") && error === true ? "Last Name is required" : ""}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                {modals.currentModal === "updatePrimaryContact" ?
                                <CTMPhone value={state.Phone !== "" ? state.Phone : ""} error={error || false} onchange={(event) => handleOnChange(event, 'Phone')} />
                                :
                                <TextField
                                    id="Phone"
                                    label="Phone"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.Phone || ""}
                                    error={error || false}
                                    helperText={(state.Phone === undefined || state.Phone === "") && error === true ? "Phone Number is required" : ""}
                                    onChange={(event) => handleOnChange(event, 'Phone')}
                                    type="number"
                                    fullWidth
                                />}
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="PhoneType">Phone Type</InputLabel>
                                    <Select onChange={(event) => handleOnChange(event, 'PhoneType')}
                                        value={(state.PhoneType === null || state.PhoneType === undefined || state.PhoneType === "") ? "Office" : state?.PhoneType}
                                        id="PhoneType"
                                        fullWidth
                                        required
                                        error={false}
                                        variant="outlined">
                                        <MenuItem value="Company">Company</MenuItem>
                                        <MenuItem value="Fax">Fax</MenuItem>
                                        <MenuItem value="Home">Home</MenuItem>
                                        <MenuItem value="Mobile">Mobile</MenuItem>
                                        <MenuItem value="Office">Office</MenuItem>
                                    </Select>
                                    <Typography data-error variant="body2"></Typography>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="Email"
                                    label="Email"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.Email || ""}
                                    onChange={(event) => handleOnChange(event, 'Email')}
                                    error={error || false}
                                    helperText={(state.Email === undefined || state.Email === "") && error === true ? "Email is required" : ((state.Email !== undefined && error) && (state.Email.lastIndexOf('.') < 0 || state.Email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
                                    fullWidth
                                    required
                                    InputProps={{
                                        readOnly: modals.currentModal === "updatePrimaryContact" ? true : false,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="Address"
                                    label="Street Address"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.Address || ""}
                                    onChange={(event) => handleOnChange(event, 'Address')}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="City"
                                    label="City"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.City || ""}
                                    onChange={(event) => handleOnChange(event, 'City')}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="State"
                                    label="State"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.State || ""}
                                    onChange={(event) => handleOnChange(event, 'State')}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <TextField
                                    id="ZipCode"
                                    label="Zip"
                                    variant="outlined"
                                    placeholder=""
                                    value={state.ZipCode || ""}
                                    onChange={(event) => handleOnChange(event, 'ZipCode')}
                                    type="number"
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6} style={{ paddingTop: 0, paddingBottom: 0 }}>
                                <ACCountry onchange={(event) => handleOnChange(event, 'Country')} value={AuthService.getCountryName(state?.Country) || 'UNITED STATES'} />
                            </Grid>
                        </Grid>
                    </>
                }
                showCancel={modals.currentModal === 'updateSecondaryContact' ? true : false}
                onClickProceed={modals.currentModal === 'addSecondaryContact' ? () => AddSecondaryContact() :
                    modals.currentModal === 'updateSecondaryContact' ? () => UpdateSecondaryContacts() : () => UpdatePrimaryContact()}
                onClickCancel={() => OpenDialog('alertdanger')}

                proceedButtonText={modals.currentModal === 'addSecondaryContact' ? 'Add Contact' : 'Update Contact'}
                cancelButtonText="Remove Contact"

                proceedButtonClass="success"
                cancelButtonClass="danger"

                ModalClose={() => CloseDialog('editcontact')}
            />

            <AlertBox
                open={modals.alertdanger === undefined ? false : modals.alertdanger}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Are you sure you want to remove this contact?</strong></Typography>
                            <Typography variant="subtitle2" gutterBottom>This contact will be removed. Select yes to proceed, no to cancel this action.</Typography>
                        </div>
                    </>
                }
                cancelButtonText="No, Cancel"
                proceedButtonText="Yes, do it!"
                proceedButtonClass="danger"
                onClickCancel={() => CloseDialog('alertdanger')}
                onClickProceed={() => DeleteSecondaryContact(state.id)}
            />

            <AlertBox
                open={modals.alertsuccess === undefined ? false : modals.alertsuccess}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <ThumbsUpIcon className={'success'} />
                            <Typography variant="h2" gutterBottom><strong>Success!</strong></Typography>
                            <Typography variant="subtitle2" gutterBottom>{modals.currentModal === "contactUpdated" ? <>contact updated</> : modals.currentModal === "contactadded" ? <>contact added</> : <>password updated</>}</Typography>
                        </div>
                    </>
                }
                showCancelButton={false}
                proceedButtonText="Continue"
                proceedButtonClass="success"
                onClickProceed={() => CloseDialog('alertsuccess')}
            />

            <AlertBox
                open={modals.networkError === undefined ? false : modals.networkError}
                content={
                    <>
                        <div style={{ textAlign: 'center' }}>
                            <InfoIcon className={'danger'} />
                            <Typography variant="h2" gutterBottom><strong>Fetching Failed!</strong></Typography>
                            <Typography variant="subtitle2" >Error getting details from server.</Typography>
                        </div>
                    </>
                }
                showCancelButton={false}
                proceedButtonText="Close"
                proceedButtonClass="danger"
                onClickProceed={() => CloseDialog('networkError')}
            />
        </BasicLayout>
    );
}