import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useParams } from "react-router-dom";
import moment from 'moment';
import BasicLayout from '../layouts/BasicLayout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DialogBox from '../elements/DialogBox';
import FallbackLoader from '../elements/FallbackLoader';

// Icons
import InfoIcon from '../assets/DetailsIco1.svg';
import GoodsIcon from '../assets/DetailsIco2.svg';
import StaffIcon from '../assets/DetailsIco3.svg';
import AttornyIcon from '../assets/DetailsIco4.svg';
import PersonIcon from '../assets/DetailsIco5.svg';
import ProsecutionIcon from '../assets/DetailsIco6.svg';
import StatusIcon from '../assets/DetailsIco8.svg';
import BasisIcon from '../assets/DetailsIco9.svg';
import Status1A from '../assets/1a.png';
import Status1B from '../assets/1b.png';
import DocumentIcon from '../assets/DetailsIco10.svg';
import { ReactComponent as ArrowIcon } from '../assets/DetailsIco7.svg';
import CommonService from '../services/CommonService';
import Excerpt from '../elements/Excerpt';

const useStyles = makeStyles((theme) => ({
  TitleBox: {
    '& .MuiTypography-h1': {
      marginTop: theme.spacing(2),
    },
    '& img': {
      maxWidth: '100%',
      float: 'right',
      maxHeight: 100,
    }
  },
  Stage: {
    backgroundColor: theme.palette.primary.main,
    padding: theme.spacing(1.5),
    '& .MuiTypography-root': {
      color: theme.palette.light.main,
      lineHeight: 1.3,
      textAlign: 'center',
    },
    '& .MuiTypography-subtitle1, & .MuiTypography-subtitle2': {
      letterSpacing: '1px',
    }
  },
  Summary: {
    marginBottom: theme.spacing(2),
    borderBottom: '3px solid ' + theme.palette.primary.main,
    '& .SummaryBox': {
      display: 'flex',
      padding: theme.spacing(1, 0),
      '& .Left': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minWidth: 85,
        maxWidth: '15%',
      }
    }
  },
  Details: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    '@media (min-width: 400px)': {
      justifyContent: 'flex-start',
    },
    '& .option': {
      textAlign: 'center',
      padding: '10px 5px',
      '@media (max-width: 360px)': {
        flex: '0 0 50%',
        maxWidth: '50%',
      },
      '@media (min-width: 400px)': {
        flex: '0 0 33.33%',
        maxWidth: '33.33%',
      },
      '@media (min-width: 660px)': {
        flex: '0 0 25%',
        maxWidth: '25%',
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: theme.palette.primary.light,
        }
      },
    }
  }

}));

export default function Home(props) {
  const classes = useStyles();
  const history = useHistory();
  const [loader, setLoader] = React.useState(true);
  const [state, setState] = React.useState([]);
  const [response, setResponse] = React.useState([]);
  const trademarkID = useParams().id;

  const GoBack = () => {
    history.goBack()
  }

  React.useEffect(() => {
    CommonService.ExecuteGet({ Url: "gettrademarkdetail?id=" + trademarkID }, (res) => {
      setResponse(res);
      if (res.GoodAndServices != null && res.GoodAndServices.length > 0) {
        var arrayObj = [];
        for (var i = 0; i < res.GoodAndServices.length; i++) {
          // var InternationalClass = [];
          // var UsClasses = [];
          // var UsClassesString = "";
          //var InternationalClassString = "";
          // for (var j = 0; j < res.GoodAndServices[i].GoodAndServiceClasses.length; j++) {
          //   if (res.GoodAndServices[i].GoodAndServiceClasses[j].Type === "UsClass") {
          //     UsClasses.push(res.GoodAndServices[i].GoodAndServiceClasses[j].Code);
          //   }
          //   else {
          //     InternationalClass.push(res.GoodAndServices[i].GoodAndServiceClasses[j].Code);
          //   }
          // }
          // UsClassesString = UsClasses.join();
          // InternationalClassString = InternationalClass.join();

          var obj = {
            FirstUseInCommerceDate: res.GoodAndServices[i].FirstUseInCommerceDate,
            FirstUseDate: res.GoodAndServices[i].FirstUseDate,
            PrimeClassCode: res.GoodAndServices[i].PrimeClassCode,
            Description: res.GoodAndServices[i].Description,
            StatusDescription: res.GoodAndServices[i].StatusDescription,
            StatusCode: res.GoodAndServices[i].StatusCode,
            //UsClassesString: UsClassesString,
            InternationalClassString: res.GoodAndServices[i].PrimeClassCode,
          };
          arrayObj.push(obj);
        }

        setState({
          ...state,
          model: arrayObj
        });
      }

      setLoader(false);
    });
  }, [trademarkID]);

  const [modals, setModals] = React.useState([])
  const OpenDialog = (name, status, isFiled) => {
    if(name === 'statuspopup'){
      if (status !== 'D' && status !== 'R') {
        if (isFiled) {
          setModals({
            ...modals,
            statusA: true,
          })
        } else {
          setModals({
            ...modals,
            statusB: true,
          })
        }
      } 
    }
    else {
      setModals({
        ...modals,
        [name]: true,
      })
    }
  }
  const CloseDialog = (name) => {
    setModals({
      ...modals,
      [name]: false,
    })
  }
  return (
    <BasicLayout>
      {loader ? <FallbackLoader allowloader={true} /> : ''}
      {response.Id !== undefined ? <>
        <Grid container className={classes.TitleBox} gutterbottomdouble="true">
          <Grid item xs={12}>
            <div onClick={() => GoBack()}><ArrowIcon style={{ cursor: 'pointer' }} /></div>
            <Typography variant="h1" gutterBottom color="primary">{response.ClientName}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle1">Last Updated Date</Typography>
            <Typography variant="subtitle2" gutterbottomdouble="true">{response.LastUpdateDate !== null ? moment(response.LastUpdateDate).format('MMM-DD-YYYY') : 'N/A'}</Typography>
            <Typography variant="subtitle1">US Serial Number</Typography>
            <Typography variant="subtitle2" gutterbottomdouble="true">{response.SerialNumber}</Typography>
            <Typography variant="subtitle1">Filing Date</Typography>
            <Typography variant="subtitle2" gutterbottomdouble="true">{response.FiliingDateText !== null ? response.FiliingDateText : 'N/A'}</Typography>
          </Grid>
          <Grid item xs={6} className={classes.TitleBox}>
            {response.Image !== "" || response.Image !== null || response.Image !== undefined ?
              <img src={response?.Image} alt="" /> : ''}
          </Grid>
          {(response.UsRegistrationDate !== null && response.UsRegistrationDate !== "") ? <Grid item xs={6}>
            <Typography variant="subtitle1">Registration Date</Typography>
            <Typography variant="subtitle2">{response.UsRegistrationDate}</Typography>
          </Grid> : ""}
          {(response.UsRegistrationNumber !== null && response.UsRegistrationNumber !== "") ? <Grid item xs={6}>
            <Typography variant="subtitle1">Registration Number</Typography>
            <Typography variant="subtitle2" style={{ marginBottom: 10 }}>{response.UsRegistrationNumber}</Typography>
          </Grid> : ""}
        </Grid>
        <Grid container>
          <Grid item xs={12} onClick={() => OpenDialog('statuspopup', response.CurrentStatus, response.IsFiledInternationLaw)} className={classes.Stage} style={{ backgroundColor: response.StatusColor === "Green" ? "#24A649" : response.StatusColor === "Red" ? "#FF0000" : response.StatusColor === "Yellow" ? "#D6B945" : "" }}>
            <Typography variant="subtitle1">{response.CurrentStatus === "R" ? "Registered" : <span>Stage {response.CurrentStatus}</span>}</Typography>
            {(response.NextDeadlineText !== null && response.NextDeadlineText !== "") ? <Typography variant="subtitle2">{"Earliest file date: " + response.NextDeadlineText}</Typography> : ""}
            {(response.MaxNextDeadline !== null && response.MaxNextDeadline !== "") ? <Typography variant="subtitle2">{"Latest file date: " + response.MaxNextDeadline}</Typography> : ""}
          </Grid>
          <Grid item xs={12} className={classes.Summary}>
            <div className={'SummaryBox'}>
              <div className={'Left'}>
                <Typography variant="subtitle1" gutterBottom>Status</Typography>
                <img src={StatusIcon} alt="" />
              </div>
              <div className={'Right'}>
                <Typography variant="subtitle2" gutterBottom>{response.Tm5StatusDescription}</Typography>
                <Typography variant="subtitle2" gutterBottom>{response.ExternalStatusDescription}</Typography>
              </div>
            </div>
            <div className={'SummaryBox'}>
              <div className={'Left'}>
                <Typography variant="subtitle1" gutterBottom>Owner</Typography>
                <img src={PersonIcon} alt="" />
              </div>
              <div className={'Right'}>
                <Typography variant="subtitle2" gutterBottom>{response.OwnerGroup?.Name}</Typography>
                <Typography variant="subtitle2" gutterBottom>{response.OwnerGroup?.Address !== undefined ? response.OwnerGroup?.Address + " " + response.OwnerGroup?.City + " " + response.OwnerGroup?.State + " " + response.OwnerGroup?.IsoName : ""}</Typography>
                <Typography variant="subtitle2" gutterBottom>{response.OwnerGroup?.TypeDescription || ""}</Typography>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} className={classes.Details}>
            <div className={'option'} onClick={() => OpenDialog('infoPopup')}>
              <Typography variant="subtitle1" gutterBottom>Mark Info</Typography>
              <img src={InfoIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('goodsPopup')}>
              <Typography variant="subtitle1" gutterBottom>Goods & Services</Typography>
              <img src={GoodsIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('basisPopup')}>
              <Typography variant="subtitle1" gutterBottom>Basis Info</Typography>
              <img src={BasisIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('attorneyPopup')}>
              <Typography variant="subtitle1" gutterBottom>Attorney</Typography>
              <img src={AttornyIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('prosecutionPopup')}>
              <Typography variant="subtitle1" gutterBottom>Prosecution History</Typography>
              <img src={ProsecutionIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('staffPopup')}>
              <Typography variant="subtitle1" gutterBottom>USPTO Staff</Typography>
              <img src={StaffIcon} alt="" />
            </div>
            <div className={'option'} onClick={() => OpenDialog('documentsPopup')}>
              <Typography variant="subtitle1" gutterBottom>Documents</Typography>
              <img src={DocumentIcon} alt="" />
            </div>
          </Grid>
        </Grid>

        {/* Popups */}
        <DialogBox
          open={modals.infoPopup === undefined ? false : modals.infoPopup}
          title="Mark Information"
          content={
            <>
              <Typography variant="subtitle1">Mark Literal Elements</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.TrademarkElement}</Typography>

              <Typography variant="subtitle1">Standard Character Claim</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsStandard ? 'YES' : 'NO'}</Typography>

              <Typography variant="subtitle1">Mark Drawing Type</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.MarkDrawingCode + " - " + response.TrademarkDrawingDescription}</Typography>

              <Typography variant="subtitle1">Description of Mark</Typography>
              <Excerpt value={response?.DescriptionOfMark || "N/A"} />

              <Typography variant="subtitle1" style={{ marginTop: 16 }}>Description of Mark</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.TrademarkElement || 'N/A'}</Typography>

              <Typography variant="subtitle1">Design Search Code(s)</Typography>
              {response?.DesignSearch?.length > 0 ? response?.DesignSearch?.map((each) => (
                <Typography gutterBottom key={Math.random()} variant="subtitle2">{each.Code + " - " + each.DesignSearchDescriptions.map((item) => (item.Description + (item.Description.length === 1 ? ", " : " ")))}</Typography>)) :
                <Typography variant="subtitle2">N/A</Typography>
              }
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('infoPopup')}
        />

        <DialogBox
          open={modals.goodsPopup === undefined ? false : modals.goodsPopup}
          title="Goods and Services"
          content={
            <>
              <Typography variant="body2">The following symbols indicate that the registrant/owner has amended the goods/services:</Typography>
              <ul>
                <li><Typography variant="body2" gutterBottom>Brackets [..] indicate deleted goods/services;</Typography></li>
                <li><Typography variant="body2" gutterBottom>Double parenthesis ((..)) identify any goods/services not claimed in a Section 15 affidavit of incontestability; and</Typography></li>
                <li><Typography variant="body2" gutterBottom>Asterisks *..* identify additional (new) wording in the goods/services.</Typography></li>
              </ul>

              {state.model?.map((each) => (
                <div style={{ marginBottom: 8, paddingTop: 8, borderTop: '1px solid #cccccc' }} key={Math.random()}>
                  <Typography variant="subtitle1">International Class(es)</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{each?.InternationalClassString || ""}</Typography>

                  <Typography variant="subtitle1">Class Status</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{each?.StatusDescription || ""}</Typography>

                  <Typography variant="subtitle1">Basis</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">1(a)</Typography>

                  <Typography variant="subtitle1">First Use</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{each?.FirstUseDate}</Typography>

                  {/* <Typography variant="subtitle1">U.S Class(es)</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{each?.UsClassesString || ""}</Typography> */}

                  <Typography variant="subtitle1">Use in Commerce</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{each?.FirstUseInCommerceDate}</Typography>

                  <Typography variant="subtitle1">For</Typography>
                  <Excerpt value={each?.Description || ""} />
                </div>
              ))}
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('goodsPopup')}
        />

        <DialogBox
          open={modals.basisPopup === undefined ? false : modals.basisPopup}
          title="Basis Information"
          content={
            <>
              <Grid container className={classes.TitleBox}>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed Use</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiledUse ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Currently Use</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsUseCurrent ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed ITU</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiledInternationLaw ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Currently ITU</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsInternationalLawCurrent ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed 44D</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiled44d ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Currently 44E</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsSect44eCurrent ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed 44E</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiled44e ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Currently 66A</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsSect66aCurrent ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed 66A</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiled66a ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Currently No Basis</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsNoBasisCurrent ? 'YES' : 'NO'}</Typography>
                </Grid>
                <Grid item xs={6} sm={4} md={3}>
                  <Typography variant="subtitle1">Filed No Basis</Typography>
                  <Typography variant="subtitle2" gutterbottomdouble="true">{response.IsFiledNoBasis ? 'YES' : 'NO'}</Typography>
                </Grid>
              </Grid>
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('basisPopup')}
        />

        <DialogBox
          open={modals.attorneyPopup === undefined ? false : modals.attorneyPopup}
          title="Attorney Information"
          content={
            <>
              <Typography variant="h5" style={{ textAlign: 'center' }} gutterbottomdouble="true">Attorney of Record</Typography>

              <Typography variant="subtitle1">Attorney Name</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.Name}</Typography>

              <Typography variant="subtitle1">Attorney Email</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.Email}</Typography>

              <Typography variant="subtitle1">Attorney Email Authorized</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.IsEmailAuthticated ? 'YES' : 'NO'}</Typography>

              <Typography variant="subtitle1">Address</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true"></Typography>

              <Typography variant="h5" style={{ textAlign: 'center' }} gutterbottomdouble="true">Correspondent</Typography>

              <Typography variant="subtitle1">Correspondent</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.FullName}</Typography>

              <Typography variant="subtitle1">Name/Address</Typography>
              <Typography variant="subtitle2">{response.Attorney?.Firm.Name}</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.Line1 + " " + response.Attorney?.City + " " + response.Attorney?.PostalCode + " " + response.Attorney?.CountryName}</Typography>

              <Typography variant="subtitle1">Phone</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.Firm.Phone}</Typography>

              <Typography variant="subtitle1">Correspondent e-mail</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.Attorney?.Firm.Email}</Typography>

              <Typography variant="subtitle1">Correspondent e-mail Authorized</Typography>
              <Typography variant="subtitle2">{response.Attorney?.Firm.IsEmailAuthticated === true ? "Yes" : "No"}</Typography>
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('attorneyPopup')}
        />

        <DialogBox
          open={modals.prosecutionPopup === undefined ? false : modals.prosecutionPopup}
          title="Procecution History"
          content={
            <>
              <table>
                <thead>
                  <tr>
                    <th width="20%">Date</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  {response.ProsecutionHistorys?.map((each) => (
                    <tr key={each.EntryNumber}>
                      <td>{each.EntryDateText}</td>
                      <td>{each.Description}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('prosecutionPopup')}
        />

        <DialogBox
          open={modals.staffPopup === undefined ? false : modals.staffPopup}
          title="USPTO Staff"
          content={
            <>
              <Typography variant="h5" gutterBottom style={{ textAlign: 'center' }}>USPTO Staff Information</Typography>

              <Typography variant="subtitle1">USPTO Attorney</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.ExaminerName || 'N/A'}</Typography>

              <Typography variant="subtitle1">Law Office Assigned</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.LawOffAssigned || 'N/A'}</Typography>

              <Typography variant="h5" gutterBottom style={{ textAlign: 'center' }}>File Location</Typography>

              <Typography variant="subtitle1">Current Location</Typography>
              <Typography variant="subtitle2" gutterbottomdouble="true">{response.CurrentLocation || 'N/A'}</Typography>

              <Typography variant="subtitle1">Date in Location</Typography>
              <Typography variant="subtitle2">{response.CurrentLocationDateString}</Typography>
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('staffPopup')}
        />

        <DialogBox
          open={modals.documentsPopup === undefined ? false : modals.documentsPopup}
          title="Documents"
          content={
            <>
              <table>
                <thead>
                  <tr>
                    <th width="20%" style={{ minWidth: 70 }}>Date</th>
                    <th>Description</th>
                    <th>Type</th>
                  </tr>
                </thead>
                <tbody>
                  {response.DocumentModels?.map((each) => (
                    <tr key={each.DocumentId}>
                      <td>{each.MailRoomDateString}</td>
                      <td><a href={each.Url} target="_blank" rel="noreferrer">{each.Description || "No Description"}</a></td>
                      <td>{each.DocumentType || ""}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('documentsPopup')}
        />

        <DialogBox
          open={modals.statusA === undefined ? false : modals.statusA}
          title="Timeline"
          content={
            <>
              <img src={Status1A} style={{ maxWidth: '100%' }} alt="1a" />
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('statusA')}
        />

        <DialogBox
          open={modals.statusB === undefined ? false : modals.statusB}
          title="Timeline"
          content={
            <>
              <img src={Status1B} style={{ maxWidth: '100%' }} alt="1b" />
            </>
          }
          cancelButtonText="Cancel"
          proceedButtonText="Accept"
          ModalClose={() => CloseDialog('statusB')}
        />

      </> : ""}
    </BasicLayout>
  );
}