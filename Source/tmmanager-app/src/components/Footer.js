import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Badge from '@material-ui/core/Badge';

// Icons
import { ReactComponent as HomeIcon } from '../assets/MenuIco1.svg';
import { ReactComponent as EnvelopeIcon } from '../assets/MenuIco2.svg';
import { ReactComponent as ProfileIcon } from '../assets/MenuIco3.svg';
import FooterLogo from '../assets/FooterLogo.jpg';
import CommonService from '../services/CommonService';
import AuthService from '../services/authService';

const useStyles = makeStyles((theme) => ({
    Footer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(0.5, 0, 1.25),
        height: 50,
        background: theme.palette.primary.medium,
        [theme.breakpoints.up('sm')]: {
            position: 'relative',
        },
    },
    BottomNavigation: {
        backgroundColor: 'transparent',
        flex: 1,
        height: 'auto',
        [theme.breakpoints.up('sm')]: {
            flex: 0,
            '& + div': {
                position: 'absolute',
                right: 0,
            }
        },
        '& a': {
            padding: '0!important',
            '&.active': {
                paddingTop: 0,
                '& svg .a': {
                    stroke: theme.palette.light.main,
                }
            },
            '& svg': {
                width: '1.5em',
                height: '1.5em'
            }
        },
        '& .MuiBadge-badge': {
            height: 20,
            width: 20,
            minWidth: 0,
            padding: theme.spacing(0, 0.5),
            backgroundColor: theme.palette.primary.main
        },
        '@media (max-width: 370px)': {
            '& .MuiBottomNavigationAction-root': {
                minWidth: 65,
            }
        },
    },
    FooterText: {
        display: 'flex',
        alignItems: 'center',
    },
}));

export default function Footer() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    React.useEffect(() => {
        var username = AuthService.getUserName();
        CommonService.ExecuteGet({ Url: "getunreadnotificationscount?username=" + username }, (res) => {
            setValue(res.Count);
        });
    }, [value]);

    return (
        <div className={classes.Footer}>
            <BottomNavigation value={value} onChange={(newValue) => { setValue(newValue) }} className={classes.BottomNavigation}>
                <Tooltip title="Home" placement="top" arrow TransitionComponent={Zoom}>
                    <BottomNavigationAction component={NavLink} to="/trademarks" activeClassName="active" label={false} icon={<HomeIcon />} />
                </Tooltip>
                <Tooltip title="Alerts" placement="top" arrow TransitionComponent={Zoom}>
                    <BottomNavigationAction component={NavLink} to="/alerts" activeClassName="active" label={false} icon={<Badge badgeContent={value === 0 ? value : value?.toString()} color="secondary"><EnvelopeIcon /></Badge>} />
                </Tooltip>
                <Tooltip title="Profile" placement="top" arrow TransitionComponent={Zoom}>
                    <BottomNavigationAction component={NavLink} to="/profile" activeClassName="active" label={false} icon={<ProfileIcon />} />
                </Tooltip>
            </BottomNavigation>
            <div className={classes.FooterText}>
                <img src={FooterLogo} alt="Footer" />
            </div>
        </div>

    );
}