import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import BasicLayout from '../../layouts/BasicLayout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FallbackLoader from '../../elements/FallbackLoader';
import CommonService from '../../services/CommonService';
import AuthService from '../../services/authService';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    AlertLists: {
        marginTop: theme.spacing(1),
        '& > div': {
            cursor: 'pointer',
            margin: theme.spacing(0, -1.5),
            flexBasis: 'calc(100% + 24px)',
            maxWidth: 'calc(100% + 24px)',
            padding: '12px 16px 12px 60px',
            '& .MuiTypography-subtitle1': {
                marginBottom: 2,
            },
            '& .MuiTypography-body1': {
                lineHeight: '1.3',
            },
            '& .MuiTypography-body2': {
                textAlign: 'right',
            },
            '&:hover, &.unread': {
                backgroundColor: theme.palette.primary.light,
                position: 'relative',
            },
            '&.unread::after': {
                content: "' '",
                backgroundColor: theme.palette.success.main,
                position: 'absolute',
                width: 12,
                height: 12,
                left: 16,
                top: 36,
                borderRadius: '100%',
            }
        }
    }

}));

export default function AlertLists(props) {
    const classes = useStyles();
    const [loader, setLoader] = React.useState(true);
    const [nodata, seNoData] = React.useState(false);
    const [response, setResponse] = React.useState([]);
    const history = useHistory();

    React.useEffect(() => {
        var username = AuthService.getUserName();
        CommonService.ExecuteGet({ Url: "getallnotifications?username=" + username }, (res) => {
            if (res.length === 0) {
                seNoData(true)
                setLoader(false);
            }
            else {
                seNoData(false)
                setResponse(res);
                setLoader(false);
            }
        });
    }, []);

    const GotoURL = (url) => {
        history.push(url);
    }

    return (
        <BasicLayout>
            {loader ? <FallbackLoader allowloader={true} /> : ''}
            <Typography variant="h1" color="primary">Alerts</Typography>
            <Typography variant="subtitle2" >Alerts for {AuthService.getClientName()}</Typography>

            {nodata === true ?
                <Grid item xs={12}>
                    <Typography variant="body1" style={{ marginTop: 16 }}><em>No Alerts Found</em></Typography>
                </Grid>
                :
                <Grid container className={classes.AlertLists}>
                    {response?.length > 0 ? response?.map((each) => (
                        <Grid item xs={12} className={each.IsRead === false ? "unread" : ""} key={each.Id} onClick={() => GotoURL('alerts/'+each.Id)}>
                            <Typography variant="body2" gutterBottom>{moment(each?.Created).isSame(moment().format('YYYY-MM-DD'), 'day') ? "Today" : moment(each?.Created).format('YYYY-MM-DD')}</Typography>
                            <Typography variant="subtitle1">{each.From}</Typography>
                            <Typography variant="body1">{each.Message}</Typography>
                        </Grid>
                    )) : ''}

                </Grid>
            }
        </BasicLayout>
    );
}