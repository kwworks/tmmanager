import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import BasicLayout from '../../layouts/BasicLayout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FallbackLoader from '../../elements/FallbackLoader';
import CommonService from '../../services/CommonService';
import CloseIcon from '@material-ui/icons/Close';
import { useParams } from 'react-router-dom';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    AlertDetails:{
        '& .TitleBar': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 16,
            '& svg':{
                cursor: 'pointer',
                width: 32,
                height: 32,
            }
        }
    }

}));

export default function AlertDetails(props) {
    const classes = useStyles();
    const [loader, setLoader] = React.useState(true);
    const [response, setResponse] = React.useState([]);
    const history = useHistory();
    const AlertID = useParams().id;

    React.useEffect(() => {
        CommonService.ExecuteGet({ Url: "getnotificationdetail?id=" + AlertID }, (res) => {
            if (res.length === 0) {
                setLoader(false);
            }
            else {
                setResponse(res);
                console.log(res);
                setLoader(false);
            }
        });
    }, [AlertID]);

    const GotoURL = (url) => {
        history.push(url);
    }

    return (
        <BasicLayout>
            {loader ? <FallbackLoader allowloader={true} /> : ''}
            
            <Grid container className={classes.AlertDetails}>
                <Grid item xs={12} className={'TitleBar'}>
                    <Typography variant="h1" color="primary">Alert Details</Typography>
                    <CloseIcon onClick={() => GotoURL('/alerts')}/>
                </Grid>
                <Grid item xs={12} className={'Details'}>
                    <Typography variant="subtitle1">From</Typography>
                    <Typography variant="body1" gutterbottomdouble="true">{response?.From || ""}</Typography>
                    <Typography variant="subtitle1">Sent</Typography>
                    <Typography variant="body1" gutterbottomdouble="true">{moment(response?.Created).format('YYYY-MM-DD') +" @ "+ moment(response?.Created).format('HH-mm a')}</Typography>
                    <Typography variant="subtitle1">Message</Typography>
                    <Typography variant="body1" gutterbottomdouble="true">{response?.Message || ""}</Typography>
                </Grid>
            </Grid>
        </BasicLayout>
    );
}