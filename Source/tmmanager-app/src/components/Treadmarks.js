import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import BasicLayout from '../layouts/BasicLayout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FallbackLoader from '../elements/FallbackLoader';
import CommonService from '../services/CommonService';
import AuthService from '../services/authService';

const useStyles = makeStyles((theme) => ({
  PostTile: {
    background: theme.palette.primary.light,
    alignItems: 'center',
    margin: theme.spacing(1.5, 0),
    padding: theme.spacing(0.5, 0.5, 0.5, 1),
    borderRadius: theme.spacing(1.5),
    cursor: 'pointer',
  },
  Image: {
    maxWidth: 65,
    '& img': {
      maxWidth: '100%',
      borderRadius: theme.spacing(1.5),
    }
  },
  Details: {
    flex: '0 0 calc(100% - 130px)',
    maxWidth: 'calc(100% - 130px)',
    margin: theme.spacing(0, 1),
    '& .MuiTypography-subtitle2 + a': {
      color: theme.palette.secondary.main,
      fontSize: 16,
      fontWeight: 600,
    }
  },
  Action: {
    padding: theme.spacing(0, 1),
    flex: 0,
    whiteSpace: 'nowrap',
    '& .MuiTypography-body2': {
      color: '#00000099',
    }
  },
  Bubble: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 'auto',
    color: theme.palette.light.main,
    background: theme.palette.primary.main,
    borderRadius: '100%',
    width: 30,
    height: 30,
  },

}));

export default function Treadmarks(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(true);
  const [nodata, seNoData] = React.useState(false);
  const [response, setResponse] = React.useState([]);
  const history = useHistory();

  React.useEffect(() => {
    var email = AuthService.getUserName();
    CommonService.ExecuteGet({ Url: "gettrademarks?email=" + email }, (res) => {
      if (res.length > 0) {
        setResponse(res)
        setLoader(false);
      }
      else if (res.length === 0) {
        seNoData(true)
        setLoader(false);
      }
      else {
        setLoader(false);
      }
    });
  }, []);

  const GotoURL = (url) => {
    history.push(url);
  }

  return (
    <BasicLayout>
      {loader ? <FallbackLoader allowloader={true} /> : ''}
      <Typography variant="h1" color="primary">Trademarks</Typography>
      <Typography variant="subtitle2" >Trademarks for {AuthService.getClientName()}</Typography>
      {response?.length > 0 ? response?.map((each) => (
        <Grid className={classes.PostTile} key={each.Id} container onClick={() => GotoURL('/trademarks/' + each.Id)}>
          <Grid item xs className={classes.Image}>
            {each.image !== "" || each.Image !== null || each.Image !== undefined ?
              <img src={each.Image} alt="" /> : ''}
          </Grid>
          <Grid item xs className={classes.Details}>
            <Typography variant="h4">{each.Name}</Typography>
            <Typography variant="subtitle2" gutterBottom>{each.Serial}</Typography>
            <Link to={"/trademarks/" + each.Id}>Details</Link>
          </Grid>
          <Grid item xs className={classes.Action}>
            <Typography variant="subtitle2" className={classes.Bubble} style={{background: each.StatusColor === "Green" ? "#24A649" : each.StatusColor === "Red" ? "#FF0000" : each.StatusColor === "Yellow" ? "#D6B945" : ""}}>{each.CurrentStatus}</Typography>
            <Typography variant="body2">Status</Typography>
          </Grid>
        </Grid>
      )) : ""}
      {nodata === true ?
        <Grid item xs={12}>
          <Typography varant="body1" style={{ marginTop: 16 }}><em>No Trademarks Found</em></Typography>
        </Grid>
        : ""}
    </BasicLayout>
  );
}