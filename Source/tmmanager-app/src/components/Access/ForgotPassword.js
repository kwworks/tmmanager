import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import FallbackLoader from '../../elements/FallbackLoader';
import AccessLayout from '../../layouts/AccessLayout';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg"
import { Link } from 'react-router-dom';
import CommonService from '../../services/CommonService';

const useStyles = makeStyles((theme) => ({
  AccessBox: {
    padding: 30,
    width: '95%',
    maxWidth: 450,
    border: '3px solid ' + theme.palette.primary.main,
    margin: '24px auto',
    borderRadius: 25,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2),
    }
  },
  Logo: {
    textAlign: 'center',
    '& img': {
      maxWidth: '100%',
    },
    '& h2': {
      fontSize: 30,
      margin: '10px 0',
    },
    '& h6': {
      margin: theme.spacing(2, 0),
      textAlign: 'left',
    }
  },
  Action: {
    textAlign: 'center',
    '& button': {
      marginBottom: theme.spacing(2),
    },
    '& a': {
      display: 'block',
      color: theme.palette.secondary.main,
      fontSize: 16,
      fontWeight: 600,
      letterSpacing: 0,
    },
  }
}));

export default function ForgotPassword(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);

  const history = useHistory();
  const GotoURL = (url) => {
    history.push(url)
  }

  const [state, setState] = React.useState([]);
  const handleOnChange = (event, name) => {
    setState({
      ...state,
      [name]: event.target.value,
    })
  }
  const sendPasswordEmail = () => {
    setLoader(true);
    CommonService.ExecuteGet({ Url: "forgotpassword?email=" + state.email }, (res) => {
      
      setLoader(true);
      if ((state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) || state.email === '' || state.email === undefined) {
        setState({
          ...state,
          error: true
        });
        setLoader(false);
      }
      else {
        if (res.Errors.length > 0) {
          setState({
            ...state,
            error: true,
            message: res.Errors
          });
          setLoader(false);
        }
        else {
          setState({
            ...state,
            error: false,
            message: '',
            currentView: 'emailSent',
          })
          setLoader(false);
        }
      }
    });
  }

  return (
    <AccessLayout>
      {loader ? <FallbackLoader allowimage={false} allowloader={true} /> : ''}
      {(state.currentView === undefined || state.currentView === "one") ? <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
          <Typography variant="h2" gutterBottom><strong>Forgot Password?</strong></Typography>
          <Typography variant="subtitle2" gutterBottom>Enter your email below and we'll send you reset instructions.</Typography>
        </Grid>
        <Grid item xs={12} className={classes.Access}>
          <div>
            <TextField
              id="email"
              label="Email"
              variant="outlined"
              placeholder=""
              value={state.email || ""}
              onChange={(event) => handleOnChange(event, 'email')}
              error={state?.error || false}
              helperText={(state.email === undefined || state.email === "") && state.error === true ? "Email is required" : (state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
              fullWidth
              required
            />
          </div>
        </Grid>
        <Grid item xs={12} className={classes.Action}>
          <Button onClick={() => sendPasswordEmail()} variant="contained" color="primary" fullWidth disableElevation size="large">Reset Password</Button>
          <Typography variant="body1" data-error gutterBottom>{state?.message || ""}</Typography>
          <Link to="/">Nevermind</Link>
        </Grid>
      </Grid>
        :
        <Grid className={classes.AccessBox} container>
          <Grid item xs={12} className={classes.Logo}>
            <img src={LogoImage} alt="" />
            <Typography variant="h2" gutterBottom><strong>Email Sent</strong></Typography>
            <Typography variant="subtitle2" gutterBottom>An email has been sent to your inbox with directions to reset your password.</Typography>
          </Grid>
          <Grid item xs={12} className={classes.Action}>
            <Button onClick={() => GotoURL('/')} variant="contained" color="primary" fullWidth disableElevation size="large">Back to Login Screen</Button>
          </Grid>
        </Grid>}
    </AccessLayout>
  );
}