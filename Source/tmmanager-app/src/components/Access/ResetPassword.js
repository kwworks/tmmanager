import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import FallbackLoader from '../../elements/FallbackLoader';
import AccessLayout from '../../layouts/AccessLayout';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg"
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  AccessBox: {
    padding: 30,
    width: '95%',
    maxWidth: 450,
    border: '3px solid ' + theme.palette.primary.main,
    margin: '24px auto',
    borderRadius: 25,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2),
    }
  },
  Logo: {
    textAlign: 'center',
    '& img': {
      maxWidth: '100%',
    },
    '& h2': {
      fontSize: 30,
      margin: '10px 0',
    },
    '& h6': {
      margin: theme.spacing(2, 0),
      textAlign: 'left',
    }
  },
  Action: {
    textAlign: 'center',
    '& button': {
      marginBottom: theme.spacing(2),
    },
    '& a': {
      display: 'block',
      color: theme.palette.secondary.main,
      fontSize: 16,
      fontWeight: 600,
      letterSpacing: 0,
    },
  }
}));

export default function ResetPassword(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);

  const history = useHistory();
  const GotoURL = (url) => {
    history.push(url)
  }

  const [state, setState] = React.useState([]);
  const handleOnChange = (event, name) => {
    setState({
      ...state,
      [name]: event.target.value,
    })
  }
  const ResetPassword = () => {
    setLoader(true);
    if (
      state.newpassword === '' || 
      state.newpassword === undefined ||
      state.confirmpassword === '' || 
      state.confirmpassword === undefined ||
      state.confirmpassword !== state.newpassword
      ) {
      setState({
        ...state,
        error: true,
      })
      setLoader(false);
    }
    else {
      setState({
        ...state,
        error: false,
        currentView: 'passwordChanged'
      })
      setLoader(false);
    }
  }

  return (
    <AccessLayout>
      {loader ? <FallbackLoader allowimage={true} allowloader={true} /> : ''}
      {(state.currentView === undefined || state.currentView === "one") ? <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
          <Typography variant="h2" gutterBottom><strong>Reset Password</strong></Typography>
          <Typography variant="subtitle2" gutterBottom>You can reset your password by entering your new password below.</Typography>
        </Grid>
        <Grid item xs={12} className={classes.Access}>
          <div>
            <TextField
              id="newpassword"
              label="New Password"
              type="password"
              variant="outlined"
              placeholder=""
              value={state.newpassword || ""}
              onChange={(event) => handleOnChange(event, 'newpassword')}
              error={state?.error || false}
              helperText={(state.newpassword === undefined || state.newpassword === "") && state.error === true ? "New Password is required" : ""}
              fullWidth
              required
            />
          </div>
          <div>
            <TextField
              id="confirmpassword"
              label="Confirm Password"
              type="password"
              variant="outlined"
              placeholder=""
              value={state.confirmpassword || ""}
              onChange={(event) => handleOnChange(event, 'confirmpassword')}
              error={state?.error || false}
              helperText={(state.confirmpassword === undefined || state.confirmpassword === "") && state.error === true ? "Confirm Password is required" : (state.confirmpassword !== undefined || state.confirmpassword !== "") && state.error === true && state.confirmpassword !== state.newpassword ? "Password not matched" : ""}
              fullWidth
              required
            />
          </div>
        </Grid>
        <Grid item xs={12} className={classes.Action}>
          <Button onClick={() => ResetPassword()} variant="contained" color="primary" fullWidth disableElevation size="large">Change Password</Button>
          <Link to="/">Nevermind</Link>
        </Grid>
      </Grid>
        :
        <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
          <Typography variant="h2" gutterBottom><strong>Success!</strong></Typography>
          <Typography variant="subtitle2" gutterBottom>Your password has been reset. You can now use it to login to the system.</Typography>
        </Grid>
        <Grid item xs={12} className={classes.Action}>
          <Button onClick={() => GotoURL('/')} variant="contained" color="primary" fullWidth disableElevation size="large">Login to the System</Button>
        </Grid>
      </Grid>}
    </AccessLayout>
  );
}