import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import AccessLayout from '../../layouts/AccessLayout';
import { Grid, Typography, TextField, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import FallbackLoader from '../../elements/FallbackLoader';

// Icons
import LogoImage from "../../assets/TManager-Logo-Old.jpg";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Link } from 'react-router-dom';

// Calls
import AuthService from "../../services/authService";

const useStyles = makeStyles((theme) => ({
  AccessBox: {
    padding: 30,
    width: '95%',
    maxWidth: 450,
    border: '3px solid ' + theme.palette.primary.main,
    margin: '24px auto',
    borderRadius: 25,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2),
    }
  },
  Logo: {
    textAlign: 'center',
    '& img': {
      maxWidth: '100%',
    },
    '& h2': {
      fontSize: 30,
      margin: '10px 0',
    },
    '& h6': {
      margin: theme.spacing(2, 0),
      textAlign: 'left',
    }
  },
  Action: {
    textAlign: 'center',
    '& button': {
      marginBottom: theme.spacing(2),
    },
    '& a': {
      display: 'block',
      color: theme.palette.secondary.main,
      fontSize: 16,
      fontWeight: 600,
      letterSpacing: 0,
    },
  }
}));

export default function Login(props) {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(true);
  const [state, setState] = React.useState([]);
  const history = useHistory();

  React.useEffect(() => {
    AuthService.syncAllCountries((res) => {
      
    });
    if (AuthService.IsAuthenticated() && localStorage.getItem('stayLogged') !== null) {
      history.push('/trademarks');
    }
    else {
      setLoader(false);
    }
  }, [history]);

  const handleOnChange = (event, name) => {
    setState({
      ...state,
      [name]: event.target.value,
    })
  }

  const handleStayLogged = (event) => {
    setState({
      ...state,
      stayLogged: event.target.checked,
    })
  }

  const handleLogin = () => {
    setLoader(true);
    if (
      (state.email !== undefined && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) || state.email === '' || state.email === undefined || state.password === '' || state.password === undefined) {
      setState({
        ...state,
        error: true,
      })
      setLoader(false);
    }
    else {
      var isSuccess, fail;
      AuthService.signIn(state.email, state.password, isSuccess, fail, (data) => {
        var res = data;
        if (res.Errors.length > 0) {
          setState({
            ...state,
            error: true,
            message: res.Errors[0]
          })
          setLoader(false);
        }
        else {
          setState({
            ...state,
            error: false,
          })
          // Stay Logged
          if (state.stayLogged === undefined || state.stayLogged === false) {
            localStorage.setItem('stayLogged', false);
          }
          else {
            localStorage.setItem('stayLogged', true);
          }
          setLoader(false);
          // Push to Treadmarks
          history.push('/trademarks');
        }
      });
    }
  }

  const handleLoginEnter = (event) => {
    if (event.keyCode === 13) {
      handleLogin();
    }

    else {
      return null
    }
  }

  return (
    <AccessLayout>
      {loader ? <FallbackLoader allowloader={true} /> : ''}
      <Grid className={classes.AccessBox} container>
        <Grid item xs={12} className={classes.Logo}>
          <img src={LogoImage} alt="" />
          <Typography variant="h2" gutterBottom><strong>Login</strong></Typography>
          <Typography variant="subtitle2" gutterBottom>Welcome to TM Manager, Enter your email and password below to access the system.</Typography>
        </Grid>
        <Grid item xs={12} className={classes.Access}>
          <div>
            <TextField
              id="email"
              label="Email"
              variant="outlined"
              placeholder=""
              value={state.email || ""}
              onChange={(event) => handleOnChange(event, 'email')}
              onKeyUp={(event) => handleLoginEnter(event)}
              error={state?.error || false}
              helperText={(state.email === undefined || state.email === "") && state.error === true ? "Email is required" : (state.email !== undefined && state.error === true && (state.email.lastIndexOf('.') < 0 || state.email.lastIndexOf('@') < 0)) ? "Enter correct email" : ""}
              fullWidth
              required
            />
          </div>
          <div>
            <TextField
              id="password"
              label="Password"
              type="password"
              variant="outlined"
              placeholder=""
              value={state.password || ""}
              onChange={(event) => handleOnChange(event, 'password')}
              onKeyUp={(event) => handleLoginEnter(event)}
              error={state?.error || false}
              helperText={(state.password === undefined || state.password === "") && state.error === true ? "Password is required" : ""}
              fullWidth
              required
            />
          </div>
        </Grid>
        <Grid item xs={12} className={classes.Action}>
          <Button onClick={() => handleLogin()} variant="contained" color="primary" fullWidth disableElevation size="large">Login</Button>
          <Typography variant="body1" data-error gutterBottom>{state?.message || ""}</Typography>
          <FormControlLabel onChange={(event) => handleStayLogged(event)} control={<Checkbox checked={state.stayLogged !== undefined ? state.stayLogged : false} color="primary" icon={<CheckCircleIcon style={{ color: '#000000' }} />} checkedIcon={<CheckCircleIcon />} name="staylogin" />} label="Stay Logged In?" />
          <Link to="/forgotpassword" style={{ margin: '35px 0' }}>Forget Your Password</Link>
          <Typography variant="subtitle2">Don't have an account?</Typography>
          <Link to="/register">Register</Link>
        </Grid>
      </Grid>
    </AccessLayout >
  );
}