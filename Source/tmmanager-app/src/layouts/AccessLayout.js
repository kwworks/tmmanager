import React from "react";
import {makeStyles} from '@material-ui/core'
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        width: '100vw',
        margin: 'auto',
        backgroundColor: theme.palette.light.main,
        overflowX: 'hidden',
    },
}));

export default function AccessLayout(props) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs>
                {props.children}
                </Grid>
            </Grid>
        </div>
    );
}

