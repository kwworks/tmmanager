import React from "react";
import { makeStyles, Typography } from '@material-ui/core'
import { useHistory } from "react-router-dom";
import AuthService from "../services/authService";
import Grid from '@material-ui/core/Grid';
import Footer from "../components/Footer";
import LogoImage from "../assets/TManager-Logo.jpg"
import LogOutImage from "../assets/LogoutIco.svg"

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        width: '100vw',
        margin: 'auto',
        backgroundColor: theme.palette.light.main,
        overflow: 'hidden',
    },
    Header:{
        justifyContent: 'space-around',
        alignItems: 'flex-end',
        padding: theme.spacing(1),
        marginBottom: theme.spacing(1),
        '& div:nth-child(1)':{
            display: 'flex',
            alignItems: 'center',
        },
        '& div:nth-child(2)':{
            display: 'flex',
            justifyContent: 'flex-end',
            position: 'relative',
            '& img + p':{
                position: 'absolute',
                bottom: -4,
            }
        }
    },
    Content:{
        height: 'calc(100vh - 175px)',
        maxWidth: 1024,
        margin: '0 auto',
        overflowY: 'auto',
        padding: theme.spacing(0, 1.5, 1.5),
    }
}));

export default function BasicLayout(props) {
    const classes = useStyles();
    const history = useHistory();

    const LogOut = () => {
        AuthService.signOut();
        history.push('/')
    };
    return (
        <div className={classes.root}>
            <Grid container className={classes.Header}>
               <Grid item xs={8} sm={11}>
                    <img src={LogoImage} style={{maxHeight:100}} alt="TM Manager"/>
               </Grid>
               <Grid item xs={4} sm={1} style={{position:'relative',top:-3,cursor:'pointer'}} onClick={() => LogOut()}>
                    <img src={LogOutImage} alt="Logout"/>
                    <Typography variant="body1" >Logout</Typography>
               </Grid>
            </Grid>
            <Grid container className={classes.Content}>
                <Grid item xs>
                {props.children}
                </Grid>
            </Grid>
            
            <Footer/>
        </div>
    );
}

