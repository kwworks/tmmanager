import AuthService from "./AuthService";
import CommonService from "../services/CommonService";
const ApiService = {
    ExecutePost(request) {
        return fetch(CommonService.serverUrl()+'/api/'+request.Url + request.Name, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': AuthService.getAuthToken()
            },
            body: JSON.stringify({ Data: JSON.stringify(request) })
        }).then(res => res.json())
            .catch((res) => {
                if (res.message === "Unexpected Token < in JSON at position 0") {
                    window.location.href = "/access/signup";
                }
        });
    },
    ExecuteGet(request) {
        return fetch(CommonService.serverUrl()+'/api/'+request.Url + request.Name, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ Data: JSON.stringify(request) })
        }).then(res => res.json())
            .catch((res) => {
                if (res.message === "Unexpected Token < in JSON at position 0") {
                    window.location.href = "/access/signup";
                }
            });
    }
};

export default ApiService;