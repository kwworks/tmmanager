import CommonService from "./CommonService";
const STORAGE_KEY = "__UserData";
const MESSAGE_KEY = "__MESSAGECOUNT";
const Country_Key = "__Country"
let UserData;
const AuthService = {
    getUserData() {
        if (UserData) {
            return UserData || {};
        } else {
            var data = JSON.parse(localStorage.getItem(STORAGE_KEY) || "{}");
            UserData = data;
            return data;
        }
    },
    setUserData(data) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
    },
    IsAuthenticated() {
        return this.getUserData().Token !== undefined;
    },
    getUserName() {
        return this.getUserData().UserName;
    },
    getClientId() {
        return this.getUserData().ClientId;
    },
    getUserId() {
        return this.getUserData().UserId;
    },
    getStreet() {
        return this.getUserData().Street + this.getUserData().Street2;
    },
    getArea() {
        return this.getUserData().Area;
    },
    getCity() {
        return this.getUserData().City;
    },
    getPostalCode() {
        return this.getUserData().PostalCode;
    },
    getUserType() {
        return this.getUserData().Type;
    },
    getEmail() {
        return this.getUserData().Email;
    },
    getFirstName() {
        return this.getUserData().FirstName;
    },
    getFirstLetter() {
        return this.getUserData().FirstName[0];
    },
    getFullName() {
        var UserData = this.getUserData();
        return UserData.FirstName + " " + UserData.LastName;
    },
    getLastName() {
        return this.getUserData().LastName;
    },
    getPhone() {
        return this.getUserData().Phone;
    },
    getClientName() {
        return this.getUserData().ClientName;
    },
    getEmailConfirmed() {
        return this.getUserData().Emailconfirmed;
    },
    getAuthToken() {
        return "Bearer " + this.getUserData().Access_Token;
    },
    signIn(username, password, success, fail, callback) {
        this.signOut()
        fetch(CommonService.serverUrl() + 'api/authenticate/oauth', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ UserName: username, password: password })
        }).then(res => res.json())
            .then(res => {
                if (res.Errors.length > 0) {
                    if (callback) callback(res);

                } else {

                    var localStorageData = res.UserData;
                    Object.assign(localStorageData, { IsAuthenticated: res.IsAuthenticated });
                    Object.assign(localStorageData, { Token: res.Token });
                    localStorage.setItem(STORAGE_KEY, JSON.stringify(localStorageData));
                    if (callback) callback(res);
                    return res;
                }
            })
            .catch(res => {
                return res;
            });
    },
    signOut() {
        UserData = null;
        localStorage.removeItem(STORAGE_KEY);
        localStorage.removeItem(MESSAGE_KEY);
    },
    syncAllCountries(callback) {
        var data = JSON.parse(localStorage.getItem(Country_Key) || "{}");
        if (localStorage.getItem(Country_Key) != null) {
            callback(data);
        } else {
            CommonService.ExecuteGet({ Url: "getallcountries" }, (res) => {
                if (res.length > 0) {
                    localStorage.setItem(Country_Key, JSON.stringify(res));
                    if (callback)
                        callback(res);
                }
            });
        }
    },
    getAllCountries() {
        return JSON.parse(localStorage.getItem(Country_Key) || "{}");
    },
    getCountryName(name) {
        if(name !== null && name !== undefined && name !== '' ){
            switch (name.toLowerCase()) {
                case "us":
                case "usa":
                case "ny":
                case "united states of america":
                    return "UNITED STATES"
                case "uk":
                    return "UNITED KINGDOM"
                default:
                    return name.toUpperCase();
            }
        }
        else {
            return 'UNITED STATES'
        }
        
    }
};
export default AuthService;