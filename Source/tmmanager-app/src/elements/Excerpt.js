import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

export default function Excerpt(props) {
    const [state, setState] = React.useState(false);
    const ToggleSummary = () => {
        setState(!state)
    }
    return (
    <Typography variant="subtitle2">{state === false ? props?.value.slice(0, 200) : props?.value.slice(0, props?.value.length)} <span style={{display: props.value.length < 200 ? "none" : "", cursor: props.value !== "" ? 'pointer' : "",color: props.value !== "" ? '#842828' : ""}} onClick={() => ToggleSummary()}>{props.value === "" ? "N/A" : state === false ? <i> Showmore</i> : <i> Showless</i>}</span></Typography>
    );
}

Excerpt.propTypes = {
    value: PropTypes.string,
};