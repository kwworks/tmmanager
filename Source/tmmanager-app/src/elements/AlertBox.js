import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    DialogWrapper: {
        '&[data-overflow="false"]':{
            '& .MuiDialogContent-root': {
                overflowY: 'initial',
                [theme.breakpoints.down('xs')]:{
                    '& + .MuiDialogActions-root button':{
                        width: '100%',
                    }
                }
            },
        },
        '& .MuiDialog-paper': {
            flex: 1,
            border: '2px solid ' + theme.palette.primary.main,
            borderRadius: theme.spacing(2),
            margin: theme.spacing(1.5),
            maxWidth: 750,
        },
        '& .MuiDialogContent-root': {
            padding: theme.spacing(3, 4, 0),
            [theme.breakpoints.down('xs')]:{
                padding: theme.spacing(3, 2, 0),
            }
        },
        '& .MuiDialogActions-root': {
            padding: theme.spacing(4),
            justifyContent: 'space-between',
            '& button': {
                minWidth: 185,
            },
            [theme.breakpoints.down('xs')]:{
                padding: theme.spacing(1.5, 1.5, 3),
                flexDirection: 'column',
                '& button': {
                    margin: theme.spacing(2, 0),
                }
            }
        }
    }
}));

export default function AlertBox(props) {
    const classes = useStyles();
    return (
        <Dialog open={props.open} maxWidth="sm" aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" className={classes.DialogWrapper} data-overflow={props.disableOverflow === undefined || props.disableOverflow === true ? true : props.disableOverflow}>
            <DialogContent>
                {props.content}
            </DialogContent>
            <DialogActions style={{justifyContent: props.showCancelButton === false ? "center" : ""}}>
                <Button size="small" onClick={props.onClickProceed} className={props.proceedButtonClass !== undefined ? props.proceedButtonClass : 'success'} color="primary" variant="contained">
                    {props.proceedButtonText || "Proceed"}
                </Button>
                <Button size="small" onClick={props.onClickCancel} className={props.cancelButtonClass !== undefined ? props.cancelButtonClass : 'darked'} style={{display: props.showCancelButton === false ? "none" : ""}} color="primary" variant="contained">
                    {props.cancelButtonText || "Cancel"}
                </Button>
            </DialogActions>
        </Dialog>
    );
}


AlertBox.propTypes = {
    open: PropTypes.bool,
    content: PropTypes.node,
    disableOverflow: PropTypes.bool,
    showCancelButton: PropTypes.bool,
    cancelButtonText: PropTypes.string,
    cancelButtonClass: PropTypes.string,
    onClickCancel: PropTypes.func,
    proceedButtonText: PropTypes.string,
    proceedButtonClass: PropTypes.string,
    onClickProceed: PropTypes.func,
};