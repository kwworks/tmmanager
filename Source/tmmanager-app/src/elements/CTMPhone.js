import React from 'react';
import MaskInput from 'react-maskinput';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        '& input': {
            padding: theme.spacing(1.75),
            borderColor: 'rgba(0, 0, 0, 0.23)',
            borderRadius: theme.spacing(0.5),
            '&:focus': {
                borderStyle: 'solid',
                borderColor: theme.palette.primary.main,
                outline: 'none',
                '& + p': {
                    color: theme.palette.primary.main,
                }
            },
            '& + p': {
                position: 'absolute',
                top: -2,
                '& > span': {
                    color: '#FF0000'
                },
                '& + p': {
                    fontSize: 13,
                    position: 'absolute',
                    bottom: 5,
                }
            }
        }
    }
}));


export default function CTMPhone(props) {
    const classes = useStyles();
    const onFocus = (event) => {
        if (event.target.value === "(000) 000-0000") {
            event.target.value = ''
        }
    }
    return (
        <div className={classes.root + " MuiFormControl-root"}>
            <MaskInput
                alwaysShowMask
                mask={'(000) 000-0000'}
                size={14}
                showMask
                maskChar="0"
                type="tel"
                onChange={props.onchange}
                onFocus={(event) => onFocus(event)}
                value={props?.value !== undefined ? props.value : ''}
            />
            <Typography variant="body1">{props.label !== undefined ? props.label : <>Phone <span>*</span></>}</Typography>
            {(props.error === true && (props.value === "(000) 000-0000" || props.value === '')) ? <Typography variant="body1" data-error="true">Enter correct number</Typography> : ""}
        </div>
    );
}

CTMPhone.propTypes = {
    onchange: PropTypes.func,
    label: PropTypes.node,
    value: PropTypes.string,
    error: PropTypes.bool,
};