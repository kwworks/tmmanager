import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';
import AuthService from '../services/authService';

export default function ACCountry(props) {
    const [inputValue, setInputValue] = React.useState('');
    return (
        <Autocomplete
        value={props.value || ""}
        onChange={props.onchange}
        inputValue={inputValue}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue);
        }}
        id="country"
        options={AuthService.getAllCountries()}
        fullWidth
        renderInput={(params) => <TextField {...params} label="Country" variant="outlined" />}
      />
    );
}

ACCountry.propTypes = {
    onchange: PropTypes.func,
    label: PropTypes.node,
    value: PropTypes.string,
    error: PropTypes.bool,
};
