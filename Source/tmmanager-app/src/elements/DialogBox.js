import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    DialogWrapper: {
        '& .MuiDialog-paper': {
            flex: 1,
            border: '2px solid ' + theme.palette.primary.main,
            borderRadius: theme.spacing(2),
            margin: theme.spacing(1.5),
        },
        '& .MuiDialogTitle-root': {
            padding: theme.spacing(2, 2, 0),
            '& .MuiTypography-root': {
                color: theme.palette.primary.main,
                fontSize: 32,
                fontFamily: 'Adobe-Jenson-Pro',
                lineHeight: '1.3',
                maxWidth: 'calc(100% - 35px)',
            },
            '& .MuiIconButton-root': {
                position: 'absolute',
                right: 0,
                top: 10,
                '& svg':{
                    width: '1.5em',
                    height: '1.5em',
                },
                '&:hover':{
                    background: 'none',
                    '& svg':{
                        color: theme.palette.primary.main,
                    }
                }
            }
        },
        '& .MuiDialogContent-root': {
            padding: theme.spacing(0, 2, 2)
        },
        '& .MuiDialogActions-root': {
            padding: theme.spacing(2),
            justifyContent: 'space-between',
            '& button': {
                minWidth: 185,
            },
            [theme.breakpoints.down('xs')]:{
                padding: theme.spacing(0, 2, 3),
                flexDirection: 'column',
                '& button': {
                    margin: theme.spacing(2, 0),
                    '&:last-child':{
                        margin: 0,
                    }
                }
            },
            [theme.breakpoints.up('sm')]:{
                '& button': {
                    '&.MuiButton-fullWidth':{
                        width: 'auto',
                    },
                    '&:last-child':{
                        marginLeft: 0,
                    },
                }
            }
        }
    }
}));

export default function DialogBox(props) {
    const classes = useStyles();

    const handleDialogClose = () => {
        props.ModalClose();
    };

    return (
        <Dialog open={props.open} onClose={handleDialogClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" className={classes.DialogWrapper} >
            <DialogTitle id="alert-dialog-title">
                {props.title}
                <IconButton disableRipple={true} aria-label="close" className={classes.closeButton} onClick={() => handleDialogClose()}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                {props.content}
            </DialogContent>
            <DialogActions style={{ display: (props.showFooter && props.showFooter !== undefined) ? "" : "none" }}>
                <Button size="small" fullWidth={props.fullWidthActions !== undefined ? props.fullWidthActions : false} onClick={props.onClickProceed} className={props.proceedButtonClass !== undefined ? props.proceedButtonClass : 'success'} color="primary" variant="contained" style={{display:props.showProceed === false ? 'none' : ''}}>
                    {props.proceedButtonText || "Proceed"}
                </Button>
                <Button size="small" fullWidth={props.fullWidthActions !== undefined ? props.fullWidthActions : false} onClick={props.onClickCancel} className={props.cancelButtonClass !== undefined ? props.cancelButtonClass : 'darked'} color="primary" variant="contained" style={{display:props.showCancel === false ? 'none' : ''}}>
                    {props.cancelButtonText || "Cancel"}
                </Button>
            </DialogActions>
        </Dialog>
    );
}


DialogBox.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    content: PropTypes.node,
    showFooter: PropTypes.bool,
    fullWidthActions: PropTypes.bool,
    showCancel: PropTypes.bool,
    cancelButtonText: PropTypes.string,
    cancelButtonClass: PropTypes.string,
    onClickCancel: PropTypes.func,
    showProceed: PropTypes.bool,
    proceedButtonText: PropTypes.string,
    proceedButtonClass: PropTypes.string,
    onClickProceed: PropTypes.func,
};