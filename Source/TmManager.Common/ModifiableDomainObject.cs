﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TmManager
{
    public abstract class ModifiableDomainObject : DomainObject
    {
        public ModifiableDomainObject()
        {
            this.Modified = DateTimeHelper.Now();
        }
        [Required]
        public DateTime Modified { get; set; }
        [Required]
        [StringLength(128)]
        public string ModifiedBy { get; set; }
    }
}
