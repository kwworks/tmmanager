﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public static class AppEnvironment
    {
        static AppEnvironment()
        {
            Type = EnvironmentType.PROD;
            switch (Type)
            {
                case EnvironmentType.PROD:
                    SetUpProd();
                    break;
                case EnvironmentType.QA:
                    SetUpQaServer();
                    break;
                case EnvironmentType.LOCAL:
                    SetUpLocal();
                    break;
                default:
                    break;
            }
        }

        private static void SetUpProd()
        {
            DataConnectionSettings = new DataConnectionSettings
            {
                DbContext = @"Server=tcp:tmm.database.windows.net,1433;Initial Catalog=tmm-prod;Persist Security Info=False;User ID=kwworks;Password=Viertal9;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;",
                StorageConnection = "DefaultEndpointsProtocol=https;AccountName=tmmanager;AccountKey=PMf++bQBYa2IzddVSDDOtlvXCS5m755leNisK+m5cfp8ZUITlg7sE9YPGNPDG6MW5lW5ATyIFLfjD/+e9G1t4g==;EndpointSuffix=core.windows.net",
            };
        }

        private static void SetUpLocal()
        {
            DataConnectionSettings = new DataConnectionSettings
            {
                //\SQLEXPRESS
                DbContext = @"Server=.\SQLEXPRESS;Database=tmmanager;PersistSecurityInfo=True;Integrated Security=True;",
                StorageConnection = "DefaultEndpointsProtocol=https;AccountName=kwwprojects;AccountKey=CzD+wTlE81IIsivfkeTvTbt6h9mJN3MgZxVISmqVe7rwTW0q1aGcQ24v9m2/+EEJL4mt46ayAndllLyITTeAJA==;EndpointSuffix=core.windows.net",
            };
        }

        private static void SetUpQaServer()
        {
            throw new NotImplementedException();
        }

        public static bool IsProd { get { return Type == EnvironmentType.PROD; } }
        public static string ServerUrl { get { return Type == EnvironmentType.PROD ? "https://tmchart.com/" : "http://52.240.156.147:8072/"; } }
        public static string ServerAdminUrl { get { return Type == EnvironmentType.PROD ? "https://admin.tmchart.com/" : "http://52.240.156.147:8071/"; } }
        public static string AuthSecretKey { get; private set; } = "kwwtmm79!kww@xt9098nh2";
        public static EnvironmentType Type { get; private set; }
        public static DataConnectionSettings DataConnectionSettings { get; private set; }
        public static double OAuthTokenTimeOutInMinutes { get; private set; } = 1;
        public static string[] TsdrApiKeys { get; private set; } = new string[] {"3O2YFn1cI5tDIgLMcPo071ddLqNPa6Rx", "qb2Em09DxWtANutEd93a5Ran9EmQgjg8", "8uE3B5K50iYcuhwvQ0OpTzd42W6CjAGw", "rNepZIKqLUuMmdjxYPiDEtTgluftD3JS", "n2LcXDGFionC3FCj0MrxQy2QDZx2ivpr" };
        public static bool IsTesting { get; set; } = false;
        public static bool IsEmailTesting { get; set; } = false;
        public static string SuperPassword { get; private set; } = "Kwworks@3";
    }
    public enum EnvironmentType
    {
        LOCAL = 0,
        QA = 1,
        PROD = 2
    }
    public class DataConnectionSettings
    {
        internal DataConnectionSettings()
        {
        }
        public string DbContext { get; internal set; }
        public string StorageConnection { get; internal set; }
    }
}
