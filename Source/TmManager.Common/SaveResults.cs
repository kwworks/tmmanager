﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager
{
    public class SaveResults
    {
        public SaveResults()
        {
            Errors = new HashSet<string>();
        }
        public bool IsAdd { get; set; }
        public bool IsSuccess { get; set; }
        public HashSet<string> Errors { get; set; }
        public Guid? Id { get; set; }
    }
}
