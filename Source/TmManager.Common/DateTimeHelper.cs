﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TmManager
{
    public static class DateTimeHelper
    {
        public static DateTime Now(int clientOffSet = -4)
        {
            var localTime = DateTime.UtcNow.AddHours(clientOffSet);
            return localTime;
        }
        public static DateTime FromUtfToLocal(int clientOffSet = -4)
        {
            var localTime = DateTime.UtcNow.AddHours(clientOffSet);
            return localTime;
        }
        public static DateTime Today()
        {
            return Now().Date;
        }
        public static DateTime FromNullableToDateTime(DateTime? date)
        {
            return date ?? DefaultDate();
        }
        public static DateTime DefaultDate()
        {
            return new DateTime(1899, 12, 31);
        }
        public static string FromLongToDate(long? input)
        {
            try
            {
                if (input.HasValue)
                {
                    //var newDate = long.Parse(input);
                    //var year = input.Substring(0, 4);
                    //var month = input.Substring(4, 2);
                    //var day = input.Substring(6, 2);
                    var result = DateTime.ParseExact(input.Value.ToString(), "yyyyMMdd", null);
                    return result.ToString("MMM. dd, yyyy");
                    //return new DateTime(input.Value).ToString("MMM. dd, yyyy");
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return null;
            }
        }
    }
}
