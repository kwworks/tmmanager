﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public static class Constants
    {
        //Notifications
        public const string TMManagerSupport = "TM Manager Support";
        public const string TMManagerLinkedMessage = "There is new activity on your TM Manager account. Please login to the system for details.";
        public const string NewTrademarkLinkedMessage = "A new trademark has been added to your account: ";
        public const string Undef = "Undef";
        public const string RegistrationEmailSubject = "TM Chart – Registration Invitation";
        public const string NewActivitySubject = "TM Chart – New Account Activity";
        public const string LogoUrl = "https://admin.tmchart.com/images/logo.png";

        public static string GetTrademarkCurrentStatus(string key, bool isFiledInternaltion)
        {
            var list1A = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("D","Trademark is dead"),
                new KeyValuePair<string, string>("R","Trademark is registered"),
                new KeyValuePair<string, string>("I","Trademark is international"),
                new KeyValuePair<string, string>("1","Application Filed"),
                new KeyValuePair<string, string>("2","USPTO reviews application"),
                new KeyValuePair<string, string>("3a","USPTO approves and publishes trademark"),
                new KeyValuePair<string, string>("3b","USPTO issues letter (office action)"),
                new KeyValuePair<string, string>("4a","You submit timely and complete response "),
                new KeyValuePair<string, string>("4b","You do not respond and application abandons"),
                new KeyValuePair<string, string>("5a","USPTO approves and publishes trademark"),
                new KeyValuePair<string, string>("5b","USPTO issues final letter (office action)"),
                new KeyValuePair<string, string>("6a","You file an appeal and/or submit timely and complete response"),
                new KeyValuePair<string, string>("6b","You do not file appeal or fix issues and application abandons"),
                new KeyValuePair<string, string>("7a","USPTO approves and publishes trademark"),
                new KeyValuePair<string, string>("7b","TTAB processes your appeal"),
                new KeyValuePair<string, string>("8","USPTO registers your trademark"),
                new KeyValuePair<string, string>("9","You file Section 8 declaration"),
                new KeyValuePair<string, string>("10","You file Section 8 declaration and Section 9 renewal"),
            };
            var list1B = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("D","Trademark is dead"),
                new KeyValuePair<string, string>("R","Trademark is registered"),
                new KeyValuePair<string, string>("I","Trademark is international"),
                new KeyValuePair<string, string>("1","Application Filed"),
                new KeyValuePair<string, string>("2","USPTO reviews application"),
                new KeyValuePair<string, string>("3a","USPTO approves trademark and publishes it for opposition."),
                new KeyValuePair<string, string>("3b","USPTO issues letter (office action)"),
                new KeyValuePair<string, string>("4a","You submit timely and complete response "),
                new KeyValuePair<string, string>("4b","You do not respond and application abandons"),
                new KeyValuePair<string, string>("5a","USPTO approves and publishes trademark"),
                new KeyValuePair<string, string>("5b","USPTO issues final letter (office action)"),
                new KeyValuePair<string, string>("6a","You file an appeal and/or submit timely and complete response"),
                new KeyValuePair<string, string>("6b","You do not file appeal or fix issues and application abandons"),
                new KeyValuePair<string, string>("7a","USPTO approves and publishes trademark"),
                new KeyValuePair<string, string>("7b","TTAB processes your appeal"),
                new KeyValuePair<string, string>("8","USPTO issues Notice of Allowance "),
                new KeyValuePair<string, string>("9a","You file extension request(s)"),
                new KeyValuePair<string, string>("9b","You timely file Statement of Use (SOU)"),
                new KeyValuePair<string, string>("9c","You do not timely file SOU or extension request and application abandons"),
                new KeyValuePair<string, string>("10","You timely file SOU after requesting extensions"),
                new KeyValuePair<string, string>("11","USPTO reviews SOU "),
                new KeyValuePair<string, string>("12a","USPTO approves SOU and registers your trademark"),
                new KeyValuePair<string, string>("12b","USPTO issues letter (office action) "),
                new KeyValuePair<string, string>("13","You file Section 8 declaration"),
                new KeyValuePair<string, string>("14","You file Section 8 declaration and Section 9 renewal"),
            };
            if (isFiledInternaltion)
                return list1B.Where(x => x.Key == key).Select(x => x.Value).FirstOrDefault();
            else
                return list1A.Where(x => x.Key == key).Select(x => x.Value).FirstOrDefault();
        }
    }
}
