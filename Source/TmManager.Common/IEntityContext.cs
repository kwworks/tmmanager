﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TmManager.Data
{
    public interface IEntityContext : IDisposable
    {
        IQueryable<T> Select<T>(Expression<Func<T, bool>> expression) where T : DomainObject;
        void Insert<T>(T entity) where T : DomainObject;
        void Update<T>(T entity) where T : DomainObject;
        void Delete<T>(T entity) where T : DomainObject;
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
