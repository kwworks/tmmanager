﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Storage
{
    public class BlobStorage : IStorage
    {
        private readonly CloudStorageAccount StorageAccount;
        private readonly CloudBlobClient BlobClient;
        public const string AttachmentStorageContainer = "emailattachments";
        public const string containerName = "tmmanager";
        public const int StorageAccessExpiryInMins = 1440;
        public BlobStorage()
        {
            StorageAccount = CloudStorageAccount.Parse(AppEnvironment.DataConnectionSettings.StorageConnection);
            BlobClient = StorageAccount.CreateCloudBlobClient();
        }
        public bool CopyImage(string container, string fileName, string name)
        {
            throw new NotImplementedException();
        }
        public async Task<bool> CopyImageAsync(string container, string fileName, string name)
        {
            var storageContainer =await GetContainer(container.ToLowerInvariant());
            CloudBlockBlob blockBlob = storageContainer.GetBlockBlobReference(name);
            CloudBlockBlob blockcopyBlob = storageContainer.GetBlockBlobReference(fileName);
            if (await blockcopyBlob.ExistsAsync())
            {
                await blockBlob.StartCopyAsync(blockcopyBlob);
                return true;
            }
            else return false;
        }

        public bool Delete(string container, string name)
        {
            throw new NotImplementedException();
        }

        public async Task<Stream> GetAsync(string container, string name)
        {
            var storageContainer =await GetContainer(container.ToLowerInvariant());
            CloudBlockBlob blockBlob = storageContainer.GetBlockBlobReference(name);
            if (await blockBlob.ExistsAsync())
            {
                var memoryStream = new MemoryStream();
                await blockBlob.DownloadToStreamAsync(memoryStream);
                memoryStream.Position = 0;
                return memoryStream;
            }
            else return null;
        }
        public async Task<string> GetAsTextAsync(string container, string name)
        {
            var storageContainer =await GetContainer(container);
            CloudBlockBlob blob = storageContainer.GetBlockBlobReference(name);
            return await blob.DownloadTextAsync();
        }

        public void Save(string container, string name, Stream stream, IDictionary<string, string> metaData = null, string fileType = null)
        {
            throw new NotImplementedException();
        }

        public async Task SaveAsTextAsync(string container, string name, string content)
        {
            var storageContainer =await GetContainer(container);
            CloudBlockBlob blob = storageContainer.GetBlockBlobReference(name);
            await blob.UploadTextAsync(content);
        }

        public async Task SaveAsync(string container, string name, Stream stream, string fileType = null)
        {
            var storageContainer = await GetContainer(container);
            CloudBlockBlob blob =  storageContainer.GetBlockBlobReference(name);
            await blob.UploadFromStreamAsync(stream);

            if (!string.IsNullOrWhiteSpace(fileType))
            {
                blob.Properties.ContentType = fileType;
                await blob.SetPropertiesAsync();
            }
        }
        public async Task<CloudBlobContainer> GetContainer(string name)
        {
            CloudBlobContainer container = BlobClient.GetContainerReference(name);
           var result = await container.CreateIfNotExistsAsync();
            return container;
        }
    }
}