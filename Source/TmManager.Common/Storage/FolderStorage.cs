﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Storage
{
    public class FolderStorage : IStorage
    {
        private string StoragePath;
        private IHostingEnvironment _env;
        public FolderStorage(IHostingEnvironment env)
        {
            StoragePath = @"clientapp/build";
            _env = env;
        }
        public bool Delete(string container, string name)
        {
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (File.Exists(Path.Combine(directory, name)))
            {
                File.Delete(Path.Combine(directory, name));
                return true;
            }
            else
            {
                return false;
            }
        }
        public Stream Get(string container, string name)
        {
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            var path = StoragePath;

            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (File.Exists(Path.Combine(directory, name)))
            {
                var streamReader = new MemoryStream(File.ReadAllBytes(Path.Combine(directory, name)));
                return streamReader;
            }
            else return null;
        }
        public void Save(string container, string name, Stream stream, IDictionary<string, string> metaData = null, string fileType = null)
        {
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }

            try
            {
                //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
                var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                using (var fileStream = File.Create(Path.Combine(directory, name)))
                {
                    var bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, bytes.Length);
                    fileStream.Write(bytes, 0, bytes.Length);
                }
            }
            catch
            {

            }
        }
        public void Save(string container, string name, Stream stream, string fileType = null)
        {
            var path = StoragePath;
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            using (var fileStream = File.Create(Path.Combine(directory, name)))
            {
                var bytes = new byte[stream.Length];
                stream.Read(bytes, 0, bytes.Length);
                fileStream.Write(bytes, 0, bytes.Length);
            }
        }
        public async System.Threading.Tasks.Task SaveAsync(string container, string name, Stream stream)
        {
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            if (File.Exists(Path.Combine(directory, name)))
            {
                File.Delete(Path.Combine(directory, name));
            }
            using (var fileStream = File.Create(Path.Combine(directory, name)))
            {
                var bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, bytes.Length);
                await fileStream.WriteAsync(bytes, 0, bytes.Length);
            }
        }
        public void SaveAsText(string container, string name, string content)
        {
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            File.WriteAllText(Path.Combine(directory, name), content);
        }
        public string GetAsText(string container, string name)
        {
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            //var directory = string.Format(@"{0}{1}", HttpContext.Current.Server.MapPath("~"), path);
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (File.Exists(Path.Combine(directory, name)))
            {
                return File.ReadAllText(Path.Combine(directory, name));
            }
            else return null;
        }
        public bool CopyImage(string container, string fileName, string name)
        {
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (File.Exists(Path.Combine(directory, fileName)))
            {
                if (File.Exists(Path.Combine(directory, name)))
                {
                    File.Delete(Path.Combine(directory, name));
                }
                File.Copy(Path.Combine(directory, fileName), Path.Combine(directory, name));
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<Stream> GetAsync(string container, string name)
        {
            await Task.Run(() =>
            {
                return Get(container, name);
            });
            return null;
        }
        public async Task SaveAsync(string container, string name, Stream stream, string fileType)
        {
            if (_env.IsDevelopment())
            {
                StoragePath = @"clientapp/public";
            }
            var path = StoragePath;
            if (!string.IsNullOrEmpty(container))
            {
                path = string.Format(@"{0}\{1}", StoragePath, container);
            }
            var directory = string.Format(@"{0}\{1}", _env.ContentRootPath, path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            if (File.Exists(Path.Combine(directory, name)))
            {
                File.Delete(Path.Combine(directory, name));
            }
            using (var fileStream = File.Create(Path.Combine(directory, name)))
            {
                var bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, bytes.Length);
                await fileStream.WriteAsync(bytes, 0, bytes.Length);
            }
        }
    }
}
