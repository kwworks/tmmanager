﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Storage
{
    public interface IStorage
    {
        bool Delete(string container, string name);
        Task<Stream> GetAsync(string container, string name);
        void Save(string container, string name, Stream stream, IDictionary<string, string> metaData = null, string fileType = null);
        //Task SaveAsync(string container, string name, Stream stream);
        //Task SaveAsync(string container, string name, Stream stream, string fileType = null);
        Task SaveAsync(string container, string name, Stream stream, string fileType);
        Task SaveAsTextAsync(string container, string name, string content);
        Task<string> GetAsTextAsync(string container, string name);
        bool CopyImage(string container, string fileName, string name);
    }
}
