﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Messaging
{
    public static class EmailSender
    {
        #region SendEmailSmtpAsync
        //public static async Task SendEmailSmtpAsync(string fromAddress, string toAddress, string subject, string body)
        //{
        //    MimeMessage message = new MimeMessage();
        //    MailboxAddress from = new MailboxAddress(fromAddress);
        //    message.From.Add(from);
        //    MailboxAddress to = new MailboxAddress(toAddress);
        //    message.To.Add(to);
        //    message.Subject = subject;
        //    BodyBuilder bodyBuilder = new BodyBuilder
        //    {
        //        HtmlBody = body
        //    };
        //    message.Body = bodyBuilder.ToMessageBody();
        //    SmtpClient client = new SmtpClient();
        //    client.Connect("smtp.sendgrid.net", 587, true);
        //    client.Authenticate("azure_ca0d297c52503579bb5133c3015f50c3@azure.com", "5cz1vkmi");
        //    await client.SendAsync(message);
        //    client.Disconnect(true);
        //    client.Dispose();
        //}
        //public static void SendEmailSmtp(string fromAddress, string toAddress, string subject, string body)
        //{
        //    MimeMessage message = new MimeMessage();
        //    MailboxAddress from = new MailboxAddress(fromAddress);
        //    message.From.Add(from);
        //    MailboxAddress to = new MailboxAddress(toAddress);
        //    message.To.Add(to);
        //    message.Subject = subject;
        //    BodyBuilder bodyBuilder = new BodyBuilder
        //    {
        //        HtmlBody = body
        //    };
        //    message.Body = bodyBuilder.ToMessageBody();
        //    SmtpClient client = new SmtpClient();
        //    client.Connect("smtp.sendgrid.net", 587, true);
        //    client.Authenticate("azure_ca0d297c52503579bb5133c3015f50c3@azure.com", "5cz1vkmi");
        //    client.SendAsync(message);
        //    client.Disconnect(true);
        //    client.Dispose();
        //}
        #endregion

        public static async Task<bool> SendEmail(string To, string Subject, string HtmlBody, string CC, string BCC)
        {
            
            if (AppEnvironment.IsEmailTesting)
            {
                To = "kirk@kwworks.com";
                CC = "araza@kwworks.com";
            }
            using (var http = new HttpClient())
            {
                var triggerUrl = "https://prod-21.northcentralus.logic.azure.com:443/workflows/9964e07bdb4a410182c8d08bcf8c4c57/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=5kocWDfsp6DBHm9Gi4JAxvwCf987bsu3mhT0T9hODjM";
                var json = new
                {
                    from = "noreply@tmchart.com",
                    to = To,
                    subject = Subject,
                    html = HtmlBody,
                    cc = string.IsNullOrEmpty(CC) ? null : CC,
                    bcc = string.IsNullOrEmpty(BCC) ? null : BCC,
                };
                var jsonSerialized = JsonConvert.SerializeObject(json);
                var request = new HttpRequestMessage(HttpMethod.Post, triggerUrl)
                {
                    Content = new StringContent(jsonSerialized, Encoding.UTF8, "application/json")
                };
                var response = await http.SendAsync(request);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
        }
    }
}
