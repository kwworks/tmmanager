﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public static class Extensions
    {
        public static string ToLogString(this Exception ex)
        {
            var stringBuilder = new StringBuilder();
            for (Exception i = ex; i != null; i = i.InnerException)
            {
                stringBuilder.AppendLine(i.Message);
                stringBuilder.AppendLine(i.StackTrace);
                stringBuilder.AppendLine();
            }
            return stringBuilder.ToString();
        }
    }
}
