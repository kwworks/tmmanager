﻿using System.ComponentModel.DataAnnotations;

namespace TmManager
{
    public abstract class EventAuditBase : DomainObject
    {
        [StringLength(500)]
        public string Description { get; set; }
        [Required]
        [StringLength(70)]
        public string Type { get; set; }
    }
}
