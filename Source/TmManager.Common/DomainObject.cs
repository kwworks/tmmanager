﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TmManager
{
    public abstract class DomainObject
    {
        public DomainObject()
        {
            this.Created = DateTimeHelper.Now();
            this.IsActive = true;
            this.Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        [StringLength(128)]
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
