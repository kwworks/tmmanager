﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace TmManager
{
    public abstract class Specification<T> where T : class
    {
        #region Specification
        protected Specification(Expression<Func<T, bool>> predicate)
        {
            Predicate = predicate ?? throw new ArgumentNullException("predicate");
        }
        #endregion

        #region IsSatisfiedBy
        protected bool IsSatisfiedBy(T entity)
        {
            return Predicate.Compile().Invoke(entity);
        }
        #endregion

        #region Predicate
        public Expression<Func<T, bool>> Predicate { get; private set; }
        #endregion
    }
}
