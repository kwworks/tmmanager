﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IEntityContext _entityContext;
        public ReportController(IEntityContext entityContext)
        {
            _entityContext = entityContext;
        }
        [Route("searchtrademarks")]
        [HttpPost]
        [Produces("application/json")]
        public async Task<ReportResponse> SearchTrademarks([DataSourceRequest] DataSourceRequest request)
        {
            var apiRequest = new TrademarkSearchRequest()
            {
                Skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize,
                Take = request.PageSize,
                SortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty,
                SortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty
            };
            var report = new TrademarkSearchReport(_entityContext);
            var response = await report.ExecuteReportAsync(apiRequest);
            return response;
        }
    }
}
