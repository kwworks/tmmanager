﻿using Kendo.Mvc;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Admin;
using TmManager.Clients;
using TmManager.Data;
using TmManager.Identity;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IEntityContext _entityContext;
        private readonly UserManager _userManager;
        public ClientController(IEntityContext entityContext, UserManager userManager)
        {
            _entityContext = entityContext;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SearchClient([DataSourceRequest] DataSourceRequest request)
        {
            string clientEmail = string.Empty;
            string clientName = string.Empty;
            string primaryName = string.Empty;
            long? serialNumber = 0;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref serialNumber, ref clientEmail, ref clientName, ref primaryName);
            var report = new SearchClientsReport(_entityContext);
            var reportRequest = new SearchClientsRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                SerialNumber = serialNumber.Value,
                ClientName = clientName,
                ClientEmail = clientEmail,
                PrimaryName = primaryName
            };
            var reportResponse = (SearchClientsResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult UserAccess()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SearchAccountRequestsList([DataSourceRequest] DataSourceRequest request)
        {
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            var report = new ClientRequestAccessReport(_entityContext);
            var reportRequest = new ClientRequestAccessRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (ClientRequestAccessResponse)await report.ExecuteReportAsync(reportRequest);
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult ClientList()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetClientList([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string userName = string.Empty;
            string name = string.Empty;

            string sortDirection = request.Sorts != null && request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts != null && request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate, ref userName, ref name);
            var report = new SearchClientListReport(_userManager);
            var reportRequest = new SearchClientListRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate,
                Username = userName,
                Name = name
            };
            var reportResponse = (SearchClientListResponse)await report.ExecuteReportAsync(reportRequest);
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }

        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref DateTime? start, ref DateTime? end, ref string username, ref string name)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "StartDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedStartDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedStartDate);
                        start = parsedStartDate;
                    }
                    if (descriptor != null && descriptor.Member == "EndDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedEndDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedEndDate);
                        end = parsedEndDate;
                    }
                    if (descriptor != null && descriptor.Member == "Username" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        username = descriptor.Value.ToString();
                    }
                    if (descriptor != null && descriptor.Member == "Name" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        name = descriptor.Value.ToString();
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref start, ref end, ref username, ref name);
                    }
                }
            }
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref long? serialnumber, ref string clientEmail, ref string clientName, ref string primaryName)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "SerialNumber" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        serialnumber = int.Parse(descriptor.Value.ToString());
                    }
                    if (descriptor != null && descriptor.Member == "ClientEmail" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        clientEmail = descriptor.Value.ToString();
                    }
                    if (descriptor != null && descriptor.Member == "ClientName" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        clientName = descriptor.Value.ToString();
                    }
                    if (descriptor != null && descriptor.Member == "PrimaryName" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        primaryName = descriptor.Value.ToString();
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref serialnumber, ref clientEmail, ref clientName,ref primaryName);
                    }
                }
            }
        }
    }
}
