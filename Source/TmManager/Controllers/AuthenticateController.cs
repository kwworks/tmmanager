﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Identity;

namespace TmManager.Controllers
{
    [Route("api/authenticate")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly IUserService _userService;
        public AuthenticateController(IUserService userService)
        {
            _userService = userService;
        }
        [AllowAnonymous]
        [HttpPost("oauth")]
        public async Task<IActionResult> OAuth([FromBody] Models.AuthenticateModel model)
        {
            var results = await _userService.AuthenticateByOAuthAsync(model.UserName, model.Password);
            if (results.IsAuthenticated)
            {
                return Ok(results);
            }
            else
            {
                return BadRequest(results);
            }
        }
        [Authorize]
        [HttpGet("changepassword")]
        public async Task<IActionResult> ChangePassword(string userName, string password)
        {
            var results = await _userService.ChangePasswordByUserNameAsync(userName, password);

            if (results.IsSuccess)
            {


                return Ok(results);
            }
            else
            {
                return BadRequest(new { results.Errors });
            }
        }
        [AllowAnonymous]
        [HttpGet("forgotpassword")]
        public async Task<IActionResult> ForgotPassword(string userName, string password)
        {
            var results = await _userService.ChangePasswordByUserNameAsync(userName, password);

            if (results.IsSuccess)
            {


                return Ok(results);
            }
            else
            {
                return BadRequest(new { results.Errors });
            }
        }
    }
}
