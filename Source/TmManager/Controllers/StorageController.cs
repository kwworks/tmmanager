﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Storage;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    [Route("storage")]
    public class StorageController : Controller
    {
        private IStorage _storage;
        public StorageController(IStorage storage)
        {
            _storage = storage;
        }

        [AllowAnonymous]
        [HttpPost("uploadfile"), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile([FromForm] IFormFile file)
        {
            var result = new UploadFileModel();
            try
            {
                if (file == null || file.Length == 0)
                {
                    result.Errors.Add("Invalid file");
                    result.IsSuccess = false;
                }
                var name = file.FileName;
                if (name.Contains(".csv") == false)
                    name = name + ".csv";

                if (!result.Errors.Any())
                {
                    await _storage.SaveAsync(BlobStorage.containerName, name, file.OpenReadStream(), file.ContentType);
                    result.FileName = name;
                    result.IsSuccess = true;
                    //return RedirectToAction("UploadFileSuccess", "Trademark", new { fileName = name });
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.Message);
                result.IsSuccess = false;
            }
            return Json(result);
        }
    }
}
