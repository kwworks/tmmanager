﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Admin;
using TmManager.Attorney;
using TmManager.Attorneys;
using TmManager.Data;
using TmManager.Identity;

namespace TmManager.Controllers
{
    [Authorize]
    public class ConfigureController : Controller
    {
        private readonly IEntityContext _entityContext;
        private readonly IRecognizedAttorneyService _recognizedAttorneyService;
        public ConfigureController(IEntityContext entityContext, IRecognizedAttorneyService recognizedAttorneyService)
        {
            _entityContext = entityContext;
            _recognizedAttorneyService = recognizedAttorneyService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AttorneyOfRecord()
        {
            return View();
        }
        public async Task<IActionResult> GetRecognizedAttorneys([DataSourceRequest] DataSourceRequest request)
        {
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            var report = new SearchRecognizedAttorneyReport(_entityContext);
            var reportRequest = new SearchRecognizedAttorneyRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (SearchRecognizedAttorneyResponse)await report.ExecuteReportAsync(reportRequest);
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public async Task<ActionResult> GetRecognizedAttorney(string email, string name)
        {
            var results = await _recognizedAttorneyService.GetByRecognizedAttorneyEmail(email, name);
            return Json(results);
        }
        public async Task<ActionResult> DeleteRecognizedAttorney(Guid id)
        {
            var results = await _recognizedAttorneyService.DeleteRecognizedAttorney(id);
            return Json(results);
        }
        [HttpPost]
        public async Task<ActionResult> SaveRecognizedAttorney(RecognizedAttorneyRequestModel model)
        {
            var results = new SaveResults();
            if (ModelState.IsValid)
            {
                if (Helper.IsValidEmail(model.Email) == false)
                    results.Errors.Add("Invalid email format");
                else
                    results = await _recognizedAttorneyService.SaveRecognizedAttorney(model);
            }
            else
            {
                results.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
                results.IsSuccess = false;
            }
            return Json(results);
        }
        [HttpPost]
        public async Task<ActionResult> SaveAdmin(AdminUserModel model)
        {
            var results = new SaveResults();
            if (ModelState.IsValid)
            {
                results = await _recognizedAttorneyService.SaveAdmin(model);
            }
            else
            {
                results.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
                results.IsSuccess = false;
            }
            return Json(results);
        }
    }
}
