﻿using Hangfire;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Storage;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    [Authorize]
    public class TrademarkController : Controller
    {
        private readonly ITrademarkService _trademarkService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEntityContext _entityContext;
        private IStorage _storage;
        public TrademarkController(ITrademarkService trademarkService, IHttpContextAccessor httpContextAccessor, IEntityContext entityContext, IStorage storage)
        {
            _trademarkService = trademarkService;
            _httpContextAccessor = httpContextAccessor;
            _entityContext = entityContext;
            _storage = storage;
        }
        public ActionResult Index()
        {
            //var result = await _trademarkService.GetTrademarks();
            //return View(result);
            return View();
        }
        public async Task<ActionResult> GetTrademarks()
        {
            var result = await _trademarkService.GetTrademarks();
            return Json(result);
        }
        [HttpPost]
        public IActionResult GetTrademarksForDataTable()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var result = _trademarkService.GetTrademarksForDataTable(pageSize, skip, sortColumn, sortColumnDirection);
            var jsonData = new { draw = draw, recordsFiltered = result.recordsTotal, recordsTotal = result.recordsTotal, data = result.data };
            return Json(jsonData);
        }
        [HttpGet]
        public async Task<ActionResult> GetTrademarkDetail(Guid id)
        {
            var result = await _trademarkService.GetTrademarkDetail(id);
            return Json(result);
        }
        [HttpGet]
        public async Task<ActionResult> GetClientDetail(string id)
        {
            var result = await _trademarkService.GetClientDetail(id);
            return Json(result);
        }
        [HttpGet]
        public IActionResult NewTrademark()
        {
            return View();
        }
        public IActionResult UploadFile()
        {
            return View();
        }
        public async Task<IActionResult> UploadFileSuccess(string FileName)
        {
            var result = new List<TrademarkImportItemsModel>();
            try
            {
                if (!string.IsNullOrEmpty(FileName))
                {
                    Stream stream = await GetFile(FileName);
                    StreamReader streamReader = new StreamReader(stream);
                    if (streamReader != null)
                    {
                        while (!streamReader.EndOfStream)
                        {
                            var line = await streamReader.ReadLineAsync();
                            var values = line.Split(',');

                            string email = values[0].Trim();
                            string serialNumber = values[1].Trim();
                            if (!string.IsNullOrEmpty(email))
                            {
                                result.Add(new TrademarkImportItemsModel()
                                {
                                    Email = email,
                                    SerialNumber = int.Parse(serialNumber)
                                });
                            }
                            else
                            {
                                return RedirectToAction("UploadFileError");
                            }
                        }
                        //_httpContextAccessor.HttpContext.Session.SetString("UploadFileSession", JsonConvert.SerializeObject(result));
                    }
                }
                TempData["fileName"] = FileName;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
                return RedirectToAction("UploadFileError");
            }
            return View(result);
        }
        public async Task<IActionResult> DuplicateCheck()
        {
            var fileName = string.Empty;
            if (TempData.ContainsKey("fileName"))
            {
                fileName = TempData["fileName"].ToString();
                TempData["fileName"] = fileName;
            }
            var model = new List<TrademarkImportItemsModel>();
            if (!string.IsNullOrEmpty(fileName))
            {
                model = await GetFileData(fileName);
            }
            if (!model.Any())
                RedirectToAction("UploadFile");
            //List<TrademarkImportItemsModel> response = JsonConvert.DeserializeObject<List<TrademarkImportItemsModel>>(jsonString);
            List<long> listTrademarks = model.Select(x => (long)x.SerialNumber).ToList();
            var result = await _trademarkService.GetDuplicateTrademarks(listTrademarks);
            if (result != null)
            {
                foreach (var item in result)
                {
                    var trademark = model.Where(x => x.SerialNumber == item.SerialNumber).FirstOrDefault();
                    trademark.Status = TrademarkStatus.TrademarkInSystem;
                    trademark.TrademarkId = item.Id;
                    trademark.IsDuplicate = true;
                }
            }
            return View(model);
        }
        public async Task<IActionResult> FetchTrademark()
        {
            var result = new TrademarkImportFileModel();
            var fileName = string.Empty;
            if (TempData.ContainsKey("fileName"))
                fileName = TempData["fileName"].ToString();
            if (string.IsNullOrEmpty(fileName))
                return RedirectToAction("UploadFile");
            List<TrademarkImportItemsModel> response = await GetFileData(fileName);
            int r = (new Random()).Next(1000, 9999);
            result.BatchName = fileName;
            result.TrademarkFileItems = response;
            if (result.TrademarkFileItems.Any())
            {
                var results = await _trademarkService.SaveFileRecords(result);
                foreach (var item in result.TrademarkFileItems)
                {
                    if (!item.IsDuplicate)
                    {
                        var jobId = BackgroundJob.Enqueue<ITrademarkService>(x => x.UpdateImportedFilesInfo(item.Id));
                    }
                }
                //var jobId = BackgroundJob.Enqueue<ITrademarkService>(x => x.SaveBatchTrademark(result));
            }
            return View(result);
        }
        [HttpPost]
        public async Task<ActionResult> NewTrademark(SearchTrademarkModel model)
        {
            var results = new TrademarkResponseModel();
            if (ModelState.IsValid)
            {
                results = await _trademarkService.GetTrademark(model.Email, model.SerialNumber.Value.ToString());
                if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkSuccess)
                    return View("TrademarkSuccess", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkDead)
                    return View("TrademarkDead", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkInSystem)
                    return View("TrademarkInSystem", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkNotFound)
                    return View("TrademarkNotFound", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkAttorneyIssue)
                    return View("TrademarkAttorneyIssue", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkEmailNotFound)
                    return View("TrademarkEmailNotFound", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkFailed)
                    return View("TrademarkFailed", results.TrademarkResponse.First());
                else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.SystemError)
                    return View("SystemError", results.TrademarkResponse.First());
            }
            else
            {
                results.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
                results.IsSuccess = false;
                return View(model);
            }
            return Json(results);
        }
        [HttpPost]
        public async Task<ActionResult> SaveTrademark(TrademarkResponse model)
        {
            var results = await _trademarkService.SaveTrademark(model.SerialNumber.ToString(), model.FallbackEmail);
            if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkLinked)
                    return View("TrademarkLinked", results.TrademarkResponse.First());
            else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkAccountCreated)
                    return View("TrademarkAccountCreated", results.TrademarkResponse.First());
            else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkInSystem)
                    return View("TrademarkInSystem", results.TrademarkResponse.First());
            else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.SystemError)
                    return View("SystemError", results.TrademarkResponse.First());
            else if (results.TrademarkResponse[0].Status == Trademarks.TrademarkStatus.TrademarkAttorneyIssue)
                    return View("TrademarkAttorneyIssueOnSave", results.TrademarkResponse.First());
            return Json(results);
        }
        [HttpPost]
        public async Task<JsonResult> UpdateTrademark(long serialNumber)
        {
            var result = new TrademarkResponse();
            var response = await _trademarkService.UpdateTrademark(serialNumber);
            if (response.IsSuccess)
            {
                result = await _trademarkService.GetTrademarkDetail(response.TrademarkResponse.FirstOrDefault().Id);
                return Json(result);
            }
            else
                return Json(response);
        }
        [HttpGet]
        public IActionResult TrademarkDeadList()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SearchTrademarkDeadList([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            var report = new TrademarkDeadReport(_entityContext);
            var reportRequest = new TrademarkDeadRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (TrademarkDeadResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [HttpPost]
        public async Task<ActionResult> SearchTrademarkUnknownAttorneyList([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            var report = new TrademarkUnknownAttorneyReport(_entityContext);
            var reportRequest = new TrademarkUnknownAttorneyRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (TrademarkUnknownAttorneyResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [HttpPost]
        public async Task<ActionResult> SearchTrademark([DataSourceRequest] DataSourceRequest request)
        {
            string clientEmail = string.Empty;
            string clientName = string.Empty;
            long? serialNumber = 0;
            string registrationNumber = string.Empty;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref serialNumber, ref clientEmail, ref clientName, ref registrationNumber);
            var report = new TrademarkSearchReport(_entityContext);
            var reportRequest = new TrademarkSearchRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                SerialNumber = serialNumber.Value,
                ClientName = clientName,
                ClientEmail = clientEmail,
                RegistrationNumber = registrationNumber
            };
            var reportResponse = (TrademarkSearchResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [HttpGet]
        public IActionResult UnknownAttorney()
        {
            return View();
        }
        public IActionResult UploadFileError()
        {
            return View();
        }
        //public async Task<IActionResult> TrademarkRetrieved()
        //{
        //    var result = await _trademarkService.GetTrademarkImportFilesGroupAsync();
        //    ViewBag.Trademarks = result;
        //    return View();
        //}
        public ActionResult TrademarkRetrieved()
        {
            var currentUser = _httpContextAccessor.HttpContext.User.Identity.Name;
            ViewBag.ShowColumn = currentUser == "webmaster@kwworks.com";
            return View();
        }
        public ActionResult TrademarkArchieved()
        {
            return View();
        }
        public ActionResult TrademarkRetrievedItem(Guid id, string batchName)
        {
            var result = new TrademarkResponseForKendo();
            var file = _entityContext.Select<TrademarkImportFile>(x => x.Id == id).FirstOrDefault();

            result.Id = id;
            result.Name = batchName;
            result.ImportedDateString = file != null ? file.Created.ToString("yyyy.MM.dd HH:mm:ss") : "N/A";
            result.Count = file != null ? file.TrademarkImportItems.Count() : 0;
            return View(result);
        }
        public ActionResult TrademarkArchivedItem(Guid id, string batchName)
        {
            var result = new TrademarkResponseForKendo();
            result.Id = id;
            result.Name = batchName;
            return View(result);
        }
        [HttpPost]
        public async Task<IActionResult> TrademarkRetrieved([DataSourceRequest] DataSourceRequest request)
        {
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;
            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            
            var report = new TrademarkRetrievedReport(_entityContext);
            var reportRequest = new TrademarkRetrievedRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (TrademarkRetrievedResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [HttpPost]
        public async Task<IActionResult> TrademarkArchieved([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;

            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;
            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            var report = new TrademarkArchievedReport(_entityContext);
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            var reportRequest = new TrademarkArchievedRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate
            };
            var reportResponse = (TrademarkArchievedResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [HttpPost]
        public async Task<IActionResult> TrademarkRetrievedItem([DataSourceRequest] DataSourceRequest request, Guid? id)
        {
            Guid? tempId = Guid.Empty;
            //var entityId = _httpContextAccessor.HttpContext.Request.QueryString;
            //var tempId = entityId.HasValue ? entityId.Value : string.Empty;
            //if (!string.IsNullOrEmpty(tempId))
            //    id = Guid.Parse(tempId);

            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;
            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref tempId);
            var report = new TrademarkRetrievedItemReport(_entityContext);
            var reportRequest = new TrademarkRetrievedItemRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                Id = id
            };
            var reportResponse = (TrademarkRetrievedItemResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [AllowAnonymous]
        public async Task<Stream> GetFile(string fileName)
        {
            try
            {
                return await _storage.GetAsync(BlobStorage.containerName, fileName);
            }
            catch
            {
                return null;
            }
        }
        public async Task<IActionResult> TrademarkReject(Guid id)
        {
            var results = new SaveResults();
            try
            {
                results = await _trademarkService.RejectFromBatch(id);
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
                results.IsSuccess = false;
            }
            
            return Ok(results);
        }
        public async Task<IActionResult> TrademarkAccept(Guid id)
        {
            var results = new SaveResults();
            try
            {
                await _trademarkService.AcceptFromBatch(id);
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            
            return Ok(results);
        }
        public IActionResult TryAgain(Guid id)
        {
            var results = new SaveResults();
            try
            {
                var ids =  _entityContext.Select<TrademarkImportItem>(x => x.TrademarkImportFileId == id && x.Status == TrademarkStatus.TrademarkNotFound).Select(x=> x.Id).ToList();
                //await _trademarkService.UpdateAllImportedFilesInfo(ids);
                var jobId = BackgroundJob.Enqueue<ITrademarkService>(x => x.UpdateAllImportedFilesInfo(ids));
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }

            return Ok(results);
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref Guid? id)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "Id" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        id = Guid.Parse(descriptor.Value.ToString());
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref id);
                    }
                }
            }
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref DateTime? start, ref DateTime? end)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "StartDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedStartDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedStartDate);
                        start = parsedStartDate;
                    }
                    if (descriptor != null && descriptor.Member == "EndDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedEndDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedEndDate);
                        end = parsedEndDate;
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref start, ref end);
                    }
                }
            }
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref long? serialnumber, ref string clientEmail, ref string clientName, ref string registrationNumber)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "SerialNumber" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        serialnumber = int.Parse(descriptor.Value.ToString());
                    }
                    if (descriptor != null && descriptor.Member == "ClientEmail" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        clientEmail = descriptor.Value.ToString();
                    }
                    if (descriptor != null && descriptor.Member == "ClientName" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        clientName = descriptor.Value.ToString();
                    }
                    if (descriptor != null && descriptor.Member == "RegistrationNumber" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        registrationNumber = descriptor.Value.ToString();
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref serialnumber, ref clientEmail, ref clientName, ref registrationNumber);
                    }
                }
            }
        }
        private async Task<List<TrademarkImportItemsModel>> GetFileData(string fileName)
        {
            var model = new List<TrademarkImportItemsModel>();
            if (!string.IsNullOrEmpty(fileName))
            {
                Stream stream = await GetFile(fileName);
                StreamReader streamReader = new StreamReader(stream);
                if (streamReader != null)
                {
                    while (!streamReader.EndOfStream)
                    {
                        var line = await streamReader.ReadLineAsync();
                        var values = line.Split(',');

                        string email = values[0];
                        string serialNumber = values[1];
                        if (!string.IsNullOrEmpty(serialNumber))
                        {
                            model.Add(new TrademarkImportItemsModel()
                            {
                                Email = email,
                                SerialNumber = int.Parse(serialNumber)
                            });
                        }

                    }
                }
            }
            return model;
        }

        #region TrademarkRetrives
        [HttpGet]
        public IActionResult TrademarkBatchList()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SearchTrademarkBatchList([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            var report = new TrademarkDeadReport(_entityContext);
            var reportRequest = new TrademarkDeadRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (TrademarkDeadResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        #endregion

    }
}
