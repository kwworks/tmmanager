﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TmManager.Identity;
using TmManager.Messaging;
using TmManager.Models;

namespace TmManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<Identity.User> _userManager;
        private readonly SignInManager<Identity.User> _signInManager;
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HomeController(ILogger<HomeController> logger, UserManager<Identity.User> userManager, SignInManager<Identity.User> signInManager, IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IActionResult> Index()
        {
            //await _userService.SeedData();
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username.Trim());
                if (user != null && user.UserType == Identity.UserType.Admin)
                {
                    if (user.Active)
                    {
                        var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                        if (result.Succeeded)
                        {
                            await _signInManager.SignInAsync(user, model.RememberMe);
                            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Name, user.UserName));

                            var key = Encoding.ASCII.GetBytes(AppEnvironment.AuthSecretKey);
                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = ClaimTransformer.Transform(new System.Security.Claims.ClaimsIdentity(), user),
                                Expires = DateTime.UtcNow.AddMinutes(AppEnvironment.OAuthTokenTimeOutInMinutes),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                            };
                            return RedirectPermanent("/admin");
                        }
                        else
                        {
                            ModelState.AddModelError("Password", "Incorrect user name and / or password");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "User is deactivated, Please contact administrator");
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "You are not authorized, Please contact administrator.");
                }
            }
            return View(model);
        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ResetPasswordModel model)
        {
            var result = new SaveResults();
            if (ModelState.IsValid)
            {
                try
                {
                    if (Helper.IsValidEmail(model.Email) == false) {
                        result.Errors.Add("Invalid email format");
                        return Json(result);
                    }
                    string email = model.Email;
                    var user = await _userManager.FindByNameAsync(email);
                    if (user != null)
                    {
                        user.ForgotPasswordExpiry = DateTimeHelper.Now().AddMinutes(20);
                        var isUpdated = await _userManager.UpdateAsync(user);

                        string subject = "Reset password";
                        string link = "http://" + _httpContextAccessor.HttpContext.Request.Host.Value + "/admin/resetpassword?id=" + user.Id;
                        string body = @"<!doctype html>
                <html>
                  <head>
                    <meta name='viewport' content='width=device-width'>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <title></title>
                  <style>
                @media only screen and (max-width: 620px) {
                  table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                  }

                  table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                  }

                  table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                  }

                  table[class=body] .content {
                    padding: 0 !important;
                  }

                  table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                  }

                  table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                  }

                  table[class=body] .btn table {
                    width: 100% !important;
                  }

                  table[class=body] .btn a {
                    width: 100% !important;
                  }

                  table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                  }
                }
                @media all {
                  .ExternalClass {
                    width: 100%;
                  }

                  .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                  }

                  .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                  }

                  .btn-primary table td:hover {
                    background-color: #b57e20 !important;
                  }

                  .btn-primary a:hover {
                    background-color: #b57e20 !important;
                    border-color: #b57e20 !important;
                  }
                }
                </style></head>
                  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                          <div class='header' style='padding: 20px 0;'>
                            <table role='presentation' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;'>
                              <tr>
                                <td class='align-center' width='100%' style='font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center;' valign='top' align='center'>
                                  <a href='https://kwworks.com/' style='color: #ec0867; text-decoration: underline;'><img src='http://52.240.156.147:8071/images/logo-tmmanager.png' height='40' alt='Postdrop' style='border: none; -ms-interpolation-mode: bicubic; max-width: 100%;'></a>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                           
                            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Name],</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>You recently requested to reset your password for your Trademark account. Click the button below to reset it.</p>
                        
                                        <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; box-sizing: border-box; width: 100%;' width='100%'>
                                          <tbody>
                                            <tr>
                                              <td align='center' style='font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;' valign='top'>
                                                <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: auto; width: auto;'>
                                                  <tbody>
                                                    <tr>
                                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top; border-radius: 5px; text-align: center; background-color: #CC922F;' valign='top' align='center' bgcolor='#CC922F'> <a href='[ResetLink]' target='_blank' style='border: solid 1px #CC922F; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; background-color: #842828; border-color: #842828; color: #ffffff;'>Reset your password</a> </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>If you did not request a password reset, please ignore this email.</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Thanks,</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Trademark Team</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                            <!-- END MAIN CONTENT AREA -->
                            </table>

                            <!-- START FOOTER -->
                            <div class='footer' style='clear: both; Margin-top: 10px; text-align: center; width: 100%;'>
                              <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
              
                                <tr>
                                  <td class='content-block powered-by' style='font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; color: #9a9ea6; font-size: 12px; text-align: center;' valign='top' align='center'>
                                    Powered by <a href='#' style='color: #9a9ea6; font-size: 12px; text-align: center; text-decoration: none;'>Trademark</a>.
                                  </td>
                                </tr>
                              </table>
                            </div>
                            <!-- END FOOTER -->

                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>";
                        body = body.Replace(Convert.ToString("[Name]"), user.FirstName);
                        body = body.Replace(Convert.ToString("[ResetLink]"), link);

                        var isEmailSent = await EmailSender.SendEmail(email, subject, body, string.Empty, string.Empty);
                        //var isEmailSent = true;
                        if (isEmailSent)
                            result.IsSuccess = true;
                        else
                            result.Errors.Add("Email sending failed");
                    }
                    else
                        result.Errors.Add("Email not found");

                }
                catch (Exception ex)
                {
                    result.Errors.Add(ex.Message);
                }
            }
            else
            {
                result.Errors.Add("Email field is required");
            }
            return Json(result);
        }
        public IActionResult Registration()
        {
            return View();
        }
        public IActionResult RegistrationStep()
        {
            return View();
        }
        public IActionResult RegistrationStep2()
        {
            return View();
        }
        public IActionResult RegistrationStep3()
        {
            return View();
        }
        public IActionResult RegistrationStep4()
        {
            return View();
        }
        public IActionResult CreateNewAttorney()
        {
            return View();
        }
        public IActionResult ViewTrademark()
        {
            return View();
        }
        public IActionResult MyProfile()
        {
            return View();
        }
        public IActionResult ShippDetail()
        {
            return View();
        }
        public IActionResult ViewAttorneyList()
        {
            return View();
        }
        public IActionResult UserDashboard()
        {
            return View();
        }
        public IActionResult TrademarkDetail()
        {
            return View();
        }
        public IActionResult RegistrationAccountAccess()
        {
            return View();
        }
        public IActionResult Admin()
        {
            return View();
        }
        public IActionResult TabsAccordion()
        {
            return View();
        }
        public IActionResult ClientSignupSuccess()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private async Task<bool> SendEmail(string to, string subject, string body, string cc, string bcc)
        {
            try
            {
                var isEmailSent = await EmailSender.SendEmail(to, subject, body, cc, bcc);
                if (isEmailSent)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
                return false;
            }
        }
    }
}
