﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TmManager.AccountRequests;
using TmManager.Data;
using TmManager.Identity;
using TmManager.Messaging;
using TmManager.Notifications;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    [Route("api/common")]
    [ApiController]
    public class CommonApiController : ControllerBase
    {
        private readonly ITrademarkService _trademarkService;
        private readonly IAccountRequestService _accountRequestService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<Identity.User> _userManager;
        private readonly IUserService _userService;
        private readonly INotificationService _notificationService;
        public CommonApiController(IUserService userService, ITrademarkService trademarkService, IAccountRequestService accountRequestService, UserManager<Identity.User> userManager, INotificationService notificationService, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _trademarkService = trademarkService;
            _httpContextAccessor = httpContextAccessor;
            _accountRequestService = accountRequestService;
            _userService = userService;
            _notificationService = notificationService;
        }

        [AllowAnonymous]
        [HttpGet("verifyemail")]
        public async Task<IActionResult> VerifyEmail(string email)
        {
            try
            {
                var result = await _trademarkService.VerifyEmail(email);
                if (result.IsSuccess == false)
                {
                    if (result.Errors.Any() && result.Errors.FirstOrDefault() == "Email found. Account not found!")
                    {
                        #region UpdateAccessCode&SendEmail
                //        var user = await _userManager.FindByNameAsync(email);
                //        //generateAccessCode
                //        var accessCode = Helper.RandomString();
                //        user.AccessCode = accessCode;
                //        await _userManager.UpdateAsync(user);
                //        string body = @"<!doctype html>
                //<html>
                //  <head>
                //    <meta name='viewport' content='width=device-width'>
                //    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                //    <title></title>
                //  <style>
                //@media only screen and (max-width: 620px) {
                //  table[class=body] h1 {
                //    font-size: 28px !important;
                //    margin-bottom: 10px !important;
                //  }

                //  table[class=body] p,
                //table[class=body] ul,
                //table[class=body] ol,
                //table[class=body] td,
                //table[class=body] span,
                //table[class=body] a {
                //    font-size: 16px !important;
                //  }

                //  table[class=body] .wrapper,
                //table[class=body] .article {
                //    padding: 10px !important;
                //  }

                //  table[class=body] .content {
                //    padding: 0 !important;
                //  }

                //  table[class=body] .container {
                //    padding: 0 !important;
                //    width: 100% !important;
                //  }

                //  table[class=body] .main {
                //    border-left-width: 0 !important;
                //    border-radius: 0 !important;
                //    border-right-width: 0 !important;
                //  }

                //  table[class=body] .btn table {
                //    width: 100% !important;
                //  }

                //  table[class=body] .btn a {
                //    width: 100% !important;
                //  }

                //  table[class=body] .img-responsive {
                //    height: auto !important;
                //    max-width: 100% !important;
                //    width: auto !important;
                //  }
                //}
                //@media all {
                //  .ExternalClass {
                //    width: 100%;
                //  }

                //  .ExternalClass,
                //.ExternalClass p,
                //.ExternalClass span,
                //.ExternalClass font,
                //.ExternalClass td,
                //.ExternalClass div {
                //    line-height: 100%;
                //  }

                //  .apple-link a {
                //    color: inherit !important;
                //    font-family: inherit !important;
                //    font-size: inherit !important;
                //    font-weight: inherit !important;
                //    line-height: inherit !important;
                //    text-decoration: none !important;
                //  }

                //  .btn-primary table td:hover {
                //    background-color: #b57e20 !important;
                //  }

                //  .btn-primary a:hover {
                //    background-color: #b57e20 !important;
                //    border-color: #b57e20 !important;
                //  }
                //}
                //</style></head>
                //  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                //    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                //      <tr>
                //        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                //        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                //          <div class='header' style='padding: 20px 0;'>
                //            <table role='presentation' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;'>
                //              <tr>
                //                <td class='align-center' width='100%' style='font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center;' valign='top' align='center'>
                //                  <a href='https://kwworks.com/' style='color: #ec0867; text-decoration: underline;'><img src='http://52.240.156.147:8071/images/logo-tmmanager.png' height='40' alt='Postdrop' style='border: none; -ms-interpolation-mode: bicubic; max-width: 100%;'></a>
                //                </td>
                //              </tr>
                //            </table>
                //          </div>
                //          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>

                //            <!-- START CENTERED WHITE CONTAINER -->
                           
                //            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                //              <!-- START MAIN CONTENT AREA -->
                //              <tr>
                //                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                //                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                //                    <tr>
                //                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                //                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Name],</p>
                //                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Your access code is <b>[Code]</b> .</p>
                        
                //                          <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Thanks,</p>
                //                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Trademark Team</p>
                //                      </td>
                //                    </tr>
                //                  </table>
                //                </td>
                //              </tr>

                //            <!-- END MAIN CONTENT AREA -->
                //            </table>

                //            <!-- START FOOTER -->
                //            <div class='footer' style='clear: both; Margin-top: 10px; text-align: center; width: 100%;'>
                //              <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
              
                //                <tr>
                //                  <td class='content-block powered-by' style='font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; color: #9a9ea6; font-size: 12px; text-align: center;' valign='top' align='center'>
                //                    Powered by <a href='http://kwworks.com/' style='color: #9a9ea6; font-size: 12px; text-align: center; text-decoration: none;'>Trademark</a>.
                //                  </td>
                //                </tr>
                //              </table>
                //            </div>
                //            <!-- END FOOTER -->

                //          <!-- END CENTERED WHITE CONTAINER -->
                //          </div>
                //        </td>
                //        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                //      </tr>
                //    </table>
                //  </body>
                //</html>";
                //        body = body.Replace(Convert.ToString("[Name]"), user.FirstName);
                //        body = body.Replace(Convert.ToString("[Code]"), accessCode);
                //        await SendEmail(email, "Access Code", body, "araza@kwworks.com", string.Empty);
                        #endregion
                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet("resendaccesscode")]
        public async Task<IActionResult> ResendAccessCode(string email)
        {
            var result = new SaveResults();
            try
            {
                #region UpdateAccessCode&SendEmail
                var user = await _userManager.FindByNameAsync(email);
                //generateAccessCode
                var accessCode = Helper.RandomString();
                user.AccessCode = accessCode;
                user.Modified = DateTimeHelper.Now();
                await _userManager.UpdateAsync(user);
                string body = @"<!doctype html>
                <html>
                  <head>
                    <meta name='viewport' content='width=device-width'>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <title></title>
                  <style>
                @media only screen and (max-width: 620px) {
                  table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                  }

                  table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                  }

                  table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                  }

                  table[class=body] .content {
                    padding: 0 !important;
                  }

                  table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                  }

                  table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                  }

                  table[class=body] .btn table {
                    width: 100% !important;
                  }

                  table[class=body] .btn a {
                    width: 100% !important;
                  }

                  table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                  }
                }
                @media all {
                  .ExternalClass {
                    width: 100%;
                  }

                  .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                  }

                  .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                  }

                  .btn-primary table td:hover {
                    background-color: #b57e20 !important;
                  }

                  .btn-primary a:hover {
                    background-color: #b57e20 !important;
                    border-color: #b57e20 !important;
                  }
                }
                </style></head>
                  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>
                            <!-- START CENTERED WHITE CONTAINER -->
                            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Name],</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>A new access code has been requested for your TM Chart account: [Code].</p>
                        
                                          <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Please register your account using the above code.</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Best,</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>TM Chart Team</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                            <!-- END MAIN CONTENT AREA -->
                            </table>

                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>";
                body = body.Replace(Convert.ToString("[Name]"), !string.IsNullOrWhiteSpace(user.ClientName) ? user.ClientName : $"{user.LastName} {user.FirstName}");
                body = body.Replace(Convert.ToString("[Code]"), accessCode);
                body = body.Replace(Convert.ToString("[LogoUrl]"), Constants.LogoUrl);
                await SendEmail(email, "Access Code", body, string.Empty, string.Empty);
                #endregion
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Errors.Add(ex.Message);
            }
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("verifyaccesscode")]
        public async Task<IActionResult> VerifyAccessCode(string email, string code)
        {
            var result = new SaveResults();
            try
            {
                var user = await _userManager.FindByNameAsync(email);
                if (code == user.AccessCode)
                {
                    result.IsSuccess = true;
                }
                else
                {
                    result.IsSuccess = false;
                    result.Errors.Add("In valid access code.");
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.Message);
                result.IsSuccess = false;
            }
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpPost("requestaccess")]
        public async Task<IActionResult> RequestAccess(AccountRequestModel model)
        {
            var result = new SaveResults();
            if (ModelState.IsValid)
            {
                result = await _accountRequestService.SaveAccountRequest(model);
            }
            else
            {
                result.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
            }
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpGet("gettrademarks")]
        public async Task<IActionResult> GetTrademarks(string email)
        {
            try
            {
                var results = await _trademarkService.GetTrademarksForApi(email);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("gettrademarkdetail")]
        public async Task<IActionResult> GetTrademarkDetail(Guid id)
        {
            try
            {
                //var results = await _trademarkService.GetTrademarkDetailForApi(id);
                var results = await _trademarkService.GetTrademarkDetail(id);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getgoodsandservicedetail")]
        public async Task<IActionResult> GetGoodsAndServiceDetail(Guid id)
        {
            try
            {
                var results = await _trademarkService.GetGoodsAndService(id);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("forgotpassword")]
        public async Task<IActionResult> ForgotPassword(string Email)
        {
            var result = new SaveResults();
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByNameAsync(Email);
                    if (user != null)
                    {
                        if (user.EmailConfirmed == false && user.UserType == UserType.AppUser)
                        {
                            result.IsSuccess = false;
                            result.Errors.Add("Your user is not verified, Kindly verfiy your account with access code.");
                            return Ok(result);
                        }
                        user.ForgotPasswordExpiry = DateTimeHelper.Now().AddMinutes(20);
                            var isUpdated = await _userManager.UpdateAsync(user);

                            string subject = "Reset password";
                            string link = AppEnvironment.ServerAdminUrl + "admin/resetpassword?id=" + user.Id;
                            string body = @"<!doctype html>
                <html>
                  <head>
                    <meta name='viewport' content='width=device-width'>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <title></title>
                  <style>
                @media only screen and (max-width: 620px) {
                  table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                  }

                  table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                  }

                  table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                  }

                  table[class=body] .content {
                    padding: 0 !important;
                  }

                  table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                  }

                  table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                  }

                  table[class=body] .btn table {
                    width: 100% !important;
                  }

                  table[class=body] .btn a {
                    width: 100% !important;
                  }

                  table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                  }
                }
                @media all {
                  .ExternalClass {
                    width: 100%;
                  }

                  .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                  }

                  .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                  }

                  .btn-primary table td:hover {
                    background-color: #b57e20 !important;
                  }

                  .btn-primary a:hover {
                    background-color: #b57e20 !important;
                    border-color: #b57e20 !important;
                  }
                }
                </style></head>
                  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                           
                            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Name],</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>You recently requested to reset your password for your Trademark account. Click the button below to reset it.</p>
                        
                                        <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; box-sizing: border-box; width: 100%;' width='100%'>
                                          <tbody>
                                            <tr>
                                              <td align='center' style='font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;' valign='top'>
                                                <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: auto; width: auto;'>
                                                  <tbody>
                                                    <tr>
                                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top; border-radius: 5px; text-align: center; background-color: #CC922F;' valign='top' align='center' bgcolor='#CC922F'> <a href='[ResetLink]' target='_blank' style='border: solid 1px #CC922F; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; background-color: #CC922F; border-color: #CC922F; color: #ffffff;'>Reset your password</a> </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>If you did not request a password reset, please ignore this email.</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Thanks,</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>TM Chart Team</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                            <!-- END MAIN CONTENT AREA -->
                            </table>

                            <!-- START FOOTER -->
                            <div class='footer' style='clear: both; Margin-top: 10px; text-align: center; width: 100%;'>
                              <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                              </table>
                            </div>
                            <!-- END FOOTER -->

                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>";
                            body = body.Replace(Convert.ToString("[Name]"), !string.IsNullOrWhiteSpace(user.ClientName) ? user.ClientName : $"{user.LastName} {user.FirstName}");
                            body = body.Replace(Convert.ToString("[ResetLink]"), link);

                            var isEmailSent = await SendEmail(Email, subject, body, string.Empty, string.Empty);
                            //var isEmailSent = true;
                            if (isEmailSent)
                                result.IsSuccess = true;
                            else
                                result.Errors.Add("Email sending failed");
                        
                        
                    }
                    else
                        result.Errors.Add("Email not found");

                }
                catch (Exception ex)
                {
                    result.Errors.Add(ex.Message);
                }
            }
            else
            {
                result.Errors.Add("Email field is required");
            }
            return Ok(result);
        }
        [AllowAnonymous]
        [HttpGet("getownerdetail")]
        public async Task<IActionResult> GetOwnerDetail(string email)
        {
            try
            {
                var results = await _trademarkService.GetOwnerDetailForApi(email);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getclientdetail")]
        public async Task<IActionResult> GetClientDetail(string email)
        {
            try
            {
                var results = await _trademarkService.GetClientDetailByEmail(email);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getownertrademarks")]
        public async Task<IActionResult> GetOwnerTrademarks(string email)
        {
            try
            {
                var results = await _trademarkService.GetOwnerTrademarksForApi(email);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpPost("updateuserinfo")]
        public async Task<IActionResult> UpdateUserInfo(Identity.UserRegistrationModel model)
        {
            try
            {
                var results = await _userService.RegisterUserWithAccessCode(model);
                if (results.IsSuccess)
                {
                    using (var context = new Data.Entity.EntityContext())
                    {
                        foreach (var item in model.UserContacts)
                        {
                            var contact = new UserContact()
                            {
                                City = item.City,
                                Country = item.Country,
                                Email = item.Email,
                                FirstName = item.FirstName,
                                LastName = item.LastName,
                                Phone = item.Phone,
                                PhoneType = item.PhoneType,
                                State = item.State,
                                Status = ContactType.Secondary,
                                StreetAddress = item.Address,
                                ZipCode = item.ZipCode,
                                UserId = results.Id.ToString(),
                                CreatedBy = model.UserName,
                                ModifiedBy = model.UserName
                            };
                            context.Add(contact);
                        }
                        await context.SaveChangesAsync();
                    }
                }
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getprofiledetails")]
        public async Task<IActionResult> GetProfileDetails(string email)
        {
            try
            {
                var model = await _userService.GetProfileDetails(email);
                if (model != null)
                {
                    using (IEntityContext context = new Data.Entity.EntityContext())
                    {
                        var currentId = _httpContextAccessor.HttpContext.User.Identity.UserId();
                        model.UserContacts = context.Select<UserContact>(x => x.IsActive && x.UserId == model.UserId).Select(x => new UserContactModel()
                        {
                            Id = x.Id,
                            UserId = x.UserId,
                            Address = x.StreetAddress,
                            City = x.City,
                            Country = x.Country,
                            Email = x.Email,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            //Office = x.Office
                            Phone = x.Phone,
                            PhoneType = x.PhoneType,
                            State = x.State,
                            Status = "Secondary",
                            ZipCode = x.ZipCode
                        }).ToArray();
                    }
                }
                return Ok(model);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpPost("updatesecondarycontact")]
        public async Task<IActionResult> UpdateSecondaryContact(UserContactModel model)
        {
            var results = new SaveResults();
            try
            {
                using (IEntityContext context = new Data.Entity.EntityContext())
                {
                    var now = DateTimeHelper.Now();
                    var currentUser = _httpContextAccessor.HttpContext.User.Identity.Name;
                    var contact = default(UserContact);
                    if (model.Id.HasValue)
                    {
                        contact = context.Select<UserContact>(x => x.Id == model.Id.Value).FirstOrDefault();
                        context.Update(contact);
                    }
                    else
                    {
                        contact = new UserContact() { UserId = model.UserId, IsActive = true, Created = now, CreatedBy = model.UserName };
                        context.Insert(contact);
                    }
                    contact.StreetAddress = model.Address;
                    contact.City = model.City;
                    contact.State = model.State;
                    contact.ZipCode = model.ZipCode;
                    contact.Country = model.Country;
                    contact.Email = model.Email;
                    contact.FirstName = model.FirstName;
                    contact.LastName = model.LastName;
                    contact.Phone = model.Phone;
                    contact.PhoneType = model.PhoneType;
                    contact.Modified = now;
                    contact.ModifiedBy = model.UserName;
                    await context.SaveChangesAsync();
                    results.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
                results.IsSuccess = false;
            }
            return Ok(results);
        }
        [AllowAnonymous]
        [HttpGet("removesecondarycontact")]
        public async Task<IActionResult> RemoveSecondaryContact(Guid id)
        {
            var results = new SaveResults();
            try
            {
                using (IEntityContext context = new Data.Entity.EntityContext())
                {
                    var now = DateTimeHelper.Now();
                    var contact = context.Select<UserContact>(x => x.Id == id).FirstOrDefault();
                    if (contact != null)
                    {
                        contact.IsActive = false;
                        context.Update(contact);
                        await context.SaveChangesAsync();
                        results.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
                results.IsSuccess = false;
            }
            return Ok(results);
        }
        [AllowAnonymous]
        [HttpPost("updateuserprofile")]
        public async Task<IActionResult> UpdateUserProfile(UserRegistrationModel model)
        {
            try
            {
                var results = await _userService.UpdateProfile(model);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, error = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpGet("getunreadnotificationscount")]
        public async Task<IActionResult> GetUnreadNotificationsCount(string username)
        {
            try
            {
                var count = await _notificationService.GetUnReadNotificationCount(username);
                return Ok(new { Count = count });
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, Error = ex.Message, Count = 0 });
            }
        }
        [AllowAnonymous]
        [HttpPost("changepassword")]
        public async Task<IActionResult> ChangePassword(UserChangePasswordModel model)
        {
            try
            {
                var results = await _userService.ChangePasswordByUserNameAsync(model.UserName, model.Password);
                return Ok(new { IsSuccess = results.IsSuccess });
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, error = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getallnotifications")]
        public async Task<IActionResult> GetAllNotifications(string username)
        {
            try
            {
                var data = await _notificationService.GetAllNotifications(username);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, Error = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getnotificationdetail")]
        public async Task<IActionResult> GetNotificationDetail(Guid id)
        {
            try
            {
                var data = await _notificationService.GetNotification(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { IsSuccess = false, Error = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("getallcountries")]
        public IActionResult GetAllCountries()
        {
            List<string> CountryList = new List<string>();
            CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in CInfoList)
            {
                RegionInfo R = new RegionInfo(CInfo.LCID);
                var name = R.EnglishName.ToUpperInvariant();
                if (!(CountryList.Contains(name)))
                {
                    CountryList.Add(name);
                }
            }

            CountryList.Sort();
            return Ok(CountryList);
        }
        private async Task<bool> SendEmail(string to, string subject, string body, string cc, string bcc)
        {
            try
            {
                var isEmailSent = await EmailSender.SendEmail(to, subject, body, cc, bcc);
                if (isEmailSent)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
                return false;
            }
        }

    }
}
