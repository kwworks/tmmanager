﻿using Kendo.Mvc;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Admin;
using TmManager.Data;
using TmManager.Identity;
using TmManager.Models;
using TmManager.Trademarks;

namespace TmManager.Controllers
{
    //[Authorize]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly UserManager _userManager;
        private readonly ITrademarkService _trademarkService;
        private readonly IEntityContext _entityContext;
        public AdminController(IUserService userService, ITrademarkService trademarkService, IEntityContext entityContext, UserManager userManager)
        {
            _userService = userService;
            _trademarkService = trademarkService;
            _entityContext = entityContext;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var response = await _trademarkService.GetDashboardCounts();
                return View(response);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return View(new TrademarkCountsResponse());
            }

        }
        public async Task<IActionResult> GetUsers([DataSourceRequest] DataSourceRequest request)
        {
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            var report = new SearchAdminReport(_userManager);
            var reportRequest = new SearchAdminRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember
            };
            var reportResponse = (SearchAdminResponse)await report.ExecuteReportAsync(reportRequest);
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> ResetPassword(Guid? id)
        {
            var result = new ForgotPasswordModel();
            if (id.HasValue)
            {
                var dbUser = await _userManager.FindByIdAsync(id.Value.ToString());
                if (dbUser != null)
                {
                    result.UserId = dbUser.Id;
                    if (dbUser.UserType == UserType.AppUser)
                        ViewBag.BackLoginUrl = AppEnvironment.ServerUrl;
                    else
                        ViewBag.BackLoginUrl = "/home/index";
                }
                else
                    result.Errors.Add("Invalid user");
            }
            else
                result.Errors.Add("Invalid user id");
            return View(result);
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ResetPassword(ForgotPasswordModel model)
        {
            var results = new SaveIdentityResults();
            if (ModelState.IsValid)
            {
                results = await _userService.ChangePasswordByIdAsync(model.UserId, model.Password);
            }
            else
            {
                results.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
                results.IsSuccess = false;
            }
            return Json(results);
        }
        [HttpGet]
        public async Task<IActionResult> ResetPasswordForAdmin(Guid? id)
        {
            var result = new ForgotPasswordForAdminModel();
            if (id.HasValue)
            {
                var model = await _userService.ForgotPassword(id.Value.ToString());
                result.UserId = model.Id;
            }
            else
                result.Errors.Add("Invalid user id");
            return View(result);
        }
        [HttpPost]
        public async Task<ActionResult> ResetPasswordForAdmin(ForgotPasswordForAdminModel model)
        {
            var results = new SaveIdentityResults();
            if (ModelState.IsValid)
            {
                results = await _userService.ChangePasswordForAdminAsync(model.UserId, model.Password);
            }
            else
            {
                results.Errors = ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage).ToHashSet<string>();
                results.IsSuccess = false;
            }
            return Json(results);
        }
        [HttpPost]
        public async Task<ActionResult> DeleteAdmin(string id)
        {
            var results = new SaveIdentityResults();
            var user = await _userManager.FindByIdAsync(id);
            var currentUser = User.Identity.Name;
            if (user != null)
            {
                if (currentUser == user.UserName)
                {
                    results.Errors.Add("Logged in user can not be deleted.");
                }
                else if (user.UserName != "webmaster@kwworks.com")
                {
                    var response = await _userManager.DeleteAsync(user);
                    if (response.Succeeded)
                        results.IsSuccess = true;
                }
                else
                    results.Errors.Add("You are not authorized for this action.");
            }
            else
                results.Errors.Add("User not found");


            return Json(results);
        }
        public IActionResult Home()
        {
            return View();
        }
        public IActionResult CreatedTrademark()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreatedTrademark([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string trademarkStatus = string.Empty;
            bool isTrademarkStatus = false;

            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate, ref trademarkStatus, ref isTrademarkStatus);
            var report = new TrademarkCreatedReport(_entityContext);
            var reportRequest = new TrademarkCreatedRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate,
                TrademarkStatus = trademarkStatus
            };
            var reportResponse = (TrademarkCreatedResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult RegistrationDeadline()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> RegistrationDeadline([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string registrationDateType = string.Empty;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate, ref registrationDateType);
            var report = new RegistrationDeadlineReport(_entityContext);
            var reportRequest = new RegistrationDeadlineRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate,
                RegistrationDateType = registrationDateType
            };
            var reportResponse = (RegistrationDeadlineResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult TmEvent()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> TmEvent([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            //var response = await _trademarkService.SearchTrademarkEventsAsync(startDate, endDate, sortDirection, sortMember, skip, take);
            var report = new TrademarkEventReport(_entityContext);
            var reportRequest = new TrademarkEventRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate
            };
            var reportResponse = (TrademarkEventResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult TmDocument()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> TmDocument([DataSourceRequest] DataSourceRequest request)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            string sortDirection = request.Sorts.Count() > 0 ? request.Sorts[0].SortDirection.ToString() : string.Empty;
            string sortMember = request.Sorts.Count() > 0 ? request.Sorts[0].Member.ToString() : string.Empty;

            int skip = request.Page == 1 ? 0 : (request.Page - 1) * request.PageSize;
            int take = request.PageSize;
            GetFilterParams(request.Filters, ref startDate, ref endDate);
            var report = new TrademarkDocumentReport(_entityContext);
            var reportRequest = new TrademarkDocumentRequest
            {
                Skip = skip,
                Take = take,
                SortDirection = sortDirection,
                SortMember = sortMember,
                StartDate = startDate,
                EndDate = endDate
            };
            var reportResponse = (TrademarkDocumentResponse)await report.ExecuteReportAsync(reportRequest);
            request.Filters.Clear();
            var response = new GridDataSourceResultModel()
            {
                Data = reportResponse.Data,
                Total = reportResponse.Total
            };

            return Json(response);
        }
        public IActionResult MessageAlert()
        {
            return View();
        }
        public IActionResult MessageSuccess()
        {
            return View();
        }
        public IActionResult Grid()
        {
            return View();
        }
        public IActionResult Configure()
        {
            return View();
        }
        public IActionResult NewTrademark()
        {
            return View();
        }
        public IActionResult DeadTrademark()
        {
            return View();
        }
        public IActionResult ArchivedFiles()
        {
            return View();
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref DateTime? start, ref DateTime? end)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "StartDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedStartDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedStartDate);
                        start = parsedStartDate;
                    }
                    if (descriptor != null && descriptor.Member == "EndDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedEndDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedEndDate);
                        end = parsedEndDate;
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref start, ref end);
                    }
                }
            }
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref DateTime? start, ref DateTime? end, ref string registrationdatetype)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "StartDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedStartDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedStartDate);
                        start = parsedStartDate;
                    }
                    if (descriptor != null && descriptor.Member == "EndDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedEndDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedEndDate);
                        end = parsedEndDate;
                    }
                    if (descriptor != null && descriptor.Member == "RegistrationDateType" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        registrationdatetype = descriptor.Value.ToString();
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref start, ref end, ref registrationdatetype);
                    }
                }
            }
        }
        private void GetFilterParams(IEnumerable<IFilterDescriptor> filters, ref DateTime? start, ref DateTime? end, ref string trademarkStatus, ref bool istrademarkstatus)
        {
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "StartDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedStartDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedStartDate);
                        start = parsedStartDate;
                    }
                    if (descriptor != null && descriptor.Member == "EndDate" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        DateTime parsedEndDate;
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime.TryParseExact(descriptor.Value.ToString(), "MM/dd/yyyy", provider, DateTimeStyles.None, out parsedEndDate);
                        end = parsedEndDate;
                    }
                    if (descriptor != null && descriptor.Member == "TrademarkStatus" && !string.IsNullOrEmpty(descriptor.Value.ToString()))
                    {
                        trademarkStatus = descriptor.Value.ToString();
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        GetFilterParams(((CompositeFilterDescriptor)filter).FilterDescriptors, ref start, ref end, ref trademarkStatus, ref istrademarkstatus);
                    }
                }
            }
        }
    }

}
