using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmManager.Attorneys;
using TmManager.Data;
using TmManager.Data.Entity;
using TmManager.Data.Repositories;
using TmManager.Trademarks;
using TmManager.Identity;
using TmManager.Identity.Data.Entity;
using TmManager.Storage;
using TmManager.AccountRequests;
using Hangfire;
using Hangfire.SqlServer;
using TmManager.Models;
using System.Text.Json.Serialization;
using TmManager.Data.CloudStorage;

namespace TmManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddKendo();
            //Data
            services.AddDbContext<EntityContext>();
            //Identity
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddScoped<IEntityContext>(provider => provider.GetService<EntityContext>());
            services.AddScoped<IdentityContext>(provider => provider.GetService<EntityContext>());
            services.AddScoped<ICloudStorageContext>(provider => provider.GetService<CloudStorageContext>());
            services.AddScoped<ITMCloudStorageContext>(provider => provider.GetService<TMCloudStorageContext>());
            services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = true)
               .AddEntityFrameworkStores<IdentityContext>()
               .AddClaimsPrincipalFactory<ClaimsPrincipalFactory>()
               .AddDefaultTokenProviders();
            services.Configure<SecurityStampValidatorOptions>(options => options.ValidationInterval = TimeSpan.FromHours(8))
                   .AddAuthentication()
                   .Services
                   .ConfigureApplicationCookie(options =>
                   {
                       options.Cookie.Name = "tmmanager";
                       options.AccessDeniedPath = "/";
                       options.LoginPath = "/";
                       options.ExpireTimeSpan = TimeSpan.FromDays(1);
                       options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                       options.SlidingExpiration = true;
                   });
            
            services.Configure<IdentityOptions>(o =>
            {
                o.Password.RequiredLength = 6;
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredUniqueChars = 0;
            });

            //services.AddSession(options =>
            //{
            //    options.IdleTimeout = TimeSpan.FromMinutes(10);
            //    options.Cookie.HttpOnly = false;
            //    options.Cookie.IsEssential = false;
            //});

#if RELEASE
            services.AddHangfire(options => options
                .UseSqlServerStorage(AppEnvironment.DataConnectionSettings.DbContext)
            );
#endif
            services.AddScoped<UserManager>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ITrademarkService, TrademarkService>();
            services.AddScoped<IAccountRequestService, AccountRequestService>();
            services.AddScoped<IStorage, BlobStorage>();
            services.AddScoped<ITrademarkRepository, TrademarkRepository>();
            services.AddScoped<IFirmRepository, FirmRepository>();
            services.AddScoped<IAttorneyRepository, AttorneyRepository>();
            services.AddScoped<IDesignSearchClassRepository, DesignSearchClassRepository>();
            services.AddScoped<IGoodAndServiceClassRepository, GoodAndServiceClassRepository>();
            services.AddScoped<IAccountRequestRepository, AccountRequestRepository>();
            services.AddScoped<IRecognizedAttorneyService, RecognizedAttorneyService>();
            services.AddScoped<IRecognizedAttorneyRepository, RecognizedAttorneyRepository>();
            services.AddScoped<Notifications.INotificationRepository, NotificationRepository>();
            services.AddScoped<Notifications.INotificationService, Notifications.NotificationService>();
            services.AddControllersWithViews()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            //services.AddControllersWithViews()
            //.AddJsonOptions(options =>
            //{
            //    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            //    options.JsonSerializerOptions.PropertyNamingPolicy = null;
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();


            app.UseRouting();
            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseSession();


#if RELEASE
            var dashboardOptions = new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorization() }
            };
            app.UseHangfireDashboard("/hangfire", dashboardOptions);
            var options = new BackgroundJobServerOptions { WorkerCount = 1 };
            app.UseHangfireServer(options);
            RecurringJob.AddOrUpdate<ITrademarkService>(x => x.SyncBatchTrademark(), Cron.Hourly);
#endif

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

        }
    }
}
