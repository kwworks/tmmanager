﻿using Hangfire.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace TmManager.Models
{
    public class HangfireAuthorization : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();
            var UserName = httpContext.User.Identity.Name;
            if (!string.IsNullOrEmpty(UserName) && UserName.ToLowerInvariant() == "webmaster@kwworks.com")
                return true;
            else
                return false;
        }
    }
}