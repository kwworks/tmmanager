﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TmManager.Models
{
    public class ForgotPasswordModel
    {
        public ForgotPasswordModel()
        {
            Errors = new HashSet<string>();
        }
        public string Email { get; set; }
        [Required(ErrorMessage = "Invalid User")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [MinLength(6, ErrorMessage = "Password must be at least 6 characters")]
        [Compare("ConfirmPassword", ErrorMessage = "Password must match confirm password field")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirm Password is required")]
        [MinLength(6, ErrorMessage = "Confirm password must be at least 6 characters")]
        public string ConfirmPassword { get; set; }
        public HashSet<string> Errors { get; set; }
    }

    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "Email field is required")]
        public string Email { get; set; }

    }

    public class ForgotPasswordForAdminModel
    {
        public ForgotPasswordForAdminModel()
        {
            Errors = new HashSet<string>();
        }
        public string UserId { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [MinLength(6, ErrorMessage = "Password must be at least 6 characters")]
        [Compare("ConfirmPassword", ErrorMessage = "Password must match confirm password field")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirm Password is required")]
        [MinLength(6, ErrorMessage = "Confirm password must be at least 6 characters")]
        public string ConfirmPassword { get; set; }
        public HashSet<string> Errors { get; set; }
    }
}
