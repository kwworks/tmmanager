﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.CloudStorage
{
    public class TMCloudStorageContext : CloudStorageContext, ITMCloudStorageContext
    {
        public TMCloudStorageContext()
            : base(AppEnvironment.DataConnectionSettings.StorageConnection)
        {

        }
        protected override void OnModelCreating(CloudStorageModelBuilder modelBuilder) {
            modelBuilder.AddTable<TradeMarkDataLog>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
