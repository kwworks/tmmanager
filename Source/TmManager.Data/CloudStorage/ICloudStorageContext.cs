﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TmManager.Data.CloudStorage
{
    public interface ICloudStorageContext : IDisposable
    {
        void Insert<T>(T entity) where T : ITableEntity;
        Task InsertAsync<T>(T entity) where T : ITableEntity;
        void Update<T>(T entity) where T : ITableEntity;
        Task UpdateAsync<T>(T entity) where T : ITableEntity;
        void InsertBatch<T>(IEnumerable<T> entities) where T : ITableEntity;
        void InsertBatchAsync<T>(IEnumerable<T> entities) where T : ITableEntity;
        T Find<T>(string partitionKey, string rowKey) where T : ITableEntity;
        Task<T> FindAsync<T>(string partitionKey, string rowKey) where T : ITableEntity;
        Task<IEnumerable<T>> Select<T>(TableQuery<T> query) where T : ITableEntity, new();
        TableQuery<T> Query<T>() where T : ITableEntity, new();
        void Delete<T>(T entity) where T : ITableEntity;
        Task DeleteAsync<T>(T entity) where T : ITableEntity;
        void DeleteBatch<T>(IEnumerable<T> entities) where T : ITableEntity;
        void DeleteBatchAsync<T>(IEnumerable<T> entities) where T : ITableEntity;
    }
}
