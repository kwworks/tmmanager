﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;

namespace TmManager.Data.CloudStorage
{
    public class CloudStorageModelBuilder
    {
        private HashSet<Type> Tables { get; set; }
        public CloudStorageModelBuilder()
        {
            Tables = new HashSet<Type>();
        }
        public void AddTable<T>() where T : ITableEntity
        {
            Tables.Add(typeof(T));
        }
        internal HashSet<Type> GetTableTypes()
        {
            return Tables;
        }
    }
}
