﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Data.CloudStorage
{
    [InheritedExport(typeof(ICloudStorageContext))]
    public abstract class CloudStorageContext : ICloudStorageContext
    {
        private static bool Initalized = false;
        readonly Dictionary<Type, CloudTable> Entities = new Dictionary<Type, CloudTable>();
        private readonly string ConnectionString;
        protected CloudStorageContext(string connectionString)
        {
            ConnectionString = connectionString;
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            tableClient.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(10), 3);
            var modelBuilder = new CloudStorageModelBuilder();
            OnModelCreating(modelBuilder);
            //TODO: move to app_init
            foreach (var type in modelBuilder.GetTableTypes())
            {
                CloudTable table = tableClient.GetTableReference(type.Name);
                if (!Initalized)
                {
                    
                    var result = table.CreateIfNotExistsAsync().Result;
                }
                Entities.Add(type, table);
            }
            Initalized = true;
        }
        //#if DEBUG
        //        public void RemoveAllTables() {
        //            foreach (var each in Entities)
        //            {
        //                each.Value.DeleteIfExists();
        //            }
        //        }
        //#endif
        protected virtual void OnModelCreating(CloudStorageModelBuilder modelBuilder)
        {
        }
        public void Insert<T>(T entity) where T : ITableEntity
        {
            TableOperation insertOperation = TableOperation.Insert(entity);
            var table = Entities[typeof(T)];
            table.ExecuteAsync(insertOperation);
        }
        public async Task InsertAsync<T>(T entity) where T : ITableEntity
        {
            TableOperation insertOperation = TableOperation.Insert(entity);
            var table = Entities[typeof(T)];
            var result = await table.ExecuteAsync(insertOperation);
        }
        public void Update<T>(T entity) where T : ITableEntity
        {
            TableOperation insertOperation = TableOperation.InsertOrReplace(entity);
            var table = Entities[typeof(T)];
            table.ExecuteAsync(insertOperation);
        }
        public async Task UpdateAsync<T>(T entity) where T : ITableEntity
        {
            TableOperation insertOperation = TableOperation.InsertOrReplace(entity);
            var table = Entities[typeof(T)];
            await table.ExecuteAsync(insertOperation);
        }
        public void InsertBatch<T>(IEnumerable<T> entities) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            var values = entities.GroupBy(c => c.PartitionKey).Select(c => new { c.Key, Values = c.Select(d => d) });
            foreach (var each in values)
            {
                var take = 100;
                var skip = 0;
                var total = each.Values.Count();
                while (skip < total)
                {
                    TableBatchOperation batchOperation = new TableBatchOperation();
                    foreach (var entites in each.Values.Skip(skip).Take(take))
                    {
                        batchOperation.InsertOrReplace(entites);
                    }
                    table.ExecuteBatchAsync(batchOperation);
                    skip = skip + take;
                }
            }
        }

        public async void InsertBatchAsync<T>(IEnumerable<T> entities) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            var values = entities.GroupBy(c => c.PartitionKey).Select(c => new { c.Key, Values = c.Select(d => d) });
            foreach (var each in values)
            {
                var take = 100;
                var skip = 0;
                var total = each.Values.Count();
                while (skip < total)
                {
                    TableBatchOperation batchOperation = new TableBatchOperation();
                    foreach (var entites in each.Values.Skip(skip).Take(take))
                    {
                        batchOperation.InsertOrReplace(entites);
                    }
                    await table.ExecuteBatchAsync(batchOperation);
                    skip = skip + take;
                }
            }
        }
        public T Find<T>(string partitionKey, string rowKey) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            TableResult retrievedResult = table.ExecuteAsync(retrieveOperation).Result;
            if (retrievedResult.Result == null)
                return default(T);
            else
                return (T)retrievedResult.Result;
        }

        public async Task<T> FindAsync<T>(string partitionKey, string rowKey) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);
            if (retrievedResult.Result == null)
                return default(T);
            else
                return (T)retrievedResult.Result;
        }

        public async Task<IEnumerable<T>> Select<T>(TableQuery<T> query) where T : ITableEntity, new()
        {
            var table = Entities[typeof(T)];
            var data = await table.ExecuteQuerySegmentedAsync(query, null);
            return data.ToArray();
        }

        public TableQuery<T> Query<T>() where T : ITableEntity, new()
        {
            var table = Entities[typeof(T)];
            TableQuery<T> query = new TableQuery<T>();
            return query;
        }

        public void Delete<T>(T entity) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            TableOperation deleteOperation = TableOperation.Delete(entity);
            table.ExecuteAsync(deleteOperation);
        }
        public async Task DeleteAsync<T>(T entity) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            TableOperation deleteOperation = TableOperation.Delete(entity);
            await table.ExecuteAsync(deleteOperation);
        }

        public void DeleteBatch<T>(IEnumerable<T> entities) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            foreach (var each in entities)
            {
                TableOperation deleteOperation = TableOperation.Delete(each);
                table.ExecuteAsync(deleteOperation);
            }
        }

        public async void DeleteBatchAsync<T>(IEnumerable<T> entities) where T : ITableEntity
        {
            var table = Entities[typeof(T)];
            foreach (var each in entities)
            {
                TableOperation deleteOperation = TableOperation.Delete(each);
                await table.ExecuteAsync(deleteOperation);
            }
        }
        public void Dispose()
        {

        }
    }
}
