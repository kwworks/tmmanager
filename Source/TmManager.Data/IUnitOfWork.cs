﻿using System;
using System.Threading.Tasks;

namespace TmManager.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        Task CommitAsync();
        void ClearError();
    }
}
