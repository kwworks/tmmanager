﻿using TmManager.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq;

namespace TmManager.Data.Entity
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Ctor
        public UnitOfWork(IEntityContext context)
        {
            Context = (DbContext)context;
        }
        #endregion

        #region IUnitOfWork.Commit
        void IUnitOfWork.Commit()
        {
            try
            {
                PreCommit();
                Context.SaveChanges();
                PostCommit();
            }
            catch (Exception dbEx)
            {
                Trace.TraceError(dbEx.ToLogString());
                throw;
            }
        }
        async Task IUnitOfWork.CommitAsync()
        {
            try
            {
                PreCommit();
                await Context.SaveChangesAsync();
                PostCommit();
            }
            catch (Exception dbEx)
            {
                Trace.TraceError(dbEx.ToLogString());
                throw;
            }
        }
        #endregion

        #region PreCommit
        private void PreCommit()
        {
        }
        #endregion

        #region PostCommit
        private void PostCommit()
        {
        }
        #endregion

        void IUnitOfWork.ClearError()
        {
            Context.ChangeTracker.Clear();
        }
        #region Dispose
        public void Dispose()
        {
            Context.Dispose();
        }
        #endregion

        #region Fields
        private readonly DbContext Context;
        #endregion
    }
}
