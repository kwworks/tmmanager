﻿using TmManager.Data.Entity.ModelConfigurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using TmManager.Trademarks;

namespace TmManager.Data.Entity
{
    public sealed class EntityContext : Identity.Data.Entity.IdentityContext
    {
        #region OnModelCreating
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new AttorneyConfiguration());
            builder.ApplyConfiguration(new DocumentCategoryConfiguration());
            builder.ApplyConfiguration(new DocumentConfiguration());
            builder.ApplyConfiguration(new DocumentTypeConfiguration());
            builder.ApplyConfiguration(new FirmConfiguration());
            builder.ApplyConfiguration(new GoodAndServiceClassConfiguration());
            builder.ApplyConfiguration(new GoodAndServiceConfiguration());
            builder.ApplyConfiguration(new OwnerGroupConfiguration());
            builder.ApplyConfiguration(new PhysicalLocationHistoryConfiguration());
            builder.ApplyConfiguration(new ProsecutionHistoryConfiguration());
            builder.ApplyConfiguration(new PublicationOfficialGazetteConfiguration());
            builder.ApplyConfiguration(new TrademarkConfiguration());
            builder.ApplyConfiguration(new ValueConfiguration());
            builder.ApplyConfiguration(new DesignSearchClassConfiguration());
            builder.ApplyConfiguration(new DesignSearchDescriptionClassConfiguration());
            builder.ApplyConfiguration(new AccountRequestConfiguration());
            builder.ApplyConfiguration(new TrademarkImportFileConfiguration());
            builder.ApplyConfiguration(new TrademarkImportItemConfiguration());
            builder.ApplyConfiguration(new RecognizedAttorneyConfiguration());
            builder.ApplyConfiguration(new NotificationConfiguration());
        }
        #endregion
    }
}
