﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class TrademarkConfiguration : ModifiableDomainObjectConfiguration<Trademark>
    {
        public override void Configure(EntityTypeBuilder<Trademark> builder)
        {
            builder.ToTable("Trademarks");
            builder.HasOne(c => c.Attorney)
               .WithMany(d => d.Trademarks)
               .HasForeignKey(d => d.AttorneyId)
               .IsRequired(false);
        }
    }
}
