﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal abstract class DomainObjectConfiguration<T> : IEntityTypeConfiguration<T> where T : DomainObject
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(t => t.Id);
        }
    }



}
