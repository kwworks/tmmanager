﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class PublicationOfficialGazetteConfiguration : IEntityTypeConfiguration<PublicationOfficialGazette>
    {
        public void Configure(EntityTypeBuilder<PublicationOfficialGazette> builder)
        {
            builder.ToTable("PublicationOfficialGazettes");

            builder.HasOne(e => e.Trademark)
                .WithMany(e => e.PublicationOfficialGazettes)
                .HasForeignKey(e => e.TrademarkId);
        }
    }
}