﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class NotificationConfiguration : DomainObjectConfiguration<Notifications.Notification>
    {
        public override void Configure(EntityTypeBuilder<Notifications.Notification> builder)
        {
            builder.ToTable("Notifications");
        }
    }
}
