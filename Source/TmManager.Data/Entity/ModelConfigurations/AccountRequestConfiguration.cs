﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TmManager.AccountRequests;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class AccountRequestConfiguration : ModifiableDomainObjectConfiguration<AccountRequest>
    {
        public override void Configure(EntityTypeBuilder<AccountRequest> builder)
        {
            builder.ToTable("AccountRequests");
        }
    }
}
