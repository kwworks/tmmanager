﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class ValueConfiguration : ModifiableDomainObjectConfiguration<Value>
    {
        public override void Configure(EntityTypeBuilder<Value> builder)
        {
            base.Configure(builder);
        }
    }
}
