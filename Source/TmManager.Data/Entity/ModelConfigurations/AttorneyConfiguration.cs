﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class AttorneyConfiguration : ModifiableDomainObjectConfiguration<Attorney>
    {
        public override void Configure(EntityTypeBuilder<Attorney> builder)
        {
            builder.ToTable("Attorneys");
        }
    }
}
