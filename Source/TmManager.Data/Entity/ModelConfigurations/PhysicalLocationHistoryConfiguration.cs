﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class PhysicalLocationHistoryConfiguration : DomainObjectConfiguration<PhysicalLocationHistory>
    {
        public override void Configure(EntityTypeBuilder<PhysicalLocationHistory> builder)
        {
            builder.ToTable("PhysicalLocationHistorys");

            builder
            .HasOne(x => x.Trademark)
            .WithMany(x => x.PhysicalLocationHistorys)
            .HasForeignKey(f => f.TrademarkId);
        }
    }
}
