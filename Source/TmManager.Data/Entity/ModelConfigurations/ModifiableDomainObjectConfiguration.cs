﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal abstract class ModifiableDomainObjectConfiguration<T> : IEntityTypeConfiguration<T> where T : ModifiableDomainObject
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(t => t.Id);
        }
    }
}
