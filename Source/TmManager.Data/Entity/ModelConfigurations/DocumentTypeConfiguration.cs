﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DocumentTypeConfiguration : DomainObjectConfiguration<DocumentType>
    {
        public override void Configure(EntityTypeBuilder<DocumentType> builder)
        {
            builder.ToTable("DocumentType");
            builder.HasMany(c => c.Documents)
               .WithOne(d => d.DocumentType)
               .HasForeignKey(d => d.DocumentTypeId);

        }
    }
}
