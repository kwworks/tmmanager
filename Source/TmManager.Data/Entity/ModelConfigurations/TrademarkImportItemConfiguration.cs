﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class TrademarkImportItemConfiguration : ModifiableDomainObjectConfiguration<TrademarkImportItem>
    {
        public override void Configure(EntityTypeBuilder<TrademarkImportItem> builder)
        {
            builder.ToTable("TrademarkImportItems");
            builder.HasOne(c => c.TrademarkImportFile)
               .WithMany(d => d.TrademarkImportItems)
               .HasForeignKey(d => d.TrademarkImportFileId);
        }
    }
}
