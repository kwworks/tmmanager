﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DocumentUrlConfiguration : IEntityTypeConfiguration<DocumentUrl>
    {
        public void Configure(EntityTypeBuilder<DocumentUrl> builder)
        {
            builder.ToTable("DocumentUrls");
        }
    }
}