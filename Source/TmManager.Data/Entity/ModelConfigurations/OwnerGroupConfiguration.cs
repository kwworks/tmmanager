﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class OwnerGroupConfiguration : ModifiableDomainObjectConfiguration<OwnerGroup>
    {
        public override void Configure(EntityTypeBuilder<OwnerGroup> builder)
        {
            builder.ToTable("OwnerGroups");
            
            builder.HasOne(e => e.Trademark)
                .WithMany(e => e.OwnerGroups)
                .HasForeignKey(e => e.TrademarkId);
        }
    }
}
