﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DesignSearchConfiguration : DomainObjectConfiguration<DesignSearch>
    {
        public override void Configure(EntityTypeBuilder<DesignSearch> builder)
        {
            builder.ToTable("DesignSearches");

            //builder
            //.HasMany(p => p.DesignSearchClass)
            //.WithMany(p => p.DesignSearches)
            //.UsingEntity(j => j.ToTable("DesignSearchClassInDesignSearches"));

            //builder
            //.HasOne(p => p.Trademark)
            //.WithMany(p => p.DesignSearches)
            //.HasForeignKey(x => x.TrademarkId).IsRequired();
        }
    }
}
