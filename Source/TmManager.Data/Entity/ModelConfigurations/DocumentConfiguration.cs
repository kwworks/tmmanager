﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DocumentConfiguration : DomainObjectConfiguration<Document>
    {
        public override void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.ToTable("Documents");
            
            
        }
    }
}