﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class RecognizedAttorneyConfiguration : ModifiableDomainObjectConfiguration<RecognizedAttorney>
    {
        public override void Configure(EntityTypeBuilder<RecognizedAttorney> builder)
        {
            builder.ToTable("RecognizedAttorneys");
        }
    }
}
