﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class ProsecutionHistoryConfiguration : DomainObjectConfiguration<ProsecutionHistory>
    {
        public override void Configure(EntityTypeBuilder<ProsecutionHistory> builder)
        {
            builder.ToTable("ProsecutionHistorys");

            builder
                .HasOne(x => x.Trademark)
                .WithMany(x => x.ProsecutionHistorys)
                .HasForeignKey(f => f.TrademarkId);
        }
    }
}
