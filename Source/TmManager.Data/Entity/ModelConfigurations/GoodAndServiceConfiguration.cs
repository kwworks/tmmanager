﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class GoodAndServiceConfiguration : DomainObjectConfiguration<GoodAndService>
    {
        public override void Configure(EntityTypeBuilder<GoodAndService> builder)
        {
            builder.ToTable("GoodAndServices");

            builder
            .HasMany(p => p.GoodAndServiceClass)
            .WithMany(p => p.GoodAndServices)
            .UsingEntity(j => j.ToTable("GoodAndServiceClassList"));
        }
    }
}
