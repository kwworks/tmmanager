﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DesignSearchClassConfiguration : DomainObjectConfiguration<DesignSearchClass>
    {
        public override void Configure(EntityTypeBuilder<DesignSearchClass> builder)
        {
            builder.ToTable("DesignSearchClasses");

            builder.HasOne(c => c.Trademark)
                .WithMany(d => d.DesignSearches)
                .HasForeignKey(d => d.TrademarkId);
        }
    }
}
