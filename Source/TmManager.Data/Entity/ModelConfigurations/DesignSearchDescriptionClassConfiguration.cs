﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Entity.ModelConfigurations
{
    internal class DesignSearchDescriptionClassConfiguration : DomainObjectConfiguration<DesignSearchDescriptionClass>
    {
        public override void Configure(EntityTypeBuilder<DesignSearchDescriptionClass> builder)
        {
            builder.ToTable("DesignSearchDescriptionClasses");

            builder
                .HasOne(x => x.DesignSearchClass)
                .WithMany(x => x.DesignSearchDescriptionClasses)
                .HasForeignKey(x => x.DesignSearchClassId);
        }
    }
}
