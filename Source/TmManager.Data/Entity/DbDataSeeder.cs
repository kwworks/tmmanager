﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TmManager.Identity;

namespace TmManager.Data.Entity
{
    public class DbDataSeeder
    {
        private EntityContext _context;

        public DbDataSeeder(EntityContext context)
        {
            _context = context;
            Initialize();
        }


        public async void Initialize()
        {
            var user = new User
            {
                FirstName = "TmManager",
                LastName = "ADMIN",
                Email = "webmaster@kwworks.com",
                NormalizedEmail = "WEBMASTER@KWWORKS.COM",
                UserName = "kww_admin",
                NormalizedUserName = "KWW.ADMIN",
                PhoneNumber = "+18006721787",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                UserType = UserType.Admin,
                Active = true,
                Street = "1327 Jones Drive,Suite 106",
                City = "Ann Arbor",
                State = "MI",
                Created = DateTimeHelper.Now(),
                LastLogIn = null,
                LockoutEnabled = false,
                Modified = DateTimeHelper.Now(),
            };


            if (!_context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, "12343");
                user.PasswordHash = hashed;

                var userStore = new UserStore<User>(_context);
                var result = await userStore.CreateAsync(user);

            }
            await _context.SaveChangesAsync();
        }
    }
}
