﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Data.Repositories
{
    public class FirmRepository : Repository<Firm>, IFirmRepository
    {
        public FirmRepository(IEntityContext context) : base(context)
        {
        }
        public async Task<Firm> FindFirmAsync(FirmSpecification specification)
        {
            return await Context.Select<Firm>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<List<Firm>> ListFirmAsync(FirmSpecification specification)
        {
            return await Context.Select<Firm>(specification.Predicate).ToListAsync();
        }
    }
}
