﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Repositories
{
    public class DesignSearchClassRepository : Repository<DesignSearchClass>, IDesignSearchClassRepository
    {
        public DesignSearchClassRepository(IEntityContext context) : base(context)
        {
        }

        public async Task<DesignSearchClass> FindDesignSearchClassAsync(DesignSearchClassSpecification specification)
        {
            return await Context.Select<DesignSearchClass>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<List<DesignSearchClass>> ListDesignSearchClassAsync(DesignSearchClassSpecification specification)
        {
            return await Context.Select<DesignSearchClass>(specification.Predicate).ToListAsync();
        }
    }
}
