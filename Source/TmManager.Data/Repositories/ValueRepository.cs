﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Data.Repositories
{
    public class ValueRepository : Repository<Value>, IValueRepository
    {
        public ValueRepository(IEntityContext context) : base(context)
        {
        }
        public void Insert(Value entity)
        {
            Context.Insert(entity);
        }
        public async Task<ICollection<T>> FindAsync<T>(ValueSpecification<T> specification) where T : Value
        {
            return await Context.Select<T>(specification.Predicate).ToListAsync();
        }
        public async Task<ICollection<T>> GetAllAsync<T>(ValueSpecification<T> specification) where T : Value
        {
            return await Context.Select<T>(specification.Predicate).ToListAsync();
        }
    }
}
