﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Repositories
{
    public class GoodAndServiceClassRepository : Repository<GoodAndServiceClass>, IGoodAndServiceClassRepository
    {
        public GoodAndServiceClassRepository(IEntityContext context) : base(context)
        {
        }

        public async Task<GoodAndServiceClass> FindGoodAndServiceClassAsync(GoodAndServiceClassSpecification specification)
        {
            return await Context.Select<GoodAndServiceClass>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<List<GoodAndServiceClass>> ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification specification)
        {
            return await Context.Select<GoodAndServiceClass>(specification.Predicate).ToListAsync();
        }
    }
}
