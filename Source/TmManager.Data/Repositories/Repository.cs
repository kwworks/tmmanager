﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Data.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : ModifiableDomainObject
    {
        protected readonly IEntityContext Context;

        public Repository(IEntityContext context)
        {
            Context = context;
        }

        public void Create(T entity)
        {
            Context.Insert(entity);
        }

        public void Delete(T entity)
        {
            Context.Update(entity);
            entity.IsActive = false;
            entity.Modified = DateTimeHelper.Now();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public T Find(Guid id)
        {
            return Context.Select<T>(c => c.Id == id).FirstOrDefault();
        }

        public async Task<T> FindAsync(Guid id)
        {
            return await Context.Select<T>(c => c.Id == id).SingleOrDefaultAsync();
        }
        public async Task<T> FindAsync(Guid id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> result = Context.Select<T>(c => c.Id == id);
            if (include != null)
            {
                result = include(result);
            }
            return await result.SingleOrDefaultAsync();
        }

        public async Task<ICollection<T>> FindAsync()
        {
            return await Context.Select<T>(c => true).ToListAsync();
        }

        public void Update(T entity)
        {
            Context.Update(entity);
            entity.Modified = DateTimeHelper.Now();
        }
    }
    public abstract class RepositoryDomain<T> : IDomainRepository<T> where T : DomainObject
    {
        protected readonly IEntityContext Context;

        public RepositoryDomain(IEntityContext context)
        {
            Context = context;
        }

        public void Create(T entity)
        {
            Context.Insert(entity);
        }

        public void Delete(T entity)
        {
            Context.Update(entity);
            entity.IsActive = false;
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public T Find(Guid id)
        {
            return Context.Select<T>(c => c.Id == id).FirstOrDefault();
        }

        public async Task<T> FindAsync(Guid id)
        {
            return await Context.Select<T>(c => c.Id == id).SingleOrDefaultAsync();
        }
        public async Task<T> FindAsync(Guid id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> result = Context.Select<T>(c => c.Id == id);
            if (include != null)
            {
                result = include(result);
            }
            return await result.SingleOrDefaultAsync();
        }

        public async Task<ICollection<T>> FindAsync()
        {
            return await Context.Select<T>(c => true).ToListAsync();
        }

        public void Update(T entity)
        {
            Context.Update(entity);
        }
    }
}
