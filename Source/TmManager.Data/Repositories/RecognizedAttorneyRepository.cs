﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Data.Repositories
{
    public class RecognizedAttorneyRepository : Repository<RecognizedAttorney>, IRecognizedAttorneyRepository
    {
        public RecognizedAttorneyRepository(IEntityContext context) : base(context)
        {
        }

        public async Task<RecognizedAttorney> FindRecognizedAttorneyAsync(RecognizedAttorneySpecification specification)
        {
            return await Context.Select<RecognizedAttorney>(specification.Predicate).FirstOrDefaultAsync();
        }

        public async Task<List<RecognizedAttorney>> ListRecognizedAttorneyAsync(RecognizedAttorneySpecification specification)
        {
            return await Context.Select<RecognizedAttorney>(specification.Predicate).ToListAsync();
        }
    }
}
