﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Data.Repositories
{
    public class TrademarkRepository : Repository<Trademark>, ITrademarkRepository
    {
        public TrademarkRepository(IEntityContext context) : base(context)
        {
        }
        public async Task<Trademark> FindTrademarkWithOutIncludeAsync(TrademarkSpecification specification)
        {
            return await Context.Select<Trademark>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<Trademark> FindTrademarkAsync(TrademarkSpecification specification)
        {
            return await Context.Select<Trademark>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<List<Trademark>> ListTrademarkAsync(TrademarkSpecification specification)
        {
            return await Context.Select<Trademark>(specification.Predicate).ToListAsync();
        }
        public async Task<List<Trademark>> ListTrademarkAsync(TrademarkSpecification specification, int? skip = null, int? take = null)
        {
            if (skip.HasValue && take.HasValue)
                return await Context.Select<Trademark>(specification.Predicate).OrderBy(o => o.Id).Skip(skip.Value).Take(take.Value).ToListAsync();
            return await Context.Select<Trademark>(specification.Predicate).ToListAsync();
        }
        public async Task<int> CountAdvertisementsAsync(TrademarkSpecification specification)
        {
            return await Context.Select<Trademark>(specification.Predicate).CountAsync();
        }
        public IQueryable<Trademark> TrademarkIQuerableAsync()
        {
            return Context.Select<Trademark>(x => x.Id != Guid.Empty);
        }
        public IQueryable<Trademark> TrademarkIQuerableAsync(TrademarkSpecification specification, int skip, int take)
        {
            return Context.Select<Trademark>(specification.Predicate).Skip(skip).Take(take);
        }
        public async Task<DocumentType> FindDocumentTypeByCode(string code)
        {
            return await Context.Select<DocumentType>(x => x.Code == code).FirstOrDefaultAsync();
        }
        public async Task<DocumentCategory> FindDocumentCategoryByCode(string code)
        {
            return await Context.Select<DocumentCategory>(x => x.Code == code).FirstOrDefaultAsync();
        }
        public async Task<TrademarkImportItem> GetTrademarkImportItem(long SerialNumber)
        {
            return await Context.Select<TrademarkImportItem>(x => x.SerialNumber == SerialNumber).FirstOrDefaultAsync();
        }
        public async Task<TrademarkImportItem> GetTrademarkImportItemById(Guid id)
        {
            return await Context.Select<TrademarkImportItem>(x => x.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<TrademarkImportFile>> GetTrademarkImportFilesAsync(TrademarkImportFileSpecification specification)
        {
            return await Context.Select<TrademarkImportFile>(specification.Predicate).OrderByDescending(x=> x.Created).ToListAsync();
        }
        public async Task<int> CountTrademarkImportFilesAsync()
        {
            return await Context.Select<TrademarkImportFile>(x => x.Created >= DateTime.Now.AddMonths(-1)).CountAsync();
        }
        public void Create(DocumentCategory entity)
        {
            Context.Insert(entity);
        }
        public void Create(OwnerGroup entity)
        {
            Context.Insert(entity);
        }
        public void Delete(DocumentCategory entity)
        {
            Context.Update(entity);
            entity.IsActive = false;
        }
        public void Create(DocumentType entity)
        {
            Context.Insert(entity);
        }
        public void Delete(DocumentType entity)
        {
            Context.Update(entity);
            entity.IsActive = false;
        }
        public void Create(TrademarkImportFile entity)
        {
            Context.Insert(entity);
        }
        public void Update(TrademarkImportItem entity)
        {
            Context.Update(entity);
        }
        public void Update(TrademarkImportFile entity)
        {
            Context.Update(entity);
        }
        public void Create(ProsecutionHistory entity)
        {
            Context.Insert(entity);
        }
        public void Update(ProsecutionHistory entity)
        {
            Context.Update(entity);
        }
        public void Create(PhysicalLocationHistory entity)
        {
            Context.Insert(entity);
        }
        public void Update(PhysicalLocationHistory entity)
        {
            Context.Update(entity);
        }
        public void Create(PublicationOfficialGazette entity)
        {
            Context.Insert(entity);
        }
        public void Update(PublicationOfficialGazette entity)
        {
            Context.Update(entity);
        }
    }
}
