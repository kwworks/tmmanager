﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Data.Repositories
{
    public class AttorneyRepository : Repository<Attorney>, IAttorneyRepository
    {
        public AttorneyRepository(IEntityContext context) : base(context)
        {
        }
        public async Task<Attorney> FindAttorney(AttorneySpecification specification)
        {
            return await Context.Select<Attorney>(specification.Predicate).FirstOrDefaultAsync();
        }
    }
}
