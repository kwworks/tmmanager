﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.AccountRequests;

namespace TmManager.Data.Repositories
{
    public class AccountRequestRepository : Repository<AccountRequest>, IAccountRequestRepository
    {
        public AccountRequestRepository(IEntityContext context) : base(context)
        {
        }
        public async Task<AccountRequest> FindAccountRequestAsync(AccountRequestSpecification specification)
        {
            return await Context.Select<AccountRequest>(specification.Predicate).FirstOrDefaultAsync();
        }
        public async Task<List<AccountRequest>> ListAccountRequestAsync(AccountRequestSpecification specification, int? skip = null, int? take = null)
        {
            if (skip.HasValue && take.HasValue)
                return await Context.Select<AccountRequest>(specification.Predicate).OrderBy(o => o.Id).Skip(skip.Value).Take(take.Value).ToListAsync();
            return await Context.Select<AccountRequest>(specification.Predicate).ToListAsync();
        }
        public async Task<int> CountAccountRequestsAsync(AccountRequestSpecification specification)
        {
            return await Context.Select<AccountRequest>(specification.Predicate).CountAsync();
        }
    }
}
