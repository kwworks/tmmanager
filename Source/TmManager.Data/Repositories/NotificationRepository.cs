﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Notifications;

namespace TmManager.Data.Repositories
{
    public class NotificationRepository : RepositoryDomain<Notification>, INotificationRepository //Repository<Notifications.Notification>, INotificationRepository
    {
        public NotificationRepository(IEntityContext context) : base(context)
        {

        }

        public async Task<Notification> FindNotificationAsync(NotificationSpecification specification)
        {
            return await Context.Select<Notification>(specification.Predicate).FirstOrDefaultAsync();
        }

        public async Task<List<Notification>> ListNotificationAsync(NotificationSpecification specification)
        {
            return await Context.Select<Notification>(specification.Predicate).OrderByDescending(x=> x.Created).ToListAsync();
        }
    }
}
