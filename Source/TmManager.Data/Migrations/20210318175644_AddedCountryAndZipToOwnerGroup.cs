﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class AddedCountryAndZipToOwnerGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "OwnerGroups",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "OwnerGroups",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "OwnerGroups");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "OwnerGroups");
        }
    }
}
