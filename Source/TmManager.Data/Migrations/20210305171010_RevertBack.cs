﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class RevertBack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentInDocumentTypeList");

            migrationBuilder.AddColumn<Guid>(
                name: "DocumentId",
                table: "DocumentType",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DocumentUrlId",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_DocumentType_DocumentId",
                table: "DocumentType",
                column: "DocumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentType_Documents_DocumentId",
                table: "DocumentType",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentType_Documents_DocumentId",
                table: "DocumentType");

            migrationBuilder.DropIndex(
                name: "IX_DocumentType_DocumentId",
                table: "DocumentType");

            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "DocumentType");

            migrationBuilder.DropColumn(
                name: "DocumentUrlId",
                table: "Documents");

            migrationBuilder.CreateTable(
                name: "DocumentInDocumentTypeList",
                columns: table => new
                {
                    DocumentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DocumentTypesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentInDocumentTypeList", x => new { x.DocumentId, x.DocumentTypesId });
                    table.ForeignKey(
                        name: "FK_DocumentInDocumentTypeList_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentInDocumentTypeList_DocumentType_DocumentTypesId",
                        column: x => x.DocumentTypesId,
                        principalTable: "DocumentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentInDocumentTypeList_DocumentTypesId",
                table: "DocumentInDocumentTypeList",
                column: "DocumentTypesId");
        }
    }
}
