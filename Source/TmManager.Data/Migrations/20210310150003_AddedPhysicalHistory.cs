﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class AddedPhysicalHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ChargeToLocation",
                table: "Trademarks",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalLocation",
                table: "Trademarks",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PhysicalLocationDate",
                table: "Trademarks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UsRegistrationDate",
                table: "Trademarks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TrademarkId",
                table: "PhysicalLocationHistorys",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_PhysicalLocationHistorys_TrademarkId",
                table: "PhysicalLocationHistorys",
                column: "TrademarkId");

            migrationBuilder.AddForeignKey(
                name: "FK_PhysicalLocationHistorys_Trademarks_TrademarkId",
                table: "PhysicalLocationHistorys",
                column: "TrademarkId",
                principalTable: "Trademarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhysicalLocationHistorys_Trademarks_TrademarkId",
                table: "PhysicalLocationHistorys");

            migrationBuilder.DropIndex(
                name: "IX_PhysicalLocationHistorys_TrademarkId",
                table: "PhysicalLocationHistorys");

            migrationBuilder.DropColumn(
                name: "ChargeToLocation",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "PhysicalLocation",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "PhysicalLocationDate",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "UsRegistrationDate",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "TrademarkId",
                table: "PhysicalLocationHistorys");
        }
    }
}
