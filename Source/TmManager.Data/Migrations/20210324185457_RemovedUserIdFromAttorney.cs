﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class RemovedUserIdFromAttorney : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attorneys_AspNetUsers_UserId",
                table: "Attorneys");

            migrationBuilder.DropIndex(
                name: "IX_Attorneys_UserId",
                table: "Attorneys");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Attorneys");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Attorneys",
                type: "nvarchar(450)",
                maxLength: 450,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attorneys_UserId",
                table: "Attorneys",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attorneys_AspNetUsers_UserId",
                table: "Attorneys",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
