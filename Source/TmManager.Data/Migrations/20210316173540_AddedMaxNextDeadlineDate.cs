﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class AddedMaxNextDeadlineDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Trademarks_TrademarkId",
                table: "Documents");

            migrationBuilder.AddColumn<DateTime>(
                name: "MaxNextDeadline",
                table: "Trademarks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "TrademarkId",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Trademarks_TrademarkId",
                table: "Documents",
                column: "TrademarkId",
                principalTable: "Trademarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Trademarks_TrademarkId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "MaxNextDeadline",
                table: "Trademarks");

            migrationBuilder.AlterColumn<Guid>(
                name: "TrademarkId",
                table: "Documents",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Trademarks_TrademarkId",
                table: "Documents",
                column: "TrademarkId",
                principalTable: "Trademarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
