﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class ModifiedUserContractFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "UserContact",
                newName: "ZipCode");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PhoneType",
                table: "UserContact",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetAddress",
                table: "UserContact",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientAddress",
                table: "AspNetUsers",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientName",
                table: "AspNetUsers",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LegalEntityType",
                table: "AspNetUsers",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimaryPhone",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "PhoneType",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "State",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "StreetAddress",
                table: "UserContact");

            migrationBuilder.DropColumn(
                name: "ClientAddress",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClientName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LegalEntityType",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PrimaryPhone",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "ZipCode",
                table: "UserContact",
                newName: "Name");
        }
    }
}
