﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class AddEmailToItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "TrademarkImportItems",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "TrademarkImportItems");
        }
    }
}
