﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TmManager.Data.Migrations
{
    public partial class RemovedExtraNullFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargeToLocation",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "PhysicalLocation",
                table: "Trademarks");

            migrationBuilder.DropColumn(
                name: "PhysicalLocationDate",
                table: "Trademarks");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ChargeToLocation",
                table: "Trademarks",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalLocation",
                table: "Trademarks",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PhysicalLocationDate",
                table: "Trademarks",
                type: "datetime2",
                nullable: true);
        }
    }
}
