﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Identity.Data.Entity
{
    public abstract class IdentityContext : IdentityDbContext<User>, IEntityContext
    {
        #region Ctor
        public IdentityContext()
            : base(new DbContextOptionsBuilder<IdentityContext>().UseLazyLoadingProxies().UseSqlServer(AppEnvironment.DataConnectionSettings.DbContext).Options)
        {
        }
        #endregion

        #region Select
        protected virtual IQueryable<T> Select<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return Set<T>().Where(expression);
        }
        #endregion

        #region Insert
        protected virtual void Insert<T>(T entity) where T : class
        {
            Set<T>().Add(entity);
        }
        #endregion

        #region Delete
        protected virtual void Delete<T>(T entity) where T : class
        {
            Set<T>().Attach(entity);
            Set<T>().Remove(entity);
        }
        #endregion

        #region IIdentityContext Members
        IQueryable<T> IEntityContext.Select<T>(Expression<Func<T, bool>> expression)
        {
            return Select<T>(expression);
        }
        void IEntityContext.Insert<T>(T entity)
        {
            Insert<T>(entity);
        }
        void IEntityContext.Update<T>(T entity)
        {
            Update<T>(entity);
        }
        void IEntityContext.Delete<T>(T entity)
        {
            Delete<T>(entity);
        }
        void IEntityContext.SaveChanges()
        {
            SaveChanges();
        }
        async Task IEntityContext.SaveChangesAsync()
        {
            await SaveChangesAsync();
        }
        #endregion

        #region OnModelCreating
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        #endregion
    }
}
