﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            UserModel = new List<UserModel>();
        }
        public List<UserModel> UserModel { get; set; }
        public decimal PageCount { get; set; }
        public int CurrentPageIndex { get; set; }
    }
}
