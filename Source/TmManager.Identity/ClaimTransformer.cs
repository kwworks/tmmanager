﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;

namespace TmManager.Identity
{
    public static class ClaimTransformer
    {
        public static ClaimsIdentity Transform(ClaimsIdentity source, User user)
        {
            source.AddClaim(new Claim(IdentityClaimTypes.IsAdmin, (user.UserType == UserType.Admin).ToString()));
            source.AddClaim(new Claim(IdentityClaimTypes.IsAppUser, (user.UserType == UserType.AppUser).ToString()));
            source.AddClaim(new Claim(IdentityClaimTypes.TimeZone, user.TimeZone.ToString()));
            source.AddClaim(new Claim(ClaimTypes.GivenName, (user.FirstName ?? string.Empty) + " " + (user.LastName ?? string.Empty)));
            return source;
        }

        public static IPrincipal SetupDefaultPrincipal(User user)
        {
            var newPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim> {
             new Claim(ClaimTypes.NameIdentifier,user.Id),
             new Claim(ClaimTypes.Name,user.UserName),
             new Claim(ClaimTypes.GivenName, (user.FirstName ?? string.Empty) + " " + (user.LastName ?? string.Empty)),
            }));
            Thread.CurrentPrincipal = newPrincipal;
            return newPrincipal;
        }
    }
}
