﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TmManager.Identity
{
    public class UserEventLog : EventAuditBase
    {
        [Required]
        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
