﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace TmManager.Identity
{
    public static class ClaimIdentityExtensions
    {
        private static string GetClaimValue(this IIdentity identity, string claimType)
        {
            var result = string.Empty;
            if (identity is ClaimsIdentity claimsIdentity)
            {
                var claim = claimsIdentity.Claims.SingleOrDefault(c => c.Type == claimType);
                if (claim != null)
                {
                    result = claim.Value;
                }
            }
            return result;
        }
        public static string UserId(this IIdentity identity)
        {
            return identity.GetClaimValue(ClaimTypes.NameIdentifier);
        }
    }
}
