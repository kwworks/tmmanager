﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public class AuthenticationResults
    {
        public AuthenticationResults()
        {
            UserData = new UserDataForClient();
            Errors = new HashSet<string>();
        }
        public bool IsAuthenticated { get; set; }
        public HashSet<string> Errors { get; set; }
        public string Token { get; set; }
        public UserDataForClient UserData { get; set; }

    }
    public class UserDataForClient
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? LastLogIn { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string County { get; set; }
        public string ProfilePicture { get; set; }
        public UserType UserType { get; set; }
        public int TimeZone { get; set; }
        public UserPrimaryRole PrimaryRole { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string ClientName { get; set; }
        public UserPrimaryRole SelectedRole { get; set; }
    }
}
