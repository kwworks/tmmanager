﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public static class IdentityClaimTypes
    {
        public const string IsAdmin = "is_admin";
        public const string IsAppUser = "is_appuser";
        public const string TimeZone = "time_zone";
    }
}
