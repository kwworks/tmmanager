﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public enum UserPrimaryRole
    {
        None = 0,
        Seeker = 1,
        Braider = 2
    }
}
