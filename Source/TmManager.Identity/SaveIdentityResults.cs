﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public class SaveIdentityResults
    {
        public SaveIdentityResults()
        {
            Errors = new HashSet<string>();
        }
        public string UserName { get; set; }
        public string Message { get; set; }
        public bool IsAdd { get; set; }
        public bool IsSuccess { get; set; }
        public HashSet<string> Errors { get; set; }
        public string Id { get; set; }
    }
}
