﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
namespace TmManager.Identity
{
    public class User : IdentityUser
    {
        public User()
        {
            Created = Modified = DateTimeHelper.Now();
            Active = true;
            TotalLogins = 0;
        }
        [IgnoreDataMember]
        public override string PasswordHash { get; set; }
        [IgnoreDataMember]
        public override string SecurityStamp { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public DateTime Modified { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string FullName { get; set; }
        [StringLength(100)]
        public string DisplayName { get; set; }
        [Required]
        public bool Active { get; set; }
        public DateTime? LastLogIn { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [StringLength(500)]
        public string Street2 { get; set; }
        [StringLength(500)]
        public string State { get; set; }
        [StringLength(500)]
        public string City { get; set; }
        [StringLength(50)]
        public string Postcode { get; set; }
        [StringLength(500)]
        public string County { get; set; }
        [StringLength(50)]
        public string LastLoginFromIp { get; set; }
        [Required]
        public int TotalLogins { get; set; }
        public override string ToString() { return UserName; }
        [StringLength(1000)]
        public string ProfilePicture { get; set; }
        public UserType UserType { get; set; }
        [Required]
        public int TimeZone { get; set; }
        public DateTime? ForgotPasswordExpiry { get; set; }
        public UserPrimaryRole PrimaryRole { get; set; }
        public Guid UserContactsId { get; set; }
        public string AccessCode { get; set; }
        [StringLength(500)]
        public string ClientName { get; set; }
        [StringLength(500)]
        public string LegalEntityType { get; set; }
        [StringLength(50)]
        public string PrimaryPhone { get; set; }
        [StringLength(50)]
        public string Country { get; set; }
        [StringLength(50)]
        public string PhoneType { get; set; }
        [StringLength(50)]
        public string ZipCode { get; set; }
        public virtual List<UserContact> UserContacts { get; set; } = new List<UserContact>();
        public virtual ICollection<UserEventLog> UserEventLogs { get; set; } = new List<UserEventLog>();
    }
}
