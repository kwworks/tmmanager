﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;

namespace TmManager.Identity
{
    public class ClaimsPrincipalFactory : UserClaimsPrincipalFactory<User>
    {
        public ClaimsPrincipalFactory(
        Microsoft.AspNetCore.Identity.UserManager<User> userManager,
        IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, optionsAccessor)
        {
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(User user)
        {
            var identity = await base.GenerateClaimsAsync(user);
            identity.AddClaim(new Claim(ClaimTypes.Email, user.Email ?? ""));
            identity.AddClaim(new Claim(ClaimTypes.GivenName, user.FullName ?? string.Format("{0} {1}", user.FirstName, user.LastName)));
            var badData = identity.Claims.Where(c => c.Type == ClaimTypes.Name).Skip(1).ToArray();
            for (int i = 0; i < badData.Length; i++)
            {
                identity.RemoveClaim(badData[i]);
            }
            return identity;
        }
    }
}
