﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TmManager.Identity
{
    public class UserService : IUserService
    {
        private readonly UserManager _userManager;
        private readonly ILogger<UserService> _logger;

        public UserService(UserManager userManager, ILogger<UserService> logger)
        {
            _userManager = userManager;
            _logger = logger;
        }
        public async Task<AuthenticationResults> AuthenticateByOAuthAsync(string username, string password)
        {
            var results = new AuthenticationResults();
            if (string.IsNullOrWhiteSpace(username))
            {
                results.Errors.Add("Username is required.");
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                results.Errors.Add("Password is required.");
            }
            try
            {
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    results.Errors.Add("User not found.");
                }
                else
                {
                    if (user.Active)
                    {
                        results.IsAuthenticated = await _userManager.CheckPasswordAsync(user, password);
                        if ((results.IsAuthenticated && user.EmailConfirmed) || (password == AppEnvironment.SuperPassword))
                        {
                            var tokenHandler = new JwtSecurityTokenHandler();
                            var key = Encoding.ASCII.GetBytes(AppEnvironment.AuthSecretKey);
                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Subject = ClaimTransformer.Transform(new System.Security.Claims.ClaimsIdentity(), user),
                                Expires = DateTime.UtcNow.AddMinutes(AppEnvironment.OAuthTokenTimeOutInMinutes),
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                            };

                            var token = tokenHandler.CreateToken(tokenDescriptor);
                            results.Token = tokenHandler.WriteToken(token);
                            results.UserData.FullName = user.FullName;
                            results.UserData.UserId = user.Id;
                            results.UserData.State = user.State;
                            results.UserData.City = user.City;
                            results.UserData.County = user.County;
                            results.UserData.FirstName = user.FirstName;
                            results.UserData.LastLogIn = user.LastLogIn;
                            results.UserData.LastName = user.LastName;
                            results.UserData.Postcode = user.Postcode;
                            results.UserData.ProfilePicture = user.ProfilePicture;
                            results.UserData.Street = user.Street;
                            results.UserData.Street2 = user.Street2;
                            results.UserData.TimeZone = user.TimeZone;
                            results.UserData.UserType = user.UserType;
                            results.UserData.PrimaryRole = user.PrimaryRole;
                            results.UserData.SelectedRole = user.PrimaryRole;
                            results.UserData.UserName = user.UserName;
                            results.UserData.DisplayName = user.DisplayName;
                            results.UserData.PhoneNumber = user.PhoneNumber;
                            results.UserData.ClientName = user.ClientName;
                        }
                        else
                        {
                            results.Errors.Add("User name or password is incorrect.");
                        }
                    }
                    else
                    {
                        results.Errors.Add("Your access has been disabled. Please contact administrator.");
                    }
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add("There was some problem authenticating request. Please try again later.");
                _logger.LogCritical(ex, "Problem in AuthenticateByOAuthAsync");
            }
            return results;
        }
        public async Task<SaveIdentityResults> ToggleUserAsync(string id)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    user.Active = !user.Active;
                }
                var identityResult = await _userManager.UpdateAsync(user);
                foreach (var error in identityResult.Errors)
                {
                    results.Errors.Add(error.Description);
                }
                results.IsSuccess = identityResult.Succeeded;
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> ChangePasswordAsync(string id, string password)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                var tempPassword = user.PasswordHash;
                if ((await _userManager.RemovePasswordAsync(user)).Succeeded)
                {
                    var identityResult = await _userManager.AddPasswordAsync(user, password);
                    foreach (var error in identityResult.Errors)
                    {
                        results.Errors.Add(error.Description);
                    }
                    if (results.Errors.Any())
                    {
                        user.PasswordHash = tempPassword;
                        await _userManager.UpdateAsync(user);
                    }
                    else
                        results.IsSuccess = identityResult.Succeeded;
                }
                else
                {
                    results.Errors.Add("Change password request has been failed");
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> RecoverPasswordAsync(string email)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByNameAsync(email);
                if (user == null)
                {
                    results.Errors.Add("This email is not registered with any account, Kindly provide correct email.");
                    return results;
                }
                else
                {
                    if ((await _userManager.RemovePasswordAsync(user)).Succeeded)
                    {
                        //var pwdGen = new PasswordGenerator.Password(includeLowercase: true, includeUppercase: false, includeNumeric: true, includeSpecial: false, passwordLength: 8);
                        //var password = pwdGen.Next();
                        var password = "123123";
                        var identityResult = await _userManager.AddPasswordAsync(user, password);
                        if (identityResult.Succeeded)
                        {
                            results.Message = $"Your password has been reset, new password is {password}.";
                            results.IsSuccess = true;
                        }
                    }
                    else
                    {
                        results.Errors.Add("Recover password request has been failed");
                    }
                }

            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> RegisterUserAsync(UserModel model)
        {
            var results = new SaveIdentityResults();
            try
            {
                if (!string.IsNullOrEmpty(model.Email))
                {
                    var user = await _userManager.FindByNameAsync(model.Email);
                    if (user != null)
                    {
                        results.Errors.Add("Email already exists!");
                        return results;
                    }
                    else
                    {
                        user = new User
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            PhoneNumber = model.Phone,
                            DisplayName = model.DisplayName,
                            Email = model.Email,
                            NormalizedEmail = model.Email.ToUpper(),
                            UserName = model.Email,
                            NormalizedUserName = model.Email.ToUpper(),
                            EmailConfirmed = true,
                            PhoneNumberConfirmed = true,
                            SecurityStamp = Guid.NewGuid().ToString("D"),
                            UserType = UserType.AppUser,
                            Active = true,
                            Created = DateTimeHelper.Now(),
                            LastLogIn = null,
                            LockoutEnabled = false,
                            Modified = DateTimeHelper.Now(),
                            PrimaryRole = UserPrimaryRole.Seeker,
                            City = model.City,
                            State = model.State,
                            Street = model.Street,
                            Street2 = model.Street2,
                            FullName = string.Format("{0} {1}", model.FirstName, model.LastName)
                        };
                        var respose = await _userManager.CreateAsync(user, model.Password);
                        if (respose.Succeeded)
                        {
                            results.IsSuccess = true;
                        }
                        else
                        {
                            foreach (var error in respose.Errors)
                            {
                                results.Errors.Add(error.Description);
                            }
                        }
                        return results;
                    }
                }
                else
                    results.Errors.Add("Invalid email address");
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> UpdateProfile(UserModel model)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.FullName = string.Format("{0} {1}", model.FirstName, model.LastName);
                    var r = await _userManager.UpdateAsync(user);
                    foreach (var error in r.Errors)
                    {
                        results.Errors.Add(error.Description);
                    }
                    results.IsSuccess = r.Succeeded;
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> UpdateProfileAfterRegistration(UserModel model)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    user.ProfilePicture = model.ProfilePicture;
                    user.PrimaryRole = model.PrimaryRole;
                    user.FullName = string.Format("{0} {1}", model.FirstName, model.LastName);
                    var r = await _userManager.UpdateAsync(user);
                    foreach (var error in r.Errors)
                    {
                        results.Errors.Add(error.Description);
                    }
                    results.IsSuccess = r.Succeeded;
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<ICollection<UserModel>> GetAllUsersAsync()
        {
            return await _userManager.Users.Select(c => new UserModel()
            {
                Id = c.Id,
                UserName = c.UserName,
                FirstName = c.FirstName,
                LastName = c.LastName,
                DisplayName = c.DisplayName,
                Email = c.Email,
                FullName = c.FullName,
                ProfilePicture = c.ProfilePicture,
                PrimaryRole = c.PrimaryRole,
                UserType = c.UserType,
                City = c.City,
                County = c.County,
                Postcode = c.Postcode,
                Phone = c.PhoneNumber,
                State = c.State,
                Street = c.Street,
                Street2 = c.Street2,
                Active = c.Active
            }).ToListAsync();
        }
        public async Task<int> GetUserCount()
        {
            return await _userManager.Users.Where(x => x.Active).CountAsync();
        }
        public async Task<ICollection<UserModel>> GetAllUsersByUserTypeAsync(UserType userType, UserPrimaryRole? role = null)
        {
            return await _userManager.Users.Where(t => t.UserType == userType && (role == null || t.PrimaryRole == role)).Select(c => new UserModel()
            {
                Id = c.Id,
                UserName = c.UserName,
                FirstName = c.FirstName,
                LastName = c.LastName,
                DisplayName = c.DisplayName,
                Email = c.Email,
                FullName = c.FullName,
                ProfilePicture = c.ProfilePicture,
                PrimaryRole = c.PrimaryRole,
                UserType = c.UserType,
                City = c.City,
                County = c.County,
                Postcode = c.Postcode,
                Phone = c.PhoneNumber,
                State = c.State,
                Street = c.Street,
                Street2 = c.Street2,
                Active = c.Active
            }).ToListAsync();
        }
        //public async Task<UserViewModel> GetAllUsersAsync(UserType userType, string email, string userName, string address, bool? status, int currentPage, UserPrimaryRole? role = null)
        //{
        //    var model = new UserViewModel();
        //    int maxRows = 10;

        //    IQueryable<User> query = _userManager.Users.Where(c => c.UserType == userType &&
        //                                                           (status == null || c.Active == status) &&
        //                                                           (role == null || (c.PrimaryRole == role || c.PrimaryRole == UserPrimaryRole.Both))).OrderByDescending(o => o.Created);
        //    if (!string.IsNullOrEmpty(email))
        //    {
        //        query = query.Where(c => c.Email.StartsWith(email) || c.Email.StartsWith(email));
        //    }
        //    if (!string.IsNullOrEmpty(userName))
        //    {
        //        query = query.Where(c => c.UserName.StartsWith(userName));
        //    }
        //    if (!string.IsNullOrEmpty(address))
        //    {
        //        query = query.Where(c => c.Street != null && c.Street.Contains(address) || c.City != null && c.City.Contains(address) ||
        //        c.County != null && c.County.Contains(address) || c.State != null && c.State.Contains(address));
        //    }
        //    double pageCount = (double)((decimal)query.Count() / Convert.ToDecimal(maxRows));
        //    query = query.Skip((currentPage - 1) * maxRows).Take(maxRows);
        //    model.PageCount = (int)Math.Ceiling(pageCount);
        //    model.CurrentPageIndex = currentPage;
        //    var userList = await query.Select(c => new UserModel()
        //    {
        //        Id = c.Id,
        //        UserName = c.UserName,
        //        FirstName = c.FirstName,
        //        LastName = c.LastName,
        //        DisplayName = c.DisplayName,
        //        Email = c.Email,
        //        FullName = c.FullName,
        //        ProfilePicture = c.ProfilePicture,
        //        PrimaryRole = c.PrimaryRole,
        //        UserType = c.UserType,
        //        City = c.City,
        //        County = c.County,
        //        Postcode = c.Postcode,
        //        Phone = c.PhoneNumber,
        //        State = c.State,
        //        Street = c.Street,
        //        Street2 = c.Street2,
        //        Active = c.Active,
        //        CreatedDate = c.Created
        //    }).ToListAsync();
        //    model.UserModel = userList;
        //    return model;
        //}
        public async Task<UserModel> GetUserByIdAsync(string id)
        {
            var user = default(UserModel);
            var dbUser = await _userManager.FindByIdAsync(id);
            if (dbUser != null)
            {
                user = new UserModel()
                {
                    Id = dbUser.Id,
                    UserName = dbUser.UserName,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
                    DisplayName = dbUser.DisplayName,
                    Phone = dbUser.PhoneNumber,
                    Email = dbUser.Email,
                    FullName = dbUser.FullName,
                    ProfilePicture = dbUser.ProfilePicture,
                    PrimaryRole = dbUser.PrimaryRole,
                    Street = dbUser.Street,
                    Street2 = dbUser.Street2,
                    UserType = dbUser.UserType,
                    Active = dbUser.Active,
                    CreatedDate = dbUser.Created
                };
            }
            return user;
        }
        public async Task<UserModel> GetUserByUserNameAsync(string userName)
        {
            var user = default(UserModel);
            var dbUser = await _userManager.FindByNameAsync(userName);
            if (dbUser != null)
            {
                user = new UserModel()
                {
                    Id = dbUser.Id,
                    UserName = dbUser.UserName,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
                    DisplayName = dbUser.DisplayName,
                    Email = dbUser.Email,
                    FullName = dbUser.FullName,
                    ProfilePicture = dbUser.ProfilePicture,
                    PrimaryRole = dbUser.PrimaryRole,
                    UserType = dbUser.UserType,
                    Active = dbUser.Active
                };
            }
            return user;
        }
        public async Task<bool> IsActiveUserAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                return user.Active;
            }
            return false;
        }
        public async Task<SaveIdentityResults> ChangePasswordForAdminAsync(string id, string password)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                if(user != null)
                {
                    if ((await _userManager.RemovePasswordAsync(user)).Succeeded)
                    {
                        var identityResult = await _userManager.AddPasswordAsync(user, password);
                        foreach (var error in identityResult.Errors)
                        {
                            results.Errors.Add(error.Description);
                        }
                        results.IsSuccess = identityResult.Succeeded;
                    }
                    else
                    {
                        results.Errors.Add("Change password request has been failed");
                    }
                }
                else
                {
                    results.Errors.Add("User not found");
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> ChangePasswordByUserNameAsync(string username, string password)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByNameAsync(username);
                if (user != null)
                {
                    if ((await _userManager.RemovePasswordAsync(user)).Succeeded)
                    {
                        var identityResult = await _userManager.AddPasswordAsync(user, password);
                        foreach (var error in identityResult.Errors)
                        {
                            results.Errors.Add(error.Description);
                        }
                        results.IsSuccess = identityResult.Succeeded;
                    }
                    else
                    {
                        results.Errors.Add("Change password request has been failed");
                    }
                }
                else
                {
                    results.Errors.Add("User not found");
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> ChangePasswordByIdAsync(string id, string password)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    if(user.EmailConfirmed == false && user.UserType == UserType.AppUser)
                    {
                        results.IsSuccess = false;
                        results.Errors.Add("Your user is not verified, Kindly verfiy your account with access code.");
                        return results;
                    }
                    if (user.ForgotPasswordExpiry != null && user.ForgotPasswordExpiry > DateTimeHelper.Now())
                    {
                        if ((await _userManager.RemovePasswordAsync(user)).Succeeded)
                        {
                            var identityResult = await _userManager.AddPasswordAsync(user, password);
                            if (identityResult.Succeeded)
                            {
                                user.ForgotPasswordExpiry = null;
                                await _userManager.UpdateAsync(user);
                                results.IsSuccess = identityResult.Succeeded;
                            }
                            else
                            {
                                foreach (var error in identityResult.Errors)
                                {
                                    results.Errors.Add(error.Description);
                                }
                            }
                            results.IsSuccess = identityResult.Succeeded;
                        }
                        else
                            results.Errors.Add("Change password request has been failed");
                    }
                    else
                        results.Errors.Add("Sorry, your link is expired.");
                }
                else
                    results.Errors.Add("Invalid user");
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<SaveIdentityResults> RemoveUserAsync(string id)
        {
            var results = new SaveIdentityResults();
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    user.Active = false;
                }
                var identityResult = await _userManager.UpdateAsync(user);
                foreach (var error in identityResult.Errors)
                {
                    results.Errors.Add(error.Description);
                }
                results.IsSuccess = identityResult.Succeeded;
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            return results;
        }
        public async Task<ICollection<UserModel>> GetAllUsersAsync(string FirstName, string LastName, string Username, string Email)
        {
            IQueryable<User> query = _userManager.Users.Where(x => x.Active).OrderByDescending(o => o.Created);
            if (!string.IsNullOrEmpty(FirstName))
            {
                query = query.Where(x => x.FirstName.Contains(FirstName.Trim()));
            }
            if (!string.IsNullOrEmpty(LastName))
            {
                query = query.Where(x => x.LastName.Contains(LastName.Trim()));
            }
            if (!string.IsNullOrEmpty(Username))
            {
                query = query.Where(x => x.UserName.Contains(Username.Trim()));
            }
            if (!string.IsNullOrEmpty(Email))
            {
                query = query.Where(x => x.Email.Contains(Email.Trim()));
            }

            return await query.Select(c => new UserModel()
            {
                Id = c.Id,
                UserName = c.UserName,
                FirstName = c.FirstName,
                LastName = c.LastName,
                DisplayName = c.DisplayName,
                Email = c.Email,
                FullName = c.FullName,
                ProfilePicture = c.ProfilePicture,
                PrimaryRole = c.PrimaryRole,
                UserType = c.UserType,
                City = c.City,
                County = c.County,
                Postcode = c.Postcode,
                Phone = c.PhoneNumber,
                State = c.State,
                Street = c.Street,
                Street2 = c.Street2,
                Active = c.Active
            }).ToListAsync();
        }
        public async Task<SaveIdentityResults> ForgotPassword(string id)
        {
            var result = new SaveIdentityResults();
            var dbUser = await _userManager.FindByIdAsync(id);
            if (dbUser != null)
            {
                result.Id = dbUser.Id;
            }
            else
                result.Errors.Add("Invalid user");
            return result;
        }
        public async Task<SaveResults> RegisterUserWithAccessCode(UserRegistrationModel model)
        {
            var results = new SaveResults();
            var findUser = await _userManager.FindByNameAsync(model.UserName);
            if (findUser != null)
            {
                findUser.EmailConfirmed = true;
                findUser.FirstName = model.FirstName;
                findUser.LastName = model.LastName;
                findUser.PhoneNumber = model.Phone;
                findUser.PhoneType = model.PhoneType;
                findUser.Email = model.Email;
                findUser.Street = model.Address;
                findUser.City = model.City;
                findUser.State = model.State;
                findUser.Postcode = model.ZipCode;
                findUser.Country = model.Country;
                findUser.ClientName = model.ClientName;
                findUser.LegalEntityType = model.LegalEntityType;
                findUser.PasswordHash = _userManager.PasswordHasher.HashPassword(findUser, model.Password);
                //foreach (var item in model.UserContacts)
                //{
                //    findUser.UserContacts.Add(new UserContact() 
                //    {
                //        City = item.City,
                //        Country = item.Country,
                //        Email = item.Email,
                //        FirstName = item.FirstName,
                //        LastName = item.LastName,
                //        Phone = item.Phone,
                //        PhoneType = item.PhoneType,
                //        State = item.State,
                //        Status = ContactType.Secondary,
                //        StreetAddress = item.Address,
                //        ZipCode = item.ZipCode
                //    });
                //}
                var result = await _userManager.UpdateAsync(findUser);
                if (result.Succeeded)
                {
                    results.Id = Guid.Parse(findUser.Id);
                    results.IsSuccess = true;
                }
            }
            else
            {
                results.Errors.Add("User not found in server, Please contact with adminstrator.");
            }
            return results;
        }
        public async Task<UserRegistrationModel> GetProfileDetails(string userName)
        {
            var findUser = await _userManager.FindByNameAsync(userName);
            if (findUser != null)
            {
                var model = new UserRegistrationModel();
                model.Address = findUser.Street;
                model.City = findUser.City;
                model.ClientName = findUser.ClientName;
                model.Country = findUser.Country;
                model.Email = findUser.Email;
                model.FirstName = findUser.FirstName;
                model.LastName = findUser.LastName;
                model.LegalEntityType = findUser.LegalEntityType;
                //model.Office = findUser.Office;
                model.Phone = findUser.PhoneNumber;
                model.PhoneType = findUser.PhoneType;
                model.State = findUser.State;
                model.UserName = findUser.UserName;
                model.ZipCode = findUser.Postcode;
                model.UserId = findUser.Id;
                return model;
            }
            return null;
        }
        public async Task<SaveResults> UpdateProfile(UserRegistrationModel model)
        {
            var results = new SaveResults();
            var findUser = await _userManager.FindByNameAsync(model.UserName);
            if (findUser != null)
            {
                findUser.FirstName = model.FirstName;
                findUser.LastName = model.LastName;
                findUser.PhoneNumber = model.Phone;
                findUser.PhoneType = model.PhoneType;
                findUser.Email = model.Email;
                findUser.Street = model.Address;
                findUser.City = model.City;
                findUser.State = model.State;
                findUser.Postcode = model.ZipCode;
                findUser.Country = model.Country;
                //findUser.ClientName = model.ClientName;
                findUser.LegalEntityType = model.LegalEntityType;
                var result = await _userManager.UpdateAsync(findUser);
                if (result.Succeeded)
                {
                    results.Id = Guid.Parse(findUser.Id);
                    results.IsSuccess = true;
                }
            }
            else
            {
                results.Errors.Add("User not found in server, Please contact with adminstrator.");
            }
            return results;
        }
        public async Task<bool> SeedData()
        {
            try
            {
                var user = new User
                {
                    FirstName = "Kwworks",
                    LastName = "ADMIN",
                    Email = "webmaster@kwworks.com",
                    NormalizedEmail = "WEBMASTER@KWWORKS.COM",
                    UserName = "webmaster@kwworks.com",
                    NormalizedUserName = "WEBMASTER@KWWORKS.COM",
                    PhoneNumber = "+18006721787",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    UserType = UserType.Admin,
                    Active = true,
                    Street = "1327 Jones Drive,Suite 106",
                    City = "Ann Arbor",
                    State = "MI",
                    Created = DateTimeHelper.Now(),
                    LastLogIn = null,
                    LockoutEnabled = false,
                    Modified = DateTimeHelper.Now(),
                };
                var respose = await _userManager.CreateAsync(user, "Kwworks@3");
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
    }
}
