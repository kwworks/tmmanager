﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TmManager.Identity
{
    public class UserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        [Required(ErrorMessage = "First Name field is required")]
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(50)]
        public string DisplayName { get; set; }
        [DisplayName("Full Name")]
        public string FullName { get; set; }
        [Display(Name = "Active")]
        public bool Active { get; set; }
        [Required(ErrorMessage = "Email field is required")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [UIHint("EmailAddress")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password field is required")]
        [Display(Name = "Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
        public string ProfilePicture { get; set; }
        public string Phone { get; set; }
        public UserPrimaryRole PrimaryRole { get; set; }
        public UserType UserType { get; set; }
        [StringLength(500)]
        public string Street { get; set; }
        [StringLength(500)]
        public string Street2 { get; set; }
        [StringLength(500)]
        public string State { get; set; }
        [StringLength(500)]
        public string City { get; set; }
        [Display(Name = "Zip")]
        [StringLength(50)]
        public string Postcode { get; set; }
        [StringLength(500)]
        public string County { get; set; }
        public string Address { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? SubscribedDate { get; set; }

    }
}
