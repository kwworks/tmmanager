﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TmManager.Identity
{
    public interface IUserService
    {
        Task<AuthenticationResults> AuthenticateByOAuthAsync(string username, string password);
        Task<SaveIdentityResults> ChangePasswordAsync(string id, string password);
        Task<SaveIdentityResults> RecoverPasswordAsync(string email);
        Task<SaveIdentityResults> RegisterUserAsync(UserModel model);
        Task<SaveIdentityResults> UpdateProfile(UserModel model);
        Task<ICollection<UserModel>> GetAllUsersAsync();
        Task<ICollection<UserModel>> GetAllUsersAsync(string FirstName, string LastName, string Username, string Email);
        Task<int> GetUserCount();
        Task<ICollection<UserModel>> GetAllUsersByUserTypeAsync(UserType userType, UserPrimaryRole? role = null);
        //Task<UserViewModel> GetAllUsersAsync(UserType userType, string email, string userName, string address, bool? status, int currentPage, UserPrimaryRole? role = null);
        Task<UserModel> GetUserByIdAsync(string id);
        Task<SaveIdentityResults> UpdateProfileAfterRegistration(UserModel model);
        Task<UserModel> GetUserByUserNameAsync(string createdBy);
        Task<SaveIdentityResults> ChangePasswordForAdminAsync(string id, string password);
        Task<SaveIdentityResults> ToggleUserAsync(string id);
        Task<bool> IsActiveUserAsync(string userName);
        Task<SaveIdentityResults> ChangePasswordByUserNameAsync(string userName, string password);
        Task<SaveIdentityResults> ChangePasswordByIdAsync(string id, string password);
        Task<SaveIdentityResults> RemoveUserAsync(string id);
        Task<SaveIdentityResults> ForgotPassword(string id);
        Task<SaveResults> RegisterUserWithAccessCode(UserRegistrationModel model);
        Task<UserRegistrationModel> GetProfileDetails(string userName);
        Task<SaveResults> UpdateProfile(UserRegistrationModel model);
        Task<bool> SeedData();
    }
}
