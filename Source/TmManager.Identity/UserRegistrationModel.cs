﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Identity
{
    public class UserRegistrationModel
    {
        public UserRegistrationModel()
        {
            UserContacts = new UserContactModel[] { };
        }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Office { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string PhoneType { get; set; }
        public string ClientName { get; set; }
        public UserContactModel[] UserContacts { get; set; }
        public string LegalEntityType { get; set; }
    }

    public class UserContactModel
    {
        public Guid? Id { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Office { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Password { get; set; }
        public PhoneType PhoneType { get; set; }
        public string UserName { get; set; }
    }
    public class UserChangePasswordModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
