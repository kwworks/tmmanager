﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TmManager.Identity
{
    public enum UserType
    {
        Admin = 1,
        AppUser = 2,
        Both = 3
    }

    public enum ContactType
    {
        Primary = 0,
        Secondary = 1
    }
    public enum PhoneType
    {
        Office = 0,
        Fax = 1,
        Home=  2,
        Mobile = 3,
        Company = 4
    }
}
