﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TmManager.Notifications
{
    public interface INotificationRepository : IDomainRepository<Notification>
    {
        Task<Notification> FindNotificationAsync(NotificationSpecification specification);
        Task<List<Notification>> ListNotificationAsync(NotificationSpecification specification);
    }
}
