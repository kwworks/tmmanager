﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Notifications
{
    
    public class NotificationSpecification : Specification<Notification>
    {
        public NotificationSpecification(Expression<Func<Notification, bool>> predicate) : base(predicate)
        {
        }
        public static NotificationSpecification GetAllByUserName(string userName)
        {
            return new NotificationSpecification(x => x.IsActive == true && x.UserName == userName);
        }
        public static NotificationSpecification GetUnRead(string userName)
        {
            return new NotificationSpecification(x => x.IsActive == true && x.UserName == userName && x.IsRead == false);
        }
    }
}
