﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Notifications
{
    public class Notification : DomainObject
    {
        [StringLength(100)]
        public string From { get; set; }
        [StringLength(2000)]
        public string Message { get; set; }
        public string UserName { get; set; }
        public bool IsRead { get; set; }
    }
}
