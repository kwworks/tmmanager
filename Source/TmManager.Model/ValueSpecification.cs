﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace TmManager
{
    public class ValueSpecification<T> : Specification<T> where T : Value
    {
        private ValueSpecification(Expression<Func<T, bool>> predicate)
            : base(predicate)
        {
        }
        public static ValueSpecification<T> GetAll()
        {
            return new ValueSpecification<T>(p => p.IsActive);
        }
        public static ValueSpecification<T> Matches(string name)
        {
            return new ValueSpecification<T>(p => p.Name.Contains(name));
        }
        public static ValueSpecification<T> Id(Guid id)
        {
            return new ValueSpecification<T>(p => p.Id == id);
        }
        public static ValueSpecification<T> Name(string name)
        {
            return new ValueSpecification<T>(p => p.Name == name);
        }
        public static ValueSpecification<T> SortOrder(int sortOrder)
        {
            return new ValueSpecification<T>(p => p.SortingOrder == sortOrder);
        }
    }
}
