﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TradeMarkDataLog : TableEntity
    {
        public TradeMarkDataLog()
        {

        }
        public TradeMarkDataLog(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public string TrademarkDetailJson { get; set; }
        public string TrademarkDocumentXml { get; set; }
        public string Action { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime Date { get; set; }
        public string UserInfo { get; set; }
    }
}
