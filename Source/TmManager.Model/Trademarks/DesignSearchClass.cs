﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DesignSearchClass : ModifiableDomainObject
    {
        [StringLength(50)]
        [Required]
        public string Code { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
        public virtual ICollection<DesignSearchDescriptionClass> DesignSearchDescriptionClasses { get; set; } = new List<DesignSearchDescriptionClass>();
    }
}
