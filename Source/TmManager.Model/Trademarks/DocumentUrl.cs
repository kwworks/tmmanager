﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DocumentUrl : DomainObject
    {
        public Guid DocumentId { get; set; }
        public virtual Document Document { get; set; }
        [StringLength(2000)]
        [Required]
        public string Url { get; set; }
        [StringLength(200)]
        public string MediaType { get; set; }
    }
}
