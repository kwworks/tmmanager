﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class GoodAndServiceClass : ModifiableDomainObject
    {
        [StringLength(10)]
        [Required]
        public string Code { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        public GoodAndServiceClassType Type { get; set; }
        public virtual ICollection<GoodAndService> GoodAndServices { get; set; }
    }
}
