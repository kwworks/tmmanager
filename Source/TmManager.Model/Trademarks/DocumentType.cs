﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DocumentType : DomainObject
    {
        [StringLength(10)]
        [Required]
        public string Code { get; set; }
        [StringLength(10)]
        public string DocumentTypeCode { get; set; }
        [StringLength(10)]
        public string CategoryTypeCode { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        [StringLength(1000)]
        public string CodeDescription { get; set; }
        public virtual ICollection<Document> Documents { get; set; } = new List<Document>();
    }
}
