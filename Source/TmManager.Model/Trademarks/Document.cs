﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class Document : DomainObject
    {
        public const string STATEMENT_OF_USE = "Statement of Use";
        public const string NOTICE_OF_ALLOWANCE = "Notice of Allowance";
        public long SerialNumber { get; set; }
        public DateTime? MailRoomDate { get; set; }
        public DateTimeOffset? ScanDateTime { get; set; }
        public int TotalPageQuantity { get; set; }
        public string SourceSystem { get; set; }
        public Guid DocumentTypeId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid DocumentUrlId { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
        public virtual ICollection<DocumentUrl> DocumentUrls { get; set; } = new List<DocumentUrl>();
        public virtual DocumentType DocumentType { get; set; }
        public virtual ICollection<DocumentCategory> DocumentCategorys { get; set; } = new List<DocumentCategory>();
    }
}
