﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkSpecification : Specification<Trademarks.Trademark>
    {
        public TrademarkSpecification(Expression<Func<Trademarks.Trademark, bool>> predicate) : base(predicate)
        {
        }
        public static TrademarkSpecification GetTrademarkByEmail(string email)
        {
            return new TrademarkSpecification(x => x.User.Email == email);
        }
        public static TrademarkSpecification GetTrademarkForUnAttorney(string email, string name)
        {
            return new TrademarkSpecification(x => x.Attorney.Email == email || x.Attorney.Name.Contains(name));
        }
        public static TrademarkSpecification GetOwnerDetailByEmail(string email)
        {
            return new TrademarkSpecification(x => x.OwnerGroups.Any(x => x.Email == email));
        }
        public static TrademarkSpecification GetTrademarksByOwner(string email)
        {
            return new TrademarkSpecification(x => x.OwnerGroups.Any(x => x.Email == email));
        }
        public static TrademarkSpecification GetTrademarkById(Guid id)
        {
            return new TrademarkSpecification(x => x.Id == id);
        }
        public static TrademarkSpecification SyncBatchTrademarks()
        {
            var date = DateTimeHelper.Now().Date;
            return new TrademarkSpecification(x => x.SyncDate.Date < date);
        }
        public static TrademarkSpecification GetTrademark()
        {
            return new TrademarkSpecification(x => x.IsActive);
        }
        public static TrademarkSpecification VerifyTrademark(long serialNumber, string email)
        {
            return new TrademarkSpecification(x => x.SerialNumber == serialNumber && x.Attorney.Email == email);
        }
        public static TrademarkSpecification GetBySerialNumber(long serialNumbers)
        {
            return new TrademarkSpecification(x => x.SerialNumber == serialNumbers);
        }
        public static TrademarkSpecification GetByStatus(string status)
        {
            if (!string.IsNullOrEmpty(status))
                return new TrademarkSpecification(x => x.Tm5StatusDescription.StartsWith(status));
            else
                return new TrademarkSpecification(x => x.Id != Guid.Empty);
        }
        public static TrademarkSpecification GetByFilters(DateTime? StartDate, DateTime? EndDate, string status)
        {
            var startDate = DateTimeHelper.Today().AddDays(-14);
            var endDate = DateTimeHelper.Today().AddDays(15).AddTicks(-1);
            if (EndDate != null)
            {
                endDate = (DateTime)EndDate;
                endDate = endDate.AddDays(1).AddMilliseconds(-1);
            }
            if (status != null)
            {
                if (StartDate != null && EndDate != null)
                    return new TrademarkSpecification(x => x.Tm5StatusDescription.StartsWith(status) && x.Created >= StartDate && x.Created <= endDate);
                else if (StartDate != null && EndDate == null)
                    return new TrademarkSpecification(x => x.Tm5StatusDescription.StartsWith(status) && x.Created >= StartDate && x.Created <= endDate);
                else if (StartDate == null && EndDate != null)
                    return new TrademarkSpecification(x => x.Tm5StatusDescription.StartsWith(status) && x.Created <= endDate);
                else
                    return new TrademarkSpecification(x => x.Tm5StatusDescription.StartsWith(status));
            }
            else
            {
                if (StartDate != null && EndDate != null)
                    return new TrademarkSpecification(x => x.Created >= StartDate && x.Created <= endDate);
                else if (StartDate != null && EndDate == null)
                    return new TrademarkSpecification(x => x.Created >= StartDate && x.Created <= endDate);
                else if (StartDate == null && EndDate != null)
                    return new TrademarkSpecification(x => x.Created <= endDate);
                else
                    return new TrademarkSpecification(x => x.Created >= startDate && x.Created <= endDate);
            }
        }
        public static TrademarkSpecification GetByProsecutionHistoryDate(DateTime? StartDate, DateTime? EndDate)
        {
            var startDate = DateTimeHelper.Today().AddDays(-7);
            var endDate = DateTimeHelper.Today();
            if (StartDate == null && EndDate == null)
            {
                startDate = DateTimeHelper.Today().AddDays(-7);
                endDate = DateTimeHelper.Today().AddDays(1).AddTicks(-1);
            }
            if (EndDate != null)
            {
                endDate = (DateTime)EndDate;
                endDate = endDate.AddDays(1).AddMilliseconds(-1);
            }
            return new TrademarkSpecification(x => x.ProsecutionHistorys.Any(s => s.EntryNumber == 0));
            //return new TrademarkSpecification(x => x.ProsecutionHistorys.Any(p => p.EntryDate >= startDate) && x.ProsecutionHistorys.Any(p => p.EntryDate <= endDate));
        }
        public static TrademarkSpecification GetUnknownAttorney()
        {
            return new TrademarkSpecification(x => x.Attorney.Name == string.Empty);
        }
        public static TrademarkSpecification GetBySerialNumbers(List<long> serialNumbers)
        {
            return new TrademarkSpecification(x => serialNumbers.Contains(x.SerialNumber));
        }
        public static TrademarkSpecification GetTrademarkByTrademarkEmail(string email)
        {
            return new TrademarkSpecification(x => x.CreatedBy.Contains(email));
        }
        public static TrademarkSpecification GetByUserUd(string userId)
        {
            return new TrademarkSpecification(x => x.User != null && x.User.Id == userId);
        }
    }
}