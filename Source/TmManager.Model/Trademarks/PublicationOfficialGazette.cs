﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class PublicationOfficialGazette : DomainObject
    {
        public Guid TrademarkId { get; set; }
        public DateTime? IssueDate { get; set; }
        [StringLength(100)]
        public string StatusCodeText { get; set; }
        [StringLength(10)]
        public string StatusCode { get; set; }
        [StringLength(10)]
        public string CategoryCode { get; set; }
        [StringLength(100)]
        public string CategoryCodeText { get; set; }
        [StringLength(500)]
        public string CategoryReason { get; set; }
        public DateTime? ActionDate { get; set; }
        public long RegistrationNumber { get; set; }
        public virtual Trademark Trademark { get; set; }
    }
}
