﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkImportFile : DomainObject
    {
        [Required]
        [StringLength(1000)]
        public string Name { get; set; }
        public int Size { get; set; }
        public virtual ICollection<TrademarkImportItem> TrademarkImportItems { get; set; } = new List<TrademarkImportItem>();
    }
}
