﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public interface IGoodAndServiceClassRepository : IDomainRepository<GoodAndServiceClass>
    {
        Task<GoodAndServiceClass> FindGoodAndServiceClassAsync(GoodAndServiceClassSpecification specification);
        Task<List<GoodAndServiceClass>> ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification specification);
    }
}
