﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public interface IDesignSearchClassRepository : IDomainRepository<DesignSearchClass>
    {
        Task<DesignSearchClass> FindDesignSearchClassAsync(DesignSearchClassSpecification specification);
        Task<List<DesignSearchClass>> ListDesignSearchClassAsync(DesignSearchClassSpecification specification);
    }
}
