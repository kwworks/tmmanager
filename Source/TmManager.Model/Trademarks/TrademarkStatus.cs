﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public enum TrademarkStatus
    {
        Pending = 0,
        TrademarkSuccess = 1,
        TrademarkInSystem = 2,
        DifferentAssociate = 3,
        TrademarkDead = 4,
        TrademarkNotFound = 5,
        TrademarkLinked = 6,
        TrademarkAccountCreated = 7,
        TrademarkAttorneyIssue = 8,
        TrademarkEmailNotFound = 9,
        TrademarkFailed = 10,
        Accepted = 11,
        Rejected = 12,
        Error = 13,
        SystemError = 14,
        Processing = 15
    }
}
