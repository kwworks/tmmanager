﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class GoodAndServiceClassSpecification : Specification<GoodAndServiceClass>
    {
        public GoodAndServiceClassSpecification(Expression<Func<GoodAndServiceClass, bool>> predicate) : base (predicate)
        {
        }
        public static GoodAndServiceClassSpecification GetByCode(string code)
        {
            return new GoodAndServiceClassSpecification(x => x.Code == code);
        }
        public static GoodAndServiceClassSpecification GetByNames(List<string> names)
        {
            return new GoodAndServiceClassSpecification(x => names.Any(y => y == x.Code));
        }
    }
}
