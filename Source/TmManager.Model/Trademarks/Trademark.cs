﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;
using TmManager.Identity;

namespace TmManager.Trademarks
{
    public class Trademark : ModifiableDomainObject
    {
        [StringLength(200)]
        public string ExaminerNumber { get; set; }
        [StringLength(1000)]
        public string ExaminerName { get; set; }
        public int Status { get; set; }
        public long SerialNumber { get; set; }
        public bool FiledAsTeasPlusApp { get; set; }
        public bool CurrentlyAsTeasPlusApp { get; set; }
        public bool FiledAsTeasRfApp { get; set; }
        public bool CurrentlyAsTeasRfApp { get; set; }
        public bool SupplementalRegister { get; set; }
        public bool AmendPrincipal { get; set; }
        public bool AmendSupplemental { get; set; }
        public bool TrademarkStatus { get; set; } 
        public bool CertificationMark { get; set; }
        public bool ServiceMark { get; set; }
        public bool CollectiveMembershipMark { get; set; }
        public bool CollectiveServiceMark { get; set; }
        public bool CollectiveTradeMark { get; set; }
        public bool IsStandard { get; set; } 
        public bool IsColorDrawing { get; set; }
        public bool IsSection2f { get; set; }
        public bool IsSection2fPartial { get; set; }
        public bool IsOthers { get; set; }
        public bool IsPreviousRegistered { get; set; }
        public int CommunityLegalServiceTotal { get; set; }
        public bool IsFiledUse { get; set; }
        public bool IsFiledInternationLaw { get; set; }
        public bool IsFiled44d { get; set; }
        public bool IsFiled44e { get; set; }
        public bool IsFiled66a { get; set; }
        public bool IsFiledNoBasis { get; set; }
        public bool IsUseCurrent { get; set; }
        public bool IsInternationalLawCurrent { get; set; }
        public bool IsSect44eCurrent { get; set; }
        public bool IsSect66aCurrent { get; set; }
        public bool IsNoBasisCurrent { get; set; }
        public bool IsUseAmended { get; set; }
        public bool IsInternationalLawAmended { get; set; }
        public bool IsSect44dAmended { get; set; }
        public bool IsSect44eAmended { get; set; }
        public bool IsSect8Filed { get; set; }
        public bool IsSect8Acpt { get; set; }
        public bool IsSect8PartialAcpt { get; set; }
        public bool IsSect15Filed { get; set; }
        public bool IsSect15Ack { get; set; }
        public bool IsSect71Filed { get; set; }
        public bool IsSect71Acpt { get; set; }
        public bool IsSect71PartialAcpt { get; set; }
        public bool IsRenewalFiled { get; set; }
        public bool IsChangeInRegistration { get; set; }
        public bool Sect44dCurrent { get; set; }
        [StringLength(50)]
        public string MarkDrawingCode { get; set; }
        [StringLength(50)]
        public string LawOffAssignedCode { get; set; }
        [StringLength(50)]
        public string CurrentLocationCode { get; set; }
        [StringLength(1000)]
        public string ExternalStatusDescription { get; set; }
        public string InternationalStatusDescription { get; set; }
        [StringLength(500)]
        public string TrademarkElement { get; set; }
        [StringLength(500)]
        public string TrademarkDrawingDescription { get; set; }
        public string DescriptionOfMark { get; set; }
        [StringLength(200)]
        public string CurrentLocation { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(50)]
        public string NewLawOffAssignedCode { get; set; }
        [StringLength(500)]
        public string LawOffAssigned { get; set; }
        public int Tm5Status { get; set; }
        [StringLength(500)]
        public string Tm5StatusDescription { get; set; }
        [StringLength(1000)]
        public string StatusDefination { get; set; }
        [StringLength(50)]
        public string UsRegistrationNumber { get; set; }
        [StringLength(1000)]
        public string Image { get; set; }
        public string CurrentStatus { get; set; }
        public string StatusColor { get; set; }
        public DateTime? NextDeadline { get; set; }
        public DateTime? MaxNextDeadline { get; set; }
        public DateTime? CurrentLocationDate { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? UsRegistrationDate { get; set; }
        public DateTime? FiliingDate { get; set; }
        public DateTime? PublicationDate { get; set; }
        public DateTime? PublicationAllowanceDate { get; set; }
        public DateTime SyncDate { get; set; }
        public Guid? AttorneyId { get; set; }
        public virtual Attorney Attorney { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<OwnerGroup> OwnerGroups { get; set; } = new List<OwnerGroup>();
        public virtual ICollection<PhysicalLocationHistory> PhysicalLocationHistorys { get; set; } = new List<PhysicalLocationHistory>();
        public virtual ICollection<ProsecutionHistory> ProsecutionHistorys { get; set; } = new List<ProsecutionHistory>();
        public virtual ICollection<DesignSearchClass> DesignSearches { get; set; } = new List<DesignSearchClass>();
        public virtual ICollection<GoodAndService> GoodAndServices { get; set; } = new List<GoodAndService>();
        public virtual ICollection<PublicationOfficialGazette> PublicationOfficialGazettes { get; set; } = new List<PublicationOfficialGazette>();
        public virtual ICollection<Document> Documents { get; set; } = new List<Document>();
        public virtual ICollection<TrademarkImportItem> TrademarkImportItems { get; set; } = new List<TrademarkImportItem>();
    }
}
