﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public interface ITrademarkRepository : IRepository<Trademark>
    {
        IQueryable<Trademark> TrademarkIQuerableAsync(TrademarkSpecification specification, int skip, int take);
        IQueryable<Trademark> TrademarkIQuerableAsync();
        Task<Trademark> FindTrademarkWithOutIncludeAsync(TrademarkSpecification specification);
        Task<Trademark> FindTrademarkAsync(TrademarkSpecification specification);
        Task<List<Trademark>> ListTrademarkAsync(TrademarkSpecification specification);
        Task<List<Trademark>> ListTrademarkAsync(TrademarkSpecification specification, int? skip = null, int? take = null);
        Task<int> CountAdvertisementsAsync(TrademarkSpecification specification);
        Task<DocumentType> FindDocumentTypeByCode(string code);
        Task<DocumentCategory> FindDocumentCategoryByCode(string code);
        Task<TrademarkImportItem> GetTrademarkImportItem(long SerialNumber);
        Task<TrademarkImportItem> GetTrademarkImportItemById(Guid id);
        Task<List<TrademarkImportFile>> GetTrademarkImportFilesAsync(TrademarkImportFileSpecification specification);
        Task<int> CountTrademarkImportFilesAsync();
        void Create(DocumentCategory entity);
        void Delete(DocumentCategory entity);
        void Create(DocumentType entity);
        void Delete(DocumentType entity);
        void Create(OwnerGroup entity);
        void Create(TrademarkImportFile entity);
        void Update(TrademarkImportFile entity);
        void Update(TrademarkImportItem entity);
        void Create(ProsecutionHistory entity);
        void Update(ProsecutionHistory entity);
        void Create(PhysicalLocationHistory entity);
        void Update(PhysicalLocationHistory entity);
        void Create(PublicationOfficialGazette entity);
        void Update(PublicationOfficialGazette entity);
    }
}
