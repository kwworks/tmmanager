﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DesignSearchDescriptionClass : DomainObject
    {
        [StringLength(1000)]
        public string Description { get; set; }
        public Guid DesignSearchClassId { get; set; }
        public virtual DesignSearchClass DesignSearchClass { get; set; }
    }
}
