﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class GoodAndService : DomainObject
    {
        public string StatusCode { get; set; }
        [StringLength(200)]
        public string StatusDescription { get; set; }
        public string StatusDate { get; set; }
        public long? FirstUseDate { get; set; }
        public long? FirstUseInCommerceDate { get; set; }
        public string PrimeClassCode { get; set; }
        public string Description { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
        public virtual ICollection<GoodAndServiceClass> GoodAndServiceClass { get; set; } = new List<GoodAndServiceClass>();
    }
}
