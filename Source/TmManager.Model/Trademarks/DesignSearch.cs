﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DesignSearch : DomainObject
    {
        public DesignSearch()
        {
            DesignSearchClass = new List<DesignSearchClass>();
        }
        public Guid TrademarkId { get; set; }        
        public virtual Trademark Trademark { get; set; }
        public virtual ICollection<DesignSearchClass> DesignSearchClass { get; set; }
    }
}
