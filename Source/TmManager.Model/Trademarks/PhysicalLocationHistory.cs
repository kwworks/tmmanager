﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class PhysicalLocationHistory : DomainObject
    {
        public DateTime? EventDate { get; set; }
        [StringLength(200)]
        public string PhysicalLocation { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public string RegistrationNumber { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
    }
}
