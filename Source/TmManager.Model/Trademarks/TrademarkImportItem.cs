﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkImportItem : ModifiableDomainObject
    {
        [Required]
        [StringLength(50)]
        public long SerialNumber { get; set; }
        [Required]
        public TrademarkStatus Status { get; set; }
        public string Email { get; set; }
        public Guid? TrademarkId { get; set; }
        public Guid? TrademarkImportFileId { get; set; }
        public virtual Trademark Trademark { get; set; }
        public virtual TrademarkImportFile TrademarkImportFile { get; set; }
        public string ClientName { get; set; }
        public string TrademarkName { get; set; }
    }
}
