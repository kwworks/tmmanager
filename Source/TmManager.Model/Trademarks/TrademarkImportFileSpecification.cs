﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkImportFileSpecification : Specification<TrademarkImportFile>
    {
        public TrademarkImportFileSpecification(Expression<Func<TrademarkImportFile, bool>> predicate) : base(predicate)
        {
        }
        public static TrademarkImportFileSpecification GetById(Guid id)
        {
            return new TrademarkImportFileSpecification(x => x.Id == id);
        }
        public static TrademarkImportFileSpecification GetThirtyDays()
        {
            return new TrademarkImportFileSpecification(x => x.Created >= DateTime.Now.AddMonths(-1));
        }
        public static TrademarkImportFileSpecification GetByFilters(DateTime StartDate, DateTime EndDate)
        {
            return new TrademarkImportFileSpecification(x => x.Created >= StartDate && x.Created <= EndDate.AddDays(1).AddTicks(-1));
        }
    }
}
