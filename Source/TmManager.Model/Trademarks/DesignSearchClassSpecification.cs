﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class DesignSearchClassSpecification : Specification<DesignSearchClass>
    {
        public DesignSearchClassSpecification(Expression<Func<DesignSearchClass, bool>> predicate) : base(predicate)
        {
        }
        public static DesignSearchClassSpecification GetByCode(List<string> codes)
        {
            return new DesignSearchClassSpecification(x => codes.Any(y => y == x.Code));
        }
    }
}
