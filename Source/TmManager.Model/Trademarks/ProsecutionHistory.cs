﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class ProsecutionHistory:DomainObject
    {
        public const string NON_FINAL_ACTION_WRITTEN = "NON-FINAL ACTION WRITTEN";
        public const string CASE_ASSIGNED_TO_INTENT_TO_USE_PARALEGAL = "CASE ASSIGNED TO INTENT TO USE PARALEGAL";
        public const string ABANDONED_NO_STATEMENT_OF_USE_FILED = "ABANDONED - NO STATEMENT OF USE FILED";
        public const string EXTENSION_1_GRANTED = "EXTENSION 1 GRANTED";
        public const string EXTENSION_2_GRANTED = "EXTENSION 2 GRANTED";
        public const string EXTENSION_3_GRANTED = "EXTENSION 3 GRANTED";
        public const string EXTENSION_4_GRANTED = "EXTENSION 4 GRANTED";
        public const string EXTENSION_5_GRANTED = "EXTENSION 5 GRANTED";
        public const string PUBLISHED_FOR_OPPOSITION = "PUBLISHED FOR OPPOSITION";
        public const string EX_PARTE_APPEAL_INSTITUTED = "EX PARTE APPEAL-INSTITUTED";
        public const string ABANDONMENT_FAILURE_TO_RESPOND_OR_LATE_RESPONSE = "ABANDONMENT - FAILURE TO RESPOND OR LATE RESPONSE";
        public const string ASSIGNED_TO_EXAMINER = "ASSIGNED TO EXAMINER";
        public const string FINAL_REFUSAL_WRITTEN = "FINAL REFUSAL WRITTEN";
        public const string TEAS_RESPONSE_TO_OFFICE_ACTION_RECEIVED = "TEAS RESPONSE TO OFFICE ACTION RECEIVED";
        public const string REGISTERED_SEC_8_6_YR_ACCEPTED = "REGISTERED - SEC. 8 (6-YR) ACCEPTED";
        public const string REGISTERED_SEC_8_10_YR_ACCEPTED = "REGISTERED - SEC. 8 (10-YR) ACCEPTED";
        public int EntryNumber { get; set; }
        [StringLength(100)]
        public string EntryCode { get; set; }
        [StringLength(10)]
        public string EntryType { get; set; }
        public int ProceedingNumber { get; set; }
        public DateTimeOffset? EntryDate { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
    }
}
