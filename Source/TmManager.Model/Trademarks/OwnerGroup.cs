﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class OwnerGroup : ModifiableDomainObject
    {
        public int Type { get; set; }
        public int EntityNumber { get; set; }
        public int TypeCode { get; set; }
        [StringLength(500)]
        public string TypeDescription { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(200)]
        public string Email { get; set; }
        [StringLength(200)]
        public string Email2 { get; set; }
        [StringLength(200)]
        public string Address { get; set; }
        [StringLength(100)]
        public string City { get; set; }
        [StringLength(50)]
        public string State { get; set; }
        [StringLength(50)]
        public string IsoCode { get; set; }
        [StringLength(50)]
        public string IsoName { get; set; }
        [StringLength(50)]
        public string WipoCode { get; set; }
        [StringLength(50)]
        public string WipoName { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        public Guid TrademarkId { get; set; }
        public virtual Trademark Trademark { get; set; }
    }
}
