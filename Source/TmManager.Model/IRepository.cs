﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public interface IRepository<T> : IDisposable where T : ModifiableDomainObject
    {
        T Find(Guid id);
        Task<T> FindAsync(Guid id);
        Task<T> FindAsync(Guid id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        Task<ICollection<T>> FindAsync();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
    public interface IDomainRepository<T> : IDisposable where T : DomainObject
    {
        T Find(Guid id);
        Task<T> FindAsync(Guid id);
        Task<T> FindAsync(Guid id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        Task<ICollection<T>> FindAsync();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
