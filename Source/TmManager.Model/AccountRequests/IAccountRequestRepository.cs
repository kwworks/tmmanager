﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.AccountRequests
{
    public interface IAccountRequestRepository : IDomainRepository<AccountRequest>
    {
        Task<AccountRequest> FindAccountRequestAsync(AccountRequestSpecification specification);
        Task<List<AccountRequest>> ListAccountRequestAsync(AccountRequestSpecification specification, int? skip = null, int? take = null);
        Task<int> CountAccountRequestsAsync(AccountRequestSpecification specification);
    }
}
