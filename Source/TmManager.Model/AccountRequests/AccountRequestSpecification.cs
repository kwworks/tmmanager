﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.AccountRequests
{
    public class AccountRequestSpecification : Specification<AccountRequest>
    {
        public AccountRequestSpecification(Expression<Func<AccountRequest, bool>> predicate) : base(predicate)
        {
        }
        public static AccountRequestSpecification GetAll()
        {
            return new AccountRequestSpecification(x => x.Id != Guid.Empty);
        }
        public static AccountRequestSpecification GetByEmail(string email)
        {
            return new AccountRequestSpecification(x => x.Email == email);
        }
    }
}
