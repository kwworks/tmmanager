﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.AccountRequests
{
    public class AccountRequest : ModifiableDomainObject
    {
        [Required]
        [StringLength(200)]
        public string Email { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        public string Company { get; set; }
        [StringLength(200)]
        public string TrademarkName { get; set; }
        public bool IsAllowed { get; set; }
    }
}
