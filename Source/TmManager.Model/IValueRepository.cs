﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public interface IValueRepository :IRepository<Value>, IDisposable
    {
        void Insert(Value entity);
        Task<ICollection<T>> FindAsync<T>(ValueSpecification<T> specification) where T : Value;
        Task<ICollection<T>> GetAllAsync<T>(ValueSpecification<T> specification) where T : Value;
    }
}
