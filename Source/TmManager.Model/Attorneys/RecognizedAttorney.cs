﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class RecognizedAttorney : ModifiableDomainObject
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
