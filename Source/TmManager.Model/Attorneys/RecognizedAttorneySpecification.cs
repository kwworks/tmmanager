﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class RecognizedAttorneySpecification : Specification<RecognizedAttorney>
    {
        public RecognizedAttorneySpecification(Expression<Func<RecognizedAttorney, bool>> predicate) : base(predicate)
        {
        }
        public static RecognizedAttorneySpecification GetAllActive()
        {
            return new RecognizedAttorneySpecification(x => x.IsActive);
        }
        public static RecognizedAttorneySpecification GetById(Guid id)
        {
            return new RecognizedAttorneySpecification(x => x.Id == id);
        }
        public static RecognizedAttorneySpecification GetByName(string name)
        {
            return new RecognizedAttorneySpecification(x => x.Name == name);
        }
        public static RecognizedAttorneySpecification GetByEmail(string email)
        {
            return new RecognizedAttorneySpecification(x => x.Email == email);
        }
        public static RecognizedAttorneySpecification GetByFilter(string email, string name)
        {
            return new RecognizedAttorneySpecification(x => x.Email == email || x.Name.Contains(name));
        }
        public static RecognizedAttorneySpecification GetByEmailAndName(string email, string name)
        {
            return new RecognizedAttorneySpecification(x => x.Email == email && x.Name == name);
        }
        public static RecognizedAttorneySpecification GetByFilters(Guid id, string email, string name)
        {
            return new RecognizedAttorneySpecification(x => x.Id != id && (x.Email == email && x.Name == name));
        }
    }
}
