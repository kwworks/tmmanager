﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class Firm : ModifiableDomainObject
    {
        [StringLength(1000)]
        [Required]
        public string Name { get; set; }
        [StringLength(100)]
        public string IndividualName { get; set; }
        [StringLength(100)]
        public string Phone { get; set; }
        [StringLength(100)]
        public string Fax { get; set; }
        [StringLength(500)]
        public string Email { get; set; }
        [StringLength(5)]
        public string IsEmailAuthticated { get; set; }
        public virtual ICollection<Attorney> Attorneys { get; set; } = new List<Attorney>();
    }
}
