﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public interface IAttorneyRepository : IRepository<Attorney>
    {
        Task<Attorney> FindAttorney(AttorneySpecification specification);
    }
}
