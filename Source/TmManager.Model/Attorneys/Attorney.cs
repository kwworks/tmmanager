﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Identity;
using TmManager.Trademarks;

namespace TmManager.Attorneys
{
    public class Attorney : ModifiableDomainObject
    {
        //[StringLength(450)]
        //public string UserId { get; set; }
        [StringLength(1000)]
        //[Required]
        public string Name { get; set; }
        [StringLength(1000)]
        public string FullName { get; set; }
        [StringLength(500)]
        public string Email { get; set; }
        [StringLength(500)]
        public string Email2 { get; set; }
        [StringLength(1000)]
        public string Line1 { get; set; }
        [StringLength(500)]
        public string City { get; set; }
        [StringLength(50)]
        public string PostalCode { get; set; }
        [StringLength(5)]
        public string CountryCode { get; set; }
        [StringLength(500)]
        public string CountryName { get; set; }
        public bool IsKnown { get; set; }
        [StringLength(5)]
        public bool IsEmailAuthticated { get; set; }
        public Guid? FirmId { get; set; }
        public virtual Firm Firm { get; set; }
        //public virtual User User { get; set; }
        public virtual ICollection<Trademark> Trademarks { get; set; } = new List<Trademark>();
    }
}
