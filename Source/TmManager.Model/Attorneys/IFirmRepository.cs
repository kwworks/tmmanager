﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;

namespace TmManager.Attorneys
{
    public interface IFirmRepository : IRepository<Firm>
    {
        Task<Firm> FindFirmAsync(FirmSpecification specification);
        Task<List<Firm>> ListFirmAsync(FirmSpecification specification);
    }
}