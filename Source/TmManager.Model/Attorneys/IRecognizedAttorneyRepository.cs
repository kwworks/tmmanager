﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public interface IRecognizedAttorneyRepository : IRepository<RecognizedAttorney>
    {
        Task<RecognizedAttorney> FindRecognizedAttorneyAsync(RecognizedAttorneySpecification specification);
        Task<List<RecognizedAttorney>> ListRecognizedAttorneyAsync(RecognizedAttorneySpecification specification);
    }
}