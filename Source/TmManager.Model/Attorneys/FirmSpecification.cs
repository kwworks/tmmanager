﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class FirmSpecification : Specification<Firm>
    {
        public FirmSpecification(Expression<Func<Firm, bool>> predicate) : base(predicate)
        {
        }
        public static FirmSpecification GetAllActive()
        {
            return new FirmSpecification(x => x.IsActive);
        }
        public static FirmSpecification GetByName(string firmName)
        {
            return new FirmSpecification(x => x.Name == firmName);
        }
        public static FirmSpecification GetByNames(List<string> name)
        {
            return new FirmSpecification(x => name.Any(y => y == x.Name));
        }

    }
}
