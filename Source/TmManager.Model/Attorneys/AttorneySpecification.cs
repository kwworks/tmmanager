﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class AttorneySpecification : Specification<Attorney>
    {
        public AttorneySpecification(Expression<Func<Attorney,bool>> predicate) : base(predicate)
        {
        }
        public static AttorneySpecification GetAllActive()
        {
            return new AttorneySpecification(x => x.IsActive);
        }
        public static AttorneySpecification GetByName(List<string> name)
        {
            return new AttorneySpecification(x => name.Any(y => y == x.Name));
        }
        public static AttorneySpecification GetByName(string name)
        {
            return new AttorneySpecification(x => x.Name == name);
        }
        public static AttorneySpecification GetByFilters(string name, string email)
        {
            return new AttorneySpecification(x => x.Name == name && x.Email == email);
        }
    }
}
