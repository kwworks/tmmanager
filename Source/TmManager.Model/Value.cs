﻿using System.ComponentModel.DataAnnotations;

namespace TmManager
{
    public class Value : ModifiableDomainObject
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [StringLength(100)]
        public string GroupName { get; set; }//For Cascading
        public int SortingOrder { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
