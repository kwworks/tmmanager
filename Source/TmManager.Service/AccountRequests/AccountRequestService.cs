﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;
using TmManager.Trademarks;

namespace TmManager.AccountRequests
{
    public class AccountRequestService : IAccountRequestService
    {
        private readonly IUnitOfWork _UnitOfWork;
        private readonly IAccountRequestRepository _accountRequestRepository;
        public AccountRequestService(IAccountRequestRepository accountRequestRepository, IUnitOfWork UnitOfWork)
        {
            _UnitOfWork = UnitOfWork;
            _accountRequestRepository = accountRequestRepository;
        }
        public async Task<GridDataSourceResultModel> SearchAccountRequestsAsync(int skip, int take)
        {
            return new GridDataSourceResultModel()
            {
                Data = (await _accountRequestRepository.ListAccountRequestAsync(AccountRequestSpecification.GetAll(), skip, take))
                .Select(x => new AccountRequestForKendo()
                {
                    Email = x.Email,
                    Name = x.Name,
                    Company = x.Company,
                    TrademarkName = x.TrademarkName,
                    IsAllowed = x.IsAllowed,
                    Created = x.Created,
                }).OrderByDescending(x => x.Created).ToList(),
                Total = await _accountRequestRepository.CountAccountRequestsAsync(AccountRequestSpecification.GetAll())
            };
        }
        public async Task<SaveResults> SaveAccountRequest(AccountRequestModel model)
        {
            var result = new SaveResults();
            try
            {
                var request = (AccountRequest)default;
                request = await _accountRequestRepository.FindAccountRequestAsync(AccountRequestSpecification.GetByEmail(model.Email));
                if (request == null)
                {
                    request = new AccountRequest()
                    {
                        Name = model.Name,
                        Email = model.Email,
                        Company = model.Company,
                        TrademarkName = model.TrademarkName,
                        CreatedBy = model.Email,
                        ModifiedBy = model.Email
                    };
                    _accountRequestRepository.Create(request);
                }
                else
                    result.Errors.Add("Already requested for account");

                if (!result.Errors.Any())
                {
                    result.IsSuccess = true;
                    await _UnitOfWork.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.Message);
                Trace.TraceError(ex.ToLogString());
            }
            return result;
        }
    }
}
 