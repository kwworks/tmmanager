﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.AccountRequests
{
    public interface IAccountRequestService
    {
        Task<GridDataSourceResultModel> SearchAccountRequestsAsync(int skip, int take);
        Task<SaveResults> SaveAccountRequest(AccountRequestModel model);
    }
}
