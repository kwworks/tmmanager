﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.AccountRequests
{
    public class AccountRequestModel
    {
        [Required(ErrorMessage = "Email field is required")]
        public string Email { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        [Required(ErrorMessage = "Trademark field is required")]
        public string TrademarkName { get; set; }
        public bool IsAllowed { get; set; }
    }

    public class AccountRequestForKendo
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string TrademarkName { get; set; }
        public bool IsAllowed { get; set; }
        public DateTime Created { get; set; }
    }
}
