﻿namespace TmManager.Tsdr
{
    public class GetTrademarkDetailsAddressstatecountry2
    {
        public GetTrademarkDetailsStatecountry stateCountry { get; set; }
        public object isoRegion { get; set; }
        public GetTrademarkDetailsIso iso { get; set; }
        public GetTrademarkDetailsWipo wipo { get; set; }
    }
}
