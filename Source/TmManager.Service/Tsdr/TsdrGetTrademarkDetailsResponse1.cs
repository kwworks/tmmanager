﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Tsdr
{
    public class TsdrGetTrademarkDetailsResponse1
    {
        public List<GetTrademarkDetailsTransactionlist1> transactionList { get; set; }
        public bool oversized { get; set; }
        public int size { get; set; }
    }

    public class GetTrademarkDetailsTransactionlist1
    {
        public List<GetTrademarkDetailsTrademark1> trademarks { get; set; }
        public string searchId { get; set; }
    }

    public class GetTrademarkDetailsTrademark1
    {
        public bool internationalData { get; set; }
        public GetTrademarkDetailsStatus1 status { get; set; }
        public GetTrademarkDetailsParties1 parties { get; set; }
        public List<GetTrademarkDetailsGslist1> gsList { get; set; }
        public List<object> foreignInfoList { get; set; }
        public List<object> relationshipBundleList { get; set; }
        public GetTrademarkDetailsPublication1 publication { get; set; }
        public GetTrademarkDetailsDivisional1 divisional { get; set; }
        public List<GetTrademarkDetailsProsecutionhistory1> prosecutionHistory { get; set; }
    }

    public class GetTrademarkDetailsStatus1
    {
        public GetTrademarkDetailsStaff1 staff { get; set; }
        public GetTrademarkDetailsCorrespondence1 correspondence { get; set; }
        public int serialNumber { get; set; }
        public List<DesignSearchList1> designSearchList { get; set; }
        public string filingDate { get; set; }
        public string usRegistrationNumber { get; set; }
        public string usRegistrationDate { get; set; }
        public bool filedAsTeasPlusApp { get; set; }
        public bool currentlyAsTeasPlusApp { get; set; }
        public bool filedAsTeasRfApp { get; set; }
        public bool currentlyAsTeasRfApp { get; set; }
        public bool supplementalRegister { get; set; }
        public bool amendPrincipal { get; set; }
        public bool amendSupplemental { get; set; }
        public bool trademark { get; set; }
        public bool certificationMark { get; set; }
        public bool serviceMark { get; set; }
        public bool collectiveMembershipMark { get; set; }
        public bool collectiveServiceMark { get; set; }
        public bool collectiveTradeMark { get; set; }
        public int status { get; set; }
        public string statusDate { get; set; }
        public bool standardChar { get; set; }
        public string markDrawingCd { get; set; }
        public bool colorDrawingCurr { get; set; }
        public bool section2f { get; set; }
        public bool section2fPartial { get; set; }
        public bool others { get; set; }
        public bool publishedPrevRegMark { get; set; }
        public int clsTotal { get; set; }
        public bool filedUse { get; set; }
        public bool filedItu { get; set; }
        public bool filed44d { get; set; }
        public bool filed44e { get; set; }
        public bool filed66a { get; set; }
        public bool filedNoBasis { get; set; }
        public bool useCurr { get; set; }
        public bool ituCurr { get; set; }
        public bool sect44dCurr { get; set; }
        public bool sect44eCurr { get; set; }
        public bool sect66aCurr { get; set; }
        public bool noBasisCurr { get; set; }
        public bool useAmended { get; set; }
        public bool ituAmended { get; set; }
        public bool sect44dAmended { get; set; }
        public bool sect44eAmended { get; set; }
        public bool sect8Filed { get; set; }
        public bool sect8Acpt { get; set; }
        public bool sect8PartialAcpt { get; set; }
        public bool sect15Filed { get; set; }
        public bool sect15Ack { get; set; }
        public bool sect71Filed { get; set; }
        public bool sect71Acpt { get; set; }
        public bool sect71PartialAcpt { get; set; }
        public bool renewalFiled { get; set; }
        public bool changeInReg { get; set; }
        public string lawOffAsgnCd { get; set; }
        public string currLocationCd { get; set; }
        public string currLocationDt { get; set; }
        public object chargeToLocation { get; set; }
        public object phyLocation { get; set; }
        public object phyLocationDt { get; set; }
        public string extStatusDesc { get; set; }
        public object intStatusDesc { get; set; }
        public string markDrawDesc { get; set; }
        public string descOfMark { get; set; }
        public string currentLoc { get; set; }
        public string markElement { get; set; }
        public List<object> parentOf { get; set; }
        public List<object> prevRegNumList { get; set; }
        public string newLawOffAsgnCd { get; set; }
        public string lawOffAssigned { get; set; }
        public int tm5Status { get; set; }
        public string tm5StatusDesc { get; set; }
        public string tm5StatusDef { get; set; }
        public List<GetTrademarkDetailsPhysicallocationhistory1> physicalLocationHistory { get; set; }
        public object pseudoMark { get; set; }
    }

    public class DesignSearchList1
    {
        public string code { get; set; }
        public List<string> descriptions { get; set; }
    }
    public class GetTrademarkDetailsStaff11
    {
        public GetTrademarkDetailsExaminer1 examiner { get; set; }
        public object paralegal { get; set; }
        public object ituParalegal { get; set; }
        public object lie { get; set; }
        public object chargeTo { get; set; }
    }

    public class GetTrademarkDetailsExaminer1
    {
        public object number { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsCorrespondence1
    {
        public List<object> freeFormAddress { get; set; }
        public GetTrademarkDetailsAddress1 address { get; set; }
        public string attorneyName { get; set; }
        public GetTrademarkDetailsAttorneyemail1 attorneyEmail { get; set; }
        public string individualFullName { get; set; }
        public string firmName { get; set; }
        public string correspondantPhone { get; set; }
        public string correspondantFax { get; set; }
        public GetTrademarkDetailsCorrespondantemail1 correspondantEmail { get; set; }
    }

    public class GetTrademarkDetailsAddress1
    {
        public string line1 { get; set; }
        public string city { get; set; }
        public string postalCode { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
    }

    public class GetTrademarkDetailsAttorneyemail1
    {
        public string authIndicator { get; set; }
        public List<string> addresses { get; set; }
    }

    public class GetTrademarkDetailsCorrespondantemail1
    {
        public string authIndicator { get; set; }
        public List<string> addresses { get; set; }
    }

    public class GetTrademarkDetailsPhysicallocationhistory1
    {
        public string eventDate { get; set; }
        public string physicalLocation { get; set; }
        public string physicalLocationDescription { get; set; }
        public string rsn { get; set; }
    }

    public class GetTrademarkDetailsParties1
    {
        public JObject ownerGroups { get; set; }
    }

    public class GetTrademarkDetailsOwnergroups1
    {
        [JsonProperty(PropertyName ="10")]
        public List<GetTrademarkDetails_10> _10 { get; set; }
        [JsonProperty(PropertyName ="20")]
        public List<GetTrademarkDetails_20> _20 { get; set; }
        [JsonProperty(PropertyName = "30")]
        public List<GetTrademarkDetails_30> _30 { get; set; }
    }

    public class GetTrademarkDetails_101
    {
        public int serialNumber { get; set; }
        public int partyType { get; set; }
        public string partyTypeDescription { get; set; }
        public object reelFrame { get; set; }
        public int entityNum { get; set; }
        public GetTrademarkDetailsEntitytype entityType { get; set; }
        public string name { get; set; }
        public GetTrademarkDetailsEmaillist emailList { get; set; }
        public object composedOf { get; set; }
        public object dbaAkaFormerly { get; set; }
        public object assignment { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public GetTrademarkDetailsAddressstatecountry2 addressStateCountry { get; set; }
        public string zip { get; set; }
        public GetTrademarkDetailsCitizenship1 citizenship { get; set; }
    }

    public class GetTrademarkDetailsEntitytype11
    {
        public int code { get; set; }
        public string description { get; set; }
    }

    public class GetTrademarkDetailsEmaillist11
    {
        public string authIndicator { get; set; }
        public List<string> addresses { get; set; }
    }

    public class GetTrademarkDetailsAddressstatecountry11
    {
        public GetTrademarkDetailsStatecountry1 stateCountry { get; set; }
        public object isoRegion { get; set; }
        public GetTrademarkDetailsIso1 iso { get; set; }
        public GetTrademarkDetailsWipo1 wipo { get; set; }
    }

    public class GetTrademarkDetailsStatecountry11
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsIso11
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsWipo11
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsCitizenship11
    {
        public GetTrademarkDetailsStatecountry11 stateCountry { get; set; }
        public object isoRegion { get; set; }
        public GetTrademarkDetailsIso11 iso { get; set; }
        public GetTrademarkDetailsWipo11 wipo { get; set; }
    }

    public class GetTrademarkDetailsStatecountry111
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsIso111
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsWipo111
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetails_201
    {
        public int serialNumber { get; set; }
        public int partyType { get; set; }
        public string partyTypeDescription { get; set; }
        public object reelFrame { get; set; }
        public int entityNum { get; set; }
        public GetTrademarkDetailsEntitytype11 entityType { get; set; }
        public string name { get; set; }
        public GetTrademarkDetailsEmaillist11 emailList { get; set; }
        public object composedOf { get; set; }
        public object dbaAkaFormerly { get; set; }
        public object assignment { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public GetTrademarkDetailsAddressstatecountry11 addressStateCountry { get; set; }
        public string zip { get; set; }
        public GetTrademarkDetailsCitizenship11 citizenship { get; set; }
    }

    public class GetTrademarkDetails_301
    {
        public int serialNumber { get; set; }
        public int partyType { get; set; }
        public string partyTypeDescription { get; set; }
        public object reelFrame { get; set; }
        public int entityNum { get; set; }
        public GetTrademarkDetailsEntitytype11 entityType { get; set; }
        public string name { get; set; }
        public GetTrademarkDetailsEmaillist11 emailList { get; set; }
        public object composedOf { get; set; }
        public object dbaAkaFormerly { get; set; }
        public object assignment { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public GetTrademarkDetailsAddressstatecountry11 addressStateCountry { get; set; }
        public string zip { get; set; }
        public GetTrademarkDetailsCitizenship11 citizenship { get; set; }
    }
    public class GetTrademarkDetailsStatecountry21
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsIso21
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsWipo21
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsCitizenship111
    {
        public GetTrademarkDetailsStatecountry31 stateCountry { get; set; }
        public object isoRegion { get; set; }
        public GetTrademarkDetailsIso31 iso { get; set; }
        public GetTrademarkDetailsWipo31 wipo { get; set; }
    }

    public class GetTrademarkDetailsStatecountry31
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsIso31
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsWipo31
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetTrademarkDetailsPublication1
    {
        public int serialNumber { get; set; }
        public string datePublished { get; set; }
        public string noticeOfAllowanceDate { get; set; }
        public List<GetTrademarkDetailsOfficialgazette1> officialGazettes { get; set; }
    }

    public class GetTrademarkDetailsOfficialgazette1
    {
        public string issueDate { get; set; }
        public string statusCodeText { get; set; }
        public string statusCode { get; set; }
        public string categoryCode { get; set; }
        public string categoryCodeText { get; set; }
        public string categoryReason { get; set; }
        public string actionDate { get; set; }
        public int registrationNumber { get; set; }
        public List<object> px { get; set; }
    }

    public class GetTrademarkDetailsDivisional1
    {
        public int serialNumber { get; set; }
        public object childOf { get; set; }
        public List<object> parentOfList { get; set; }
    }

    public class GetTrademarkDetailsGslist1
    {
        public GetTrademarkDetailsGslist1()
        {
            usClasses = new List<GetTrademarkDetailsUsclass1>();
            internationalClasses = new List<GetTrademarkDetailsInternationalclass1>();
        }
        public int serialNumber { get; set; }
        public bool internationalClassPrime { get; set; }
        public List<GetTrademarkDetailsUsclass1> usClasses { get; set; }
        public List<GetTrademarkDetailsInternationalclass1> internationalClasses { get; set; }
        public List<object> pseudoClasses { get; set; }
        public string statusCode { get; set; }
        public string statusDescription { get; set; }
        public string statusDate { get; set; }
        public string firstUseDate { get; set; }
        public string firstUseInCommerceDate { get; set; }
        public object firstUseDateDescription { get; set; }
        public object firstUseInCommerceDateDescription { get; set; }
        public string description { get; set; }
        public object classBasis { get; set; }
        public string primeClassCode { get; set; }
    }

    public class GetTrademarkDetailsUsclass1
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class GetTrademarkDetailsInternationalclass1
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class GetTrademarkDetailsProsecutionhistory1
    {
        public int entryNumber { get; set; }
        public string entryCode { get; set; }
        public string entryType { get; set; }
        public int proceedingNum { get; set; }
        public DateTime entryDate { get; set; }
        public string entryDesc { get; set; }
    }
}
