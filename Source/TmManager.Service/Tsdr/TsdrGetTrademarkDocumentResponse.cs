﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Tsdr
{

    public class TsdrGetTrademarkDocumentResponse
    { }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "urn:us:gov:doc:uspto:trademark", IsNullable = false)]

    public class DocumentList
    {

        private DocumentListDocument[] documentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Document")]
        public DocumentListDocument[] Document
        {
            get
            {
                return this.documentField;
            }
            set
            {
                this.documentField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "urn:us:gov:doc:uspto:trademark")]
    public partial class DocumentListDocument
    {

        private uint serialNumberField;

        private System.DateTime mailRoomDateField;

        private System.DateTime scanDateTimeField;

        private byte totalPageQuantityField;

        private string[] pageMediaTypeListField;

        private string[] urlPathListField;

        private string sourceSystemField;

        private string documentTypeCodeField;

        private string categoryTypeCodeField;

        private string documentTypeCodeDescriptionTextField;

        private string documentTypeDescriptionTextField;

        private string categoryTypeCodeDescriptionTextField;

        /// <remarks/>
        public uint SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime MailRoomDate
        {
            get
            {
                return this.mailRoomDateField;
            }
            set
            {
                this.mailRoomDateField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ScanDateTime
        {
            get
            {
                return this.scanDateTimeField;
            }
            set
            {
                this.scanDateTimeField = value;
            }
        }

        /// <remarks/>
        public byte TotalPageQuantity
        {
            get
            {
                return this.totalPageQuantityField;
            }
            set
            {
                this.totalPageQuantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PageMediaTypeName", IsNullable = false)]
        public string[] PageMediaTypeList
        {
            get
            {
                return this.pageMediaTypeListField;
            }
            set
            {
                this.pageMediaTypeListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("UrlPath", IsNullable = false)]
        public string[] UrlPathList
        {
            get
            {
                return this.urlPathListField;
            }
            set
            {
                this.urlPathListField = value;
            }
        }

        /// <remarks/>
        public string SourceSystem
        {
            get
            {
                return this.sourceSystemField;
            }
            set
            {
                this.sourceSystemField = value;
            }
        }

        /// <remarks/>
        public string DocumentTypeCode
        {
            get
            {
                return this.documentTypeCodeField;
            }
            set
            {
                this.documentTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string CategoryTypeCode
        {
            get
            {
                return this.categoryTypeCodeField;
            }
            set
            {
                this.categoryTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string DocumentTypeCodeDescriptionText
        {
            get
            {
                return this.documentTypeCodeDescriptionTextField;
            }
            set
            {
                this.documentTypeCodeDescriptionTextField = value;
            }
        }

        /// <remarks/>
        public string DocumentTypeDescriptionText
        {
            get
            {
                return this.documentTypeDescriptionTextField;
            }
            set
            {
                this.documentTypeDescriptionTextField = value;
            }
        }

        /// <remarks/>
        public string CategoryTypeCodeDescriptionText
        {
            get
            {
                return this.categoryTypeCodeDescriptionTextField;
            }
            set
            {
                this.categoryTypeCodeDescriptionTextField = value;
            }
        }
    }
}
