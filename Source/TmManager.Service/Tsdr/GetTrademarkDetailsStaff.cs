﻿namespace TmManager.Tsdr
{
    public class GetTrademarkDetailsStaff
    {
        public GetTrademarkDetailsExaminer examiner { get; set; }
        public object paralegal { get; set; }
        public object ituParalegal { get; set; }
        public object lie { get; set; }
        public object chargeTo { get; set; }
    }
}
