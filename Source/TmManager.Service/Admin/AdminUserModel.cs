﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Admin
{
    public class AdminUserModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Email field is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; }
    }

    public class UserRequestModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Name field is required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Username field is required")]
        public string Username { get; set; }
    }
}
