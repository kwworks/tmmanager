﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Identity;

namespace TmManager.Admin
{
    public class ClientDetailResponse
    {
        public ClientDetailModel ClientDetail { get; set; } = new ClientDetailModel();
        public List<ClientDetailContacts> ClientContacts { get; set; } = new List<ClientDetailContacts>();
        public List<ClientTrademarkList> ClientTrademarkList { get; set; } = new List<ClientTrademarkList>();
    }
    public class ClientDetailModel
    {
        public Guid Id { get; set; }
        public string ClientName { get; set; }
        public string LegalEntity { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string PhoneType { get; set; }
    }

    public class ClientDetailContacts
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public PhoneType PhoneType { get; set; }
        public ContactType Status { get; set; }
    }

    public class ClientTrademarkList
    {
        public Guid Id { get; set; }
        public string TrademarkName { get; set; }
        public long SerialNumber { get; set; }
        public string ApplicationFilingDate { get; set; }
        public string DateImported { get; set; }
    }

    public class ClientListModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
    }
}
