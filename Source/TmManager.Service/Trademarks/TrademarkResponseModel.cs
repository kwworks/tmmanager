﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkResponseModel
    {
        public List<TrademarkResponse> TrademarkResponse { get; set; } = new List<TrademarkResponse>();
        public TrademarkResponseModel()
        {
            Errors = new HashSet<string>();
        }
        public bool IsSuccess { get; set; }
        public HashSet<string> Errors { get; set; }
    }

    public class DatatTableResponse
    {
        public int draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<TrademarkResponse> data { get; set; }
    }
    public class ImportFileKendoModel
    {
        public string BatchName { get; set; }
        public List<TrademarkImportFileModel> FileItems { get; set; } = new List<TrademarkImportFileModel>();
        public DateTime Created { get; set; }
    }
    public class TrademarkResponseForKendo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string PrimaryName { get; set; }
        public string ClientEmail { get; set; }
        public string ImportedDateString { get; set; }
        public string PrimaryEmail { get; set; }
        public string StatusDescription { get; set; }
        public string AttorneyName { get; set; }
        public DateTime? FilingDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Count { get; set; }
    }
    public class TrademarkResponse
    {
        public Guid Id { get; set; }
        public string FallbackEmail { get; set; }
        public int SerialNumber { get; set; }
        public TrademarkStatus Status { get; set; }
        public string Image { get; set; }
        public string AttorneyName { get; set; }
        public string Serial { get; set; }
        public string ClientName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public int StatusNumber { get; set; }
        public int TrademarkStatus { get; set; }
        public string StatusDescription { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime ImportedDate { get; set; }
        public string ImportedDateString { get; set; }
        public bool IsDuplicate { get; set; }
        public string ExternalStatusDescription { get; set; }
        public string TrademarkElement { get; set; }
        public string TrademarkDrawingDescription { get; set; }
        public string ExaminerName { get; set; }
        public bool FiledAsTeasPlusApp { get; set; }
        public bool CurrentlyAsTeasPlusApp { get; set; }
        public bool FiledAsTeasRfApp { get; set; }
        public bool CurrentlyAsTeasRfApp { get; set; }
        public bool SupplementalRegister { get; set; }
        public bool AmendPrincipal { get; set; }
        public bool AmendSupplemental { get; set; }
        public bool CertificationMark { get; set; }
        public bool ServiceMark { get; set; }
        public bool CollectiveMembershipMark { get; set; }
        public bool CollectiveServiceMark { get; set; }
        public bool CollectiveTradeMark { get; set; }
        public DateTime? StatusDate { get; set; }
        public string StatusDateText { get; set; }
        public bool IsStandard { get; set; }
        public bool IsColorDrawing { get; set; }
        public bool IsSection2f { get; set; }
        public bool IsSection2fPartial { get; set; }
        public bool IsOthers { get; set; }
        public bool IsPreviousRegistered { get; set; }
        public int CommunityLegalServiceTotal { get; set; }
        public bool IsFiledUse { get; set; }
        public bool IsFiledInternationLaw { get; set; }
        public bool IsFiled44d { get; set; }
        public bool IsFiled44e { get; set; }
        public bool IsFiled66a { get; set; }
        public bool IsFiledNoBasis { get; set; }
        public bool IsUseCurrent { get; set; }
        public bool IsInternationalLawCurrent { get; set; }
        public bool IsSect44eCurrent { get; set; }
        public bool IsSect66aCurrent { get; set; }
        public bool IsNoBasisCurrent { get; set; }
        public bool IsUseAmended { get; set; }
        public bool IsInternationalLawAmended { get; set; }
        public bool IsSect44dAmended { get; set; }
        public bool IsSect44eAmended { get; set; }
        public bool IsSect8Filed { get; set; }
        public bool IsSect8Acpt { get; set; }
        public bool IsSect8PartialAcpt { get; set; }
        public bool IsSect15Filed { get; set; }
        public bool IsSect15Ack { get; set; }
        public bool IsSect71Filed { get; set; }
        public bool IsSect71Acpt { get; set; }
        public bool IsSect71PartialAcpt { get; set; }
        public bool IsRenewalFiled { get; set; }
        public bool IsChangeInRegistration { get; set; }
        public bool Sect44dCurrent { get; set; }
        public bool IsTrademark { get; set; }
        public string MarkDrawingCode { get; set; }
        public string LawOffAssignedCode { get; set; }
        public string CurrentLocationCode { get; set; }
        public DateTime? CurrentLocationDate { get; set; }
        public string CurrentLocationDateString { get; set; }
        public string InternationalStatusDescription { get; set; }
        public string DescriptionOfMark { get; set; }
        public string CurrentLocation { get; set; }
        public string NewLawOffAssignedCode { get; set; }
        public string LawOffAssigned { get; set; }
        public int Tm5Status { get; set; }
        public string Tm5StatusDescription { get; set; }
        public string StatusDefination { get; set; }
        public string UsRegistrationNumber { get; set; }
        public DateTime? FiliingDate { get; set; }
        public DateTime? PublicationDate { get; set; }
        public DateTime? PublicationAllowanceDate { get; set; }
        public string FiliingDateText { get; set; }
        public string PublicationDateText { get; set; }
        public string PublicationAllowanceDateText { get; set; }
        public string ExaminerNumber { get; set; }
        public string JsonString { get; set; }
        public string XMLString { get; set; }
        public string UserInfoString { get; set; }
        public DateTime? NextDeadline { get; set; }
        public string NextDeadlineText { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentStatusDescription { get; set; }
        public string StatusColor { get; set; }
        public DateTime SyncDate { get; set; }
        public string UsRegistrationDate { get; set; }
        public string MaxNextDeadline { get; set; }
        public string MaxNextDeadlineWithFee { get; set; }
        public string LastUpdateDate { get; set; }
        public Attorneys.AttorneyModel Attorney { get; set; } = new Attorneys.AttorneyModel();
        public OwnerGroupModel OwnerGroup { get; set; } = new OwnerGroupModel();
        public List<GoodAndServiceModel> GoodAndServices { get; set; } = new List<GoodAndServiceModel>();
        public List<PublicationOfficialGazetteModel> PublicationOfficialGazettes { get; set; } = new List<PublicationOfficialGazetteModel>();
        public List<DesignSearchModel> DesignSearch { get; set; } = new List<DesignSearchModel>();
        public List<ProsecutionHistoryModel> ProsecutionHistorys { get; set; } = new List<ProsecutionHistoryModel>();
        public List<DocumentModel> DocumentModels { get; set; } = new List<DocumentModel>();
        public string LegalEntityType { get; set; }
    }
    public class GoodAndServiceModel
    {
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string StatusDate { get; set; }
        public string FirstUseDate { get; set; }
        public string FirstUseInCommerceDate { get; set; }
        public string PrimeClassCode { get; set; }
        public string Description { get; set; }
        public List<GoodAndServiceClassModel> GoodAndServiceClasses { get; set; } = new List<GoodAndServiceClassModel>();
    }
    public class GoodAndServiceClassModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public GoodAndServiceClassType Type { get; set; }
    }
    public class ProsecutionHistoryModel
    {
        public Guid Id { get; set; }
        public int EntryNumber { get; set; }
        public string EntryCode { get; set; }
        public string EntryType { get; set; }
        public int ProceedingNumber { get; set; }
        public DateTimeOffset? EntryDate { get; set; }
        public string EntryDateText { get; set; }
        public string Description { get; set; }
        public string TrademarkName { get; set; }
        public string ClientName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class PublicationOfficialGazetteModel
    {
        public Guid TrademarkId { get; set; }
        public DateTime? IssueDate { get; set; }
        public string StatusCodeText { get; set; }
        public string StatusCode { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryCodeText { get; set; }
        public string CategoryReason { get; set; }
        public DateTime? ActionDate { get; set; }
        public long RegistrationNumber { get; set; }
    }
    public class DesignSearchModel
    {
        public string Code { get; set; }
        public List<DesignSearchDescriptionModel> DesignSearchDescriptions { get; set; } = new List<DesignSearchDescriptionModel>();
    }
    public class DesignSearchDescriptionModel
    {
        public string Description { get; set; }
    }
    public class OwnerGroupModel
    {
        public Guid TrademarkId { get; set; }
        public int Type { get; set; }
        public int EntityNumber { get; set; }
        public int TypeCode { get; set; }
        public string TypeDescription { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string IsoCode { get; set; }
        public string IsoName { get; set; }
        public string WipoCode { get; set; }
        public string WipoName { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
    }

    public class DocumentModel
    {
        public Guid DocumentId { get; set; }
        public long SerialNumber { get; set; }
        public DateTime? MailRoomDate { get; set; }
        public string MailRoomDateString { get; set; }
        public DateTimeOffset? ScanDateTime { get; set; }
        public int TotalPageQuantity { get; set; }
        public string SourceSystem { get; set; }
        public List<DocumentTypeModel> DocumentTypeModels { get; set; } = new List<DocumentTypeModel>();
        public List<DocumentUrlModel> DocumentUrlModels { get; set; } = new List<DocumentUrlModel>();
        public string Url { get; set; }
        public string DocumentType { get; set; }
        public string Description { get; set; }
    }
    public class DocumentUrlModel
    {
        public string Url { get; set; }
        public string MediaType { get; set; }
    }
    public class DocumentTypeModel
    {
        public string Code { get; set; }
        public string DocumentTypeCode { get; set; }
        public string CategoryTypeCode { get; set; }
        public string Description { get; set; }
        public string CodeDescription { get; set; }
    }

    public class UserInfo
    {
        public string Email { get; set; }
        public long SerialNumber { get; set; }
        public string ClientName { get; set; }
        public string LegalEntityType { get; set; }
        public string ClientAddress { get; set; }
        public string Country { get; set; }
        public string ClientCity { get; set; }
        public string ClientZipCode { get; set; }
        public string ClientState { get; set; }
    }
    public class UploadFileModel : SaveResults
    {
        public string FileName { get; set; }
    }

    public class TrademarkImportFileModel
    {
        public string BatchName { get; set; }
        public List<TrademarkImportItemsModel> TrademarkFileItems { get; set; } = new List<TrademarkImportItemsModel>();
    }

    public class TrademarkImportItemsModel
    {
        public bool IsActive { get; set; }
        public Guid TrademarkId { get; set; }
        public string Email { get; set; }
        public string ClientName { get; set; }
        public string BatchName { get; set; }
        public string TrademarkName { get; set; }
        public DateTime Created { get; set; }
        public DateTime? DateImported { get; set; }
        public TrademarkStatus Status { get; set; }
        public string StatusString { get; set; }
        public long SerialNumber { get; set; }
        public bool IsDuplicate { get; set; }
        public Guid Id { get; set; }
        public string ImagePath { get; set; }
    }
}
