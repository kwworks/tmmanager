﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public sealed class TrademarkStatusChecker
    {
        private Trademark _trademark;
        private ProsecutionHistory[] _prosecutionHistory;
        private Document[] _documents;
        public bool IsApplication { get; private set; }
        public bool IsRegistered { get; private set; }
        public string CurrentStatus { get; private set; }
        public DateTime? NextDeadline { get; private set; }
        public RegisteredTrademarkStatusColor RegisteredStatusColor { get; private set; }
        public TrademarkStatusChecker(Trademark trademark)
        {
            _trademark = trademark;
            _prosecutionHistory = _trademark.ProsecutionHistorys.OrderByDescending(c => c.EntryDate).ToArray();
            _documents = _trademark.Documents.OrderByDescending(c => c.MailRoomDate).ToArray();
            if (_trademark != null)
            {
                if (_trademark.Tm5StatusDescription.ToLowerInvariant().StartsWith("dead"))
                {
                    CurrentStatus = TrademarkProgressStatus.Dead;
                    RegisteredStatusColor = RegisteredTrademarkStatusColor.Red;
                }
                else if (_trademark.UsRegistrationDate.HasValue)
                {
                    IsRegistered = true;
                    ParsePostRegistrationStatus();
                }
                else
                {
                    IsApplication = true;
                    ParseApplicationStatus();
                }
                
            }
        }


        private void ParsePostRegistrationStatus()
        {
            CurrentStatus = TrademarkProgressStatus.Registered;
            var years = (DateTime.Now - _trademark.UsRegistrationDate.Value).TotalDays / 365;
            if (years < 5)
            {
                RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                NextDeadline = _trademark.UsRegistrationDate.Value.AddYears(5);
            }
            else
            {
                if (years >= 5 && years <= 6)
                {
                    var sixYearsRenewalAcceptedAction = GetHistory(_trademark.UsRegistrationDate.Value, ProsecutionHistory.REGISTERED_SEC_8_6_YR_ACCEPTED);
                    if (sixYearsRenewalAcceptedAction == null)
                    {
                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Yellow;
                        NextDeadline = _trademark.UsRegistrationDate.Value.AddYears(5);
                    }
                    else
                    {
                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                        NextDeadline = _trademark.UsRegistrationDate.Value.AddYears(9);
                    }
                }
                else
                {
                    var sixYearsRenewalAcceptedAction = GetHistory(_trademark.UsRegistrationDate.Value, ProsecutionHistory.REGISTERED_SEC_8_6_YR_ACCEPTED);
                    if (sixYearsRenewalAcceptedAction == null)
                    {
                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Red;
                        NextDeadline = _trademark.UsRegistrationDate.Value.AddYears(5);
                    }
                    else
                    {
                        if (years < 9)
                        {
                            RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                            NextDeadline = _trademark.UsRegistrationDate.Value.AddYears(9);
                        }
                        else
                        {
                            var all = _prosecutionHistory.Where(c => c.IsActive && c.Description.StartsWith(ProsecutionHistory.REGISTERED_SEC_8_10_YR_ACCEPTED)).OrderByDescending(c => c.EntryDate).ToArray();
                            var nextRenewalDate = _trademark.UsRegistrationDate.Value.AddYears(((all.Length + 1) * 10) - 1);
                            var maxRenewalDate = nextRenewalDate.AddYears(1);
                            var hasRenewed = all.Any(c => c.EntryDate.Value >= nextRenewalDate);
                            var now = DateTime.Now;
                            if (nextRenewalDate > now)
                            {
                                RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                                NextDeadline = nextRenewalDate;
                            }
                            else
                            {
                                if (now > nextRenewalDate && now < maxRenewalDate)
                                {
                                    if (hasRenewed)
                                    {
                                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                                        NextDeadline = nextRenewalDate.AddYears(10);
                                    }
                                    else
                                    {
                                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Yellow;
                                        NextDeadline = nextRenewalDate;
                                    }
                                }
                                else
                                {
                                    if (hasRenewed)
                                    {
                                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Green;
                                        NextDeadline = nextRenewalDate.AddYears(10);
                                    }
                                    else
                                    {
                                        RegisteredStatusColor = RegisteredTrademarkStatusColor.Red;
                                        NextDeadline = nextRenewalDate;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ParseApplicationStatus()
        {
            if (_trademark.IsFiled44d || _trademark.IsFiled44e || _trademark.IsFiled66a)
            {
                CurrentStatus = TrademarkProgressStatus.International;
            }
            else
            {
                if (_trademark.IsFiledInternationLaw)//FiledITU
                {
                    ParseStatusUsing1BTimeline();
                }
                else
                {
                    ParseStatusUsing1ATimeline();
                }
            }
        }
        private void ParseStatusUsing1BTimeline()
        {
            var statementOfUseDocument = GetDocument(Document.STATEMENT_OF_USE);
            if (statementOfUseDocument == null)
            {
                var noticeOfAllowanceDocument = GetDocument(Document.NOTICE_OF_ALLOWANCE);
                if (noticeOfAllowanceDocument == null)
                {
                    ParseStatusUsing1ATimeline();
                }
                else
                {
                    if (TryCalculateStatementOfUseDeadLine(noticeOfAllowanceDocument))
                    {
                        var noStatementOfUseFiledAction = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.ABANDONED_NO_STATEMENT_OF_USE_FILED);
                        if (noStatementOfUseFiledAction == null)
                        {
                            CurrentStatus = TrademarkProgressStatus.NineA;
                        }
                        else
                        {
                            CurrentStatus = TrademarkProgressStatus.NineC;
                        }
                    }
                    else
                    {
                        CurrentStatus = TrademarkProgressStatus.Eight;
                    }
                }
            }
            else
            {
                var intentToUseParalegalAction = GetHistory(statementOfUseDocument.MailRoomDate.Value, ProsecutionHistory.CASE_ASSIGNED_TO_INTENT_TO_USE_PARALEGAL);
                if (intentToUseParalegalAction == null)
                {
                    CurrentStatus = TrademarkProgressStatus.NineB;
                }
                else
                {
                    var officeAction = GetHistory(intentToUseParalegalAction.EntryDate.Value.Date, ProsecutionHistory.NON_FINAL_ACTION_WRITTEN);
                    if (officeAction == null)
                    {
                        CurrentStatus = TrademarkProgressStatus.Eleven;
                    }
                    else
                    {
                        CurrentStatus = TrademarkProgressStatus.TweleveB;
                    }
                }
            }
        }

        private bool TryCalculateStatementOfUseDeadLine(Document noticeOfAllowanceDocument)
        {
            var hasExtension = false;
            NextDeadline = noticeOfAllowanceDocument.MailRoomDate.Value.AddMonths(6);
            var extension5Action = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.EXTENSION_5_GRANTED);
            if (extension5Action == null)
            {
                var extension4Action = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.EXTENSION_4_GRANTED);
                if (extension4Action == null)
                {
                    var extension3Action = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.EXTENSION_3_GRANTED);
                    if (extension3Action == null)
                    {
                        var extension2Action = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.EXTENSION_2_GRANTED);
                        if (extension2Action == null)
                        {
                            var extension1Action = GetHistory(noticeOfAllowanceDocument.MailRoomDate.Value, ProsecutionHistory.EXTENSION_1_GRANTED);
                            if (extension1Action != null)
                            {
                                NextDeadline = NextDeadline.Value.AddMonths(6).Date;
                                hasExtension = true;
                            }
                        }
                        else
                        {
                            NextDeadline = NextDeadline.Value.AddMonths(6).Date;
                            hasExtension = true;
                        }
                    }
                    else
                    {
                        NextDeadline = NextDeadline.Value.AddMonths(6).Date;
                        hasExtension = true;
                    }
                }
                else
                {
                    NextDeadline = NextDeadline.Value.AddMonths(6).Date;
                    hasExtension = true;
                }
            }
            else
            {
                NextDeadline = NextDeadline.Value.AddMonths(6).Date;
                hasExtension = true;
            }
            return hasExtension;
        }

        private Document GetDocument(string name)
        {
            return _documents.FirstOrDefault(c => c.IsActive && c.DocumentType.Description == name);
        }
        private ProsecutionHistory GetHistory(DateTime start, string name)
        {
            foreach (var each in _prosecutionHistory)
            {
                if (each.EntryDate.Value.Date >= start && each.Description.StartsWith(name))
                {
                    return each;
                }
            }
            return null;
        }
        private ProsecutionHistory GetHistoryStartsWithName(DateTime start, string name)
        {
            foreach (var each in _prosecutionHistory)
            {
                if (each.EntryDate.Value.Date >= start && each.Description.StartsWith(name))
                {
                    return each;
                }
            }
            return null;
        }
        //private ProsecutionHistory GetHistoryAny(DateTime start, params string[] names)
        //{
        //    foreach (var name in names)
        //    {
        //        foreach (var each in _prosecutionHistory)
        //        {
        //            if (each.EntryDate >= start && each.Description == name)
        //            {
        //                return each;
        //            }
        //        }
        //    }
        //    return null;
        //}

        private void ParseStatusUsing1ATimeline()
        {
            var publishedForOppositionAction = GetHistory(DateTime.MinValue, ProsecutionHistory.PUBLISHED_FOR_OPPOSITION);
            if (publishedForOppositionAction == null)
            {
                var exParteAppealInstitutedAction = GetHistory(DateTime.MinValue, ProsecutionHistory.EX_PARTE_APPEAL_INSTITUTED);
                if (exParteAppealInstitutedAction == null)
                {
                    var abandonmentFailureToResponseOrLateAction = GetHistory(DateTime.MinValue, ProsecutionHistory.ABANDONMENT_FAILURE_TO_RESPOND_OR_LATE_RESPONSE);
                    if (abandonmentFailureToResponseOrLateAction == null)
                    {
                        var officeAction = GetHistory(DateTime.MinValue, ProsecutionHistory.NON_FINAL_ACTION_WRITTEN);
                        var approvedForPubAction = GetHistoryStartsWithName(DateTime.MinValue, "APPROVED FOR PUB");
                        if (officeAction == null)
                        {
                            if (approvedForPubAction == null)
                            {
                                var finalRefusalWrittenAction = GetHistory(DateTime.MinValue, ProsecutionHistory.FINAL_REFUSAL_WRITTEN);
                                if (finalRefusalWrittenAction == null)
                                {
                                    var teasResponseToOfficeActionReceivedAction = GetHistory(DateTime.MinValue, ProsecutionHistory.TEAS_RESPONSE_TO_OFFICE_ACTION_RECEIVED);
                                    if (teasResponseToOfficeActionReceivedAction == null)
                                    {
                                        var nonFinalActionWrittenAction = GetHistory(DateTime.MinValue, ProsecutionHistory.NON_FINAL_ACTION_WRITTEN);
                                        if (nonFinalActionWrittenAction == null)
                                        {
                                            var assignedToExaminerAction = GetHistory(DateTime.MinValue, ProsecutionHistory.ASSIGNED_TO_EXAMINER);
                                            if (assignedToExaminerAction == null)
                                            {
                                                CurrentStatus = TrademarkProgressStatus.One;
                                            }
                                            else
                                            {
                                                CurrentStatus = TrademarkProgressStatus.Two;
                                            }
                                        }
                                        else
                                        {
                                            CurrentStatus = TrademarkProgressStatus.ThreeB;
                                        }
                                    }
                                    else
                                    {
                                        CurrentStatus = TrademarkProgressStatus.FourA;
                                    }
                                }
                                else
                                {
                                    CurrentStatus = TrademarkProgressStatus.FiveB;
                                }
                            }
                            else
                            {
                                CurrentStatus = TrademarkProgressStatus.FiveA;
                            }
                        }
                        else
                        {
                            if (approvedForPubAction == null)
                            {
                                var assignedToExaminerAction = GetHistory(DateTime.MinValue, ProsecutionHistory.ASSIGNED_TO_EXAMINER);
                                if (assignedToExaminerAction == null)
                                {
                                    CurrentStatus = TrademarkProgressStatus.One;
                                }
                                else
                                {
                                    CurrentStatus = TrademarkProgressStatus.Two;
                                }
                            }
                            else
                            {
                                CurrentStatus = TrademarkProgressStatus.ThreeA;
                            }
                        }
                    }
                    else
                    {
                        CurrentStatus = TrademarkProgressStatus.FourB;
                    }
                }
                else
                {
                    CurrentStatus = TrademarkProgressStatus.SixA;
                }
            }
            else
            {
                CurrentStatus = TrademarkProgressStatus.SevenA;
            }
        }
    }
}
