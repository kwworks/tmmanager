﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public static class TrademarkProgressStatus
    {
        public const string International = "I";
        public const string Registered = "R";
        public const string Dead = "D";
        public const string NineB = "9b";
        public const string NineA = "9a";
        public const string NineC = "9c";
        public const string Eleven = "11";
        public const string TweleveB = "12b";
        public const string Eight = "8";
        public const string SevenA = "7a";
        public const string SixA = "6a";
        public const string FourB = "4b";
        public const string ThreeA = "3a";
        public const string FiveA = "5a";
        public const string FiveB = "5b";
        public const string FourA = "4a";
        public const string ThreeB = "3b";
        public const string Two = "2";
        public const string One = "1";
    }
}
