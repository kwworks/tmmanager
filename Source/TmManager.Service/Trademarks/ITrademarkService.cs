﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Admin;

namespace TmManager.Trademarks
{
    public interface ITrademarkService
    {
        IQueryable<Trademark> TrademarksIQuerable();
        Task<List<TrademarkResponse>> GetDuplicateTrademarks(List<long> serialNumbers);
        Task<List<TrademarkResponse>> GetTrademarks();
        Task<OwnerGroupModel> GetOwnerDetailForApi(string email);
        Task<List<TrademarkResponse>> GetOwnerTrademarksForApi(string email);
        Task<SaveResults> VerifyEmail(string email);
        Task<SaveResults> RequestAccess(RequestAccessModel model);
        DatatTableResponse GetTrademarksForDataTable(int draw, int skip, string sortColumn, string sortColumnDirection);
        Task<GridDataSourceResultModel> SearchTrademarksAsync(string status, int skip, int take);
        Task<GridDataSourceResultModel> SearchTrademarksAsync(DateTime? StartDate, DateTime? EndDate, string SortDirection, string SortMember, string Status, int skip, int take);
        Task<GridDataSourceResultModel> SearchTrademarkEventsAsync(DateTime? StartDate, DateTime? EndDate, string SortDirection, string SortMember, int skip, int take);
        Task<int> CountTrademarksAsync(string status);
        Task<int> CountUnknownAttorneyTrademarksAsync();
        Task<TrademarkResponse> GetTrademarkDetail(Guid id);
        Task<ClientDetailResponse> GetClientDetail(string id);
        Task<ClientDetailResponse> GetClientDetailByEmail(string email);
        Task<TrademarkResponse> GetTrademarkDetail(long serialNumber);
        Task<List<TrademarkResponse>> GetTrademarksForApi(string email);
        Task<TrademarkResponse> GetTrademarkDetailForApi(Guid id);
        Task<TrademarkResponseModel> GetTrademark(string email, string id);
        Task<GridDataSourceResultModel> GetTrademarkImportFiles();
        Task<List<TrademarkImportFileModel>> GetTrademarkImportFilesAsync();
        Task<List<TrademarkImportItemsModel>> GetTrademarkImportFilesGroupAsync();
        Task<List<TrademarkImportItemsModel>> GetTrademarkImportFilesGroupAsync(DateTime StartDate, DateTime EndDate);
        Task<TrademarkResponseModel> SaveTrademark(string serialNo, string fallbackemail);
        Task SaveBatchTrademark(TrademarkImportFileModel model);
        Task SyncBatchTrademark();
        Task<TrademarkResponseModel> UpdateTrademark(long serialNumber);
        Task<List<GoodAndServiceModel>>GetGoodsAndService(Guid id);
        Task<SaveResults> RejectFromBatch(Guid id);
        Task<SaveResults> AcceptFromBatch(Guid id);
        Task<TrademarkCountsResponse> GetDashboardCounts();
        Task UpdateImportedFilesInfo(Guid id);
        Task UpdateAllImportedFilesInfo(List<Guid> ids);
        Task<TrademarkResponseModel> SaveFileRecords(TrademarkImportFileModel model);
    }
}
