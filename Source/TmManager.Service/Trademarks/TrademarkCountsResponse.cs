﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class TrademarkCountsResponse
    {
        public int TrademarkCounts { get; set; }
        public int DocumentsCounts { get; set; }
        public int EventsCounts { get; set; }
        public int ClientsCount { get; set; }
    }
}
