﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Attorneys;
using TmManager.Data;
using TmManager.Tsdr;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using TmManager.Identity;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using TmManager.Data.CloudStorage;
using TmManager.Admin;
using TmManager.Messaging;
using TmManager.Notifications;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace TmManager.Trademarks
{
    public class TrademarkService : ITrademarkService
    {
        private readonly ITrademarkRepository _trademarkRepository;
        private readonly IUnitOfWork _UnitOfWork;
        private readonly IFirmRepository _firmRepository;
        private readonly IAttorneyRepository _attorneyRepository;
        private readonly IRecognizedAttorneyRepository _recognizedAttorneyRepository;
        private readonly IGoodAndServiceClassRepository _goodAndServiceClassRepository;
        private readonly IDesignSearchClassRepository _designSearchClassRepository;
        private readonly INotificationService _notificationService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager _userManager;
        public TrademarkService(ITrademarkRepository trademarkRepository, IFirmRepository firmRepository, IAttorneyRepository attorneyRepository, IGoodAndServiceClassRepository goodAndServiceClassRepository, IDesignSearchClassRepository designSearchClassRepository, IRecognizedAttorneyRepository recognizedAttorneyRepository, INotificationService notificationService, IHttpContextAccessor httpContextAccessor, UserManager userManager, IUnitOfWork UnitOfWork)
        {
            _UnitOfWork = UnitOfWork;
            _userManager = userManager;
            _trademarkRepository = trademarkRepository;
            _firmRepository = firmRepository;
            _attorneyRepository = attorneyRepository;
            _recognizedAttorneyRepository = recognizedAttorneyRepository;
            _httpContextAccessor = httpContextAccessor;
            _goodAndServiceClassRepository = goodAndServiceClassRepository;
            _designSearchClassRepository = designSearchClassRepository;
            _notificationService = notificationService;
        }
        public async Task<List<TrademarkResponse>> GetTrademarks()
        {
            var response = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetTrademark());
            return response.Select(x => new TrademarkResponse()
            {
                Id = x.Id,
                CurrentStatus = x.CurrentStatus,
                NextDeadline = x.NextDeadline,
                StatusColor = x.StatusColor,
                Image = x.Image,
                Name = x.Name,
                ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                ImportedDate = x.Created,
                PrimaryEmail = x.User.Email,
                StatusDescription = x.Tm5StatusDescription,
                SerialNumber = (int)x.SerialNumber,
                Serial = x.SerialNumber.ToString(),
                AttorneyName = x.Attorney.Name ?? "N/A"
            }).ToList();
        }
        public DatatTableResponse GetTrademarksForDataTable(int draw, int skip, string sortColumn, string sortColumnDirection)
        {
            var response = _trademarkRepository.TrademarkIQuerableAsync().Skip(skip).Take(draw);
            int totalCount = _trademarkRepository.TrademarkIQuerableAsync().Count();
            switch (sortColumn)
            {
                case "Name":
                    if (sortColumnDirection == "asc") { response.OrderBy(x => x.Name); } else { response.OrderByDescending(x => x.Name); }
                    break;

                case "Serial Number":
                    if (sortColumnDirection == "asc") { response.OrderBy(x => x.SerialNumber); } else { response.OrderByDescending(x => x.SerialNumber); }
                    break;

                case "Client Name":
                    if (sortColumnDirection == "asc") { response.OrderBy(x => x.OwnerGroups.OrderByDescending(x => x.Type).First().Name); } else { response.OrderByDescending(x => x.OwnerGroups.OrderByDescending(x => x.Type).First().Name); }
                    break;

                case "Client Email":
                    if (sortColumnDirection == "asc") { response.OrderBy(x => x.User.Email); } else { response.OrderByDescending(x => x.User.Email); }
                    break;

                case "Date Imported":
                    if (sortColumnDirection == "asc") { response.OrderBy(x => x.Created); } else { response.OrderByDescending(x => x.Created); }
                    break;

                default:
                    response.OrderBy(x => x.Name);
                    break;
            }

            var result = new DatatTableResponse();
            if (response.Count() > 0)
            {
                result.data = response.Select(x => new TrademarkResponse()
                {
                    Id = x.Id,
                    Name = x.TrademarkElement,
                    ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                    ImportedDateString = x.Created.ToShortDateString(),
                    PrimaryEmail = x.User.Email,
                    StatusDescription = x.Tm5StatusDescription,
                    SerialNumber = (int)x.SerialNumber,
                    Serial = x.SerialNumber.ToString(),
                    AttorneyName = x.Attorney.Name ?? "N/A"
                }).ToList();
            }
            else
                result.data = new List<TrademarkResponse>();

            result.draw = draw;
            result.recordsTotal = totalCount;
            result.recordsFiltered = totalCount;
            return result;
        }
        public async Task<GridDataSourceResultModel> SearchTrademarksAsync(string status, int skip, int take)
        {
            return new GridDataSourceResultModel()
            {
                Data = (await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetByStatus(status), skip, take))
                .Select(x => new TrademarkResponseForKendo()
                {
                    Id = x.Id,
                    Name = x.TrademarkElement,
                    ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                    ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                    FilingDate = x.FiliingDate,
                    ImportedDateString = x.Created.ToShortDateString(),
                    PrimaryEmail = x.User.Email,
                    StatusDescription = x.Tm5StatusDescription,
                    SerialNumber = (int)x.SerialNumber,
                    //Serial = x.SerialNumber.ToString(),
                    AttorneyName = x.Attorney.Name ?? "N/A",
                }).OrderByDescending(x => x.FilingDate).ToList(),
                Total = await _trademarkRepository.CountAdvertisementsAsync(TrademarkSpecification.GetByStatus(status))
            };
        }
        public async Task<GridDataSourceResultModel> SearchTrademarksAsync(DateTime? StartDate, DateTime? EndDate, string SortDirection, string SortMember, string Status, int skip, int take)
        {
            var data = _trademarkRepository.TrademarkIQuerableAsync(TrademarkSpecification.GetByFilters(StartDate, EndDate, Status), skip, take);
            if (!string.IsNullOrEmpty(SortDirection))
            {
                switch (SortMember)
                {
                    case "Name":
                        data = SortDirection == "Ascending" ? data.OrderBy(x => x.TrademarkElement) : data.OrderByDescending(x => x.TrademarkElement);
                        break;
                    case "SerialNumber":
                        data = SortDirection == "Ascending" ? data.OrderBy(x => x.SerialNumber) : data.OrderByDescending(x => x.SerialNumber);
                        break;
                    case "ClientName":
                        data = SortDirection == "Ascending" ? data.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : data.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                        break;
                    case "PrimaryEmail":
                        data = SortDirection == "Ascending" ? data.OrderBy(x => x.User.Email) : data.OrderByDescending(x => x.User.Email);
                        break;
                    case "ImportedDateString":
                        data = SortDirection == "Ascending" ? data.OrderBy(x => x.Created) : data.OrderByDescending(x => x.Created);
                        break;
                    default:
                        break;
                }
            }
            else
                data = data.OrderByDescending(x => x.FiliingDate);

            return new GridDataSourceResultModel()
            {
                Data = (data)
                .Select(x => new TrademarkResponseForKendo()
                {
                    Id = x.Id,
                    Name = x.TrademarkElement,
                    ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                    ClientEmail = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Email : string.Empty,
                    FilingDate = x.FiliingDate,
                    ImportedDateString = x.Created.ToShortDateString(),
                    PrimaryEmail = x.User.Email,
                    StatusDescription = x.Tm5StatusDescription,
                    SerialNumber = (int)x.SerialNumber,
                    AttorneyName = x.Attorney.Name ?? "N/A",
                }).OrderByDescending(x => x.FilingDate).ToList(),
                Total = await _trademarkRepository.CountAdvertisementsAsync(TrademarkSpecification.GetByFilters(StartDate, EndDate, Status))
            };
        }
        public async Task<GridDataSourceResultModel> SearchTrademarkEventsAsync(DateTime? StartDate, DateTime? EndDate, string SortDirection, string SortMember, int skip, int take)
        {
            try
            {
                var data = _trademarkRepository.TrademarkIQuerableAsync(TrademarkSpecification.GetByProsecutionHistoryDate(StartDate, EndDate), skip, take);
                if (!string.IsNullOrEmpty(SortDirection))
                {
                    switch (SortMember)
                    {
                        case "EventDate":
                            data = SortDirection == "Ascending" ? data.OrderBy(x => x.ProsecutionHistorys.Select(p => p.EntryDate)) : data.OrderByDescending(x => x.ProsecutionHistorys.Select(p => p.EntryDate));
                            break;
                        case "EventName":
                            data = SortDirection == "Ascending" ? data.OrderBy(x => x.ProsecutionHistorys.Select(p => p.Description)) : data.OrderByDescending(x => x.ProsecutionHistorys.Select(p => p.Description));
                            break;
                        case "Name":
                            data = SortDirection == "Ascending" ? data.OrderBy(x => x.TrademarkElement) : data.OrderByDescending(x => x.TrademarkElement);
                            break;
                        case "ClientName":
                            data = SortDirection == "Ascending" ? data.OrderBy(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty) : data.OrderByDescending(x => x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty);
                            break;
                        default:
                            break;
                    }
                }
                else
                    data = data.OrderByDescending(x => x.ProsecutionHistorys.Select(p => p.EntryDate));

                var prosecutionList = new List<ProsecutionHistoryModel>();
                if (data.AsEnumerable().Any())
                {
                    foreach (var pHistory in data.AsEnumerable())
                    {
                        var prosecutionHistory = pHistory.ProsecutionHistorys;
                        foreach (var item in prosecutionHistory)
                        {
                            prosecutionList.Add(new ProsecutionHistoryModel()
                            {
                                Id = item.Id,
                                TrademarkName = item.Trademark.Name,
                                ClientName = item.Trademark.OwnerGroups.Count() > 0 ? item.Trademark.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                                EntryDate = item.EntryDate,
                                Description = item.Description,
                                EntryCode = item.EntryCode,
                                ProceedingNumber = item.ProceedingNumber,
                                EntryType = item.EntryType
                            });
                        }
                    }
                }
                return new GridDataSourceResultModel()
                {
                    Data = prosecutionList,
                    Total = await _trademarkRepository.CountAdvertisementsAsync(TrademarkSpecification.GetByProsecutionHistoryDate(StartDate, EndDate))
                };
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            return null;
        }
        public async Task<int> CountTrademarksAsync(string status)
        {
            return await _trademarkRepository.CountAdvertisementsAsync(TrademarkSpecification.GetByStatus(status));
        }
        public async Task<int> CountUnknownAttorneyTrademarksAsync()
        {
            return await _trademarkRepository.CountAdvertisementsAsync(TrademarkSpecification.GetUnknownAttorney());
        }
        public IQueryable<Trademark> TrademarksIQuerable()
        {
            return _trademarkRepository.TrademarkIQuerableAsync();
        }
        public async Task<List<TrademarkResponse>> GetTrademarksForApi(string email)
        {
            var response = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetTrademarkByEmail(email));
            return response.Select(x => new TrademarkResponse()
            {
                Id = x.Id,
                Image = x.Image,
                CurrentStatus = x.CurrentStatus,
                CurrentStatusDescription = x.IsFiledInternationLaw ? Constants.GetTrademarkCurrentStatus(x.CurrentStatus, true) : Constants.GetTrademarkCurrentStatus(x.CurrentStatus, false),
                NextDeadline = x.NextDeadline,
                StatusColor = x.StatusColor,
                Name = x.TrademarkElement,
                ClientName = x.OwnerGroups.Any() ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                ImportedDate = x.Created,
                PrimaryEmail = x.User != null ? x.User.Email : string.Empty,
                TrademarkStatus = x.Tm5Status,
                StatusDescription = x.Tm5StatusDescription,
                SerialNumber = (int)x.SerialNumber,
                Serial = x.SerialNumber.ToString(),
                AttorneyName = x.Attorney.Name ?? string.Empty,
            }).ToList();
        }
        public async Task<List<TrademarkResponse>> GetOwnerTrademarksForApi(string email)
        {
            var response = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetTrademarksByOwner(email));
            return response.Select(x => new TrademarkResponse()
            {
                Id = x.Id,
                Image = x.Image,
                CurrentStatus = x.CurrentStatus,
                CurrentStatusDescription = x.IsFiledInternationLaw ? Constants.GetTrademarkCurrentStatus(x.CurrentStatus, true) : Constants.GetTrademarkCurrentStatus(x.CurrentStatus, false),
                NextDeadline = x.NextDeadline,
                StatusColor = x.StatusColor,
                Name = x.TrademarkElement,
                ClientName = x.OwnerGroups.Any() ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                ImportedDate = x.Created,
                PrimaryEmail = x.User.Email,
                TrademarkStatus = x.Tm5Status,
                StatusDescription = x.Tm5StatusDescription,
                SerialNumber = (int)x.SerialNumber,
                Serial = x.SerialNumber.ToString(),
                AttorneyName = x.Attorney.Name ?? string.Empty,
            }).ToList();
        }
        public async Task<OwnerGroupModel> GetOwnerDetailForApi(string email)
        {
            var response = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetOwnerDetailByEmail(email));
            var owner = response.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
            return new OwnerGroupModel()
            {
                Email = owner.Email,
                Name = owner.Name,
                Address = owner.Address,
                City = owner.City,
                State = owner.State,
                IsoName = owner.IsoName,
                Country = owner.Country,
                ZipCode = owner.ZipCode,

            };
        }
        public async Task<TrademarkResponse> GetTrademarkDetail(Guid id)
        {
            var response = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetTrademarkById(id));
            var goodAndServices = new List<GoodAndServiceModel>();
            var designSearch = new List<DesignSearchModel>();
            var documentsList = new List<DocumentModel>();

            if (response.Documents != null)
            {
                foreach (var item in response.Documents)
                {
                    var docArray = item.DocumentUrls.Select(x => x.Url).FirstOrDefault().Split('/');
                    var documentType = item.TotalPageQuantity > 1 && item.DocumentUrls.Where(x => x.DocumentId == item.Id).Count() > 1 ? "MULTI" : item.DocumentUrls.Where(x => x.DocumentId == item.Id).OrderByDescending(x => x.MediaType).FirstOrDefault().MediaType;
                    string docId = docArray[9];
                    if (docId == "registration-certificate")
                        docId = string.Empty;

                    switch (documentType)
                    {
                        case "application/xml":
                            documentType = "XML";
                            break;
                        case "image/jpeg":
                            documentType = "JPEG";
                            break;
                        case "MULTI":
                            documentType = "MULTI";
                            break;
                        default:
                            documentType = "MULTI";
                            break;
                    }
                    if (item.DocumentType.Description == "Specimen")
                        documentType = "JPEG";


                    documentsList.Add(new DocumentModel()
                    {
                        DocumentId = item.Id,
                        MailRoomDate = item.MailRoomDate,
                        MailRoomDateString = item.MailRoomDate.HasValue ? item.MailRoomDate.Value.ToString("MMM. dd, yyyy") : string.Empty,
                        Url = "https://tsdr.uspto.gov/documentviewer?caseId=sn" + item.SerialNumber + "&docId=" + docId,
                        DocumentType = documentType,
                        Description = item.DocumentType.Description
                    });
                }
            }

            foreach (var item in response.GoodAndServices)
            {
                var gosList = item.GoodAndServiceClass.Select(x => new GoodAndServiceClassModel
                {
                    Code = x.Code,
                    Description = x.Description,
                    Type = x.Type,
                }).ToList();

                var data = new GoodAndServiceModel
                {
                    StatusCode = item.StatusCode,
                    StatusDescription = item.StatusDescription,
                    StatusDate = item.StatusDate,
                    FirstUseDate = DateTimeHelper.FromLongToDate(item.FirstUseDate),
                    FirstUseInCommerceDate = DateTimeHelper.FromLongToDate(item.FirstUseInCommerceDate),
                    //FirstUseDate = DateTime.Parse(item.FirstUseDate.ToString().Substring(0, 4) + "-" + (item.FirstUseDate.ToString().Substring(4, 2) == "00" ? "01" : item.FirstUseDate.ToString().Substring(4, 2)) + "-" + (item.FirstUseDate.ToString().Substring(6, 2) == "00" ? "01" : item.FirstUseDate.ToString().Substring(6, 2))).ToString("MMM. dd, yyyy"),
                    //FirstUseInCommerceDate = DateTime.Parse(item.FirstUseInCommerceDate.ToString().Substring(0, 4) + "-" + item.FirstUseInCommerceDate.ToString().Substring(4, 2) + "-" + (item.FirstUseInCommerceDate.ToString().Substring(6, 2) == "00" ? "01" : item.FirstUseInCommerceDate.ToString().Substring(6, 2))).ToString("MMM. dd, yyyy"),
                    PrimeClassCode = item.PrimeClassCode,
                    Description = item.Description,
                    GoodAndServiceClasses = gosList.OrderBy(x => x.Code).ToList()
                };
                goodAndServices.Add(data);
            }

            if (response.DesignSearches.Any())
            {
                foreach (var item in response.DesignSearches)
                {
                    var designSearchModel = new DesignSearchModel
                    {
                        Code = item.Code,
                        DesignSearchDescriptions = item.DesignSearchDescriptionClasses.Select(d => new DesignSearchDescriptionModel { Description = d.Description }).ToList()
                    };
                    designSearch.Add(designSearchModel);
                }
            }
            var owner = response.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
            var publicationOfficialGazettes = response.PublicationOfficialGazettes.Select(x => new PublicationOfficialGazetteModel
            {
                IssueDate = x.IssueDate,
                StatusCodeText = x.StatusCodeText,
                StatusCode = x.StatusCode,
                CategoryCode = x.CategoryCode,
                CategoryCodeText = x.CategoryCodeText,
                CategoryReason = x.CategoryReason,
                ActionDate = x.ActionDate,
                RegistrationNumber = x.RegistrationNumber
            }).ToList();
            var prosecutionHistorys = response.ProsecutionHistorys.Select(x => new ProsecutionHistoryModel
            {
                EntryNumber = x.EntryNumber,
                EntryDate = x.EntryDate,
                EntryCode = x.EntryCode,
                EntryType = x.EntryType,
                ProceedingNumber = x.ProceedingNumber,
                EntryDateText = x.EntryDate.HasValue ? x.EntryDate.Value.ToString("MMM. dd, yyyy") : string.Empty,
                Description = x.Description
            }).OrderByDescending(x => x.EntryDate).ToList();
            var firm = new FirmModel
            {
                Name = response.Attorney.Firm.Name,
                IndividualName = response.Attorney.Firm.IndividualName,
                Phone = response.Attorney.Firm.Phone,
                Fax = response.Attorney.Firm.Fax,
                Email = response.Attorney.Firm.Email,
                IsEmailAuthticated = response.Attorney.Firm.IsEmailAuthticated
            };
            var attorney = new AttorneyModel
            {
                Id = response.Attorney.Id,
                //UserId = response.Attorney.UserId,
                Name = response.Attorney.Name,
                FullName = response.Attorney.FullName,
                Email = response.Attorney.Email,
                Email2 = response.Attorney.Email2,
                IsEmailAuthticated = response.Attorney.IsEmailAuthticated,
                Line1 = response.Attorney.Line1,
                City = response.Attorney.City,
                PostalCode = response.Attorney.PostalCode,
                CountryCode = response.Attorney.CountryCode,
                CountryName = response.Attorney.CountryName,
                Firm = firm
            };
            var ownerGroup = (OwnerGroupModel)default;
            if (owner != null)
            {
                new OwnerGroupModel();
                ownerGroup = new OwnerGroupModel
                {
                    TrademarkId = owner.TrademarkId,
                    Type = owner.Type,
                    EntityNumber = owner.EntityNumber,
                    TypeCode = owner.TypeCode,
                    TypeDescription = owner.TypeDescription,
                    Name = owner.Name,
                    Email = owner.Email,
                    Email2 = owner.Email2,
                    Address = owner.Address,
                    City = owner.City,
                    State = owner.State,
                    Country = owner.Country,
                    IsoCode = owner.IsoCode,
                    IsoName = owner.IsoName,
                    WipoCode = owner.WipoCode,
                    WipoName = owner.WipoName,
                    ZipCode = owner.ZipCode
                };
            }
            return new TrademarkResponse
            {
                Id = response.Id,
                CurrentStatusDescription = Constants.GetTrademarkCurrentStatus(response.Tm5StatusDescription.ToLowerInvariant().StartsWith("dead") ? "D" : response.CurrentStatus, response.IsFiledInternationLaw),
                NextDeadlineText = response.NextDeadline.HasValue ? response.NextDeadline.Value.ToString("MMM. dd,yyyy") : string.Empty,
                MaxNextDeadline = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.ToString("MMM. dd,yyyy") : string.Empty,
                MaxNextDeadlineWithFee = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.AddMonths(6).ToString("MMM. dd,yyyy") : string.Empty,
                CurrentStatus = response.Tm5StatusDescription.ToLowerInvariant().StartsWith("dead") ? "D" : response.CurrentStatus,
                StatusColor = response.StatusColor,
                Image = response.Image,
                Name = response.TrademarkElement,
                ClientName = response.OwnerGroups != null && response.OwnerGroups.Count() > 0 ? response.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                SerialNumber = (int)response.SerialNumber,
                TrademarkStatus = response.Status,
                LastUpdateDate = response.Modified.ToString("yyyy-MM-dd HH:mm:ss"),
                IsTrademark = response.TrademarkStatus,
                ExaminerNumber = response.ExaminerNumber,
                ExaminerName = response.ExaminerName,
                FiledAsTeasPlusApp = response.FiledAsTeasPlusApp,
                CurrentlyAsTeasPlusApp = response.CurrentlyAsTeasPlusApp,
                FiledAsTeasRfApp = response.FiledAsTeasRfApp,
                CurrentlyAsTeasRfApp = response.CurrentlyAsTeasRfApp,
                SupplementalRegister = response.SupplementalRegister,
                AmendPrincipal = response.AmendPrincipal,
                AmendSupplemental = response.AmendSupplemental,
                CertificationMark = response.CertificationMark,
                ServiceMark = response.ServiceMark,
                CollectiveMembershipMark = response.CollectiveMembershipMark,
                CollectiveServiceMark = response.CollectiveServiceMark,
                CollectiveTradeMark = response.CollectiveTradeMark,
                StatusDate = response.StatusDate,
                IsStandard = response.IsStandard,
                IsColorDrawing = response.IsColorDrawing,
                IsSection2f = response.IsSection2f,
                IsSection2fPartial = response.IsSection2fPartial,
                IsOthers = response.IsOthers,
                IsPreviousRegistered = response.IsPreviousRegistered,
                CommunityLegalServiceTotal = response.CommunityLegalServiceTotal,
                IsFiledUse = response.IsFiledUse,
                IsFiledInternationLaw = response.IsFiledInternationLaw,
                IsFiled44d = response.IsFiled44d,
                IsFiled44e = response.IsFiled44e,
                IsFiled66a = response.IsFiled66a,
                IsFiledNoBasis = response.IsFiledNoBasis,
                IsUseCurrent = response.IsUseCurrent,
                IsInternationalLawCurrent = response.IsInternationalLawCurrent,
                IsSect44eCurrent = response.IsSect44eCurrent,
                IsSect66aCurrent = response.IsSect66aCurrent,
                IsNoBasisCurrent = response.IsNoBasisCurrent,
                IsUseAmended = response.IsUseAmended,
                IsInternationalLawAmended = response.IsInternationalLawAmended,
                IsSect44dAmended = response.IsSect44dAmended,
                IsSect44eAmended = response.IsSect44eAmended,
                IsSect8Filed = response.IsSect8Filed,
                IsSect8Acpt = response.IsSect8Acpt,
                IsSect8PartialAcpt = response.IsSect8PartialAcpt,
                IsSect15Filed = response.IsSect15Filed,
                IsSect15Ack = response.IsSect15Ack,
                IsSect71Filed = response.IsSect71Filed,
                IsSect71Acpt = response.IsSect71Acpt,
                IsSect71PartialAcpt = response.IsSect71PartialAcpt,
                IsRenewalFiled = response.IsRenewalFiled,
                IsChangeInRegistration = response.IsChangeInRegistration,
                Sect44dCurrent = response.Sect44dCurrent,
                MarkDrawingCode = response.MarkDrawingCode,
                LawOffAssignedCode = response.LawOffAssignedCode,
                CurrentLocationCode = response.CurrentLocationCode,
                CurrentLocationDateString = response.CurrentLocationDate.HasValue ? response.CurrentLocationDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                ExternalStatusDescription = response.ExternalStatusDescription,
                InternationalStatusDescription = response.InternationalStatusDescription,
                DescriptionOfMark = response.DescriptionOfMark,
                CurrentLocation = response.CurrentLocation,
                NewLawOffAssignedCode = response.NewLawOffAssignedCode,
                LawOffAssigned = response.LawOffAssigned,
                Tm5Status = response.Tm5Status,
                Tm5StatusDescription = response.Tm5StatusDescription,
                StatusDefination = response.StatusDefination,
                UsRegistrationNumber = response.UsRegistrationNumber,
                UsRegistrationDate = response.UsRegistrationDate.HasValue ? response.UsRegistrationDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                StatusDateText = response.StatusDate.HasValue ? response.StatusDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                FiliingDateText = response.FiliingDate.HasValue ? response.FiliingDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                PublicationDateText = response.PublicationDate.HasValue ? response.PublicationDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                PublicationAllowanceDateText = response.PublicationAllowanceDate.HasValue ? response.PublicationAllowanceDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                TrademarkElement = response.TrademarkElement,
                TrademarkDrawingDescription = response.TrademarkDrawingDescription,
                PublicationOfficialGazettes = publicationOfficialGazettes,
                ProsecutionHistorys = prosecutionHistorys,
                GoodAndServices = goodAndServices.OrderBy(x => x.PrimeClassCode).ToList(),
                Attorney = attorney,
                OwnerGroup = ownerGroup,
                DesignSearch = designSearch.OrderBy(x => x.Code).ToList(),
                DocumentModels = documentsList.OrderByDescending(x => x.MailRoomDate).ThenBy(x => x.Description).ToList()
            };
        }
        public async Task<ClientDetailResponse> GetClientDetail(string id)
        {
            var response = new ClientDetailResponse();

            var clientList = await _userManager.Users.Where(x => x.Id == id).FirstOrDefaultAsync();
            var trademarkList = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetTrademarkByEmail(clientList.UserName));

            response.ClientDetail.ClientName = clientList.ClientName;
            response.ClientDetail.Email = clientList.Email;
            response.ClientDetail.LegalEntity = clientList.LegalEntityType;
            response.ClientDetail.Address = clientList.Street ?? "";
            response.ClientDetail.State = clientList.State ?? "";
            response.ClientDetail.Country = clientList.Country ?? "";
            response.ClientDetail.City = clientList.City ?? "";
            response.ClientDetail.Phone = clientList.PhoneNumber ?? "";
            response.ClientDetail.PhoneType = clientList.PhoneType ?? "";

            response.ClientContacts = clientList.UserContacts.Select(x => new ClientDetailContacts()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Phone = x.Phone,
                StreetAddress = x.StreetAddress ?? "",
                City = x.City ?? "",
                State = x.State ?? "",
                ZipCode = x.ZipCode ?? "",
                Country = x.Country ?? "",
                Email = x.Email,
                PhoneType = x.PhoneType,
                Status = x.Status
            }).ToList();

            response.ClientTrademarkList = trademarkList.Select(x => new ClientTrademarkList()
            {
                Id = x.Id,
                TrademarkName = x.TrademarkElement,
                SerialNumber = x.SerialNumber,
                ApplicationFilingDate = x.FiliingDate.HasValue ? x.FiliingDate.Value.ToString("yyyy.MM.dd") : string.Empty,
                DateImported = x.Created.ToString("yyyy.MM.dd")
            }).ToList();

            return response;
        }
        public async Task<ClientDetailResponse> GetClientDetailByEmail(string email)
        {
            var response = new ClientDetailResponse();

            var clientList = await _userManager.Users.Where(x => x.Email == email).FirstOrDefaultAsync();
            var trademarkList = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetTrademarkByEmail(clientList.Email));

            response.ClientDetail.Id = Guid.Parse(clientList.Id);
            response.ClientDetail.ClientName = clientList.ClientName;
            response.ClientDetail.Email = clientList.Email;
            response.ClientDetail.LegalEntity = clientList.LegalEntityType;
            response.ClientDetail.Address = clientList.Street;
            response.ClientDetail.State = clientList.State;
            response.ClientDetail.Country = clientList.Country;
            response.ClientDetail.City = clientList.City;
            response.ClientDetail.Phone = clientList.PhoneNumber;
            response.ClientDetail.PhoneType = clientList.PhoneType;
            response.ClientDetail.ZipCode = clientList.ZipCode;

            response.ClientContacts = clientList.UserContacts.Where(x => x.IsActive).Select(x => new ClientDetailContacts()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Phone = x.Phone,
                StreetAddress = x.StreetAddress,
                City = x.City,
                State = x.State,
                ZipCode = x.ZipCode,
                Country = x.Country,
                Email = x.Email,
                PhoneType = x.PhoneType,
                Status = x.Status
            }).ToList();

            response.ClientTrademarkList = trademarkList.Select(x => new ClientTrademarkList()
            {
                Id = x.Id,
                TrademarkName = x.TrademarkElement,
                SerialNumber = x.SerialNumber,
                ApplicationFilingDate = x.FiliingDate.HasValue ? x.FiliingDate.Value.ToString("yyyy.MM.dd") : string.Empty,
                DateImported = x.Created.ToString("yyyy.MM.dd")
            }).ToList();

            return response;
        }
        public async Task<TrademarkResponse> GetTrademarkDetail(long serialNumber)
        {
            var response = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetBySerialNumber(serialNumber));
            var goodAndServices = new List<GoodAndServiceModel>();
            var designSearch = new List<DesignSearchModel>();
            var documentsList = new List<DocumentModel>();

            foreach (var item in response.Documents)
            {
                var docArray = item.DocumentUrls.Select(x => x.Url).FirstOrDefault().Split('/');
                var documentType = item.TotalPageQuantity > 1 && item.DocumentUrls.Where(x => x.DocumentId == item.Id).Count() > 1 ? "MULTI" : item.DocumentUrls.Where(x => x.DocumentId == item.Id).Select(x => x.MediaType).FirstOrDefault();
                string docId = docArray[9];

                switch (documentType)
                {
                    case "application/xml":
                        documentType = "XML";
                        break;
                    case "image/jpeg":
                        documentType = "JPEG";
                        break;
                    case "MULTI":
                        documentType = "MULTI";
                        break;
                    default:
                        documentType = "MULTI";
                        break;
                }

                documentsList.Add(new DocumentModel()
                {
                    DocumentId = item.Id,
                    MailRoomDate = item.MailRoomDate,
                    MailRoomDateString = item.MailRoomDate.HasValue ? item.MailRoomDate.Value.ToString("MMM. dd, yyyy") : string.Empty,
                    Url = "https://tsdr.uspto.gov/documentviewer?caseId=sn" + item.SerialNumber + "&docId=" + docId,
                    DocumentType = documentType,
                    Description = item.DocumentType.Description
                });
            }

            foreach (var item in response.GoodAndServices)
            {
                var gosList = item.GoodAndServiceClass.Select(x => new GoodAndServiceClassModel
                {
                    Code = x.Code,
                    Description = x.Description,
                    Type = x.Type,
                }).ToList();

                var data = new GoodAndServiceModel
                {
                    StatusCode = item.StatusCode,
                    StatusDescription = item.StatusDescription,
                    StatusDate = item.StatusDate,
                    FirstUseDate = DateTimeHelper.FromLongToDate(item.FirstUseDate),
                    FirstUseInCommerceDate = DateTimeHelper.FromLongToDate(item.FirstUseInCommerceDate),
                    //FirstUseDate = DateTime.Parse(item.FirstUseDate.ToString().Substring(0, 4) + "-" + item.FirstUseDate.ToString().Substring(4, 2) + "-" + item.FirstUseDate.ToString().Substring(6, 2)).ToString("MMM. dd, yyyy"),
                    //FirstUseInCommerceDate = DateTime.Parse(item.FirstUseInCommerceDate.ToString().Substring(0, 4) + "-" + item.FirstUseInCommerceDate.ToString().Substring(4, 2) + "-" + item.FirstUseInCommerceDate.ToString().Substring(6, 2)).ToString("MMM. dd, yyyy"),
                    PrimeClassCode = item.PrimeClassCode,
                    Description = item.Description,
                    GoodAndServiceClasses = gosList
                };
                goodAndServices.Add(data);
            }

            if (response.DesignSearches.Count() > 0)
            {
                foreach (var item in response.DesignSearches)
                {
                    var designSearchModel = new DesignSearchModel
                    {
                        Code = item.Code,
                        DesignSearchDescriptions = item.DesignSearchDescriptionClasses.Select(d => new DesignSearchDescriptionModel { Description = d.Description }).ToList()
                    };
                    designSearch.Add(designSearchModel);
                }
            }

            var owner = response.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
            var publicationOfficialGazettes = response.PublicationOfficialGazettes.Select(x => new PublicationOfficialGazetteModel
            {
                IssueDate = x.IssueDate,
                StatusCodeText = x.StatusCodeText,
                StatusCode = x.StatusCode,
                CategoryCode = x.CategoryCode,
                CategoryCodeText = x.CategoryCodeText,
                CategoryReason = x.CategoryReason,
                ActionDate = x.ActionDate,
                RegistrationNumber = x.RegistrationNumber
            }).ToList();
            var prosecutionHistorys = response.ProsecutionHistorys.Select(x => new ProsecutionHistoryModel
            {
                EntryNumber = x.EntryNumber,
                EntryCode = x.EntryCode,
                EntryType = x.EntryType,
                ProceedingNumber = x.ProceedingNumber,
                EntryDateText = x.EntryDate.HasValue ? x.EntryDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                Description = x.Description
            }).ToList();

            var firm = new FirmModel
            {
                Name = response.Attorney.Firm.Name,
                Phone = response.Attorney.Firm.Phone,
                Fax = response.Attorney.Firm.Fax,
                Email = response.Attorney.Firm.Email,
            };

            var attorney = new AttorneyModel
            {
                Id = response.Attorney.Id,
                //UserId = response.Attorney.UserId,
                Name = response.Attorney.Name,
                FullName = response.Attorney.FullName,
                Email = response.Attorney.Email,
                Email2 = response.Attorney.Email2,
                Line1 = response.Attorney.Line1,
                City = response.Attorney.City,
                PostalCode = response.Attorney.PostalCode,
                CountryCode = response.Attorney.CountryCode,
                CountryName = response.Attorney.CountryName,
                Firm = firm
            };
            var ownerGroup = (OwnerGroupModel)default;
            if (owner != null)
            {
                ownerGroup = new OwnerGroupModel
                {
                    TrademarkId = owner.TrademarkId,
                    Type = owner.Type,
                    EntityNumber = owner.EntityNumber,
                    TypeCode = owner.TypeCode,
                    TypeDescription = owner.TypeDescription,
                    Name = owner.Name,
                    Email = owner.Email,
                    Email2 = owner.Email2,
                    Address = owner.Address,
                    City = owner.City,
                    State = owner.State,
                    IsoCode = owner.IsoCode,
                    IsoName = owner.IsoName,
                    WipoCode = owner.WipoCode,
                    WipoName = owner.WipoName,
                };
            }
            return new TrademarkResponse
            {
                Id = response.Id,
                NextDeadline = response.NextDeadline,
                MaxNextDeadline = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.ToString("MMM. dd, yyyy") : string.Empty,
                MaxNextDeadlineWithFee = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.AddMonths(6).ToString("MMM. dd, yyyy") : string.Empty,
                CurrentStatus = response.CurrentStatus,
                CurrentStatusDescription = response.IsFiledInternationLaw ? Constants.GetTrademarkCurrentStatus(response.CurrentStatus, true) : Constants.GetTrademarkCurrentStatus(response.CurrentStatus, false),
                StatusColor = response.StatusColor,
                Image = response.Image,
                Name = response.TrademarkElement,
                ClientName = response.OwnerGroups.OrderByDescending(x => x.Type).First().Name,
                SerialNumber = (int)response.SerialNumber,
                TrademarkStatus = response.Status,
                LastUpdateDate = response.Modified.ToString("yyyy-MM-dd HH:mm:ss"),
                ExaminerNumber = response.ExaminerNumber,
                ExaminerName = response.ExaminerName,
                FiledAsTeasPlusApp = response.FiledAsTeasPlusApp,
                CurrentlyAsTeasPlusApp = response.CurrentlyAsTeasPlusApp,
                FiledAsTeasRfApp = response.FiledAsTeasRfApp,
                CurrentlyAsTeasRfApp = response.CurrentlyAsTeasRfApp,
                SupplementalRegister = response.SupplementalRegister,
                AmendPrincipal = response.AmendPrincipal,
                AmendSupplemental = response.AmendSupplemental,
                CertificationMark = response.CertificationMark,
                ServiceMark = response.ServiceMark,
                CollectiveMembershipMark = response.CollectiveMembershipMark,
                CollectiveServiceMark = response.CollectiveServiceMark,
                CollectiveTradeMark = response.CollectiveTradeMark,
                StatusDate = response.StatusDate,
                IsStandard = response.IsStandard,
                IsColorDrawing = response.IsColorDrawing,
                IsSection2f = response.IsSection2f,
                IsSection2fPartial = response.IsSection2fPartial,
                IsOthers = response.IsOthers,
                IsPreviousRegistered = response.IsPreviousRegistered,
                CommunityLegalServiceTotal = response.CommunityLegalServiceTotal,
                IsFiledUse = response.IsFiledUse,
                IsFiledInternationLaw = response.IsFiledInternationLaw,
                IsFiled44d = response.IsFiled44d,
                IsFiled44e = response.IsFiled44e,
                IsFiled66a = response.IsFiled66a,
                IsFiledNoBasis = response.IsFiledNoBasis,
                IsUseCurrent = response.IsUseCurrent,
                IsInternationalLawCurrent = response.IsInternationalLawCurrent,
                IsSect44eCurrent = response.IsSect44eCurrent,
                IsSect66aCurrent = response.IsSect66aCurrent,
                IsNoBasisCurrent = response.IsNoBasisCurrent,
                IsUseAmended = response.IsUseAmended,
                IsInternationalLawAmended = response.IsInternationalLawAmended,
                IsSect44dAmended = response.IsSect44dAmended,
                IsSect44eAmended = response.IsSect44eAmended,
                IsSect8Filed = response.IsSect8Filed,
                IsSect8Acpt = response.IsSect8Acpt,
                IsSect8PartialAcpt = response.IsSect8PartialAcpt,
                IsSect15Filed = response.IsSect15Filed,
                IsSect15Ack = response.IsSect15Ack,
                IsSect71Filed = response.IsSect71Filed,
                IsSect71Acpt = response.IsSect71Acpt,
                IsSect71PartialAcpt = response.IsSect71PartialAcpt,
                IsRenewalFiled = response.IsRenewalFiled,
                IsChangeInRegistration = response.IsChangeInRegistration,
                Sect44dCurrent = response.Sect44dCurrent,
                MarkDrawingCode = response.MarkDrawingCode,
                LawOffAssignedCode = response.LawOffAssignedCode,
                CurrentLocationCode = response.CurrentLocationCode,
                CurrentLocationDate = response.CurrentLocationDate,
                ExternalStatusDescription = response.ExternalStatusDescription,
                InternationalStatusDescription = response.InternationalStatusDescription,
                DescriptionOfMark = response.DescriptionOfMark,
                CurrentLocation = response.CurrentLocation,
                NewLawOffAssignedCode = response.NewLawOffAssignedCode,
                LawOffAssigned = response.LawOffAssigned,
                Tm5Status = response.Tm5Status,
                Tm5StatusDescription = response.Tm5StatusDescription,
                StatusDefination = response.StatusDefination,
                UsRegistrationNumber = response.UsRegistrationNumber,
                StatusDateText = response.StatusDate.HasValue ? response.StatusDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                FiliingDateText = response.FiliingDate.HasValue ? response.FiliingDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                PublicationDateText = response.PublicationDate.HasValue ? response.PublicationDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                PublicationAllowanceDateText = response.PublicationAllowanceDate.HasValue ? response.PublicationAllowanceDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                TrademarkElement = response.TrademarkElement,
                TrademarkDrawingDescription = response.TrademarkDrawingDescription,
                PublicationOfficialGazettes = publicationOfficialGazettes,
                ProsecutionHistorys = prosecutionHistorys,
                GoodAndServices = goodAndServices,
                Attorney = attorney,
                OwnerGroup = ownerGroup,
                DesignSearch = designSearch
            };
        }
        public async Task<TrademarkResponse> GetTrademarkDetailForApi(Guid id)
        {
            var response = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetTrademarkById(id));
            var goodAndServices = new List<GoodAndServiceModel>();
            var designSearch = new List<DesignSearchModel>();
            var documentsList = new List<DocumentModel>();

            foreach (var item in response.Documents)
            {
                var docArray = item.DocumentUrls.Select(x => x.Url).FirstOrDefault().Split('/');
                var documentType = item.TotalPageQuantity > 1 && item.DocumentUrls.Where(x => x.DocumentId == item.Id).Count() > 1 ? "MULTI" : item.DocumentUrls.Where(x => x.DocumentId == item.Id).OrderByDescending(x => x.MediaType).FirstOrDefault().MediaType;
                string docId = docArray[9];
                if (docId == "registration-certificate")
                    docId = string.Empty;

                switch (documentType)
                {
                    case "application/xml":
                        documentType = "XML";
                        break;
                    case "image/jpeg":
                        documentType = "JPEG";
                        break;
                    case "MULTI":
                        documentType = "MULTI";
                        break;
                    default:
                        documentType = "MULTI";
                        break;
                }
                if (item.DocumentType.Description == "Specimen")
                    documentType = "JPEG";


                documentsList.Add(new DocumentModel()
                {
                    DocumentId = item.Id,
                    MailRoomDate = item.MailRoomDate,
                    MailRoomDateString = item.MailRoomDate.HasValue ? item.MailRoomDate.Value.ToString("MMM. dd, yyyy") : string.Empty,
                    Url = "https://tsdr.uspto.gov/documentviewer?caseId=sn" + item.SerialNumber + "&docId=" + docId,
                    DocumentType = documentType,
                    Description = item.DocumentType.Description
                });
            }

            foreach (var item in response.GoodAndServices)
            {
                var gosList = item.GoodAndServiceClass.Select(x => new GoodAndServiceClassModel
                {
                    Code = x.Code,
                    Description = x.Description,
                    Type = x.Type,
                }).ToList();

                var data = new GoodAndServiceModel
                {
                    StatusCode = item.StatusCode,
                    StatusDescription = item.StatusDescription,
                    StatusDate = item.StatusDate,
                    FirstUseDate = DateTimeHelper.FromLongToDate(item.FirstUseDate),
                    FirstUseInCommerceDate = DateTimeHelper.FromLongToDate(item.FirstUseInCommerceDate),
                    //FirstUseDate = DateTime.Parse(item.FirstUseDate.ToString().Substring(0, 4) + "-" + item.FirstUseDate.ToString().Substring(4, 2) + "-" + item.FirstUseDate.ToString().Substring(6, 2)).ToString("MMM. dd, yyyy"),
                    //FirstUseInCommerceDate = DateTime.Parse(item.FirstUseInCommerceDate.ToString().Substring(0, 4) + "-" + item.FirstUseInCommerceDate.ToString().Substring(4, 2) + "-" + item.FirstUseInCommerceDate.ToString().Substring(6, 2)).ToString("MMM. dd, yyyy"),
                    PrimeClassCode = item.PrimeClassCode,
                    Description = item.Description,
                    GoodAndServiceClasses = gosList
                };
                goodAndServices.Add(data);
            }

            if (response.DesignSearches.Count() > 0)
            {
                foreach (var item in response.DesignSearches)
                {
                    var designSearchModel = new DesignSearchModel
                    {
                        Code = item.Code,
                        DesignSearchDescriptions = item.DesignSearchDescriptionClasses.Select(d => new DesignSearchDescriptionModel { Description = d.Description }).ToList()
                    };
                    designSearch.Add(designSearchModel);
                }
            }

            var owner = response.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
            var publicationOfficialGazettes = response.PublicationOfficialGazettes.Select(x => new PublicationOfficialGazetteModel
            {
                IssueDate = x.IssueDate,
                StatusCodeText = x.StatusCodeText,
                StatusCode = x.StatusCode,
                CategoryCode = x.CategoryCode,
                CategoryCodeText = x.CategoryCodeText,
                CategoryReason = x.CategoryReason,
                ActionDate = x.ActionDate,
                RegistrationNumber = x.RegistrationNumber
            }).ToList();

            var prosecutionHistorys = response.ProsecutionHistorys.Select(x => new ProsecutionHistoryModel
            {
                EntryNumber = x.EntryNumber,
                EntryCode = x.EntryCode,
                EntryType = x.EntryType,
                ProceedingNumber = x.ProceedingNumber,
                EntryDate = x.EntryDate,
                Description = x.Description
            }).OrderByDescending(c => c.EntryDate).ToList();

            var attorney = new AttorneyModel
            {
                Id = response.Attorney.Id,
                //UserId = response.Attorney.UserId,
                Name = response.Attorney.Name,
                FullName = response.Attorney.FullName,
                Email = response.Attorney.Email,
                Email2 = response.Attorney.Email2,
                Line1 = response.Attorney.Line1,
                City = response.Attorney.City,
                PostalCode = response.Attorney.PostalCode,
                CountryCode = response.Attorney.CountryCode,
                CountryName = response.Attorney.CountryName,
                FirmId = response.Attorney.FirmId,
                FirmName = response.Attorney.Firm.Name
            };

            var ownerGroup = new OwnerGroupModel
            {
                TrademarkId = owner.TrademarkId,
                Type = owner.Type,
                EntityNumber = owner.EntityNumber,
                TypeCode = owner.TypeCode,
                TypeDescription = owner.TypeDescription,
                Name = owner.Name,
                Email = owner.Email,
                Email2 = owner.Email2,
                Address = owner.Address,
                City = owner.City,
                State = owner.State,
                IsoCode = owner.IsoCode,
                IsoName = owner.IsoName,
                WipoCode = owner.WipoCode,
                WipoName = owner.WipoName,
            };
            return new TrademarkResponse
            {
                Id = response.Id,
                CurrentStatusDescription = response.IsFiledInternationLaw ? Constants.GetTrademarkCurrentStatus(response.CurrentStatus, true) : Constants.GetTrademarkCurrentStatus(response.CurrentStatus, false),
                FiliingDate = response.FiliingDate,
                NextDeadline = response.NextDeadline,
                MaxNextDeadline = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.ToString("MMM. dd,yyyy") : string.Empty,
                MaxNextDeadlineWithFee = response.MaxNextDeadline.HasValue ? response.MaxNextDeadline.Value.AddMonths(6).ToString("MMM. dd,yyyy") : string.Empty,
                CurrentStatus = response.CurrentStatus,
                StatusColor = response.StatusColor,
                Image = response.Image,
                Name = response.TrademarkElement,
                ClientName = response.OwnerGroups.OrderByDescending(x => x.Type).First().Name,
                SerialNumber = (int)response.SerialNumber,
                TrademarkStatus = response.Status,
                IsTrademark = response.TrademarkStatus,
                ExaminerNumber = response.ExaminerNumber,
                ExaminerName = response.ExaminerName,
                LastUpdateDate = response.Modified.ToString("yyyy-MM-dd HH:mm:ss"),
                FiledAsTeasPlusApp = response.FiledAsTeasPlusApp,
                CurrentlyAsTeasPlusApp = response.CurrentlyAsTeasPlusApp,
                FiledAsTeasRfApp = response.FiledAsTeasRfApp,
                CurrentlyAsTeasRfApp = response.CurrentlyAsTeasRfApp,
                SupplementalRegister = response.SupplementalRegister,
                AmendPrincipal = response.AmendPrincipal,
                AmendSupplemental = response.AmendSupplemental,
                CertificationMark = response.CertificationMark,
                ServiceMark = response.ServiceMark,
                CollectiveMembershipMark = response.CollectiveMembershipMark,
                CollectiveServiceMark = response.CollectiveServiceMark,
                CollectiveTradeMark = response.CollectiveTradeMark,
                StatusDate = response.StatusDate,
                IsStandard = response.IsStandard,
                IsColorDrawing = response.IsColorDrawing,
                IsSection2f = response.IsSection2f,
                IsSection2fPartial = response.IsSection2fPartial,
                IsOthers = response.IsOthers,
                IsPreviousRegistered = response.IsPreviousRegistered,
                CommunityLegalServiceTotal = response.CommunityLegalServiceTotal,
                IsFiledUse = response.IsFiledUse,
                IsFiledInternationLaw = response.IsFiledInternationLaw,
                IsFiled44d = response.IsFiled44d,
                IsFiled44e = response.IsFiled44e,
                IsFiled66a = response.IsFiled66a,
                IsFiledNoBasis = response.IsFiledNoBasis,
                IsUseCurrent = response.IsUseCurrent,
                IsInternationalLawCurrent = response.IsInternationalLawCurrent,
                IsSect44eCurrent = response.IsSect44eCurrent,
                IsSect66aCurrent = response.IsSect66aCurrent,
                IsNoBasisCurrent = response.IsNoBasisCurrent,
                IsUseAmended = response.IsUseAmended,
                IsInternationalLawAmended = response.IsInternationalLawAmended,
                IsSect44dAmended = response.IsSect44dAmended,
                IsSect44eAmended = response.IsSect44eAmended,
                IsSect8Filed = response.IsSect8Filed,
                IsSect8Acpt = response.IsSect8Acpt,
                IsSect8PartialAcpt = response.IsSect8PartialAcpt,
                IsSect15Filed = response.IsSect15Filed,
                IsSect15Ack = response.IsSect15Ack,
                IsSect71Filed = response.IsSect71Filed,
                IsSect71Acpt = response.IsSect71Acpt,
                IsSect71PartialAcpt = response.IsSect71PartialAcpt,
                IsRenewalFiled = response.IsRenewalFiled,
                IsChangeInRegistration = response.IsChangeInRegistration,
                Sect44dCurrent = response.Sect44dCurrent,
                MarkDrawingCode = response.MarkDrawingCode,
                LawOffAssignedCode = response.LawOffAssignedCode,
                CurrentLocationCode = response.CurrentLocationCode,
                CurrentLocationDateString = response.CurrentLocationDate.HasValue ? response.CurrentLocationDate.Value.ToString("MMM. dd,yyyy") : string.Empty,
                ExternalStatusDescription = response.ExternalStatusDescription,
                InternationalStatusDescription = response.InternationalStatusDescription,
                DescriptionOfMark = response.DescriptionOfMark,
                CurrentLocation = response.CurrentLocation,
                NewLawOffAssignedCode = response.NewLawOffAssignedCode,
                LawOffAssigned = response.LawOffAssigned,
                Tm5Status = response.Tm5Status,
                Tm5StatusDescription = response.Tm5StatusDescription,
                StatusDefination = response.StatusDefination,
                UsRegistrationNumber = response.UsRegistrationNumber,
                StatusDateText = response.StatusDate.HasValue ? response.StatusDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                FiliingDateText = response.FiliingDate.HasValue ? response.FiliingDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                PublicationDateText = response.PublicationDate.HasValue ? response.PublicationDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                PublicationAllowanceDateText = response.PublicationAllowanceDate.HasValue ? response.PublicationAllowanceDate.Value.ToString("MM-dd-yyyy") : string.Empty,
                TrademarkElement = response.TrademarkElement,
                TrademarkDrawingDescription = response.TrademarkDrawingDescription,
                PublicationOfficialGazettes = publicationOfficialGazettes,
                ProsecutionHistorys = prosecutionHistorys,
                GoodAndServices = goodAndServices,
                Attorney = attorney,
                OwnerGroup = ownerGroup,
                DesignSearch = designSearch,
                DocumentModels = documentsList.OrderByDescending(x => x.MailRoomDate).ThenBy(x => x.Description).ToList()
            };
        }
        public async Task<List<TrademarkResponse>> GetDuplicateTrademarks(List<long> serialNumbers)
        {
            var response = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetBySerialNumbers(serialNumbers));
            return response.Select(x => new TrademarkResponse()
            {
                Id = x.Id,
                Name = x.TrademarkElement,
                Image = x.Image,
                ClientName = x.OwnerGroups.Count() > 0 ? x.OwnerGroups.OrderByDescending(x => x.Type).First().Name : string.Empty,
                ImportedDate = x.Created,
                PrimaryEmail = x.User != null ? x.User.Email : string.Empty,
                StatusDescription = x.Tm5StatusDescription,
                SerialNumber = (int)x.SerialNumber,
                Serial = x.SerialNumber.ToString(),
                LastUpdateDate = x.Modified.ToString("yyyy-MM-dd HH:mm:ss"),
                AttorneyName = x.Attorney.Name ?? "N/A"
            }).ToList();
        }
        public async Task<TrademarkResponseModel> GetTrademark(string fallbackEmail, string id)
        {
            var result = new TrademarkResponseModel();
            var response = new TrademarkResponse();
            var jsonString = string.Empty;
            var xmlString = string.Empty;
            try
            {
                var trademarkExists = await _trademarkRepository.FindTrademarkWithOutIncludeAsync(TrademarkSpecification.GetBySerialNumber(Convert.ToInt64(id)));
                //using (var context = new TMCloudStorageContext())
                //{
                //var findLog = await context.FindAsync<TradeMarkDataLog>(id, id);
                if (trademarkExists == null)
                {
                    var ids = new string[] { id };
                    string type = "sn";
                    if (int.Parse(ids[0]) < 10000000)
                        type = "rn";
                    jsonString = await TsdrApiService.GetTrademarkDetail(type, ids, false);
                    //jsonString = findLog != null ? findLog.TrademarkDetailJson : await TsdrApiService.GetTrademarkDetail(type, ids);
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        //xmlString = findLog != null ? findLog.TrademarkDocumentXml : await TsdrApiService.GetTrademarkDocumentDetails(type, ids);
                        xmlString = await TsdrApiService.GetTrademarkDocumentDetails(type, false, ids);
                        //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDocument", xmlString);
                        //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDetail", jsonString);

                        TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);

                        response.FallbackEmail = fallbackEmail;
                        string email = string.Empty;
                        string ClientName = string.Empty;
                        string LegalEntityType = string.Empty;
                        string ClientAddress = string.Empty;
                        string ClientCity = string.Empty;
                        string ClientCountry = string.Empty;
                        string ClientZipCode = string.Empty;
                        string ClientState = string.Empty;

                        foreach (var transactionsEach in trademarkDetail.transactionList)
                        {
                            foreach (var tmark in transactionsEach.trademarks)
                            {
                                if (tmark.status.tm5StatusDesc.Substring(0, 4) == "DEAD")
                                    response.Status = TrademarkStatus.TrademarkDead;
                                else
                                    response.Status = TrademarkStatus.TrademarkSuccess;

                                if (string.IsNullOrEmpty(tmark.status.correspondence.attorneyName))
                                    response.Status = TrademarkStatus.TrademarkAttorneyIssue;

                                response.Name = tmark.status.markElement;
                                response.SerialNumber = int.Parse(transactionsEach.searchId);
                                response.StatusNumber = tmark.status.tm5Status;
                                response.StatusDescription = tmark.status.tm5StatusDesc;
                                #region OwnerGroups
                                var ownerGroups = FillOwnerGroupData(tmark.parties.ownerGroups);
                                //OwnerGroupCollection.AddRange(ownerGroups);

                                var findOwner = ownerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
                                if (findOwner != null)
                                {
                                    response.ClientName = findOwner.Name;
                                    response.PrimaryEmail = findOwner.Email;
                                    response.StreetAddress = findOwner.Address;
                                    response.City = findOwner.City;
                                    response.State = findOwner.State;
                                    response.ZipCode = findOwner.ZipCode;
                                    response.Country = findOwner.Country;
                                }
                                //if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null)
                                //{
                                //    ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].name != null ? tmark.parties.ownerGroups._30[0].name : string.Empty;
                                //    LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].entityType != null && tmark.parties.ownerGroups._30[0].entityType.description != null ? tmark.parties.ownerGroups._30[0].entityType.description : string.Empty;
                                //    ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].address1 != null ? tmark.parties.ownerGroups._30[0].address1 : string.Empty;
                                //    ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].citizenship != null && tmark.parties.ownerGroups._30[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._30[0].citizenship.stateCountry.name : string.Empty;
                                //    email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].emailList != null && tmark.parties.ownerGroups._30[0].emailList.addresses != null && tmark.parties.ownerGroups._30[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._30[0].emailList.addresses[0] : string.Empty;
                                //    ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].zip != null ? tmark.parties.ownerGroups._30[0].zip : string.Empty;
                                //    ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].addressStateCountry != null && tmark.parties.ownerGroups._30[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._30[0].addressStateCountry.stateCountry.code : string.Empty;
                                //    ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._30 != null && tmark.parties.ownerGroups._30[0].city != null ? tmark.parties.ownerGroups._30[0].city : string.Empty;

                                //    response.ClientName = ClientName;
                                //    response.PrimaryEmail = email;
                                //    response.StreetAddress = ClientAddress;
                                //    response.City = ClientCity;
                                //    response.State = ClientState;
                                //    response.ZipCode = ClientZipCode;
                                //    response.Country = ClientCountry;
                                //}
                                //else if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null)
                                //{
                                //    ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].name != null ? tmark.parties.ownerGroups._20[0].name : string.Empty;
                                //    LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].entityType != null && tmark.parties.ownerGroups._20[0].entityType.description != null ? tmark.parties.ownerGroups._20[0].entityType.description : string.Empty;
                                //    ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].address1 != null ? tmark.parties.ownerGroups._20[0].address1 : string.Empty;
                                //    ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].citizenship != null && tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name : string.Empty;
                                //    email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].emailList != null && tmark.parties.ownerGroups._20[0].emailList.addresses != null && tmark.parties.ownerGroups._20[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._20[0].emailList.addresses[0] : string.Empty;
                                //    ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].zip != null ? tmark.parties.ownerGroups._20[0].zip : string.Empty;
                                //    ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].addressStateCountry != null && tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code : string.Empty;
                                //    ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].city != null ? tmark.parties.ownerGroups._20[0].city : string.Empty;

                                //    response.ClientName = ClientName;
                                //    response.PrimaryEmail = email;
                                //    response.StreetAddress = ClientAddress;
                                //    response.City = ClientCity;
                                //    response.State = ClientState;
                                //    response.ZipCode = ClientZipCode;
                                //    response.Country = ClientCountry;
                                //}
                                //else if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null)
                                //{
                                //    ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].name != null ? tmark.parties.ownerGroups._10[0].name : string.Empty;
                                //    LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].entityType != null && tmark.parties.ownerGroups._10[0].entityType.description != null ? tmark.parties.ownerGroups._10[0].entityType.description : string.Empty;
                                //    ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].address1 != null ? tmark.parties.ownerGroups._10[0].address1 : string.Empty;
                                //    ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].citizenship != null && tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name : string.Empty;
                                //    email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].emailList != null && tmark.parties.ownerGroups._10[0].emailList.addresses != null && tmark.parties.ownerGroups._10[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._10[0].emailList.addresses[0] : string.Empty;
                                //    ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].zip != null ? tmark.parties.ownerGroups._10[0].zip : string.Empty;
                                //    ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].addressStateCountry != null && tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code : string.Empty;
                                //    ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].city != null ? tmark.parties.ownerGroups._10[0].city : string.Empty;

                                //    response.ClientName = ClientName;
                                //    response.PrimaryEmail = email;
                                //    response.StreetAddress = ClientAddress;
                                //    response.City = ClientCity;
                                //    response.State = ClientState;
                                //    response.ZipCode = ClientZipCode;
                                //    response.Country = ClientCountry;
                                //}
                                //else
                                //{
                                //    response.Status = TrademarkStatus.TrademarkAttorneyIssue;
                                //}
                                #endregion


                                if (string.IsNullOrEmpty(email))
                                {
                                    response.Email = fallbackEmail;
                                    email = fallbackEmail;
                                }
                                response.Country = tmark.status.correspondence != null && tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryName != null ? tmark.status.correspondence.address.countryName : string.Empty;
                                if (string.IsNullOrWhiteSpace(response.PrimaryEmail))
                                    response.Status = TrademarkStatus.TrademarkEmailNotFound;
                                //if (tmark.parties.ownerGroups != null && (tmark.parties.ownerGroups._10 != null || tmark.parties.ownerGroups._20 != null) && string.IsNullOrEmpty(response.PrimaryEmail))
                                //    response.Status = TrademarkStatus.TrademarkEmailNotFound;
                            }
                        }

                        //var userInfo = new UserInfo();
                        //userInfo.Email = email;
                        //userInfo.ClientName = ClientName;
                        //userInfo.LegalEntityType = LegalEntityType;
                        //userInfo.ClientAddress = ClientAddress;
                        //userInfo.ClientCity = ClientCity;
                        //userInfo.Country = ClientCountry;
                        //userInfo.ClientState = ClientState;
                        //userInfo.ClientZipCode = ClientZipCode;

                        //userInfo.SerialNumber = Convert.ToInt64(id);
                        //_httpContextAccessor.HttpContext.Session.SetString("GetUserInfo", JsonConvert.SerializeObject(userInfo));
                    }
                    else
                    {
                        response.SerialNumber = int.Parse(id);
                        response.Status = TrademarkStatus.TrademarkNotFound;
                    }
                }
                else
                {
                    response.SerialNumber = int.Parse(trademarkExists.SerialNumber.ToString());
                    response.Name = trademarkExists.Attorney.Name;
                    response.Status = TrademarkStatus.TrademarkInSystem;
                    result.Errors.Add("Record already exists");
                }

                if (result.Errors.Any())
                    result.IsSuccess = false;
                else
                    result.IsSuccess = true;

                result.TrademarkResponse.Add(response);
                //}
            }
            catch (Exception ex)
            {
                result.Errors.Add(ex.Message);
                result.IsSuccess = false;
                response.Status = TrademarkStatus.SystemError;
                result.TrademarkResponse.Add(response);
            }
            return result;
        }
        public async Task<GridDataSourceResultModel> GetTrademarkImportFiles()
        {
            //var response = new List<TrademarkImportFileModel>();
            var itemData = new List<TrademarkImportItemsModel>();
            var result = await _trademarkRepository.GetTrademarkImportFilesAsync(TrademarkImportFileSpecification.GetThirtyDays());
            foreach (var importFile in result)
            {
                foreach (var item in importFile.TrademarkImportItems)
                {
                    string statusString = string.Empty;
                    switch (item.Status)
                    {
                        case TrademarkStatus.Pending:
                            statusString = "Pending";
                            break;
                        case TrademarkStatus.TrademarkSuccess:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkInSystem:
                            statusString = "In System";
                            break;
                        case TrademarkStatus.DifferentAssociate:
                            statusString = "Different Associate";
                            break;
                        case TrademarkStatus.TrademarkDead:
                            statusString = "Dead";
                            break;
                        case TrademarkStatus.TrademarkNotFound:
                            statusString = "Not Found";
                            break;
                        case TrademarkStatus.TrademarkLinked:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkAccountCreated:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkAttorneyIssue:
                            statusString = "Attorney Issue";
                            break;
                        case TrademarkStatus.TrademarkEmailNotFound:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkFailed:
                            statusString = "Failed";
                            break;
                        case TrademarkStatus.Accepted:
                            statusString = "Accepted";
                            break;
                        case TrademarkStatus.Rejected:
                            statusString = "Rejected";
                            break;
                        case TrademarkStatus.Error:
                            statusString = "Error";
                            break;
                        default:
                            break;
                    }

                    itemData.Add(new TrademarkImportItemsModel()
                    {
                        Id = item.Id,
                        SerialNumber = item.SerialNumber,
                        StatusString = statusString,
                        Email = item.Trademark != null ? item.Trademark.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault().Email : string.Empty,
                        ClientName = item.Trademark != null ? item.Trademark.Name : string.Empty,
                        DateImported = item.Trademark != null ? item.Trademark.FiliingDate : null,
                        TrademarkName = item.Trademark != null ? item.Trademark.TrademarkElement : string.Empty,
                        Created = importFile.Created
                    });
                }
            }

            return new GridDataSourceResultModel()
            {
                Data = (itemData)
                .Select(x => new TrademarkImportItemsModel()
                {
                    Id = x.Id,
                    SerialNumber = x.SerialNumber,
                    StatusString = x.StatusString,
                    Email = x.Email,
                    ClientName = x.ClientName,
                    DateImported = x.DateImported,
                    TrademarkName = x.TrademarkName,
                    Created = x.Created

                }).OrderByDescending(x => x.Created).ToList(),
                Total = itemData.Count()
            };
        }
        public async Task<List<TrademarkImportFileModel>> GetTrademarkImportFilesAsync()
        {
            var response = new List<TrademarkImportFileModel>();
            var result = await _trademarkRepository.GetTrademarkImportFilesAsync(TrademarkImportFileSpecification.GetThirtyDays());
            foreach (var importFile in result)
            {
                var fileItem = new TrademarkImportFileModel();
                var itemData = new List<TrademarkImportItemsModel>();
                foreach (var item in importFile.TrademarkImportItems)
                {
                    string statusString = string.Empty;
                    switch (item.Status)
                    {
                        case TrademarkStatus.Pending:
                            statusString = "Pending";
                            break;
                        case TrademarkStatus.TrademarkSuccess:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkInSystem:
                            statusString = "In System";
                            break;
                        case TrademarkStatus.DifferentAssociate:
                            statusString = "Different Associate";
                            break;
                        case TrademarkStatus.TrademarkDead:
                            statusString = "Dead";
                            break;
                        case TrademarkStatus.TrademarkNotFound:
                            statusString = "Not Found";
                            break;
                        case TrademarkStatus.TrademarkLinked:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkAccountCreated:
                            statusString = "Success";
                            break;
                        case TrademarkStatus.TrademarkAttorneyIssue:
                            statusString = "Attorney Issue";
                            break;
                        case TrademarkStatus.TrademarkEmailNotFound:
                            statusString = "Email Not Found";
                            break;
                        case TrademarkStatus.TrademarkFailed:
                            statusString = "Failed";
                            break;
                        case TrademarkStatus.Accepted:
                            statusString = "Accepted";
                            break;
                        case TrademarkStatus.Rejected:
                            statusString = "Rejected";
                            break;
                        case TrademarkStatus.Error:
                            statusString = "Error";
                            break;
                        default:
                            break;
                    }

                    itemData.Add(new TrademarkImportItemsModel()
                    {
                        Id = item.Id,
                        SerialNumber = item.SerialNumber,
                        StatusString = statusString,
                        Email = item.Trademark != null ? item.Trademark.OwnerGroups.OrderByDescending(x => x.Type).FirstOrDefault().Email : string.Empty,
                        ClientName = item.Trademark != null ? item.Trademark.Name : string.Empty,
                        DateImported = item.Trademark != null ? item.Trademark.FiliingDate : null,
                        TrademarkName = item.Trademark != null ? item.Trademark.TrademarkElement : string.Empty,
                        Created = importFile.Created
                    });
                }
                fileItem.BatchName = importFile.Name;
                fileItem.TrademarkFileItems = itemData;
                response.Add(fileItem);
            }
            return response;
        }
        public async Task<List<TrademarkImportItemsModel>> GetTrademarkImportFilesGroupAsync()
        {
            var response = new List<TrademarkImportItemsModel>();
            var result = await _trademarkRepository.GetTrademarkImportFilesAsync(TrademarkImportFileSpecification.GetThirtyDays());
            foreach (var importFile in result)
            {
                foreach (var item in importFile.TrademarkImportItems)
                {
                    string statusString = string.Empty;
                    string imagePath = string.Empty;
                    if (item.Status == TrademarkStatus.Pending)
                    {
                        var findTrademark = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetBySerialNumber(item.SerialNumber));
                        if (findTrademark != null)
                        {
                            item.Status = TrademarkStatus.TrademarkInSystem;
                            item.IsActive = false;
                        }
                    }
                    //else if (item.Status == TrademarkStatus.Processing || item.Status == TrademarkStatus.TrademarkInSystem || item.Status == TrademarkStatus.TrademarkSuccess || item.Status == TrademarkStatus.TrademarkAttorneyIssue)
                    //{
                    //    item.IsActive = false;
                    //}


                    switch (item.Status)
                    {
                        case TrademarkStatus.Pending:
                            statusString = "Pending";
                            imagePath = "";
                            break;
                        case TrademarkStatus.TrademarkSuccess:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkInSystem:
                            statusString = "In System";
                            imagePath = "/images/status-accepted.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.DifferentAssociate:
                            statusString = "Different Associate";
                            imagePath = "/images/redalert.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkDead:
                            statusString = "Dead TM";
                            imagePath = "/images/cross.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkNotFound:
                            statusString = "Not Found";
                            imagePath = "/images/status-unavailable.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkLinked:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkAccountCreated:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkAttorneyIssue:
                            statusString = "Attorney Issue";
                            imagePath = "/images/redalert.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkEmailNotFound:
                            statusString = "Email Not Found";
                            imagePath = "/images/status-unavailable.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.TrademarkFailed:
                            statusString = "Failed";
                            imagePath = "/images/status-rejected.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.Accepted:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.Rejected:
                            statusString = "Rejected";
                            imagePath = "/images/status-rejected.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.Error:
                            statusString = "Error";
                            imagePath = "/images/status-rejected.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.SystemError:
                            statusString = "System Error";
                            imagePath = "/images/status-rejected.png";
                            item.IsActive = false;
                            break;
                        case TrademarkStatus.Processing:
                            statusString = "Processing";
                            imagePath = "/images/status-unavailable.png";
                            item.IsActive = false;
                            break;
                        default:
                            break;
                    }

                    response.Add(new TrademarkImportItemsModel()
                    {
                        Id = item.Id,
                        SerialNumber = item.SerialNumber,
                        StatusString = statusString,
                        ImagePath = imagePath,
                        Email = item.Email,
                        ClientName = item.ClientName,
                        DateImported = item.Created,
                        TrademarkName = item.TrademarkName,
                        BatchName = importFile.Name,
                        Created = importFile.Created,
                        IsActive = item.IsActive
                    });
                }
            }
            return response.OrderBy(x => x.Created).ToList();
        }
        public async Task<List<TrademarkImportItemsModel>> GetTrademarkImportFilesGroupAsync(DateTime StartDate, DateTime EndDate)
        {
            var response = new List<TrademarkImportItemsModel>();
            var result = await _trademarkRepository.GetTrademarkImportFilesAsync(TrademarkImportFileSpecification.GetByFilters(StartDate, EndDate));
            foreach (var importFile in result)
            {
                foreach (var item in importFile.TrademarkImportItems)
                {
                    string statusString = string.Empty;
                    string imagePath = string.Empty;
                    switch (item.Status)
                    {
                        case TrademarkStatus.Pending:
                            statusString = "Pending";
                            imagePath = "";
                            break;
                        case TrademarkStatus.TrademarkSuccess:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            break;
                        case TrademarkStatus.TrademarkInSystem:
                            statusString = "In System";
                            imagePath = "/images/status-accepted.png";
                            break;
                        case TrademarkStatus.DifferentAssociate:
                            statusString = "Different Associate";
                            imagePath = "/images/redalert.png";
                            break;
                        case TrademarkStatus.TrademarkDead:
                            statusString = "Dead TM";
                            imagePath = "/images/cross.png";
                            break;
                        case TrademarkStatus.TrademarkNotFound:
                            statusString = "Not Found";
                            imagePath = "/images/status-unavailable.png";
                            break;
                        case TrademarkStatus.TrademarkLinked:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            break;
                        case TrademarkStatus.TrademarkAccountCreated:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            break;
                        case TrademarkStatus.TrademarkAttorneyIssue:
                            statusString = "Attorney Issue";
                            imagePath = "/images/redalert.png";
                            break;
                        case TrademarkStatus.TrademarkEmailNotFound:
                            statusString = "Unavailable";
                            imagePath = "/images/status-unavailable.png";
                            break;
                        case TrademarkStatus.TrademarkFailed:
                            statusString = "Failed";
                            imagePath = "/images/status-rejected.png";
                            break;
                        case TrademarkStatus.Accepted:
                            statusString = "Accepted";
                            imagePath = "/images/status-accepted.png";
                            break;
                        case TrademarkStatus.Rejected:
                            statusString = "Rejected";
                            imagePath = "/images/status-rejected.png";
                            break;
                        case TrademarkStatus.Error:
                            statusString = "System Error";
                            imagePath = "/images/status-rejected.png";
                            break;
                        case TrademarkStatus.SystemError:
                            statusString = "System Error";
                            imagePath = "/images/status-rejected.png";
                            break;
                        default:
                            break;
                    }

                    response.Add(new TrademarkImportItemsModel()
                    {
                        Id = item.Id,
                        SerialNumber = item.SerialNumber,
                        StatusString = statusString,
                        ImagePath = imagePath,
                        Email = item.Email,
                        ClientName = item.ClientName,
                        //DateImported = item.Trademark != null ? item.Trademark.FiliingDate : null,
                        TrademarkName = item.TrademarkName,
                        BatchName = importFile.Name,
                        Created = importFile.Created
                    });
                }
            }
            return response.OrderBy(x => x.Created).ToList();
        }
        public async Task<TrademarkResponseModel> SaveTrademark(string serialNo, string fallbackEmail)
        {
            var results = new TrademarkResponseModel();
            var response = new TrademarkResponse();
            var currentUser = _httpContextAccessor.HttpContext != null ? _httpContextAccessor.HttpContext.User.Identity.Name : "webmaster@kwworks.com";
            var now = DateTimeHelper.Now();
            try
            {
                List<Trademarks.GoodAndService> GoodAndServiceCollection = new List<Trademarks.GoodAndService>();
                List<Trademarks.OwnerGroup> OwnerGroupCollection = new List<Trademarks.OwnerGroup>();
                List<Trademarks.DesignSearchClass> DesignSearchClassCollection = new List<Trademarks.DesignSearchClass>();
                List<Trademarks.ProsecutionHistory> ProsecutionHistoryCollection = new List<Trademarks.ProsecutionHistory>();
                List<Trademarks.PhysicalLocationHistory> PhysicalLocationHistoryCollection = new List<Trademarks.PhysicalLocationHistory>();
                List<Trademarks.PublicationOfficialGazette> PublicationOfficialGazetteCollection = new List<Trademarks.PublicationOfficialGazette>();

                var ids = new string[] { serialNo };
                string type = "sn";
                if (int.Parse(ids[0]) < 10000000)
                    type = "rn";

                //GetFromStorage
                using (var context = new TMCloudStorageContext())
                {
                    
                    //var findLog = await context.FindAsync<TradeMarkDataLog>(serialNo, serialNo);
                    //if (findLog != null)
                    {
                        string email = string.Empty;
                        string LegalEntityType = string.Empty;
                        string ClientAddress = string.Empty;
                        string ClientCity = string.Empty;
                        string ClientCountry = string.Empty;
                        string ClientZipCode = string.Empty;
                        string ClientState = string.Empty;
                        string trademarkName = string.Empty;
                        var jsonString = await TsdrApiService.GetTrademarkDetail(type, ids, true); ;
                        var xmlString = await TsdrApiService.GetTrademarkDocumentDetails(type, true, ids);

                        string code = Helper.RandomString();
                        var user = (User)default;
                        bool isknown = false;

                        if (!string.IsNullOrEmpty(jsonString))
                        {
                            TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);
                            var transactionList = trademarkDetail.transactionList;

                            var trademarkFilter = new List<long>();
                            foreach (var tmark in transactionList)
                            {
                                trademarkFilter.Add(tmark.trademarks.Select(x => (long)x.status.serialNumber).FirstOrDefault());
                            }

                            var findTrademarks = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetBySerialNumbers(trademarkFilter));

                            foreach (var transactionsEach in transactionList)
                            {
                                foreach (var tmark in transactionsEach.trademarks)
                                {
                                    string attorneyName = !string.IsNullOrWhiteSpace(tmark.status.correspondence.attorneyName) ? tmark.status.correspondence.attorneyName : string.Empty;
                                    string attorneyEmail = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Any() ? (!string.IsNullOrWhiteSpace(tmark.status.correspondence.attorneyEmail.addresses[0]) ? tmark.status.correspondence.attorneyEmail.addresses[0] : string.Empty) : string.Empty;
                                    string authIndicator = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.authIndicator != null ? tmark.status.correspondence.attorneyEmail.authIndicator : string.Empty;
                                    string attorneyLineOne = tmark.status.correspondence.address != null && tmark.status.correspondence.address.line1 != null ? tmark.status.correspondence.address.line1 : string.Empty;
                                    if (tmark.status.correspondence.address != null && tmark.status.correspondence.address.line2 != null)
                                        attorneyLineOne = $"{attorneyLineOne} {tmark.status.correspondence.address.line2}";
                                    string attorneyPostalCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.postalCode != null ? tmark.status.correspondence.address.postalCode : string.Empty;
                                    string attorneyCity = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;
                                    if (!string.IsNullOrWhiteSpace(attorneyEmail) || !string.IsNullOrWhiteSpace(attorneyName))
                                    {
                                        var findRecognisedattorney = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilter(attorneyEmail.Trim(), attorneyName.Trim()));
                                        isknown = findRecognisedattorney == null ? false : true;
                                    }


                                    if (findTrademarks.Any(x => x.SerialNumber == tmark.status.serialNumber))
                                    {
                                        response.SerialNumber = tmark.status.serialNumber;
                                        response.Status = TrademarkStatus.TrademarkInSystem;
                                        response.Id = findTrademarks.FirstOrDefault(x => x.SerialNumber == tmark.status.serialNumber).Id;
                                        results.Errors.Add("Trademark already in system");
                                    }
                                    else
                                    {
                                        var findFirm = (Firm)default;
                                        var findAttorney = (Attorneys.Attorney)default;

                                        findFirm = await _firmRepository.FindFirmAsync(FirmSpecification.GetByName(tmark.status.correspondence.firmName));
                                        if (findFirm == null)
                                        {
                                            findFirm = new Firm
                                            {
                                                Name = tmark.status.correspondence.firmName ?? string.Empty,
                                                IndividualName = tmark.status.correspondence.individualFullName ?? string.Empty,
                                                Email = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.addresses != null && tmark.status.correspondence.correspondantEmail.addresses.Count() > 0 ? tmark.status.correspondence.correspondantEmail.addresses[0] : string.Empty,
                                                IsEmailAuthticated = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.authIndicator != null ? tmark.status.correspondence.correspondantEmail.authIndicator : string.Empty,
                                                Phone = tmark.status.correspondence.correspondantPhone ?? string.Empty,
                                                Fax = tmark.status.correspondence.correspondantFax ?? string.Empty,
                                                CreatedBy = currentUser,
                                                ModifiedBy = currentUser,

                                            };
                                            findFirm.Name = !string.IsNullOrWhiteSpace(findFirm.Name) ? findFirm.Name : tmark.status.correspondence.individualFullName;
                                            _firmRepository.Create(findFirm);
                                        }

                                        if (!string.IsNullOrWhiteSpace(attorneyName) || !string.IsNullOrWhiteSpace(attorneyEmail))
                                        {
                                            findAttorney = await _attorneyRepository.FindAttorney(AttorneySpecification.GetByFilters(attorneyName, attorneyEmail));
                                        }

                                        findAttorney = new Attorneys.Attorney()
                                        {
                                            //UserId = user != null ? user.Id : string.Empty,
                                            Name = attorneyName,
                                            FullName = attorneyName,
                                            Email = attorneyEmail,
                                            Email2 = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() > 1 ? tmark.status.correspondence.attorneyEmail.addresses[1] : string.Empty,
                                            IsEmailAuthticated = authIndicator == "Y" ? true : false,
                                            CreatedBy = currentUser,
                                            ModifiedBy = currentUser,
                                            Line1 = attorneyLineOne,
                                            City = attorneyCity,
                                            PostalCode = attorneyPostalCode,
                                            CountryCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryCode != null ? tmark.status.correspondence.address.countryCode : string.Empty,
                                            CountryName = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryName != null ? tmark.status.correspondence.address.countryName : string.Empty,
                                            //IsKnown = isknown,
                                            Firm = findFirm
                                        };
                                        _attorneyRepository.Create(findAttorney);
                                        email = findAttorney.Email;

                                        if (tmark.status.designSearchList != null && tmark.status.designSearchList.Count() > 0)
                                        {
                                            var designsearch = new List<string>();
                                            foreach (var designsearchList in tmark.status.designSearchList)
                                            {
                                                designsearch.Add(designsearchList.code);
                                            }

                                            var dsList = await _designSearchClassRepository.ListDesignSearchClassAsync(DesignSearchClassSpecification.GetByCode(designsearch));
                                            foreach (var item in tmark.status.designSearchList)
                                            {
                                                var ds = (DesignSearchClass)default;
                                                ds = dsList.Where(x => x.Code == item.code).FirstOrDefault();
                                                if (ds == null)
                                                {
                                                    ds = new DesignSearchClass();
                                                    var dsdList = new List<DesignSearchDescriptionClass>();
                                                    foreach (var dsdItem in item.descriptions)
                                                    {
                                                        dsdList.Add(new DesignSearchDescriptionClass()
                                                        {
                                                            DesignSearchClassId = ds.Id,
                                                            Description = dsdItem,
                                                            CreatedBy = currentUser,
                                                            IsActive = true
                                                        });
                                                    }
                                                    ds.Code = item.code;
                                                    ds.CreatedBy = currentUser;
                                                    ds.ModifiedBy = currentUser;
                                                    ds.DesignSearchDescriptionClasses = dsdList;
                                                    //_designSearchClassRepository.Create(ds);
                                                }
                                                DesignSearchClassCollection.Add(ds);
                                            }

                                        }

                                        var goodandservice = new List<string>();
                                        foreach (var gslist in tmark.gsList)
                                        {
                                            foreach (var item in gslist.usClasses)
                                            {
                                                goodandservice.Add(item.code);
                                            }
                                            foreach (var item in gslist.internationalClasses)
                                            {
                                                goodandservice.Add(item.code);
                                            }
                                        }
                                        var gosList = await _goodAndServiceClassRepository.ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification.GetByNames(goodandservice));
                                        foreach (var gslist in tmark.gsList)
                                        {
                                            var GoodAndServiceClassCollection = new List<Trademarks.GoodAndServiceClass>();

                                            foreach (var item in gslist.usClasses)
                                            {
                                                var gs = (GoodAndServiceClass)default;
                                                gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                                var isExists = GoodAndServiceCollection.SelectMany(c => c.GoodAndServiceClass).FirstOrDefault(x => x.Code == item.code && x.Type == GoodAndServiceClassType.UsClass);
                                                if (isExists == null)
                                                {
                                                    if (gs == null)
                                                    {
                                                        gs = new GoodAndServiceClass
                                                        {
                                                            Code = item.code,
                                                            Description = item.description,
                                                            CreatedBy = currentUser,
                                                            ModifiedBy = currentUser,
                                                            Type = GoodAndServiceClassType.UsClass
                                                        };
                                                    }
                                                    GoodAndServiceClassCollection.Add(gs);
                                                }
                                                else
                                                    GoodAndServiceClassCollection.Add(isExists);
                                            }

                                            foreach (var item in gslist.internationalClasses)
                                            {
                                                var gs = (GoodAndServiceClass)default;
                                                gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                                var isExists = GoodAndServiceCollection.SelectMany(c => c.GoodAndServiceClass).FirstOrDefault(x => x.Code == item.code && x.Type == GoodAndServiceClassType.InternationalClass);
                                                if (isExists == null)
                                                {
                                                    if (gs == null)
                                                    {
                                                        gs = new GoodAndServiceClass
                                                        {
                                                            Code = item.code,
                                                            Description = item.description,
                                                            CreatedBy = currentUser,
                                                            ModifiedBy = currentUser,
                                                            Type = GoodAndServiceClassType.InternationalClass
                                                        };
                                                    }
                                                    GoodAndServiceClassCollection.Add(gs);
                                                }
                                                else
                                                    GoodAndServiceClassCollection.Add(isExists);
                                            }

                                            GoodAndServiceCollection.Add(new Trademarks.GoodAndService()
                                            {
                                                StatusCode = gslist.statusCode,
                                                StatusDescription = gslist.statusDescription,
                                                StatusDate = gslist.statusDate,
                                                FirstUseDate = gslist.firstUseDate,
                                                FirstUseInCommerceDate = gslist.firstUseInCommerceDate,
                                                PrimeClassCode = gslist.primeClassCode,
                                                Description = gslist.description,
                                                CreatedBy = currentUser,
                                                GoodAndServiceClass = GoodAndServiceClassCollection
                                            });
                                        }


                                        #region OwnerGroup
                                        var ownerGroups = FillOwnerGroupData(tmark.parties.ownerGroups);
                                        OwnerGroupCollection.AddRange(ownerGroups);

                                        var findOwner = OwnerGroupCollection.OrderByDescending(x => x.Type).FirstOrDefault();
                                        if (findOwner != null)
                                        {
                                            response.ClientName = findOwner.Name;
                                            response.PrimaryEmail = findOwner.Email;
                                            response.StreetAddress = findOwner.Address;
                                            response.City = findOwner.City;
                                            response.State = findOwner.State;
                                            response.ZipCode = findOwner.ZipCode;
                                            response.Country = findOwner.Country;
                                            response.LegalEntityType = findOwner.TypeDescription;
                                        }

                                        #endregion


                                        if (string.IsNullOrEmpty(response.PrimaryEmail))
                                        {
                                            response.PrimaryEmail = fallbackEmail;
                                            email = response.PrimaryEmail;
                                        }
                                        else
                                        {
                                            email = response.PrimaryEmail;
                                        }

                                        response.Name = tmark.status.markElement;
                                        response.SerialNumber = int.Parse(transactionsEach.searchId);
                                        response.StatusNumber = tmark.status.tm5Status;
                                        response.StatusDescription = tmark.status.tm5StatusDesc;
                                        //response.Email = email;

                                        if (isknown)
                                        {
                                            user = await _userManager.FindByNameAsync(email);
                                            if (user == null)
                                            {
                                                user = new User();
                                                user.AccessCode = code;
                                                user.UserName = response.PrimaryEmail;
                                                user.Email = response.PrimaryEmail;
                                                user.FullName = response.PrimaryEmail.Contains("@") ? response.PrimaryEmail.Substring(0, email.IndexOf('@')) : string.Empty;
                                                user.Active = true;
                                                user.UserType = UserType.AppUser;
                                                user.Created = DateTime.Now;
                                                user.Modified = DateTime.Now;
                                                user.ClientName = response.ClientName;
                                                user.LegalEntityType = response.LegalEntityType;
                                                user.Street = response.StreetAddress;
                                                user.State = response.State;
                                                user.City = response.City;
                                                user.ZipCode = response.ZipCode;
                                                user.Country = response.Country;
                                                response.Status = TrademarkStatus.TrademarkAccountCreated;


                                                await _userManager.CreateAsync(user, "Kwworks@3");
                                            }
                                            else
                                                response.Status = TrademarkStatus.TrademarkLinked;
                                        }
                                        else
                                            response.Status = TrademarkStatus.TrademarkAttorneyIssue;

                                        if (tmark.prosecutionHistory != null && tmark.prosecutionHistory.Count() > 0)
                                        {
                                            foreach (var item in tmark.prosecutionHistory)
                                            {
                                                ProsecutionHistoryCollection.Add(new ProsecutionHistory()
                                                {
                                                    ProceedingNumber = item.proceedingNum,
                                                    Description = item.entryDesc,
                                                    EntryCode = item.entryCode,
                                                    EntryDate = item.entryDate,
                                                    EntryNumber = item.entryNumber,
                                                    EntryType = item.entryType,
                                                    CreatedBy = currentUser,
                                                    IsActive = true
                                                });
                                            }
                                        }

                                        if (tmark.status.physicalLocationHistory != null && tmark.status.physicalLocationHistory.Count() > 0)
                                        {
                                            foreach (var item in tmark.status.physicalLocationHistory)
                                            {
                                                PhysicalLocationHistoryCollection.Add(new PhysicalLocationHistory()
                                                {
                                                    Description = item.physicalLocationDescription,
                                                    EventDate = item.eventDate != null ? Convert.ToDateTime(item.eventDate) : null,
                                                    PhysicalLocation = item.physicalLocation,
                                                    RegistrationNumber = item.rsn,
                                                    CreatedBy = currentUser,
                                                    IsActive = true
                                                });
                                            }
                                        }

                                        if (tmark.publication.officialGazettes != null)
                                        {
                                            foreach (var item in tmark.publication.officialGazettes)
                                            {
                                                PublicationOfficialGazetteCollection.Add(new PublicationOfficialGazette()
                                                {
                                                    CategoryCode = item.categoryCode,
                                                    CategoryCodeText = item.categoryCodeText,
                                                    CategoryReason = item.categoryReason,
                                                    IssueDate = item.issueDate != null ? DateTime.Parse(item.issueDate) : null,
                                                    RegistrationNumber = item.registrationNumber,
                                                    StatusCode = item.statusCode,
                                                    StatusCodeText = item.statusCodeText,
                                                    ActionDate = item.actionDate != null ? DateTime.Parse(item.actionDate) : null,
                                                    CreatedBy = currentUser,
                                                    IsActive = true
                                                });
                                            }
                                        }

                                        var trademark = new Trademarks.Trademark()
                                        {
                                            User = user,
                                            TrademarkElement = tmark.status.markElement,
                                            DescriptionOfMark = tmark.status.descOfMark,
                                            TrademarkDrawingDescription = tmark.status.markDrawDesc,
                                            ExaminerName = tmark.status.staff.examiner != null && tmark.status.staff.examiner.name != null ? tmark.status.staff.examiner.name : string.Empty,
                                            ExaminerNumber = tmark.status.staff.examiner != null && tmark.status.staff.examiner.number != null ? Convert.ToString(tmark.status.staff.examiner.number) : string.Empty,
                                            Name = tmark.status.correspondence != null && tmark.status.correspondence.individualFullName != null ? tmark.status.correspondence.individualFullName : string.Empty,
                                            InternationalStatusDescription = tmark.status.intStatusDesc != null ? tmark.status.intStatusDesc.ToString() : string.Empty,
                                            Status = tmark.status.status,
                                            SerialNumber = tmark.status.serialNumber,
                                            FiledAsTeasPlusApp = tmark.status.filedAsTeasPlusApp,
                                            CurrentlyAsTeasPlusApp = tmark.status.currentlyAsTeasPlusApp,
                                            FiledAsTeasRfApp = tmark.status.filedAsTeasRfApp,
                                            CurrentlyAsTeasRfApp = tmark.status.currentlyAsTeasRfApp,
                                            SupplementalRegister = tmark.status.supplementalRegister,
                                            AmendPrincipal = tmark.status.amendPrincipal,
                                            AmendSupplemental = tmark.status.amendSupplemental,
                                            CertificationMark = tmark.status.certificationMark,
                                            ServiceMark = tmark.status.serviceMark,
                                            CollectiveMembershipMark = tmark.status.collectiveMembershipMark,
                                            CollectiveServiceMark = tmark.status.collectiveServiceMark,
                                            CollectiveTradeMark = tmark.status.collectiveTradeMark,
                                            StatusDate = tmark.status.statusDate != null ? DateTime.Parse(tmark.status.statusDate) : null,
                                            TrademarkStatus = tmark.status.trademark,
                                            IsStandard = tmark.status.standardChar,
                                            IsColorDrawing = tmark.status.colorDrawingCurr,
                                            IsSection2f = tmark.status.section2f,
                                            IsSection2fPartial = tmark.status.section2fPartial,
                                            IsOthers = tmark.status.others,
                                            IsPreviousRegistered = tmark.status.publishedPrevRegMark,
                                            CommunityLegalServiceTotal = tmark.status.clsTotal,
                                            IsFiledUse = tmark.status.filedUse,
                                            IsFiledInternationLaw = tmark.status.filedItu,
                                            IsFiled44d = tmark.status.filed44d,
                                            IsFiled44e = tmark.status.filed44e,
                                            IsFiled66a = tmark.status.filed66a,
                                            IsFiledNoBasis = tmark.status.filedNoBasis,
                                            IsUseCurrent = tmark.status.useCurr,
                                            IsInternationalLawCurrent = tmark.status.ituCurr,
                                            IsSect44eCurrent = tmark.status.sect44eCurr,
                                            IsSect66aCurrent = tmark.status.sect66aCurr,
                                            IsNoBasisCurrent = tmark.status.noBasisCurr,
                                            IsUseAmended = tmark.status.useAmended,
                                            IsInternationalLawAmended = tmark.status.ituAmended,
                                            IsSect44dAmended = tmark.status.sect44dAmended,
                                            IsSect44eAmended = tmark.status.sect44eAmended,
                                            IsSect8Filed = tmark.status.sect8Filed,
                                            IsSect8Acpt = tmark.status.sect8Acpt,
                                            IsSect8PartialAcpt = tmark.status.sect8PartialAcpt,
                                            IsSect15Filed = tmark.status.sect15Filed,
                                            IsSect15Ack = tmark.status.sect15Ack,
                                            IsSect71Filed = tmark.status.sect71Filed,
                                            IsSect71Acpt = tmark.status.sect71Acpt,
                                            IsSect71PartialAcpt = tmark.status.sect71PartialAcpt,
                                            IsRenewalFiled = tmark.status.renewalFiled,
                                            IsChangeInRegistration = tmark.status.changeInReg,
                                            Sect44dCurrent = tmark.status.sect44dCurr,
                                            MarkDrawingCode = tmark.status.markDrawingCd,
                                            LawOffAssignedCode = tmark.status.lawOffAsgnCd,
                                            CurrentLocationCode = tmark.status.currLocationCd,
                                            CurrentLocationDate = tmark.status.currLocationDt != null ? DateTime.Parse(tmark.status.currLocationDt) : null,
                                            ExternalStatusDescription = tmark.status.extStatusDesc,
                                            CurrentLocation = tmark.status.currentLoc,
                                            NewLawOffAssignedCode = tmark.status.newLawOffAsgnCd,
                                            LawOffAssigned = tmark.status.lawOffAssigned,
                                            Tm5Status = tmark.status.tm5Status,
                                            Tm5StatusDescription = tmark.status.tm5StatusDesc,
                                            StatusDefination = tmark.status.tm5StatusDef,
                                            UsRegistrationNumber = tmark.status.usRegistrationNumber,
                                            UsRegistrationDate = tmark.status.usRegistrationDate != null ? DateTime.Parse(tmark.status.usRegistrationDate) : null,
                                            FiliingDate = tmark.status.filingDate != null ? DateTime.Parse(tmark.status.filingDate) : null,
                                            PublicationDate = tmark.publication.datePublished != null ? DateTime.Parse(tmark.publication.datePublished) : null,
                                            PublicationAllowanceDate = tmark.publication.noticeOfAllowanceDate != null ? DateTime.Parse(tmark.publication.noticeOfAllowanceDate) : null,
                                            CreatedBy = currentUser,
                                            ModifiedBy = currentUser,
                                            Attorney = findAttorney,
                                            GoodAndServices = GoodAndServiceCollection,
                                            OwnerGroups = OwnerGroupCollection,
                                            ProsecutionHistorys = ProsecutionHistoryCollection,
                                            PublicationOfficialGazettes = PublicationOfficialGazetteCollection,
                                            DesignSearches = DesignSearchClassCollection,
                                            PhysicalLocationHistorys = PhysicalLocationHistoryCollection,
                                            SyncDate = DateTimeHelper.Now(),
                                            Modified = DateTimeHelper.Now()
                                        };
                                        trademarkName = trademark.Name;
                                        response.Id = trademark.Id;
                                        var document = (Document)default;
                                        using (TextReader sr = new StringReader(xmlString))
                                        {
                                            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(DocumentList));
                                            DocumentList xmlResponse = (DocumentList)serializer.Deserialize(sr);

                                            foreach (var item in xmlResponse.Document)
                                            {
                                                document = new Document();
                                                var docType = await _trademarkRepository.FindDocumentTypeByCode(item.DocumentTypeCode);
                                                docType = docType == null ? trademark.Documents.Select(x => x.DocumentType).Where(d => d.Code == item.DocumentTypeCode).FirstOrDefault() : docType;
                                                var docCategory = await _trademarkRepository.FindDocumentCategoryByCode(item.CategoryTypeCode);
                                                docCategory = docCategory == null ? trademark.Documents.SelectMany(x => x.DocumentCategorys).Where(d => d.Code == item.CategoryTypeCode).FirstOrDefault() : docCategory;
                                                if (docType == null)
                                                {
                                                    docType = new DocumentType();
                                                    docType.Code = item.DocumentTypeCode;
                                                    docType.DocumentTypeCode = item.DocumentTypeCode;
                                                    docType.CategoryTypeCode = item.CategoryTypeCode;
                                                    docType.CodeDescription = item.CategoryTypeCodeDescriptionText;
                                                    docType.Description = item.DocumentTypeCodeDescriptionText;
                                                    docType.CreatedBy = currentUser;
                                                    _trademarkRepository.Create(docType);
                                                }
                                                document.DocumentType = docType;

                                                if (docCategory == null)
                                                {
                                                    docCategory = new DocumentCategory()
                                                    {
                                                        Code = item.CategoryTypeCode,
                                                        Description = item.CategoryTypeCodeDescriptionText,
                                                        CreatedBy = currentUser
                                                    };
                                                    document.DocumentCategorys.Add(docCategory);
                                                }

                                                document.SerialNumber = (long)item.SerialNumber;
                                                document.MailRoomDate = item.MailRoomDate;
                                                document.ScanDateTime = item.ScanDateTime;
                                                document.TotalPageQuantity = item.TotalPageQuantity;
                                                document.SourceSystem = item.SourceSystem;
                                                document.CreatedBy = currentUser;
                                                document.DocumentTypeId = docType.Id;
                                                document.CategoryId = docCategory.Id;

                                                for (int i = 0; i < item.PageMediaTypeList.Length; i++)
                                                {
                                                    //if (item.CategoryTypeCodeDescriptionText == "Drawing")
                                                    //{
                                                    //    trademark.Image = item.UrlPathList[i];
                                                    //}
                                                    trademark.Image = "https://tsdr.uspto.gov/img/" + trademark.SerialNumber + "/large";
                                                    var docUrl = new DocumentUrl()
                                                    {
                                                        MediaType = item.PageMediaTypeList[i],
                                                        Url = item.UrlPathList[i],
                                                        CreatedBy = currentUser,
                                                        DocumentId = document.Id
                                                    };
                                                    document.DocumentUrls.Add(docUrl);
                                                }
                                                //if (string.IsNullOrEmpty(trademark.Image))
                                                //    trademark.Image = "https://tsdr.uspto.gov/img/" + trademark.SerialNumber + "/large";
                                                trademark.Documents.Add(document);
                                            }
                                        }

                                        var statusChecker = new TrademarkStatusChecker(trademark);
                                        trademark.CurrentStatus = statusChecker.CurrentStatus;
                                        trademark.StatusColor = statusChecker.RegisteredStatusColor.ToString();
                                        trademark.NextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value : null;
                                        trademark.MaxNextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value.AddYears(1) : null;
                                        _trademarkRepository.Create(trademark);
                                    }
                                }
                            }
                            if (results.Errors.Count == 0)
                            {
                                await _UnitOfWork.CommitAsync();
                               
                                results.IsSuccess = true;
                                //await context.DeleteAsync<TradeMarkDataLog>(findLog);
                                if (isknown && user != null)
                                {
                                    if (response.Status == TrademarkStatus.TrademarkLinked)
                                    {
                                        await _notificationService.AddNotification(response.PrimaryEmail, Constants.NewTrademarkLinkedMessage + trademarkName, Constants.TMManagerSupport, currentUser);
                                        var usr = await _userManager.FindByNameAsync(response.PrimaryEmail);
                                        string cc = string.Join(',', usr.UserContacts.Select(x => x.Email).ToList());
                                        await EmailSender.SendEmail(response.PrimaryEmail, Constants.NewActivitySubject, GetEmailBody(TrademarkStatus.TrademarkLinked, response.ClientName, string.Empty, string.Empty, string.Empty), cc, string.Empty);
                                    }
                                    else if (response.Status == TrademarkStatus.TrademarkAccountCreated)
                                    {
                                        string link = $"{AppEnvironment.ServerUrl}register/" + response.PrimaryEmail;
                                        await EmailSender.SendEmail(response.PrimaryEmail, Constants.RegistrationEmailSubject, GetEmailBody(TrademarkStatus.TrademarkAccountCreated, !string.IsNullOrWhiteSpace(user.ClientName) ? user.ClientName : user.LastName + " " + user.FirstName, user.UserName, link, code), string.Empty, string.Empty);
                                    }
                                }
                            }
                        }
                        else
                        {
                            response.Status = TrademarkStatus.SystemError;
                            results.Errors.Add("Error while saving trademark");
                        }
                    }
                    //else
                    //{
                    //    response.Status = TrademarkStatus.SystemError;
                    //    results.Errors.Add("Error while saving trademark");
                    //}

                }
            }
            catch (Exception e)
            {
                response.Status = TrademarkStatus.SystemError;
                results.Errors.Add(e.Message);
            }
            //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDetail", string.Empty);
            //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDocument", string.Empty);
            //_httpContextAccessor.HttpContext.Session.Clear();
            results.TrademarkResponse.Add(response);
            return results;
        }

        public async Task<TrademarkResponseModel> UpdateTrademark(long serialNumber)
        {
            var result = new TrademarkResponseModel();
            var response = new TrademarkResponse();
            var userName = string.Empty;
            string trademarkName = string.Empty;
            try
            {
                List<Trademarks.GoodAndService> GoodAndServiceCollection = new List<Trademarks.GoodAndService>();
                var trademark = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetBySerialNumber(serialNumber));
                if (trademark != null)
                {
                    var ids = new string[] { serialNumber.ToString() };
                    string jsonString = await TsdrApiService.GetTrademarkDetail("sn", ids);
                    string code = Helper.RandomString();
                    bool isknown = false;

                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);
                        var transactionList = trademarkDetail.transactionList;

                        foreach (var transactionsEach in transactionList)
                        {
                            foreach (var tmark in transactionsEach.trademarks)
                            {
                                string attorneyName = tmark.status.correspondence.attorneyName != null ? tmark.status.correspondence.attorneyName : string.Empty;
                                string attorneyEmail = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() >= 0 ? tmark.status.correspondence.attorneyEmail.addresses[0] : string.Empty;
                                string authIndicator = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.authIndicator != null ? tmark.status.correspondence.attorneyEmail.authIndicator : string.Empty;
                                string attorneyLineOne = tmark.status.correspondence.address != null && tmark.status.correspondence.address.line1 != null ? tmark.status.correspondence.address.line1 : string.Empty;
                                if (tmark.status.correspondence.address != null && tmark.status.correspondence.address.line2 != null)
                                    attorneyLineOne = $"{attorneyLineOne} {tmark.status.correspondence.address.line2}";
                                string attorneyPostalCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.postalCode != null ? tmark.status.correspondence.address.postalCode : string.Empty;
                                string attorneyCity = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;
                                string attorneyCountry = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;
                                string attorneyCountryCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryCode != null ? tmark.status.correspondence.address.countryCode : string.Empty;
                                string attorneyState = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;
                                string attorneyLegalEntity = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;

                                //isknown = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilter(attorneyEmail, attorneyName)) == null ? false : true;
                                if (!string.IsNullOrWhiteSpace(attorneyEmail) || !string.IsNullOrWhiteSpace(attorneyName))
                                {
                                    var findRecognisedattorney = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilter(attorneyEmail.Trim(), attorneyName.Trim()));
                                    isknown = findRecognisedattorney == null ? false : true;
                                }
                                //response.ClientName = attorneyName;
                                var attorney = trademark.Attorney;
                                var firm = attorney.Firm;

                                firm.Name = tmark.status.correspondence.firmName ?? string.Empty;
                                firm.IndividualName = tmark.status.correspondence.individualFullName ?? string.Empty;
                                firm.Email = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.addresses != null && tmark.status.correspondence.correspondantEmail.addresses.Count() > 0 ? tmark.status.correspondence.correspondantEmail.addresses[0] : string.Empty;
                                firm.IsEmailAuthticated = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.authIndicator != null ? tmark.status.correspondence.correspondantEmail.authIndicator : string.Empty;
                                firm.Phone = tmark.status.correspondence.correspondantPhone ?? string.Empty;
                                firm.Fax = tmark.status.correspondence.correspondantFax ?? string.Empty;

                                attorney.Name = attorneyName;
                                attorney.FullName = attorneyName;
                                attorney.Email = attorneyEmail;
                                attorney.Email2 = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() > 1 ? tmark.status.correspondence.attorneyEmail.addresses[1] : string.Empty;
                                attorney.Line1 = attorneyLineOne;
                                attorney.IsEmailAuthticated = authIndicator == "Y" ? true : false;
                                attorney.City = attorneyCity;
                                attorney.PostalCode = attorneyPostalCode;
                                attorney.CountryCode = attorneyCountryCode;
                                attorney.CountryName = attorneyCountry;
                                //attorney.IsKnown = isknown;

                                if (tmark.status.designSearchList != null && tmark.status.designSearchList.Count() > 0)
                                {
                                    var dsList = await _designSearchClassRepository.ListDesignSearchClassAsync(DesignSearchClassSpecification.GetByCode(tmark.status.designSearchList.Select(c => c.code).ToList()));
                                    foreach (var item in tmark.status.designSearchList)
                                    {
                                        var ds = dsList.Where(x => x.Code == item.code).FirstOrDefault();

                                        if (ds == null)
                                        {
                                            ds = new DesignSearchClass()
                                            {
                                                Trademark = trademark,
                                            };
                                            ds.Code = item.code;
                                            ds.CreatedBy = trademark.User.Email;
                                            ds.ModifiedBy = trademark.User.Email;

                                            foreach (var dsdItem in item.descriptions)
                                            {
                                                var dsSearchClasss = new DesignSearchDescriptionClass()
                                                {
                                                    Description = dsdItem
                                                };
                                                dsSearchClasss.CreatedBy = trademark.User.Email;
                                                ds.DesignSearchDescriptionClasses.Add(dsSearchClasss);
                                            }
                                            trademark.DesignSearches.Add(ds);
                                        }
                                    }
                                }

                                var goodandservice = new List<string>();
                                foreach (var gslist in tmark.gsList)
                                {
                                    foreach (var item in gslist.usClasses)
                                    {
                                        goodandservice.Add(item.code);
                                    }
                                    foreach (var item in gslist.internationalClasses)
                                    {
                                        goodandservice.Add(item.code);
                                    }
                                }

                                var gosList = await _goodAndServiceClassRepository.ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification.GetByNames(goodandservice));
                                var gs = (GoodAndService)default;
                                if (goodandservice.Distinct().Count() > gosList.Count())
                                {
                                    gs = new GoodAndService();
                                    foreach (var gslist in tmark.gsList)
                                    {
                                        foreach (var item in gslist.usClasses)
                                        {
                                            var gsClass = (GoodAndServiceClass)default;
                                            gsClass = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                            if (gsClass == null)
                                            {
                                                gsClass = new GoodAndServiceClass
                                                {
                                                    Code = item.code,
                                                    Description = item.description,
                                                    CreatedBy = trademark.User.Email,
                                                    ModifiedBy = trademark.User.Email,
                                                    Type = GoodAndServiceClassType.UsClass
                                                };
                                                _goodAndServiceClassRepository.Create(gsClass);
                                                gs.GoodAndServiceClass.Add(gsClass);
                                            }
                                        }

                                        //foreach (var item in gslist.internationalClasses)
                                        //{
                                        //    var gs = (GoodAndServiceClass)default;
                                        //    gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                        //    if (gs == null)
                                        //    {
                                        //        gs = new GoodAndServiceClass
                                        //        {
                                        //            Code = item.code,
                                        //            Description = item.description,
                                        //            CreatedBy = trademark.User.Email,
                                        //            ModifiedBy = trademark.User.Email,
                                        //            Type = GoodAndServiceClassType.InternationalClass
                                        //        };
                                        //        _goodAndServiceClassRepository.Create(gs);
                                        //    }
                                        //    GoodAndServiceClassCollection.Add(gs);
                                        //}

                                        gs.StatusCode = gslist.statusCode;
                                        gs.StatusDescription = gslist.statusDescription;
                                        gs.StatusDate = gslist.statusDate;
                                        gs.FirstUseDate = gslist.firstUseDate;
                                        gs.PrimeClassCode = gslist.primeClassCode;
                                        gs.Description = gslist.description;
                                        gs.CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
                                        gs.TrademarkId = trademark.Id;
                                        trademark.GoodAndServices.Add(gs);
                                    }
                                }
                                #region OwnerGroups
                                var ownerGroups = FillOwnerGroupData(tmark.parties.ownerGroups);
                                foreach (var item in ownerGroups)
                                {
                                    var findOwnerGroup = trademark.OwnerGroups.Where(x => x.Type == item.Type && x.Name == item.Name && x.City == item.City).FirstOrDefault();
                                    if (findOwnerGroup == null)
                                    {
                                        var owner = new OwnerGroup
                                        {
                                            Name = item.Name,
                                            Address = item.Address,
                                            City = item.City,
                                            Email = item.Email,
                                            Email2 = item.Email2,
                                            State = item.State,
                                            IsoCode = item.IsoCode,
                                            IsoName = item.IsoName,
                                            WipoCode = item.WipoCode,
                                            WipoName = item.WipoName,
                                            Type = item.Type,
                                            ZipCode = item.ZipCode,
                                            Country = item.Country,
                                            TypeCode = item.TypeCode,
                                            TypeDescription = item.TypeDescription,
                                            CreatedBy = trademark.User.Email,
                                            ModifiedBy = trademark.User.Email,
                                            TrademarkId = trademark.Id,
                                            IsActive = true,
                                        };
                                        _trademarkRepository.Create(owner);
                                    }
                                }

                                var findOwner = ownerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
                                if (findOwner != null)
                                {
                                    response.ClientName = findOwner.Name;
                                    response.PrimaryEmail = findOwner.Email;
                                    response.StreetAddress = findOwner.Address;
                                    response.City = findOwner.City;
                                    response.State = findOwner.State;
                                    response.ZipCode = findOwner.ZipCode;
                                    response.Country = findOwner.Country;
                                    response.LegalEntityType = findOwner.TypeDescription;
                                }
                                #endregion


                                var user = (User)default;
                                if (isknown)
                                {
                                    if (trademark.User != null)
                                    {
                                        user = trademark.User;
                                        response.PrimaryEmail = user.Email;
                                        isknown = false;
                                    }
                                    else
                                    {
                                        user = await _userManager.FindByNameAsync(response.PrimaryEmail);
                                        isknown = false;
                                    }
                                    if (user == null)
                                    {
                                        user = new User();
                                        string email = response.PrimaryEmail;
                                        user.UserName = response.PrimaryEmail;
                                        user.Email = response.PrimaryEmail;
                                        user.FullName = response.PrimaryEmail.Contains("@") ? response.PrimaryEmail.Substring(0, email.IndexOf('@')) : string.Empty;
                                        user.Active = true;
                                        user.UserType = UserType.AppUser;
                                        user.Created = DateTime.Now;
                                        user.Modified = DateTime.Now;
                                        user.ClientName = response.ClientName;
                                        user.LegalEntityType = response.LegalEntityType;
                                        user.Street = attorneyLineOne;
                                        user.State = response.State;
                                        user.City = response.City;
                                        user.ZipCode = response.ZipCode;
                                        user.Country = response.Country;
                                        response.Status = TrademarkStatus.TrademarkAccountCreated;
                                        await _userManager.CreateAsync(user, "Kwworks@3");
                                        userName = user.UserName;
                                        trademark.User = user;
                                    }
                                    else
                                    {
                                        response.Status = TrademarkStatus.TrademarkLinked;
                                        trademark.User = user;
                                    }
                                    response.ClientName = user.ClientName;
                                }
                                else
                                {
                                    response.Status = TrademarkStatus.TrademarkAttorneyIssue;
                                }

                                if (tmark.prosecutionHistory != null && tmark.prosecutionHistory.Count() > 0)
                                {
                                    foreach (var item in tmark.prosecutionHistory)
                                    {
                                        var findProsecution = trademark.ProsecutionHistorys.Where(x => x.ProceedingNumber == item.proceedingNum && x.EntryCode == item.entryCode && x.EntryNumber == item.entryNumber && x.EntryType == item.entryType).FirstOrDefault();
                                        if (findProsecution == null)
                                        {
                                            var prosecution = new ProsecutionHistory()
                                            {
                                                ProceedingNumber = item.proceedingNum,
                                                Description = item.entryDesc,
                                                EntryCode = item.entryCode,
                                                EntryDate = item.entryDate,
                                                EntryNumber = item.entryNumber,
                                                EntryType = item.entryType,
                                                CreatedBy = trademark.User != null ? trademark.User.UserName : trademark.CreatedBy,
                                                TrademarkId = trademark.Id,
                                                IsActive = true
                                            };
                                            //trademark.ProsecutionHistorys.Add(prosecution);
                                            _trademarkRepository.Create(prosecution);
                                        }
                                    }
                                }


                                if (tmark.status.physicalLocationHistory != null && tmark.status.physicalLocationHistory.Count() > 0)
                                {
                                    foreach (var item in tmark.status.physicalLocationHistory)
                                    {
                                        var findProsecution = trademark.PhysicalLocationHistorys.Where(x => x.RegistrationNumber == item.rsn).FirstOrDefault();
                                        if (findProsecution == null)
                                        {
                                            var physicalHistory = new PhysicalLocationHistory()
                                            {
                                                Description = item.physicalLocationDescription,
                                                EventDate = item.eventDate != null ? Convert.ToDateTime(item.eventDate) : null,
                                                PhysicalLocation = item.physicalLocation,
                                                RegistrationNumber = item.rsn,
                                                CreatedBy = trademark.User != null ? trademark.User.UserName : trademark.CreatedBy,
                                                TrademarkId = trademark.Id,
                                                IsActive = true
                                            };
                                            //trademark.PhysicalLocationHistorys.Add(physicalHistory);
                                            _trademarkRepository.Create(physicalHistory);
                                        }
                                    }
                                }

                                if (tmark.publication.officialGazettes != null)
                                {
                                    foreach (var item in tmark.publication.officialGazettes)
                                    {
                                        var findPublication = trademark.PublicationOfficialGazettes.Where(x => x.CategoryCode == item.categoryCode && x.RegistrationNumber == item.registrationNumber && x.StatusCode == item.statusCode).FirstOrDefault();
                                        if (findPublication == null)
                                        {
                                            var publication = new PublicationOfficialGazette()
                                            {
                                                CategoryCode = item.categoryCode,
                                                CategoryCodeText = item.categoryCodeText,
                                                CategoryReason = item.categoryReason,
                                                IssueDate = item.issueDate != null ? DateTime.Parse(item.issueDate) : null,
                                                RegistrationNumber = item.registrationNumber,
                                                StatusCode = item.statusCode,
                                                StatusCodeText = item.statusCodeText,
                                                ActionDate = item.actionDate != null ? DateTime.Parse(item.actionDate) : null,
                                                CreatedBy = trademark.User != null ? trademark.User.UserName : trademark.CreatedBy,
                                                TrademarkId = trademark.Id,
                                                IsActive = true
                                            };
                                            //trademark.PublicationOfficialGazettes.Add(publication);
                                            _trademarkRepository.Create(publication);
                                        }
                                    }
                                }

                                trademark.TrademarkElement = tmark.status.markElement;
                                trademark.DescriptionOfMark = tmark.status.descOfMark;
                                trademark.TrademarkDrawingDescription = tmark.status.markDrawDesc;
                                trademark.ExaminerName = tmark.status.staff.examiner != null && tmark.status.staff.examiner.name != null ? tmark.status.staff.examiner.name : string.Empty;
                                trademark.ExaminerNumber = tmark.status.staff.examiner != null && tmark.status.staff.examiner.number != null ? Convert.ToString(tmark.status.staff.examiner.number) : string.Empty;
                                trademark.Name = tmark.status.correspondence != null && tmark.status.correspondence.individualFullName != null ? tmark.status.correspondence.individualFullName : string.Empty;
                                trademark.InternationalStatusDescription = tmark.status.intStatusDesc != null ? tmark.status.intStatusDesc.ToString() : string.Empty;
                                trademark.Status = tmark.status.status;
                                trademark.SerialNumber = tmark.status.serialNumber;
                                trademark.FiledAsTeasPlusApp = tmark.status.filedAsTeasPlusApp;
                                trademark.CurrentlyAsTeasPlusApp = tmark.status.currentlyAsTeasPlusApp;
                                trademark.FiledAsTeasRfApp = tmark.status.filedAsTeasRfApp;
                                trademark.CurrentlyAsTeasRfApp = tmark.status.currentlyAsTeasRfApp;
                                trademark.SupplementalRegister = tmark.status.supplementalRegister;
                                trademark.AmendPrincipal = tmark.status.amendPrincipal;
                                trademark.AmendSupplemental = tmark.status.amendSupplemental;
                                trademark.CertificationMark = tmark.status.certificationMark;
                                trademark.ServiceMark = tmark.status.serviceMark;
                                trademark.CollectiveMembershipMark = tmark.status.collectiveMembershipMark;
                                trademark.CollectiveServiceMark = tmark.status.collectiveServiceMark;
                                trademark.CollectiveTradeMark = tmark.status.collectiveTradeMark;
                                trademark.StatusDate = tmark.status.statusDate != null ? DateTime.Parse(tmark.status.statusDate) : null;
                                trademark.IsStandard = tmark.status.standardChar;
                                trademark.IsColorDrawing = tmark.status.colorDrawingCurr;
                                trademark.IsSection2f = tmark.status.section2f;
                                trademark.IsSection2fPartial = tmark.status.section2fPartial;
                                trademark.IsOthers = tmark.status.others;
                                trademark.IsPreviousRegistered = tmark.status.publishedPrevRegMark;
                                trademark.CommunityLegalServiceTotal = tmark.status.clsTotal;
                                trademark.IsFiledUse = tmark.status.filedUse;
                                trademark.IsFiledInternationLaw = tmark.status.filedItu;
                                trademark.IsFiled44d = tmark.status.filed44d;
                                trademark.IsFiled44e = tmark.status.filed44e;
                                trademark.IsFiled66a = tmark.status.filed66a;
                                trademark.IsFiledNoBasis = tmark.status.filedNoBasis;
                                trademark.IsUseCurrent = tmark.status.useCurr;
                                trademark.IsInternationalLawCurrent = tmark.status.ituCurr;
                                trademark.IsSect44eCurrent = tmark.status.sect44eCurr;
                                trademark.IsSect66aCurrent = tmark.status.sect66aCurr;
                                trademark.IsNoBasisCurrent = tmark.status.noBasisCurr;
                                trademark.IsUseAmended = tmark.status.useAmended;
                                trademark.IsInternationalLawAmended = tmark.status.ituAmended;
                                trademark.IsSect44dAmended = tmark.status.sect44dAmended;
                                trademark.IsSect44eAmended = tmark.status.sect44eAmended;
                                trademark.IsSect8Filed = tmark.status.sect8Filed;
                                trademark.IsSect8Acpt = tmark.status.sect8Acpt;
                                trademark.IsSect8PartialAcpt = tmark.status.sect8PartialAcpt;
                                trademark.IsSect15Filed = tmark.status.sect15Filed;
                                trademark.IsSect15Ack = tmark.status.sect15Ack;
                                trademark.IsSect71Filed = tmark.status.sect71Filed;
                                trademark.IsSect71Acpt = tmark.status.sect71Acpt;
                                trademark.IsSect71PartialAcpt = tmark.status.sect71PartialAcpt;
                                trademark.IsRenewalFiled = tmark.status.renewalFiled;
                                trademark.IsChangeInRegistration = tmark.status.changeInReg;
                                trademark.Sect44dCurrent = tmark.status.sect44dCurr;
                                trademark.MarkDrawingCode = tmark.status.markDrawingCd;
                                trademark.LawOffAssignedCode = tmark.status.lawOffAsgnCd;
                                trademark.CurrentLocationCode = tmark.status.currLocationCd;
                                trademark.CurrentLocationDate = tmark.status.currLocationDt != null ? DateTime.Parse(tmark.status.currLocationDt) : null;
                                trademark.ExternalStatusDescription = tmark.status.extStatusDesc;
                                trademark.CurrentLocation = tmark.status.currentLoc;
                                trademark.NewLawOffAssignedCode = tmark.status.newLawOffAsgnCd;
                                trademark.LawOffAssigned = tmark.status.lawOffAssigned;
                                trademark.Tm5Status = tmark.status.tm5Status;
                                trademark.Tm5StatusDescription = tmark.status.tm5StatusDesc;
                                trademark.StatusDefination = tmark.status.tm5StatusDef;
                                trademark.UsRegistrationNumber = tmark.status.usRegistrationNumber;
                                trademark.FiliingDate = tmark.status.filingDate != null ? DateTime.Parse(tmark.status.filingDate) : null;
                                trademark.PublicationDate = tmark.publication.datePublished != null ? DateTime.Parse(tmark.publication.datePublished) : null;
                                trademark.PublicationAllowanceDate = tmark.publication.noticeOfAllowanceDate != null ? DateTime.Parse(tmark.publication.noticeOfAllowanceDate) : null;
                                trademark.SyncDate = DateTimeHelper.Now();
                                trademark.Modified = DateTimeHelper.Now();

                                var statusChecker = new TrademarkStatusChecker(trademark);
                                trademark.CurrentStatus = statusChecker.CurrentStatus;
                                trademark.StatusColor = statusChecker.RegisteredStatusColor.ToString();
                                trademark.NextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value : null;
                                trademark.MaxNextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value.AddYears(1) : null;

                                _trademarkRepository.Update(trademark);
                            }
                            trademarkName = trademark.Name;
                        }

                        if (!result.Errors.Any())
                        {
                            await _UnitOfWork.CommitAsync();
                            result.IsSuccess = true;

                            if (isknown)
                            {
                                if (response.Status == TrademarkStatus.TrademarkLinked)
                                {
                                    var usr = await _userManager.FindByNameAsync(response.PrimaryEmail);
                                    await _notificationService.AddNotification(response.PrimaryEmail, Constants.NewTrademarkLinkedMessage + trademarkName, Constants.TMManagerSupport, (_httpContextAccessor.HttpContext!= null ? _httpContextAccessor.HttpContext.User.Identity.Name : response.PrimaryEmail));
                                    await EmailSender.SendEmail(response.PrimaryEmail, Constants.NewActivitySubject, GetEmailBody(TrademarkStatus.TrademarkLinked, response.Name, string.Empty, string.Empty, string.Empty), string.Empty, string.Empty);
                                }
                                else if (response.Status == TrademarkStatus.TrademarkAccountCreated)
                                {
                                    string link = $"{AppEnvironment.ServerUrl}register/" + response.Email + "";
                                    await EmailSender.SendEmail(response.PrimaryEmail, Constants.RegistrationEmailSubject, GetEmailBody(TrademarkStatus.TrademarkAccountCreated, response.ClientName, userName, link, code), string.Empty, string.Empty);
                                }
                            }
                        }
                        var tdResponse = new TrademarkResponse();
                        tdResponse.Id = trademark.Id;
                        result.TrademarkResponse.Add(tdResponse);
                    }
                    else
                        result.Errors.Add("Error while fetching data");
                }
                else
                    result.Errors.Add("Invalid Serial Number");

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
                _UnitOfWork.ClearError();
                result.IsSuccess = false;
                result.Errors.Add("Something went wrong while fetching data, Please check your internet connection!");
            }
            return result;
        }
        public async Task SyncBatchTrademark()
        {
            var response = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.SyncBatchTrademarks());
            response.OrderBy(x => x.SyncDate).Take(20).ToList();

            foreach (var item in response)
            {
                await UpdateTrademark(item.SerialNumber);
            }
        }
        public async Task<SaveResults> VerifyEmail(string email)
        {
            var result = new SaveResults();

            var user = await _userManager.FindByNameAsync(email);
            if (user != null)
            {
                var trademarkExists = await _trademarkRepository.FindTrademarkAsync(TrademarkSpecification.GetByUserUd(user.Id));
                if (trademarkExists != null && user.EmailConfirmed)
                {
                    result.IsSuccess = true;
                }
                else if (trademarkExists != null && user.EmailConfirmed == false)
                {
                    result.IsSuccess = false;
                    result.Errors.Add("Email found. Account not found!");
                }
                else
                    result.Errors.Add("Account not found!");
            }
            else
                result.Errors.Add("Email not found!");

            return result;
        }
        public async Task<SaveResults> RequestAccess(RequestAccessModel model)
        {
            var result = new SaveResults();

            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                //TODO: Send email or Insert record to table
            }
            else
                result.Errors.Add("Email already exists!");

            return result;
        }
        public Task<List<GoodAndServiceModel>> GetGoodsAndService(Guid id)
        {
            throw new NotImplementedException();
        }
        public async Task<TrademarkResponseModel> SaveFileRecords(TrademarkImportFileModel model)
        {
            var result = new TrademarkResponseModel();
            var trademarkImportFile = new TrademarkImportFile();
            trademarkImportFile.Name = model.BatchName;
            trademarkImportFile.CreatedBy = "webmaster@kwworks.com";
            var failedStatus = new[] { TrademarkStatus.TrademarkNotFound, TrademarkStatus.TrademarkInSystem, TrademarkStatus.TrademarkFailed };
            foreach (var tmItm in model.TrademarkFileItems)
            {
                //var getTrademarkDetail = await GetTrademarkForBatch(tmItm.Email, tmItm.SerialNumber.ToString());
                var tmFileItems = new TrademarkImportItem()
                {
                    SerialNumber = tmItm.SerialNumber,
                    Email = tmItm.Email,
                    ClientName = string.Empty,
                    TrademarkName = string.Empty,
                    Status = tmItm.IsDuplicate ? TrademarkStatus.TrademarkInSystem : (!string.IsNullOrEmpty(tmItm.Email) ? TrademarkStatus.Processing : TrademarkStatus.TrademarkEmailNotFound),
                    ModifiedBy = "webmaster@kwworks.com",
                    CreatedBy = "webmaster@kwworks.com"
                };
                trademarkImportFile.TrademarkImportItems.Add(tmFileItems);
                tmItm.Id = tmFileItems.Id;
            }
            _trademarkRepository.Create(trademarkImportFile);
            if (!result.Errors.Any())
            {
                await _UnitOfWork.CommitAsync();
                result.IsSuccess = true;
            }
            return result;
        }
        public async Task SaveBatchTrademark(TrademarkImportFileModel model)
        {

            foreach (var item in model.TrademarkFileItems)
            {
                if (!item.IsDuplicate)
                {
                    await UpdateImportedFilesInfo(item.Id);
                }

            }
        }
        private async Task<TrademarkResponse> GetTrademarkForBatch(string fallbackEmail, string id)
        {
            var response = new TrademarkResponse();
            try
            {
                var trademarkExists = await _trademarkRepository.FindTrademarkWithOutIncludeAsync(TrademarkSpecification.GetBySerialNumber(Convert.ToInt64(id)));

                if (trademarkExists == null)
                {
                    var ids = new string[] { id };
                    string type = "sn";
                    if (int.Parse(ids[0]) < 10000000)
                        type = "rn";
                    string jsonString = await TsdrApiService.GetTrademarkDetail(type, ids);
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        string xmlString = await TsdrApiService.GetTrademarkDocumentDetails(type, false, ids);
                        TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);
                        response.JsonString = jsonString;
                        response.XMLString = xmlString;
                        //string email = string.Empty;
                        //string ClientName = string.Empty;
                        //string LegalEntityType = string.Empty;
                        //string ClientAddress = string.Empty;
                        //string ClientCity = string.Empty;
                        //string ClientCountry = string.Empty;
                        //string ClientZipCode = string.Empty;
                        //string ClientState = string.Empty;

                        foreach (var transactionsEach in trademarkDetail.transactionList)
                        {
                            foreach (var tmark in transactionsEach.trademarks)
                            {
                                if (tmark.status.tm5StatusDesc.Substring(0, 4) == "DEAD")
                                    response.Status = TrademarkStatus.TrademarkDead;
                                else
                                    response.Status = TrademarkStatus.TrademarkSuccess;

                                if (string.IsNullOrEmpty(tmark.status.correspondence.attorneyName))
                                    response.Status = TrademarkStatus.TrademarkAttorneyIssue;

                                response.Name = tmark.status.markElement;
                                response.SerialNumber = int.Parse(transactionsEach.searchId);
                                response.StatusNumber = tmark.status.tm5Status;
                                response.StatusDescription = tmark.status.tm5StatusDesc;
                                #region OwnerGRoups
                                var ownerGroups = FillOwnerGroupData(tmark.parties.ownerGroups);

                                var findOwner = ownerGroups.OrderByDescending(x => x.Type).FirstOrDefault();
                                if (findOwner != null)
                                {
                                    response.ClientName = findOwner.Name;
                                    response.PrimaryEmail = findOwner.Email;
                                    response.StreetAddress = findOwner.Address;
                                    response.City = findOwner.City;
                                    response.State = findOwner.State;
                                    response.ZipCode = findOwner.ZipCode;
                                    response.Country = findOwner.Country;
                                }
                                //if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null)
                                //{
                                //    ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].name != null ? tmark.parties.ownerGroups._20[0].name : string.Empty;
                                //    LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].entityType != null && tmark.parties.ownerGroups._20[0].entityType.description != null ? tmark.parties.ownerGroups._20[0].entityType.description : string.Empty;
                                //    ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].address1 != null ? tmark.parties.ownerGroups._20[0].address1 : string.Empty;
                                //    ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].citizenship != null && tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name : string.Empty;
                                //    email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].emailList != null && tmark.parties.ownerGroups._20[0].emailList.addresses != null && tmark.parties.ownerGroups._20[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._20[0].emailList.addresses[0] : string.Empty;
                                //    ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].zip != null ? tmark.parties.ownerGroups._20[0].zip : string.Empty;
                                //    ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].addressStateCountry != null && tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code : string.Empty;
                                //    ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].city != null ? tmark.parties.ownerGroups._20[0].city : string.Empty;

                                //    response.ClientName = ClientName;
                                //    response.PrimaryEmail = email;
                                //    response.StreetAddress = ClientAddress;
                                //    response.City = ClientCity;
                                //    response.State = ClientState;
                                //    response.ZipCode = ClientZipCode;
                                //    response.Country = ClientCountry;
                                //}
                                //else if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null)
                                //{
                                //    ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].name != null ? tmark.parties.ownerGroups._10[0].name : string.Empty;
                                //    LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].entityType != null && tmark.parties.ownerGroups._10[0].entityType.description != null ? tmark.parties.ownerGroups._10[0].entityType.description : string.Empty;
                                //    ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].address1 != null ? tmark.parties.ownerGroups._10[0].address1 : string.Empty;
                                //    ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].citizenship != null && tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name : string.Empty;
                                //    email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].emailList != null && tmark.parties.ownerGroups._10[0].emailList.addresses != null && tmark.parties.ownerGroups._10[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._10[0].emailList.addresses[0] : string.Empty;
                                //    ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].zip != null ? tmark.parties.ownerGroups._10[0].zip : string.Empty;
                                //    ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].addressStateCountry != null && tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code : string.Empty;
                                //    ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].city != null ? tmark.parties.ownerGroups._10[0].city : string.Empty;

                                //    response.ClientName = ClientName;
                                //    response.PrimaryEmail = email;
                                //    response.StreetAddress = ClientAddress;
                                //    response.City = ClientCity;
                                //    response.State = ClientState;
                                //    response.ZipCode = ClientZipCode;
                                //    response.Country = ClientCountry;
                                //}
                                #endregion

                                if (string.IsNullOrEmpty(response.PrimaryEmail))
                                {
                                    response.PrimaryEmail = fallbackEmail;
                                    response.Status = TrademarkStatus.TrademarkEmailNotFound;
                                    //email = fallbackEmail;
                                }
                            }
                        }
                    }
                    else
                    {
                        response.SerialNumber = int.Parse(id);
                        response.Status = TrademarkStatus.TrademarkNotFound;
                    }
                }
                else
                {
                    response.SerialNumber = int.Parse(trademarkExists.SerialNumber.ToString());
                    response.Name = trademarkExists.Attorney.Name;
                    response.Status = TrademarkStatus.TrademarkInSystem;
                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToLogString());
                response.Status = TrademarkStatus.TrademarkFailed;
            }
            return response;
        }
        //private async Task<TrademarkResponseModel> SaveTrademarkForBatch(string jsonString, string xmlString, string userInfoString)
        //{
        //    var results = new TrademarkResponseModel();
        //    var response = new TrademarkResponse();

        //    var now = DateTimeHelper.Now();
        //    try
        //    {
        //        List<Trademarks.GoodAndService> GoodAndServiceCollection = new List<Trademarks.GoodAndService>();
        //        List<Trademarks.OwnerGroup> OwnerGroupCollection = new List<Trademarks.OwnerGroup>();
        //        List<Trademarks.GoodAndServiceClass> GoodAndServiceClassCollection = new List<Trademarks.GoodAndServiceClass>();
        //        List<Trademarks.DesignSearchClass> DesignSearchClassCollection = new List<Trademarks.DesignSearchClass>();
        //        List<Trademarks.ProsecutionHistory> ProsecutionHistoryCollection = new List<Trademarks.ProsecutionHistory>();
        //        List<Trademarks.PhysicalLocationHistory> PhysicalLocationHistoryCollection = new List<Trademarks.PhysicalLocationHistory>();
        //        List<Trademarks.PublicationOfficialGazette> PublicationOfficialGazetteCollection = new List<Trademarks.PublicationOfficialGazette>();

        //        var userInfo = JsonConvert.DeserializeObject<UserInfo>(userInfoString);
        //        var user = (User)default;
        //        user = await _userManager.FindByNameAsync(userInfo.Email);
        //        if (user == null)
        //        {
        //            user = new User();
        //            response.Status = TrademarkStatus.TrademarkAccountCreated;
        //            string email = userInfo.Email;
        //            user.UserName = email;
        //            user.Email = email;
        //            user.FullName = email.Contains("@") ? email.Substring(0, email.IndexOf('@')) : string.Empty;
        //            user.Active = true;
        //            user.UserType = UserType.AppUser;
        //            user.Created = DateTime.Now;
        //            user.Modified = DateTime.Now;
        //            user.ClientName = userInfo.ClientName;
        //            user.LegalEntityType = userInfo.LegalEntityType;
        //            user.Street = userInfo.ClientAddress;
        //            user.State = userInfo.ClientState;
        //            user.City = userInfo.ClientCity;
        //            user.ZipCode = userInfo.ClientZipCode;
        //            user.Country = userInfo.Country;
        //            await _userManager.CreateAsync(user, "Kwworks@3");
        //        }
        //        else
        //            response.Status = TrademarkStatus.TrademarkLinked;

        //        if (!string.IsNullOrEmpty(jsonString))
        //        {
        //            TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);
        //            var transactionList = trademarkDetail.transactionList;

        //            var trademarkFilter = new List<long>();
        //            foreach (var tmark in transactionList)
        //            {
        //                trademarkFilter.Add(tmark.trademarks.Select(x => (long)x.status.serialNumber).FirstOrDefault());
        //            }

        //            var findTrademarks = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetBySerialNumbers(trademarkFilter));

        //            foreach (var transactionsEach in transactionList)
        //            {
        //                foreach (var tmark in transactionsEach.trademarks)
        //                {
        //                    response.Name = tmark.status.markElement;
        //                    response.SerialNumber = int.Parse(transactionsEach.searchId);
        //                    response.StatusNumber = tmark.status.tm5Status;
        //                    response.StatusDescription = tmark.status.tm5StatusDesc;
        //                    response.Email = userInfo.Email;

        //                    if (findTrademarks.Any(x => x.SerialNumber == tmark.status.serialNumber))
        //                    {
        //                        response.SerialNumber = tmark.status.serialNumber;
        //                        response.Status = TrademarkStatus.TrademarkInSystem;
        //                        results.Errors.Add("Trademark already in system");
        //                    }
        //                    else
        //                    {
        //                        var findFirm = (Firm)default;
        //                        var findAttorney = (Attorneys.Attorney)default;

        //                        findFirm = await _firmRepository.FindFirmAsync(FirmSpecification.GetByName(tmark.status.correspondence.firmName));
        //                        if (findFirm == null)
        //                        {
        //                            findFirm = new Firm
        //                            {
        //                                Name = tmark.status.correspondence.firmName ?? string.Empty,
        //                                Email = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() > 0 ? tmark.status.correspondence.attorneyEmail.addresses[0] : string.Empty,
        //                                Phone = tmark.status.correspondence.correspondantPhone ?? string.Empty,
        //                                Fax = tmark.status.correspondence.correspondantFax ?? string.Empty,
        //                                CreatedBy = user.Email,
        //                                ModifiedBy = user.Email
        //                            };
        //                            _firmRepository.Create(findFirm);
        //                        }

        //                        findAttorney = await _attorneyRepository.FindAttorney(AttorneySpecification.GetByName(tmark.status.correspondence.attorneyName));
        //                        if (findAttorney == null)
        //                        {
        //                            string name = tmark.status.correspondence.attorneyName ?? string.Empty;
        //                            string email = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() >= 0 ? tmark.status.correspondence.attorneyEmail.addresses[0] : string.Empty;
        //                            bool isknown = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilter(email, name)) == null ? false : true;

        //                            findAttorney = new Attorneys.Attorney
        //                            {
        //                                //UserId = user.Id,
        //                                Name = name,
        //                                FullName = tmark.status.correspondence.individualFullName ?? string.Empty,
        //                                Email = email,
        //                                Email2 = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() > 1 ? tmark.status.correspondence.attorneyEmail.addresses[1] : string.Empty,
        //                                Line1 = tmark.status.correspondence.address != null && tmark.status.correspondence.address.line1 != null ? tmark.status.correspondence.address.line1 : string.Empty,
        //                                City = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty,
        //                                PostalCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.postalCode != null ? tmark.status.correspondence.address.postalCode : string.Empty,
        //                                CountryCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryCode != null ? tmark.status.correspondence.address.countryCode : string.Empty,
        //                                CountryName = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryName != null ? tmark.status.correspondence.address.countryName : string.Empty,
        //                                CreatedBy = user.Email,
        //                                ModifiedBy = user.Email,
        //                                //IsKnown = isknown,
        //                                Firm = findFirm
        //                            };
        //                            _attorneyRepository.Create(findAttorney);
        //                        }

        //                        if (tmark.status.designSearchList != null && tmark.status.designSearchList.Count() > 0)
        //                        {
        //                            var designsearch = new List<string>();
        //                            foreach (var designsearchList in tmark.status.designSearchList)
        //                            {
        //                                designsearch.Add(designsearchList.code);
        //                            }

        //                            var dsList = await _designSearchClassRepository.ListDesignSearchClassAsync(DesignSearchClassSpecification.GetByCode(designsearch));
        //                            foreach (var item in tmark.status.designSearchList)
        //                            {
        //                                var ds = (DesignSearchClass)default;
        //                                ds = dsList.Where(x => x.Code == item.code).FirstOrDefault();
        //                                if (ds == null)
        //                                {
        //                                    ds = new DesignSearchClass();
        //                                    var dsdList = new List<DesignSearchDescriptionClass>();
        //                                    foreach (var dsdItem in item.descriptions)
        //                                    {
        //                                        dsdList.Add(new DesignSearchDescriptionClass()
        //                                        {
        //                                            DesignSearchClassId = ds.Id,
        //                                            Description = dsdItem,
        //                                            CreatedBy = user.Email,
        //                                            IsActive = true
        //                                        });
        //                                    }
        //                                    ds.Code = item.code;
        //                                    ds.CreatedBy = user.Email;
        //                                    ds.ModifiedBy = user.Email;
        //                                    ds.DesignSearchDescriptionClasses = dsdList;
        //                                    //_designSearchClassRepository.Create(ds);
        //                                }
        //                                DesignSearchClassCollection.Add(ds);
        //                            }

        //                        }

        //                        var goodandservice = new List<string>();
        //                        foreach (var gslist in tmark.gsList)
        //                        {
        //                            foreach (var item in gslist.usClasses)
        //                            {
        //                                goodandservice.Add(item.code);
        //                            }
        //                            foreach (var item in gslist.internationalClasses)
        //                            {
        //                                goodandservice.Add(item.code);
        //                            }
        //                        }
        //                        var gosList = await _goodAndServiceClassRepository.ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification.GetByNames(goodandservice));
        //                        foreach (var gslist in tmark.gsList)
        //                        {
        //                            foreach (var item in gslist.usClasses)
        //                            {
        //                                var gs = (GoodAndServiceClass)default;
        //                                gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
        //                                if (gs == null)
        //                                {
        //                                    gs = new GoodAndServiceClass
        //                                    {
        //                                        Code = item.code,
        //                                        Description = item.description,
        //                                        CreatedBy = user.Email,
        //                                        ModifiedBy = user.Email,
        //                                        Type = GoodAndServiceClassType.UsClass
        //                                    };
        //                                    _goodAndServiceClassRepository.Create(gs);
        //                                }
        //                                GoodAndServiceClassCollection.Add(gs);
        //                            }

        //                            foreach (var item in gslist.internationalClasses)
        //                            {
        //                                var gs = (GoodAndServiceClass)default;
        //                                gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
        //                                if (gs == null)
        //                                {
        //                                    gs = new GoodAndServiceClass
        //                                    {
        //                                        Code = item.code,
        //                                        Description = item.description,
        //                                        CreatedBy = user.Email,
        //                                        ModifiedBy = user.Email,
        //                                        Type = GoodAndServiceClassType.InternationalClass
        //                                    };
        //                                    //_goodAndServiceClassRepository.Create(gs);
        //                                }
        //                                GoodAndServiceClassCollection.Add(gs);
        //                            }

        //                            GoodAndServiceCollection.Add(new Trademarks.GoodAndService()
        //                            {
        //                                StatusCode = gslist.statusCode,
        //                                StatusDescription = gslist.statusDescription,
        //                                StatusDate = gslist.statusDate,
        //                                FirstUseDate = gslist.firstUseDate,
        //                                FirstUseInCommerceDate = gslist.firstUseInCommerceDate,
        //                                PrimeClassCode = gslist.primeClassCode,
        //                                Description = gslist.description,
        //                                CreatedBy = user.Email,
        //                                GoodAndServiceClass = GoodAndServiceClassCollection
        //                            });
        //                        }
        //                        #region OwnerGroup
        //                        //if (tmark.parties.ownerGroups._20 != null)
        //                        //{
        //                        //    foreach (var item in tmark.parties.ownerGroups._20)
        //                        //    {
        //                        //        OwnerGroupCollection.Add(new OwnerGroup()
        //                        //        {
        //                        //            Name = item.name,
        //                        //            Address = item.address1,
        //                        //            City = item.city,
        //                        //            Email = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() >= 0 ? item.emailList.addresses[0] : string.Empty,
        //                        //            Email2 = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() > 1 ? item.emailList.addresses[1] : string.Empty,
        //                        //            State = item.addressStateCountry.stateCountry != null ? item.addressStateCountry.stateCountry.code : string.Empty,
        //                        //            IsoCode = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.code : string.Empty,
        //                        //            IsoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
        //                        //            WipoCode = item.addressStateCountry.wipo != null ? item.addressStateCountry.wipo.code : string.Empty,
        //                        //            WipoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
        //                        //            Type = 20,
        //                        //            ZipCode = item.zip,
        //                        //            Country = item.citizenship != null && item.citizenship.stateCountry != null && item.citizenship.stateCountry.name != null ? item.citizenship.stateCountry.name : string.Empty,
        //                        //            TypeCode = item.entityType.code,
        //                        //            TypeDescription = item.entityType.description,
        //                        //            CreatedBy = user.Email,
        //                        //            ModifiedBy = user.Email,
        //                        //            IsActive = true,
        //                        //        });
        //                        //    }
        //                        //}

        //                        //if (tmark.parties.ownerGroups._10 != null)
        //                        //{
        //                        //    foreach (var item in tmark.parties.ownerGroups._10)
        //                        //    {
        //                        //        OwnerGroupCollection.Add(new OwnerGroup()
        //                        //        {
        //                        //            Name = item.name,
        //                        //            Address = item.address1,
        //                        //            City = item.city,
        //                        //            Email = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() >= 0 ? item.emailList.addresses[0] : string.Empty,
        //                        //            Email2 = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() > 1 ? item.emailList.addresses[1] : string.Empty,
        //                        //            State = item.addressStateCountry.stateCountry != null ? item.addressStateCountry.stateCountry.code : string.Empty,
        //                        //            IsoCode = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.code : string.Empty,
        //                        //            IsoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
        //                        //            WipoCode = item.addressStateCountry.wipo != null ? item.addressStateCountry.wipo.code : string.Empty,
        //                        //            WipoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
        //                        //            Type = 10,
        //                        //            ZipCode = item.zip,
        //                        //            Country = item.citizenship != null && item.citizenship.stateCountry != null && item.citizenship.stateCountry.name != null ? item.citizenship.stateCountry.name : string.Empty,
        //                        //            TypeCode = item.entityType.code,
        //                        //            TypeDescription = item.entityType.description,
        //                        //            CreatedBy = "TM.admin",
        //                        //            ModifiedBy = "TM.admin",
        //                        //            IsActive = true,
        //                        //        });
        //                        //    }
        //                        //}

        //                        //if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null)
        //                        //{
        //                        //    response.ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].name != null ? tmark.parties.ownerGroups._20[0].name : string.Empty;
        //                        //    //response.SerialNumber = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null ? tmark.parties.ownerGroups._20[0].serialNumber : 0;
        //                        //    response.PrimaryEmail = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].emailList != null && tmark.parties.ownerGroups._20[0].emailList.addresses != null && tmark.parties.ownerGroups._20[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._20[0].emailList.addresses[0] : string.Empty;
        //                        //    response.StreetAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].address1 != null ? tmark.parties.ownerGroups._20[0].address1 : string.Empty;
        //                        //    response.City = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].city != null ? tmark.parties.ownerGroups._20[0].city : string.Empty;
        //                        //    response.State = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].addressStateCountry != null && tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code : string.Empty;
        //                        //    response.ZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].zip != null ? tmark.parties.ownerGroups._20[0].zip : string.Empty;
        //                        //    response.Country = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].addressStateCountry != null && tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.name != null ? tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.name : string.Empty;
        //                        //}
        //                        //else
        //                        //{
        //                        //    response.ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].name != null ? tmark.parties.ownerGroups._10[0].name : string.Empty;
        //                        //    //response.SerialNumber = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null ? tmark.parties.ownerGroups._10[0].serialNumber : 0;
        //                        //    response.PrimaryEmail = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].emailList != null && tmark.parties.ownerGroups._10[0].emailList.addresses != null && tmark.parties.ownerGroups._10[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._10[0].emailList.addresses[0] : string.Empty;
        //                        //    response.StreetAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].address1 != null ? tmark.parties.ownerGroups._10[0].address1 : string.Empty;
        //                        //    response.City = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].city != null ? tmark.parties.ownerGroups._10[0].city : string.Empty;
        //                        //    response.State = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].addressStateCountry != null && tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code : string.Empty;
        //                        //    response.ZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].zip != null ? tmark.parties.ownerGroups._10[0].zip : string.Empty;
        //                        //    response.Country = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].addressStateCountry != null && tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.name != null ? tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.name : string.Empty;
        //                        //}
        //                        #endregion


        //                        if (tmark.prosecutionHistory != null && tmark.prosecutionHistory.Count() > 0)
        //                        {
        //                            foreach (var item in tmark.prosecutionHistory)
        //                            {
        //                                ProsecutionHistoryCollection.Add(new ProsecutionHistory()
        //                                {
        //                                    ProceedingNumber = item.proceedingNum,
        //                                    Description = item.entryDesc,
        //                                    EntryCode = item.entryCode,
        //                                    EntryDate = item.entryDate,
        //                                    EntryNumber = item.entryNumber,
        //                                    EntryType = item.entryType,
        //                                    CreatedBy = user.Email,
        //                                    IsActive = true
        //                                });
        //                            }
        //                        }

        //                        if (tmark.publication.officialGazettes != null)
        //                        {
        //                            foreach (var item in tmark.publication.officialGazettes)
        //                            {
        //                                PublicationOfficialGazetteCollection.Add(new PublicationOfficialGazette()
        //                                {
        //                                    CategoryCode = item.categoryCode,
        //                                    CategoryCodeText = item.categoryCodeText,
        //                                    CategoryReason = item.categoryReason,
        //                                    IssueDate = item.issueDate != null ? DateTime.Parse(item.issueDate) : null,
        //                                    RegistrationNumber = item.registrationNumber,
        //                                    StatusCode = item.statusCode,
        //                                    StatusCodeText = item.statusCodeText,
        //                                    ActionDate = item.actionDate != null ? DateTime.Parse(item.actionDate) : null,
        //                                    CreatedBy = user.Email,
        //                                    IsActive = true
        //                                });
        //                            }
        //                        }

        //                        if (tmark.status.physicalLocationHistory != null && tmark.status.physicalLocationHistory.Count() > 0)
        //                        {
        //                            foreach (var item in tmark.status.physicalLocationHistory)
        //                            {
        //                                PhysicalLocationHistoryCollection.Add(new PhysicalLocationHistory()
        //                                {
        //                                    Description = item.physicalLocationDescription,
        //                                    EventDate = item.eventDate != null ? Convert.ToDateTime(item.eventDate) : null,
        //                                    PhysicalLocation = item.physicalLocation,
        //                                    RegistrationNumber = item.rsn,
        //                                    CreatedBy = user.Email,
        //                                    IsActive = true
        //                                });
        //                            }
        //                        }

        //                        var trademark = new Trademarks.Trademark
        //                        {
        //                            User = user,
        //                            TrademarkElement = tmark.status.markElement,
        //                            DescriptionOfMark = tmark.status.descOfMark,
        //                            TrademarkDrawingDescription = tmark.status.markDrawDesc,
        //                            ExaminerName = tmark.status.staff.examiner != null && tmark.status.staff.examiner.name != null ? tmark.status.staff.examiner.name : string.Empty,
        //                            ExaminerNumber = tmark.status.staff.examiner != null && tmark.status.staff.examiner.number != null ? Convert.ToString(tmark.status.staff.examiner.number) : string.Empty,
        //                            Name = tmark.status.correspondence != null && tmark.status.correspondence.individualFullName != null ? tmark.status.correspondence.individualFullName : string.Empty,
        //                            InternationalStatusDescription = tmark.status.intStatusDesc != null ? tmark.status.intStatusDesc.ToString() : string.Empty,
        //                            Status = tmark.status.status,
        //                            SerialNumber = tmark.status.serialNumber,
        //                            FiledAsTeasPlusApp = tmark.status.filedAsTeasPlusApp,
        //                            CurrentlyAsTeasPlusApp = tmark.status.currentlyAsTeasPlusApp,
        //                            FiledAsTeasRfApp = tmark.status.filedAsTeasRfApp,
        //                            CurrentlyAsTeasRfApp = tmark.status.currentlyAsTeasRfApp,
        //                            SupplementalRegister = tmark.status.supplementalRegister,
        //                            AmendPrincipal = tmark.status.amendPrincipal,
        //                            AmendSupplemental = tmark.status.amendSupplemental,
        //                            CertificationMark = tmark.status.certificationMark,
        //                            ServiceMark = tmark.status.serviceMark,
        //                            CollectiveMembershipMark = tmark.status.collectiveMembershipMark,
        //                            CollectiveServiceMark = tmark.status.collectiveServiceMark,
        //                            CollectiveTradeMark = tmark.status.collectiveTradeMark,
        //                            StatusDate = tmark.status.statusDate != null ? DateTime.Parse(tmark.status.statusDate) : null,
        //                            IsStandard = tmark.status.standardChar,
        //                            IsColorDrawing = tmark.status.colorDrawingCurr,
        //                            IsSection2f = tmark.status.section2f,
        //                            IsSection2fPartial = tmark.status.section2fPartial,
        //                            IsOthers = tmark.status.others,
        //                            IsPreviousRegistered = tmark.status.publishedPrevRegMark,
        //                            CommunityLegalServiceTotal = tmark.status.clsTotal,
        //                            IsFiledUse = tmark.status.filedUse,
        //                            IsFiledInternationLaw = tmark.status.filedItu,
        //                            IsFiled44d = tmark.status.filed44d,
        //                            IsFiled44e = tmark.status.filed44e,
        //                            IsFiled66a = tmark.status.filed66a,
        //                            IsFiledNoBasis = tmark.status.filedNoBasis,
        //                            IsUseCurrent = tmark.status.useCurr,
        //                            IsInternationalLawCurrent = tmark.status.ituCurr,
        //                            IsSect44eCurrent = tmark.status.sect44eCurr,
        //                            IsSect66aCurrent = tmark.status.sect66aCurr,
        //                            IsNoBasisCurrent = tmark.status.noBasisCurr,
        //                            IsUseAmended = tmark.status.useAmended,
        //                            IsInternationalLawAmended = tmark.status.ituAmended,
        //                            IsSect44dAmended = tmark.status.sect44dAmended,
        //                            IsSect44eAmended = tmark.status.sect44eAmended,
        //                            IsSect8Filed = tmark.status.sect8Filed,
        //                            IsSect8Acpt = tmark.status.sect8Acpt,
        //                            IsSect8PartialAcpt = tmark.status.sect8PartialAcpt,
        //                            IsSect15Filed = tmark.status.sect15Filed,
        //                            IsSect15Ack = tmark.status.sect15Ack,
        //                            IsSect71Filed = tmark.status.sect71Filed,
        //                            IsSect71Acpt = tmark.status.sect71Acpt,
        //                            IsSect71PartialAcpt = tmark.status.sect71PartialAcpt,
        //                            IsRenewalFiled = tmark.status.renewalFiled,
        //                            IsChangeInRegistration = tmark.status.changeInReg,
        //                            Sect44dCurrent = tmark.status.sect44dCurr,
        //                            MarkDrawingCode = tmark.status.markDrawingCd,
        //                            LawOffAssignedCode = tmark.status.lawOffAsgnCd,
        //                            CurrentLocationCode = tmark.status.currLocationCd,
        //                            CurrentLocationDate = tmark.status.currLocationDt != null ? DateTime.Parse(tmark.status.currLocationDt) : null,
        //                            ExternalStatusDescription = tmark.status.extStatusDesc,
        //                            CurrentLocation = tmark.status.currentLoc,
        //                            NewLawOffAssignedCode = tmark.status.newLawOffAsgnCd,
        //                            LawOffAssigned = tmark.status.lawOffAssigned,
        //                            Tm5Status = tmark.status.tm5Status,
        //                            Tm5StatusDescription = tmark.status.tm5StatusDesc,
        //                            StatusDefination = tmark.status.tm5StatusDef,
        //                            UsRegistrationNumber = tmark.status.usRegistrationNumber,
        //                            UsRegistrationDate = tmark.status.usRegistrationDate != null ? DateTime.Parse(tmark.status.usRegistrationDate) : null,
        //                            FiliingDate = tmark.status.filingDate != null ? DateTime.Parse(tmark.status.filingDate) : null,
        //                            PublicationDate = tmark.publication.datePublished != null ? DateTime.Parse(tmark.publication.datePublished) : null,
        //                            PublicationAllowanceDate = tmark.publication.noticeOfAllowanceDate != null ? DateTime.Parse(tmark.publication.noticeOfAllowanceDate) : null,
        //                            CreatedBy = user.Email,
        //                            ModifiedBy = user.Email,
        //                            Attorney = findAttorney,
        //                            GoodAndServices = GoodAndServiceCollection,
        //                            OwnerGroups = OwnerGroupCollection,
        //                            ProsecutionHistorys = ProsecutionHistoryCollection,
        //                            PublicationOfficialGazettes = PublicationOfficialGazetteCollection,
        //                            DesignSearches = DesignSearchClassCollection,
        //                            PhysicalLocationHistorys = PhysicalLocationHistoryCollection,
        //                            SyncDate = DateTimeHelper.Now()
        //                        };

        //                        var document = (Document)default;
        //                        using (TextReader sr = new StringReader(xmlString))
        //                        {
        //                            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(DocumentList));
        //                            DocumentList xmlResponse = (DocumentList)serializer.Deserialize(sr);

        //                            foreach (var item in xmlResponse.Document)
        //                            {
        //                                document = new Document();
        //                                var docType = await _trademarkRepository.FindDocumentTypeByCode(item.DocumentTypeCode);
        //                                docType = docType == null ? trademark.Documents.Select(x => x.DocumentType).Where(d => d.Code == item.DocumentTypeCode).FirstOrDefault() : docType;
        //                                var docCategory = await _trademarkRepository.FindDocumentCategoryByCode(item.CategoryTypeCode);
        //                                docCategory = docCategory == null ? trademark.Documents.SelectMany(x => x.DocumentCategorys).Where(d => d.Code == item.CategoryTypeCode).FirstOrDefault() : docCategory;
        //                                if (docType == null)
        //                                {
        //                                    docType = new DocumentType();
        //                                    docType.Code = item.DocumentTypeCode;
        //                                    docType.DocumentTypeCode = item.DocumentTypeCode;
        //                                    docType.CategoryTypeCode = item.CategoryTypeCode;
        //                                    docType.CodeDescription = item.CategoryTypeCodeDescriptionText;
        //                                    docType.Description = item.DocumentTypeCodeDescriptionText;
        //                                    docType.CreatedBy = user.Email;
        //                                    _trademarkRepository.Create(docType);
        //                                }
        //                                document.DocumentType = docType;

        //                                if (docCategory == null)
        //                                {
        //                                    docCategory = new DocumentCategory()
        //                                    {
        //                                        Code = item.CategoryTypeCode,
        //                                        Description = item.CategoryTypeCodeDescriptionText,
        //                                        CreatedBy = user.Email
        //                                    };
        //                                    document.DocumentCategorys.Add(docCategory);
        //                                }

        //                                document.SerialNumber = (long)item.SerialNumber;
        //                                document.MailRoomDate = item.MailRoomDate;
        //                                document.ScanDateTime = item.ScanDateTime;
        //                                document.TotalPageQuantity = item.TotalPageQuantity;
        //                                document.SourceSystem = item.SourceSystem;
        //                                document.CreatedBy = user.Email;
        //                                document.DocumentTypeId = docType.Id;
        //                                document.CategoryId = docCategory.Id;

        //                                for (int i = 0; i < item.PageMediaTypeList.Length; i++)
        //                                {
        //                                    if (item.CategoryTypeCodeDescriptionText == "Drawing")
        //                                    {
        //                                        trademark.Image = item.UrlPathList[i];
        //                                    }
        //                                    var docUrl = new DocumentUrl()
        //                                    {
        //                                        MediaType = item.PageMediaTypeList[i],
        //                                        Url = item.UrlPathList[i],
        //                                        CreatedBy = user.Email,
        //                                        DocumentId = document.Id
        //                                    };
        //                                    document.DocumentUrls.Add(docUrl);
        //                                }
        //                                if (string.IsNullOrEmpty(trademark.Image))
        //                                    trademark.Image = "https://tsdr.uspto.gov/img/" + trademark.SerialNumber + "/large";
        //                                trademark.Documents.Add(document);
        //                            }
        //                        }

        //                        var batchData = default(TrademarkImportItem);
        //                        batchData = await _trademarkRepository.GetTrademarkImportItem(trademark.SerialNumber);

        //                        batchData.TrademarkId = trademark.Id;
        //                        batchData.ModifiedBy = user.Email;
        //                        batchData.Status = response.Status;
        //                        _trademarkRepository.Update(batchData);

        //                        var statusChecker = new TrademarkStatusChecker(trademark);
        //                        trademark.CurrentStatus = statusChecker.CurrentStatus;
        //                        trademark.StatusColor = statusChecker.RegisteredStatusColor.ToString();
        //                        trademark.NextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value : null;
        //                        trademark.MaxNextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value.AddYears(1) : null;

        //                        _trademarkRepository.Create(trademark);
        //                    }
        //                }

        //            }
        //            if (results.Errors.Count == 0)
        //            {
        //                await _UnitOfWork.CommitAsync();
        //                results.IsSuccess = true;
        //            }
        //        }
        //        else
        //        {
        //            response.Status = TrademarkStatus.TrademarkNotFound;
        //            results.Errors.Add("Error while saving trademark");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        response.Status = TrademarkStatus.SystemError;
        //        results.Errors.Add(e.Message);
        //    }
        //    results.TrademarkResponse.Add(response);
        //    return results;
        //}
        //public async Task<TrademarkResponseModel> GetBatchTrademark(string fallbackEmail, string id)
        //{
        //    var result = new TrademarkResponseModel();
        //    var response = new TrademarkResponse();
        //    var jsonString = string.Empty;
        //    var xmlString = string.Empty;
        //    try
        //    {
        //        var trademarkExists = await _trademarkRepository.FindTrademarkWithOutIncludeAsync(TrademarkSpecification.GetBySerialNumber(Convert.ToInt64(id)));
        //        if (trademarkExists == null)
        //        {
        //            var ids = new string[] { id };
        //            string type = "sn";
        //            if (int.Parse(ids[0]) < 10000000)
        //                type = "rn";
        //            jsonString = await TsdrApiService.GetTrademarkDetail(type, ids, false);
        //            if (!string.IsNullOrEmpty(jsonString))
        //            {
        //                xmlString = await TsdrApiService.GetTrademarkDocumentDetails(type, false, ids);
        //                TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);

        //                string email = string.Empty;
        //                string ClientName = string.Empty;
        //                string LegalEntityType = string.Empty;
        //                string ClientAddress = string.Empty;
        //                string ClientCity = string.Empty;
        //                string ClientCountry = string.Empty;
        //                string ClientZipCode = string.Empty;
        //                string ClientState = string.Empty;

        //                foreach (var transactionsEach in trademarkDetail.transactionList)
        //                {
        //                    foreach (var tmark in transactionsEach.trademarks)
        //                    {
        //                        if (tmark.status.tm5StatusDesc.Substring(0, 4) == "DEAD")
        //                            response.Status = TrademarkStatus.TrademarkDead;
        //                        else
        //                            response.Status = TrademarkStatus.TrademarkSuccess;

        //                        if (string.IsNullOrEmpty(tmark.status.correspondence.attorneyName))
        //                            response.Status = TrademarkStatus.TrademarkAttorneyIssue;

        //                        response.Name = tmark.status.markElement;
        //                        response.SerialNumber = int.Parse(transactionsEach.searchId);
        //                        response.StatusNumber = tmark.status.tm5Status;
        //                        response.StatusDescription = tmark.status.tm5StatusDesc;


        //                        if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null)
        //                        {
        //                            ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].name != null ? tmark.parties.ownerGroups._20[0].name : string.Empty;
        //                            LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].entityType != null && tmark.parties.ownerGroups._20[0].entityType.description != null ? tmark.parties.ownerGroups._20[0].entityType.description : string.Empty;
        //                            ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].address1 != null ? tmark.parties.ownerGroups._20[0].address1 : string.Empty;
        //                            ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].citizenship != null && tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._20[0].citizenship.stateCountry.name : string.Empty;
        //                            email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].emailList != null && tmark.parties.ownerGroups._20[0].emailList.addresses != null && tmark.parties.ownerGroups._20[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._20[0].emailList.addresses[0] : string.Empty;
        //                            ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].zip != null ? tmark.parties.ownerGroups._20[0].zip : string.Empty;
        //                            ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].addressStateCountry != null && tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._20[0].addressStateCountry.stateCountry.code : string.Empty;
        //                            ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._20 != null && tmark.parties.ownerGroups._20[0].city != null ? tmark.parties.ownerGroups._20[0].city : string.Empty;

        //                            response.ClientName = ClientName;
        //                            response.PrimaryEmail = email;
        //                            response.StreetAddress = ClientAddress;
        //                            response.City = ClientCity;
        //                            response.State = ClientState;
        //                            response.ZipCode = ClientZipCode;
        //                            response.Country = ClientCountry;
        //                        }
        //                        else if (tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null)
        //                        {
        //                            ClientName = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].name != null ? tmark.parties.ownerGroups._10[0].name : string.Empty;
        //                            LegalEntityType = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].entityType != null && tmark.parties.ownerGroups._10[0].entityType.description != null ? tmark.parties.ownerGroups._10[0].entityType.description : string.Empty;
        //                            ClientAddress = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].address1 != null ? tmark.parties.ownerGroups._10[0].address1 : string.Empty;
        //                            ClientCountry = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].citizenship != null && tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name != null ? tmark.parties.ownerGroups._10[0].citizenship.stateCountry.name : string.Empty;
        //                            email = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].emailList != null && tmark.parties.ownerGroups._10[0].emailList.addresses != null && tmark.parties.ownerGroups._10[0].emailList.addresses.Count() >= 0 ? tmark.parties.ownerGroups._10[0].emailList.addresses[0] : string.Empty;
        //                            ClientZipCode = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].zip != null ? tmark.parties.ownerGroups._10[0].zip : string.Empty;
        //                            ClientState = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].addressStateCountry != null && tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code != null ? tmark.parties.ownerGroups._10[0].addressStateCountry.stateCountry.code : string.Empty;
        //                            ClientCity = tmark.parties.ownerGroups != null && tmark.parties.ownerGroups._10 != null && tmark.parties.ownerGroups._10[0].city != null ? tmark.parties.ownerGroups._10[0].city : string.Empty;

        //                            response.ClientName = ClientName;
        //                            response.PrimaryEmail = email;
        //                            response.StreetAddress = ClientAddress;
        //                            response.City = ClientCity;
        //                            response.State = ClientState;
        //                            response.ZipCode = ClientZipCode;
        //                            response.Country = ClientCountry;
        //                        }
        //                        else
        //                        {
        //                            response.Status = TrademarkStatus.TrademarkAttorneyIssue;
        //                        }

        //                        if (string.IsNullOrEmpty(email))
        //                        {
        //                            response.Email = fallbackEmail;
        //                            email = fallbackEmail;
        //                        }
        //                        response.Country = tmark.status.correspondence != null && tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryName != null ? tmark.status.correspondence.address.countryName : string.Empty;
        //                        if (tmark.parties.ownerGroups != null && (tmark.parties.ownerGroups._10 != null || tmark.parties.ownerGroups._20 != null) && string.IsNullOrEmpty(response.PrimaryEmail))
        //                            response.Status = TrademarkStatus.TrademarkEmailNotFound;
        //                    }
        //                }

        //                var userInfo = new UserInfo();
        //                userInfo.Email = email;
        //                userInfo.ClientName = ClientName;
        //                userInfo.LegalEntityType = LegalEntityType;
        //                userInfo.ClientAddress = ClientAddress;
        //                userInfo.ClientCity = ClientCity;
        //                userInfo.Country = ClientCountry;
        //                userInfo.ClientState = ClientState;
        //                userInfo.ClientZipCode = ClientZipCode;

        //                //userInfo.SerialNumber = Convert.ToInt64(id);
        //                //_httpContextAccessor.HttpContext.Session.SetString("GetUserInfo", JsonConvert.SerializeObject(userInfo));
        //                //findLog.UserInfo = JsonConvert.SerializeObject(userInfo);
        //                //try
        //                //{
        //                //    await context.UpdateAsync<TradeMarkDataLog>(findLog);
        //                //}
        //                //catch (Microsoft.WindowsAzure.Storage.StorageException ex)
        //                //{
        //                //    Trace.TraceError(ex.Message);
        //                //}
        //            }
        //            else
        //            {
        //                response.SerialNumber = int.Parse(id);
        //                response.Status = TrademarkStatus.TrademarkNotFound;
        //            }
        //        }
        //        else
        //        {
        //            response.SerialNumber = int.Parse(trademarkExists.SerialNumber.ToString());
        //            response.Name = trademarkExists.Attorney.Name;
        //            response.Status = TrademarkStatus.TrademarkInSystem;
        //            result.Errors.Add("Record already exists");
        //        }

        //        if (result.Errors.Any())
        //            result.IsSuccess = false;
        //        else
        //            result.IsSuccess = true;

        //        result.TrademarkResponse.Add(response);
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        result.Errors.Add(ex.Message);
        //        result.IsSuccess = false;
        //        response.Status = TrademarkStatus.SystemError;
        //    }
        //    return result;
        //}
        public async Task<TrademarkResponseModel> SaveBatchTrademark(string serialNo, string fallbackEmail)
        {
            var results = new TrademarkResponseModel();
            var response = new TrademarkResponse();
            var jsonString = string.Empty;
            var xmlString = string.Empty;
            var now = DateTimeHelper.Now();
            try
            {
                var ids = new string[] { serialNo };
                string type = "sn";
                if (int.Parse(ids[0]) < 10000000)
                    type = "rn";
                List<Trademarks.GoodAndService> GoodAndServiceCollection = new List<Trademarks.GoodAndService>();
                List<Trademarks.OwnerGroup> OwnerGroupCollection = new List<Trademarks.OwnerGroup>();
                List<Trademarks.DesignSearchClass> DesignSearchClassCollection = new List<Trademarks.DesignSearchClass>();
                List<Trademarks.ProsecutionHistory> ProsecutionHistoryCollection = new List<Trademarks.ProsecutionHistory>();
                List<Trademarks.PhysicalLocationHistory> PhysicalLocationHistoryCollection = new List<Trademarks.PhysicalLocationHistory>();
                List<Trademarks.PublicationOfficialGazette> PublicationOfficialGazetteCollection = new List<Trademarks.PublicationOfficialGazette>();
                var currentUser = _httpContextAccessor.HttpContext.User.Identity.Name;
                {
                    jsonString = await TsdrApiService.GetTrademarkDetail(type, ids, true);
                    xmlString = await TsdrApiService.GetTrademarkDocumentDetails(type, true, ids);
                    string code = Helper.RandomString();
                    var user = (User)default;
                    bool isknown = false;
                    string email = string.Empty;
                    string ClientName = string.Empty;
                    string legalEntityType = string.Empty;
                    string clientAddress = string.Empty;
                    string clientCity = string.Empty;
                    string clientCountry = string.Empty;
                    string clientZipCode = string.Empty;
                    string clientState = string.Empty;
                    string trademarkName = string.Empty;
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        TsdrGetTrademarkDetailsResponse trademarkDetail = JsonConvert.DeserializeObject<TsdrGetTrademarkDetailsResponse>(jsonString);
                        var transactionList = trademarkDetail.transactionList;

                        var trademarkFilter = new List<long>();
                        foreach (var tmark in transactionList)
                        {
                            trademarkFilter.Add(tmark.trademarks.Select(x => (long)x.status.serialNumber).FirstOrDefault());
                        }

                        var findTrademarks = await _trademarkRepository.ListTrademarkAsync(TrademarkSpecification.GetBySerialNumbers(trademarkFilter));

                        foreach (var transactionsEach in transactionList)
                        {
                            foreach (var tmark in transactionsEach.trademarks)
                            {
                                string attorneyName = tmark.status.correspondence.attorneyName;
                                string attorneyEmail = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() >= 0 ? tmark.status.correspondence.attorneyEmail.addresses[0] : string.Empty;
                                string authIndicator = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.authIndicator != null ? tmark.status.correspondence.attorneyEmail.authIndicator : string.Empty;
                                string attorneyLineOne = tmark.status.correspondence.address != null && tmark.status.correspondence.address.line1 != null ? tmark.status.correspondence.address.line1 : string.Empty;
                                string attorneyPostalCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.postalCode != null ? tmark.status.correspondence.address.postalCode : string.Empty;
                                string attorneyCity = tmark.status.correspondence.address != null && tmark.status.correspondence.address.city != null ? tmark.status.correspondence.address.city : string.Empty;
                                if (!string.IsNullOrWhiteSpace(attorneyEmail) || !string.IsNullOrWhiteSpace(attorneyName))
                                {
                                    var findRecognisedattorney = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilter(attorneyEmail.Trim(), attorneyName.Trim()));
                                    isknown = findRecognisedattorney == null ? false : true;
                                }

                                response.Name = tmark.status.markElement;
                                response.SerialNumber = int.Parse(transactionsEach.searchId);
                                response.StatusNumber = tmark.status.tm5Status;
                                response.StatusDescription = tmark.status.tm5StatusDesc;
                                response.Email = string.Empty;

                                if (findTrademarks.Any(x => x.SerialNumber == tmark.status.serialNumber))
                                {
                                    response.SerialNumber = tmark.status.serialNumber;
                                    response.Status = TrademarkStatus.TrademarkInSystem;
                                    results.Errors.Add("Trademark already in system");
                                }
                                else
                                {
                                    var findFirm = (Firm)default;
                                    var findAttorney = (Attorneys.Attorney)default;

                                    findFirm = await _firmRepository.FindFirmAsync(FirmSpecification.GetByName(tmark.status.correspondence.firmName));
                                    if (findFirm == null)
                                    {
                                        findFirm = new Firm
                                        {
                                            Name = tmark.status.correspondence.firmName ?? string.Empty,
                                            Email = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.addresses != null && tmark.status.correspondence.correspondantEmail.addresses.Count() > 0 ? tmark.status.correspondence.correspondantEmail.addresses[0] : string.Empty,
                                            IsEmailAuthticated = tmark.status.correspondence.correspondantEmail != null && tmark.status.correspondence.correspondantEmail.authIndicator != null ? tmark.status.correspondence.correspondantEmail.authIndicator : string.Empty,
                                            Phone = tmark.status.correspondence.correspondantPhone ?? string.Empty,
                                            Fax = tmark.status.correspondence.correspondantFax ?? string.Empty,
                                            CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                            ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name
                                        };
                                        _firmRepository.Create(findFirm);
                                    }

                                    findAttorney = await _attorneyRepository.FindAttorney(AttorneySpecification.GetByFilters(attorneyName, attorneyEmail));
                                    if (findAttorney == null)
                                    {
                                        findAttorney = new Attorneys.Attorney()
                                        {
                                            //UserId = user != null ? user.Id : string.Empty,
                                            Name = attorneyName,
                                            FullName = attorneyName,
                                            Email = attorneyEmail,
                                            Email2 = tmark.status.correspondence.attorneyEmail != null && tmark.status.correspondence.attorneyEmail.addresses != null && tmark.status.correspondence.attorneyEmail.addresses.Count() > 1 ? tmark.status.correspondence.attorneyEmail.addresses[1] : string.Empty,
                                            IsEmailAuthticated = authIndicator == "Y" ? true : false,
                                            Line1 = attorneyLineOne,
                                            City = attorneyCity,
                                            PostalCode = attorneyPostalCode,
                                            CountryCode = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryCode != null ? tmark.status.correspondence.address.countryCode : string.Empty,
                                            CountryName = tmark.status.correspondence.address != null && tmark.status.correspondence.address.countryName != null ? tmark.status.correspondence.address.countryName : string.Empty,
                                            CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                            ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                            //IsKnown = isknown,
                                            Firm = findFirm
                                        };
                                        _attorneyRepository.Create(findAttorney);
                                        email = findAttorney.Email;
                                    }
                                    else
                                    {
                                        email = findAttorney.Email;
                                    }

                                    if (tmark.status.designSearchList != null && tmark.status.designSearchList.Count() > 0)
                                    {
                                        var designsearch = new List<string>();
                                        foreach (var designsearchList in tmark.status.designSearchList)
                                        {
                                            designsearch.Add(designsearchList.code);
                                        }

                                        var dsList = await _designSearchClassRepository.ListDesignSearchClassAsync(DesignSearchClassSpecification.GetByCode(designsearch));
                                        foreach (var item in tmark.status.designSearchList)
                                        {
                                            var ds = (DesignSearchClass)default;
                                            ds = dsList.Where(x => x.Code == item.code).FirstOrDefault();
                                            if (ds == null)
                                            {
                                                ds = new DesignSearchClass();
                                                var dsdList = new List<DesignSearchDescriptionClass>();
                                                foreach (var dsdItem in item.descriptions)
                                                {
                                                    dsdList.Add(new DesignSearchDescriptionClass()
                                                    {
                                                        DesignSearchClassId = ds.Id,
                                                        Description = dsdItem,
                                                        CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                        IsActive = true
                                                    });
                                                }
                                                ds.Code = item.code;
                                                ds.CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
                                                ds.ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
                                                ds.DesignSearchDescriptionClasses = dsdList;
                                                //_designSearchClassRepository.Create(ds);
                                            }
                                            DesignSearchClassCollection.Add(ds);
                                        }

                                    }



                                    var goodandservice = new List<string>();
                                    foreach (var gslist in tmark.gsList)
                                    {
                                        foreach (var item in gslist.usClasses)
                                        {
                                            goodandservice.Add(item.code);
                                        }
                                        foreach (var item in gslist.internationalClasses)
                                        {
                                            goodandservice.Add(item.code);
                                        }
                                    }
                                    var gosList = await _goodAndServiceClassRepository.ListGoodAndServiceClassAsync(GoodAndServiceClassSpecification.GetByNames(goodandservice));
                                    foreach (var gslist in tmark.gsList)
                                    {
                                        var GoodAndServiceClassCollection = new List<Trademarks.GoodAndServiceClass>();

                                        foreach (var item in gslist.usClasses)
                                        {
                                            var gs = (GoodAndServiceClass)default;
                                            gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                            var isExists = GoodAndServiceCollection.SelectMany(c => c.GoodAndServiceClass).FirstOrDefault(x => x.Code == item.code);
                                            if (isExists == null)
                                            {
                                                if (gs == null)
                                                {
                                                    gs = new GoodAndServiceClass
                                                    {
                                                        Code = item.code,
                                                        Description = item.description,
                                                        CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                        ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                        Type = GoodAndServiceClassType.UsClass
                                                    };
                                                }
                                                GoodAndServiceClassCollection.Add(gs);
                                            }
                                            else
                                                GoodAndServiceClassCollection.Add(isExists);
                                        }

                                        foreach (var item in gslist.internationalClasses)
                                        {
                                            var gs = (GoodAndServiceClass)default;
                                            gs = gosList.Where(x => x.Code == item.code).FirstOrDefault();
                                            var isExists = GoodAndServiceCollection.SelectMany(c => c.GoodAndServiceClass).FirstOrDefault(x => x.Code == item.code);
                                            if (isExists == null)
                                            {
                                                if (gs == null)
                                                {
                                                    gs = new GoodAndServiceClass
                                                    {
                                                        Code = item.code,
                                                        Description = item.description,
                                                        CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                        ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                        Type = GoodAndServiceClassType.InternationalClass
                                                    };
                                                }
                                                GoodAndServiceClassCollection.Add(gs);
                                            }
                                            else
                                                GoodAndServiceClassCollection.Add(isExists);
                                        }

                                        GoodAndServiceCollection.Add(new Trademarks.GoodAndService()
                                        {
                                            StatusCode = gslist.statusCode,
                                            StatusDescription = gslist.statusDescription,
                                            StatusDate = gslist.statusDate,
                                            FirstUseDate = gslist.firstUseDate,
                                            FirstUseInCommerceDate = gslist.firstUseInCommerceDate,
                                            PrimeClassCode = gslist.primeClassCode,
                                            Description = gslist.description,
                                            CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                            GoodAndServiceClass = GoodAndServiceClassCollection
                                        });
                                    }
                                    #region OwnerGroups
                                    var ownerGroups = FillOwnerGroupData(tmark.parties.ownerGroups);
                                    OwnerGroupCollection.AddRange(ownerGroups);

                                    var findOwner = OwnerGroupCollection.OrderByDescending(x => x.Type).FirstOrDefault();
                                    if (findOwner != null)
                                    {
                                        response.ClientName = findOwner.Name;
                                        response.PrimaryEmail = findOwner.Email;
                                        response.StreetAddress = findOwner.Address;
                                        response.City = findOwner.City;
                                        response.State = findOwner.State;
                                        response.ZipCode = findOwner.ZipCode;
                                        response.Country = findOwner.Country;
                                    }
                                    #endregion

                                    if (string.IsNullOrWhiteSpace(response.PrimaryEmail))
                                    {
                                        response.PrimaryEmail = fallbackEmail;
                                    }
                                    #region Creating User If Known 
                                    if (isknown)
                                    {
                                        user = await _userManager.FindByNameAsync(response.PrimaryEmail);
                                        if (user == null)
                                        {
                                            user = new User();
                                            response.Status = TrademarkStatus.TrademarkAccountCreated;
                                            email = response.PrimaryEmail;
                                            user.UserName = email;
                                            user.Email = email;
                                            user.FullName = email.Contains("@") ? email.Substring(0, email.IndexOf('@')) : string.Empty;
                                            user.Active = true;
                                            user.AccessCode = code;
                                            user.UserType = UserType.AppUser;
                                            user.Created = DateTime.Now;
                                            user.Modified = DateTime.Now;
                                            user.ClientName = response.ClientName;
                                            user.LegalEntityType = legalEntityType;
                                            user.Street = clientAddress;
                                            user.State = clientState;
                                            user.City = clientCity;
                                            user.ZipCode = clientZipCode;
                                            user.Country = clientCountry;
                                            await _userManager.CreateAsync(user, "Kwworks@3");
                                        }
                                        else
                                            response.Status = TrademarkStatus.TrademarkLinked;
                                    }
                                    else
                                        response.Status = TrademarkStatus.TrademarkAttorneyIssue;
                                    #endregion
                                    if (tmark.prosecutionHistory != null && tmark.prosecutionHistory.Count() > 0)
                                    {
                                        foreach (var item in tmark.prosecutionHistory)
                                        {
                                            ProsecutionHistoryCollection.Add(new ProsecutionHistory()
                                            {
                                                ProceedingNumber = item.proceedingNum,
                                                Description = item.entryDesc,
                                                EntryCode = item.entryCode,
                                                EntryDate = item.entryDate,
                                                EntryNumber = item.entryNumber,
                                                EntryType = item.entryType,
                                                CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                IsActive = true
                                            });
                                        }
                                    }

                                    if (tmark.status.physicalLocationHistory != null && tmark.status.physicalLocationHistory.Count() > 0)
                                    {
                                        foreach (var item in tmark.status.physicalLocationHistory)
                                        {
                                            PhysicalLocationHistoryCollection.Add(new PhysicalLocationHistory()
                                            {
                                                Description = item.physicalLocationDescription,
                                                EventDate = item.eventDate != null ? Convert.ToDateTime(item.eventDate) : null,
                                                PhysicalLocation = item.physicalLocation,
                                                RegistrationNumber = item.rsn,
                                                CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                IsActive = true
                                            });
                                        }
                                    }

                                    if (tmark.publication.officialGazettes != null)
                                    {
                                        foreach (var item in tmark.publication.officialGazettes)
                                        {
                                            PublicationOfficialGazetteCollection.Add(new PublicationOfficialGazette()
                                            {
                                                CategoryCode = item.categoryCode,
                                                CategoryCodeText = item.categoryCodeText,
                                                CategoryReason = item.categoryReason,
                                                IssueDate = item.issueDate != null ? DateTime.Parse(item.issueDate) : null,
                                                RegistrationNumber = item.registrationNumber,
                                                StatusCode = item.statusCode,
                                                StatusCodeText = item.statusCodeText,
                                                ActionDate = item.actionDate != null ? DateTime.Parse(item.actionDate) : null,
                                                CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                IsActive = true
                                            });
                                        }
                                    }

                                    var trademark = new Trademarks.Trademark
                                    {
                                        User = user,
                                        TrademarkElement = tmark.status.markElement,
                                        DescriptionOfMark = tmark.status.descOfMark,
                                        TrademarkDrawingDescription = tmark.status.markDrawDesc,
                                        ExaminerName = tmark.status.staff.examiner != null && tmark.status.staff.examiner.name != null ? tmark.status.staff.examiner.name : string.Empty,
                                        ExaminerNumber = tmark.status.staff.examiner != null && tmark.status.staff.examiner.number != null ? Convert.ToString(tmark.status.staff.examiner.number) : string.Empty,
                                        Name = tmark.status.correspondence != null && tmark.status.correspondence.individualFullName != null ? tmark.status.correspondence.individualFullName : string.Empty,
                                        InternationalStatusDescription = tmark.status.intStatusDesc != null ? tmark.status.intStatusDesc.ToString() : string.Empty,
                                        Status = tmark.status.status,
                                        SerialNumber = tmark.status.serialNumber,
                                        FiledAsTeasPlusApp = tmark.status.filedAsTeasPlusApp,
                                        CurrentlyAsTeasPlusApp = tmark.status.currentlyAsTeasPlusApp,
                                        FiledAsTeasRfApp = tmark.status.filedAsTeasRfApp,
                                        CurrentlyAsTeasRfApp = tmark.status.currentlyAsTeasRfApp,
                                        SupplementalRegister = tmark.status.supplementalRegister,
                                        AmendPrincipal = tmark.status.amendPrincipal,
                                        AmendSupplemental = tmark.status.amendSupplemental,
                                        CertificationMark = tmark.status.certificationMark,
                                        ServiceMark = tmark.status.serviceMark,
                                        CollectiveMembershipMark = tmark.status.collectiveMembershipMark,
                                        CollectiveServiceMark = tmark.status.collectiveServiceMark,
                                        CollectiveTradeMark = tmark.status.collectiveTradeMark,
                                        StatusDate = tmark.status.statusDate != null ? DateTime.Parse(tmark.status.statusDate) : null,
                                        TrademarkStatus = tmark.status.trademark,
                                        IsStandard = tmark.status.standardChar,
                                        IsColorDrawing = tmark.status.colorDrawingCurr,
                                        IsSection2f = tmark.status.section2f,
                                        IsSection2fPartial = tmark.status.section2fPartial,
                                        IsOthers = tmark.status.others,
                                        IsPreviousRegistered = tmark.status.publishedPrevRegMark,
                                        CommunityLegalServiceTotal = tmark.status.clsTotal,
                                        IsFiledUse = tmark.status.filedUse,
                                        IsFiledInternationLaw = tmark.status.filedItu,
                                        IsFiled44d = tmark.status.filed44d,
                                        IsFiled44e = tmark.status.filed44e,
                                        IsFiled66a = tmark.status.filed66a,
                                        IsFiledNoBasis = tmark.status.filedNoBasis,
                                        IsUseCurrent = tmark.status.useCurr,
                                        IsInternationalLawCurrent = tmark.status.ituCurr,
                                        IsSect44eCurrent = tmark.status.sect44eCurr,
                                        IsSect66aCurrent = tmark.status.sect66aCurr,
                                        IsNoBasisCurrent = tmark.status.noBasisCurr,
                                        IsUseAmended = tmark.status.useAmended,
                                        IsInternationalLawAmended = tmark.status.ituAmended,
                                        IsSect44dAmended = tmark.status.sect44dAmended,
                                        IsSect44eAmended = tmark.status.sect44eAmended,
                                        IsSect8Filed = tmark.status.sect8Filed,
                                        IsSect8Acpt = tmark.status.sect8Acpt,
                                        IsSect8PartialAcpt = tmark.status.sect8PartialAcpt,
                                        IsSect15Filed = tmark.status.sect15Filed,
                                        IsSect15Ack = tmark.status.sect15Ack,
                                        IsSect71Filed = tmark.status.sect71Filed,
                                        IsSect71Acpt = tmark.status.sect71Acpt,
                                        IsSect71PartialAcpt = tmark.status.sect71PartialAcpt,
                                        IsRenewalFiled = tmark.status.renewalFiled,
                                        IsChangeInRegistration = tmark.status.changeInReg,
                                        Sect44dCurrent = tmark.status.sect44dCurr,
                                        MarkDrawingCode = tmark.status.markDrawingCd,
                                        LawOffAssignedCode = tmark.status.lawOffAsgnCd,
                                        CurrentLocationCode = tmark.status.currLocationCd,
                                        CurrentLocationDate = tmark.status.currLocationDt != null ? DateTime.Parse(tmark.status.currLocationDt) : null,
                                        ExternalStatusDescription = tmark.status.extStatusDesc,
                                        CurrentLocation = tmark.status.currentLoc,
                                        NewLawOffAssignedCode = tmark.status.newLawOffAsgnCd,
                                        LawOffAssigned = tmark.status.lawOffAssigned,
                                        Tm5Status = tmark.status.tm5Status,
                                        Tm5StatusDescription = tmark.status.tm5StatusDesc,
                                        StatusDefination = tmark.status.tm5StatusDef,
                                        UsRegistrationNumber = tmark.status.usRegistrationNumber,
                                        UsRegistrationDate = tmark.status.usRegistrationDate != null ? DateTime.Parse(tmark.status.usRegistrationDate) : null,
                                        FiliingDate = tmark.status.filingDate != null ? DateTime.Parse(tmark.status.filingDate) : null,
                                        PublicationDate = tmark.publication.datePublished != null ? DateTime.Parse(tmark.publication.datePublished) : null,
                                        PublicationAllowanceDate = tmark.publication.noticeOfAllowanceDate != null ? DateTime.Parse(tmark.publication.noticeOfAllowanceDate) : null,
                                        CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                        ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                        Attorney = findAttorney,
                                        GoodAndServices = GoodAndServiceCollection,
                                        OwnerGroups = OwnerGroupCollection,
                                        ProsecutionHistorys = ProsecutionHistoryCollection,
                                        PublicationOfficialGazettes = PublicationOfficialGazetteCollection,
                                        DesignSearches = DesignSearchClassCollection,
                                        PhysicalLocationHistorys = PhysicalLocationHistoryCollection,
                                        SyncDate = DateTimeHelper.Now(),
                                        Modified = DateTimeHelper.Now()
                                    };

                                    var document = (Document)default;
                                    using (TextReader sr = new StringReader(xmlString))
                                    {
                                        var serializer = new System.Xml.Serialization.XmlSerializer(typeof(DocumentList));
                                        DocumentList xmlResponse = (DocumentList)serializer.Deserialize(sr);

                                        foreach (var item in xmlResponse.Document)
                                        {
                                            document = new Document();
                                            var docType = await _trademarkRepository.FindDocumentTypeByCode(item.DocumentTypeCode);
                                            docType = docType == null ? trademark.Documents.Select(x => x.DocumentType).Where(d => d.Code == item.DocumentTypeCode).FirstOrDefault() : docType;
                                            var docCategory = await _trademarkRepository.FindDocumentCategoryByCode(item.CategoryTypeCode);
                                            docCategory = docCategory == null ? trademark.Documents.SelectMany(x => x.DocumentCategorys).Where(d => d.Code == item.CategoryTypeCode).FirstOrDefault() : docCategory;
                                            if (docType == null)
                                            {
                                                docType = new DocumentType();
                                                docType.Code = item.DocumentTypeCode;
                                                docType.DocumentTypeCode = item.DocumentTypeCode;
                                                docType.CategoryTypeCode = item.CategoryTypeCode;
                                                docType.CodeDescription = item.CategoryTypeCodeDescriptionText;
                                                docType.Description = item.DocumentTypeCodeDescriptionText;
                                                docType.CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
                                                _trademarkRepository.Create(docType);
                                            }
                                            document.DocumentType = docType;

                                            if (docCategory == null)
                                            {
                                                docCategory = new DocumentCategory()
                                                {
                                                    Code = item.CategoryTypeCode,
                                                    Description = item.CategoryTypeCodeDescriptionText,
                                                    CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name
                                                };
                                                document.DocumentCategorys.Add(docCategory);
                                            }

                                            document.SerialNumber = (long)item.SerialNumber;
                                            document.MailRoomDate = item.MailRoomDate;
                                            document.ScanDateTime = item.ScanDateTime;
                                            document.TotalPageQuantity = item.TotalPageQuantity;
                                            document.SourceSystem = item.SourceSystem;
                                            document.CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
                                            document.DocumentTypeId = docType.Id;
                                            document.CategoryId = docCategory.Id;

                                            for (int i = 0; i < item.PageMediaTypeList.Length; i++)
                                            {
                                                //if (item.CategoryTypeCodeDescriptionText == "Drawing")
                                                //{
                                                //    trademark.Image = item.UrlPathList[i];
                                                //}
                                                trademark.Image = "https://tsdr.uspto.gov/img/" + trademark.SerialNumber + "/large";
                                                var docUrl = new DocumentUrl()
                                                {
                                                    MediaType = item.PageMediaTypeList[i],
                                                    Url = item.UrlPathList[i],
                                                    CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name,
                                                    DocumentId = document.Id
                                                };
                                                document.DocumentUrls.Add(docUrl);
                                            }
                                            //if (string.IsNullOrEmpty(trademark.Image))
                                            //    trademark.Image = "https://tsdr.uspto.gov/img/" + trademark.SerialNumber + "/large";
                                            trademark.Documents.Add(document);
                                        }
                                    }

                                    var statusChecker = new TrademarkStatusChecker(trademark);
                                    trademark.CurrentStatus = statusChecker.CurrentStatus;
                                    trademark.StatusColor = statusChecker.RegisteredStatusColor.ToString();
                                    trademark.NextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value : null;
                                    trademark.MaxNextDeadline = statusChecker.NextDeadline.HasValue ? statusChecker.NextDeadline.Value.AddYears(1) : null;
                                    _trademarkRepository.Create(trademark);
                                    trademarkName = trademark.Name;
                                    response.Id = trademark.Id;
                                }
                            }
                        }
                        if (results.Errors.Count == 0)
                        {
                            await _UnitOfWork.CommitAsync();

                            results.IsSuccess = true;

                            //await context.DeleteAsync<TradeMarkDataLog>(findLog);
                            if (isknown)
                            {
                                if (response.Status == TrademarkStatus.TrademarkLinked)
                                {
                                    await _notificationService.AddNotification(response.PrimaryEmail, Constants.NewTrademarkLinkedMessage + trademarkName, Constants.TMManagerSupport, _httpContextAccessor.HttpContext.User.Identity.Name);
                                    var usr = await _userManager.FindByNameAsync(response.PrimaryEmail);
                                    string cc = string.Join(',', usr.UserContacts.Select(x => x.Email).ToList());
                                    await EmailSender.SendEmail(response.PrimaryEmail, Constants.NewActivitySubject, GetEmailBody(TrademarkStatus.TrademarkLinked, response.ClientName, string.Empty, string.Empty, string.Empty), cc, string.Empty);
                                }
                                else if (response.Status == TrademarkStatus.TrademarkAccountCreated)
                                {
                                    string link = $"{AppEnvironment.ServerUrl}register/" + response.PrimaryEmail + "";
                                    string cc = string.Empty;
                                    await EmailSender.SendEmail(response.PrimaryEmail, Constants.RegistrationEmailSubject, GetEmailBody(TrademarkStatus.TrademarkAccountCreated, !string.IsNullOrWhiteSpace(user.ClientName) ? user.ClientName : (user.LastName + " " + user.FirstName), user.UserName, link, code), cc, string.Empty);
                                }
                            }
                        }
                        else
                        {
                            response.Id = Guid.Empty;
                        }
                    }
                    else
                    {
                        response.Status = TrademarkStatus.SystemError;
                        results.Errors.Add("Error while saving trademark");
                    }
                }
            }
            catch (Exception e)
            {
                response.Status = TrademarkStatus.SystemError;
                results.Errors.Add(e.Message);
            }
            //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDetail", string.Empty);
            //_httpContextAccessor.HttpContext.Session.SetString("GetTrademarkDocument", string.Empty);
            //_httpContextAccessor.HttpContext.Session.Clear();
            results.TrademarkResponse.Add(response);
            return results;
        }
        public async Task<SaveResults> RejectFromBatch(Guid id)
        {
            var results = new SaveResults();
            var data = await _trademarkRepository.GetTrademarkImportItemById(id);
            if (data != null)
            {
                data.IsActive = false;
                data.Status = TrademarkStatus.Rejected;
                _trademarkRepository.Update(data);
                await _UnitOfWork.CommitAsync();
                results.IsSuccess = true;
            }
            return results;
        }
        public async Task<SaveResults> AcceptFromBatch(Guid id)
        {
            var results = new SaveResults();
            try
            {
                var data = await _trademarkRepository.GetTrademarkImportItemById(id);
                if (data != null)
                {
                    var response = await SaveTrademark(data.SerialNumber.ToString(), data.Email);
                    //var response = await SaveBatchTrademark(data.SerialNumber.ToString(), data.Email);
                    if (response.Errors.Any() == false)
                    {
                        data.SerialNumber = response.TrademarkResponse[0].SerialNumber;
                    }
                    data.Status = response.TrademarkResponse[0].Status;
                    data.TrademarkId = response.TrademarkResponse[0].Id;
                    _trademarkRepository.Update(data);
                    await _UnitOfWork.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                results.Errors.Add(ex.Message);
            }
            
            return results;
        }
        private string GetEmailBody(TrademarkStatus status, string name, string email, string link, string accessCode)
        {
            string body = string.Empty;
            if (status == TrademarkStatus.TrademarkLinked)
            {
                body = @"<!doctype html>
                <html>
                  <head>
                    <meta name='viewport' content='width=device-width'>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <title></title>
                  <style>
                @media only screen and (max-width: 620px) {
                  table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                  }

                  table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                  }

                  table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                  }

                  table[class=body] .content {
                    padding: 0 !important;
                  }

                  table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                  }

                  table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                  }

                  table[class=body] .btn table {
                    width: 100% !important;
                  }

                  table[class=body] .btn a {
                    width: 100% !important;
                  }

                  table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                  }
                }
                @media all {
                  .ExternalClass {
                    width: 100%;
                  }

                  .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                  }

                  .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                  }

                  .btn-primary table td:hover {
                    background-color: #b57e20 !important;
                  }

                  .btn-primary a:hover {
                    background-color: #b57e20 !important;
                    border-color: #b57e20 !important;
                  }
                }
                </style></head>
                  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                           
                            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Username],</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>There is new activity on your TM Chart account. Please login to the system for additional details.</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 3px;'>Best,</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>The TM Chart Team</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                            <!-- END MAIN CONTENT AREA -->
                            </table>


                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>";
            }
            else if (status == TrademarkStatus.TrademarkAccountCreated)
            {
                body = @"<!doctype html>
                <html>
                  <head>
                    <meta name='viewport' content='width=device-width'>
                    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                    <title></title>
                  <style>
                @media only screen and (max-width: 620px) {
                  table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                  }

                  table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                  }

                  table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                  }

                  table[class=body] .content {
                    padding: 0 !important;
                  }

                  table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                  }

                  table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                  }

                  table[class=body] .btn table {
                    width: 100% !important;
                  }

                  table[class=body] .btn a {
                    width: 100% !important;
                  }

                  table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                  }
                }
                @media all {
                  .ExternalClass {
                    width: 100%;
                  }

                  .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                  }

                  .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                  }
                }
                </style></head>
                  <body class='' style='background-color: #eaebed; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>
                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background-color: #eaebed; width: 100%;' width='100%' bgcolor='#eaebed'>
                      <tr>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                        <td class='container' style='font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; Margin: 0 auto;' width='580' valign='top'>
                          <div class='content' style='box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'>

                            <!-- START CENTERED WHITE CONTAINER -->
                           
                            <table role='presentation' class='main' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #ffffff; border-radius: 3px; width: 100%;' width='100%'>

                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;' valign='top'>
                                  <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;' width='100%'>
                                    <tr>
                                      <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Dear [Username],</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>An account has been set up by an admin for the TM Chart app.</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 0px;'>Username: [Email]</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Access code: [AccessCode]</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>Please go to this URL to register your account:</p>
                                        <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='btn btn-primary' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; box-sizing: border-box; width: 100%;' width='100%'>
                                          <tbody>
                                            <tr>
                                              <td align='center' style='font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;' valign='top'>
                                                <table role='presentation' border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: auto; width: auto;'>
                                                  <tbody>
                                                    <tr>
                                                      <td  valign='top' align='left'> <a href='[Link]' target='_blank'>Register Account</a> </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>

                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 3px;'>Best,</p>
                                        <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;'>The TM Chart Team</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                            <!-- END MAIN CONTENT AREA -->
                            </table>

                          <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;' valign='top'>&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>";

                body = body.Replace(Convert.ToString("[Email]"), email);
                body = body.Replace(Convert.ToString("[Link]"), link);
                body = body.Replace(Convert.ToString("[AccessCode]"), accessCode);
                body = body.Replace(Convert.ToString("[LogoUrl]"), Constants.LogoUrl);
            }
            body = body.Replace(Convert.ToString("[Username]"), name);
            return body;
        }

        public async Task<TrademarkCountsResponse> GetDashboardCounts()
        {
            var response = new TrademarkCountsResponse();
            var query = _trademarkRepository.TrademarkIQuerableAsync().Where(x => x.IsActive);
            var now = DateTimeHelper.Now();
            var startDate = now.AddDays(-7).Date;
            response.TrademarkCounts = await query.CountAsync();
            var docs = query.SelectMany(x => x.Documents).Where(x => x.IsActive && x.MailRoomDate >= startDate && x.MailRoomDate <= now);
            var events = query.SelectMany(x => x.ProsecutionHistorys).Where(x => x.IsActive && x.EntryDate.HasValue && x.EntryDate >= startDate && x.EntryDate <= now);
            var users = await _userManager.Users.Where(x => x.Active && x.UserType == UserType.AppUser).CountAsync();
            response.EventsCounts = await events.CountAsync();
            response.DocumentsCounts = await docs.CountAsync();
            response.ClientsCount = users;
            return response;
        }

        public async Task UpdateImportedFilesInfo(Guid id)
        {
            var failedStatus = new[] { TrademarkStatus.TrademarkNotFound, TrademarkStatus.TrademarkInSystem, TrademarkStatus.TrademarkFailed };
            var tmImportFile = await _trademarkRepository.GetTrademarkImportItemById(id);
            if (tmImportFile.Status != TrademarkStatus.TrademarkEmailNotFound)
            {
                Thread.Sleep(1000);
                var getTrademarkDetail = await GetTrademarkForBatch(tmImportFile.Email, tmImportFile.SerialNumber.ToString());
                tmImportFile.Email = getTrademarkDetail.PrimaryEmail;
                tmImportFile.ClientName = getTrademarkDetail.ClientName;
                tmImportFile.TrademarkName = getTrademarkDetail.Name;
                tmImportFile.Status = failedStatus.Any(x => x == (TrademarkStatus)getTrademarkDetail.Status) ? (TrademarkStatus)getTrademarkDetail.Status : TrademarkStatus.Pending;
                //tmImportFile.TrademarkId = item.TrademarkId;
                _trademarkRepository.Update(tmImportFile);
                await _UnitOfWork.CommitAsync();
            }
            else
            {
                tmImportFile.Status = TrademarkStatus.TrademarkEmailNotFound;
                _trademarkRepository.Update(tmImportFile);
                await _UnitOfWork.CommitAsync();
            }
        }

        public async Task UpdateAllImportedFilesInfo(List<Guid> ids)
        {
            var failedStatus = new[] { TrademarkStatus.TrademarkNotFound, TrademarkStatus.TrademarkInSystem, TrademarkStatus.TrademarkFailed };
            foreach (var id in ids)
            {
                await UpdateImportedFilesInfo(id);
            }
        }

        private List<OwnerGroup> FillOwnerGroupData(JObject ownerGroups)
        {
            var createdBy = _httpContextAccessor.HttpContext != null ? _httpContextAccessor.HttpContext.User.Identity.Name : "webmaster@kwworks.com";
            var OwnerGroupCollection = new List<OwnerGroup>();
            var ownerGroupsList = new List<GetTrademarkDetails_10>();
            foreach (var each in ownerGroups)
            {
                var ownergroup = each.Value.ToObject<List<GetTrademarkDetails_10>>();
                foreach (var item in ownergroup)
                {
                    var address1 = !string.IsNullOrEmpty(item.address1) ? item.address1 : "";
                    OwnerGroupCollection.Add(new OwnerGroup()
                    {
                        Name = item.name,
                        Address = $"{address1} {item.address2}",
                        City = item.city,
                        Email = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() >= 0 ? item.emailList.addresses[0] : string.Empty,
                        Email2 = item.emailList != null && item.emailList.addresses != null && item.emailList.addresses.Count() > 1 ? item.emailList.addresses[1] : string.Empty,
                        State = item.addressStateCountry.stateCountry != null ? item.addressStateCountry.stateCountry.code : string.Empty,
                        IsoCode = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.code : string.Empty,
                        IsoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
                        WipoCode = item.addressStateCountry.wipo != null ? item.addressStateCountry.wipo.code : string.Empty,
                        WipoName = item.addressStateCountry.iso != null ? item.addressStateCountry.iso.name : string.Empty,
                        Type = item.partyType,
                        ZipCode = item.zip,
                        Country = item.citizenship != null && item.citizenship.iso != null && item.citizenship.iso.code != null ? CountryHelper.GetCountryByTwoDigitCode(item.citizenship.iso.code) : string.Empty,
                        TypeCode = item.entityType.code,
                        TypeDescription = item.entityType.description,
                        CreatedBy = createdBy,
                        ModifiedBy = createdBy,
                        IsActive = true,
                    });
                }
            }

            return OwnerGroupCollection;
        }
    }
}
