﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public enum RegisteredTrademarkStatusColor
    {
        None=0,
        Green=1,
        Yellow=2,
        Red=3
    }
}
