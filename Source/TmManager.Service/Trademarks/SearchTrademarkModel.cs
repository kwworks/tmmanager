﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Trademarks
{
    public class SearchTrademarkModel
    {
        [Required(ErrorMessage = "Serial Number field is required")]
        //[Range(10000000, 99999999, ErrorMessage = "Serial Number(s) is/are not in the proper format. A U.S. Serial Number consists of 8 digits, with no alpha characters.")]
        public long? SerialNumber { get; set; }
        [Required(ErrorMessage = "Email field is required")]
        public string Email { get; set; }
    }
}
