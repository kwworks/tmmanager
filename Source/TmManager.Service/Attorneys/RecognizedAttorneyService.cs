﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Admin;
using TmManager.Data;
using TmManager.Identity;
using TmManager.Trademarks;

namespace TmManager.Attorneys
{
    public class RecognizedAttorneyService : IRecognizedAttorneyService
    {
        private readonly UserManager _userManager;
        private readonly IRecognizedAttorneyRepository _recognizedAttorneyRepository;
        private readonly ITrademarkRepository _trademarkRepository;
        private readonly IUnitOfWork _UnitOfWork;
        public RecognizedAttorneyService(UserManager userManager, IRecognizedAttorneyRepository recognizedAttorneyRepository, ITrademarkRepository trademarkRepository, IUnitOfWork UnitOfWork)
        {
            _userManager = userManager;
            _trademarkRepository = trademarkRepository;
            _recognizedAttorneyRepository = recognizedAttorneyRepository;
            _UnitOfWork = UnitOfWork;
        }
        public async Task<SaveResults> SaveRecognizedAttorney(RecognizedAttorneyRequestModel model)
        {
            var results = new SaveResults();
            var now = DateTimeHelper.Now();
            try
            {
                var attorney = default(RecognizedAttorney);
                if (model.Id.HasValue)
                    attorney = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetById(model.Id.Value));

                if (attorney == null)
                {
                    attorney = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByEmailAndName(model.Email.Trim(), model.Name.Trim()));
                    if (attorney == null)
                    {
                        attorney = new RecognizedAttorney();
                        _recognizedAttorneyRepository.Create(attorney);
                    }
                    else
                        results.Errors.Add("Duplicate record found");
                }
                else
                {
                    var checkDuplicate = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByFilters(model.Id.Value, model.Email.Trim(), model.Name.Trim()));
                    if (checkDuplicate == null)
                    {
                        _recognizedAttorneyRepository.Update(attorney);
                    }
                    else
                        results.Errors.Add("Duplicate record found");
                }

                attorney.Name = model.Name;
                attorney.Email = model.Email;
                attorney.CreatedBy = model.Email;
                attorney.ModifiedBy = model.Email;
                attorney.IsActive = true;

                if (results.Errors.Count == 0)
                {
                    await _UnitOfWork.CommitAsync();
                    results.Id = attorney.Id;
                    results.IsSuccess = true;
                }
            }
            catch (Exception e)
            {
                results.Errors.Add(e.Message);
            }
            return results;
        }
        public async Task<SaveResults> SaveAdmin(AdminUserModel model)
        {
            var results = new SaveResults();
            var now = DateTimeHelper.Now();
            try
            {
                var user = default(User);
                if (string.IsNullOrEmpty(model.Id))
                    user = await _userManager.FindByIdAsync(model.Id);

                if (user == null)
                {
                    user = new User();
                    user.FirstName = model.Name;
                    user.FullName = model.Name;
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    user.Modified = DateTime.Now;
                    user.EmailConfirmed = true;
                    user.PhoneNumberConfirmed = true;
                    user.Active = true;
                    user.LastLogIn = null;
                    user.LockoutEnabled = false;
                    user.UserType = UserType.Admin;
                    user.Created = DateTime.Now;
                    var response = await _userManager.CreateAsync(user, model.Password);
                    if (response.Errors.Any())
                    {
                        results.Errors = response.Errors.Select(e => e.Description).ToHashSet<string>();
                    }
                }
                else
                {
                    user.FirstName = model.Name;
                    user.FullName = model.Name;
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    user.Modified = DateTime.Now;
                    var response = await _userManager.UpdateAsync(user);
                }

                if (results.Errors.Count == 0)
                {
                    await _UnitOfWork.CommitAsync();
                    results.Id = Guid.Parse(user.Id);
                    results.IsSuccess = true;
                }
            }
            catch (Exception e)
            {
                results.Errors.Add(e.Message);
            }
            return results;
        }
        public async Task<SaveResults> DeleteRecognizedAttorney(Guid id)
        {
            var model = new SaveResults();
            var result = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetById(id));
            if (result != null)
            {
                _recognizedAttorneyRepository.Delete(result);
                await _UnitOfWork.CommitAsync();
                model.IsSuccess = true;
            }
            return model;
        }
        public async Task<RecognizedAttorneyModel> GetByRecognizedAttorneyName(string name)
        {
            var model = new RecognizedAttorneyModel();
            var result = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByName(name));
            if (result != null)
            {
                model.Id = result.Id;
                model.Name = result.Name;
                model.Email = result.Email;
            }
            return model;
        }
        public async Task<RecognizedAttorneyModel> GetByRecognizedAttorneyEmail(string email,string name)
        {
            var model = new RecognizedAttorneyModel();
            var result = await _recognizedAttorneyRepository.FindRecognizedAttorneyAsync(RecognizedAttorneySpecification.GetByEmail(email));
            if (result != null)
            {
                model.Id = result.Id;
                model.Name = result.Name;
                model.Email = result.Email;
            }
            return model;
        }
    }
}