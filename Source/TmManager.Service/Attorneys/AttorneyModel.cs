﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Trademarks;

namespace TmManager.Attorneys
{
    public class AttorneyModel
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Line1 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public bool IsEmailAuthticated { get; set; }
        public Guid? FirmId { get; set; }
        public string FirmName { get; set; }
        public FirmModel Firm { get; set; }
    }
}
