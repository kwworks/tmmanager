﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class RecognizedAttorneyModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class RecognizedAttorneyRequestModel
    {
        public Guid? Id { get; set; }
        [Required(ErrorMessage = "Attorney Name field is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Primary Email field is required")]
        public string Email { get; set; }
    }
}
