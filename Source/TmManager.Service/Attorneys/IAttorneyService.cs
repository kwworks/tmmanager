﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public interface IAttorneyService
    {
        Task<SaveResults> SaveAttorney(AttorneyModel model);
        Task<AttorneyModel> GetByAttorneyName(string name);
        Task<FirmModel> GetByFirmName(string firmName);
        Task<SaveResults> SaveFirm(FirmModel model);
    }
}
