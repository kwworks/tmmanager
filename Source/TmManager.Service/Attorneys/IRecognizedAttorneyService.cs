﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Admin;

namespace TmManager.Attorneys
{
    public interface IRecognizedAttorneyService
    {
        Task<SaveResults> SaveRecognizedAttorney(RecognizedAttorneyRequestModel model);
        Task<SaveResults> SaveAdmin(AdminUserModel model);
        Task<SaveResults> DeleteRecognizedAttorney(Guid id);
        Task<RecognizedAttorneyModel> GetByRecognizedAttorneyName(string name);
        Task<RecognizedAttorneyModel> GetByRecognizedAttorneyEmail(string email, string name);
    }
}
