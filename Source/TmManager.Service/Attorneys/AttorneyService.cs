﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Attorneys
{
    public class AttorneyService : IAttorneyService
    {
        private readonly IFirmRepository _firmRepository;
        private readonly IAttorneyRepository _attorneyRepository;
        private readonly IUnitOfWork _UnitOfWork;
        public AttorneyService(IAttorneyRepository attorneyRepository, IFirmRepository firmRepository, IUnitOfWork UnitOfWork)
        {
            _attorneyRepository = attorneyRepository;
            _firmRepository = firmRepository;
            _UnitOfWork = UnitOfWork;
        }

        public async Task<SaveResults> SaveAttorney(AttorneyModel model)
        {
            var results = new SaveResults();
            var now = DateTimeHelper.Now();
            try
            {
                var attorney = new Attorneys.Attorney();
                attorney.Name = model.Name;
                attorney.FullName = model.FullName;
                attorney.Email = model.Email;
                attorney.Email2 = model.Email2;
                attorney.FirmId = model.FirmId;
                attorney.Line1 = model.Line1;
                attorney.City = model.City;
                attorney.PostalCode = model.PostalCode;
                attorney.CountryCode = model.CountryCode;
                attorney.CountryName = model.CountryName;
                attorney.IsActive = true;

                _attorneyRepository.Create(attorney);
                if (results.Errors.Count == 0)
                {
                    await _UnitOfWork.CommitAsync();
                    results.Id = attorney.Id;
                    results.IsSuccess = true;
                }
            }
            catch (Exception e)
            {
                results.Errors.Add(e.Message);
            }
            return results;
        }
        public async Task<AttorneyModel> GetByAttorneyName(string name)
        {
            var model = new AttorneyModel();
            var result = await _attorneyRepository.FindAttorney(AttorneySpecification.GetByName(name));
            if (result != null)
            {
                model.Id = result.Id;
                model.Name = result.Name;
                model.FullName = result.FullName;
                model.Email = result.Email;
                model.Email2 = result.Email2;
                model.FirmId = result.FirmId;
                model.FirmName = result.Firm.Name;
                model.Line1 = result.Line1;
                model.City = result.City;
                model.PostalCode = result.PostalCode;
                model.CountryCode = result.CountryCode;
                model.CountryName = result.CountryName;
            }
            return model;
        }
        public async Task<FirmModel> GetByFirmName(string firmName)
        {
            var model = new FirmModel();
            var result = await _firmRepository.FindFirmAsync(FirmSpecification.GetByName(firmName));
            if (result != null)
            {
                model.Id = result.Id;
                model.Email = result.Email;
                model.Fax = result.Fax;
                model.Name = result.Name;
                model.Phone = result.Phone;
            }
            return model;
        }
        public async Task<SaveResults> SaveFirm(FirmModel model)
        {
            var results = new SaveResults();
            var now = DateTimeHelper.Now();
            try
            {
                var firmExists = await _firmRepository.FindFirmAsync(FirmSpecification.GetByName(model.Name));
                if (firmExists == null)
                {
                    var firm = new Attorneys.Firm();
                    firm.Name = model.Name;
                    firm.Phone = model.Phone;
                    firm.Email = model.Email;
                    firm.Fax = model.Fax;
                    firm.CreatedBy = "TM.admin";
                    firm.ModifiedBy = "TM.admin";
                    firm.IsActive = true;
                    _firmRepository.Create(firm);
                    if (results.Errors.Count == 0)
                    {
                        await _UnitOfWork.CommitAsync();
                        results.Id = firm.Id;
                        results.IsSuccess = true;
                    }
                }
                else
                {
                    results.IsSuccess = true;
                    results.Id = firmExists.Id;
                }
            }
            catch (Exception e)
            {
                results.Errors.Add(e.Message);
            }
            return results;
        }
    }
}