﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Attorneys
{
    public class FirmModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string IndividualName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string IsEmailAuthticated { get; set; }
    }
}
