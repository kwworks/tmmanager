﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager
{
    public class GridDataSourceResultModel
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }
}
