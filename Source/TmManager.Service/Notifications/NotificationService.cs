﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmManager.Data;

namespace TmManager.Notifications
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _repository;
        private readonly IUnitOfWork _UnitOfWork;
        public NotificationService(INotificationRepository repository, IUnitOfWork uow)
        {
            _repository = repository;
            _UnitOfWork = uow;
        }
        public async Task<List<Notification>> GetAllNotifications(string username)
        {
            return await _repository.ListNotificationAsync(NotificationSpecification.GetAllByUserName(username));
        }

        public async Task<int> GetUnReadNotificationCount(string username)
        {
           var data = await _repository.ListNotificationAsync(NotificationSpecification.GetUnRead(username));
            return data.Count();
        }
        public async Task<SaveResults> AddNotification(string username,string message,string from, string createdBy)
        {
            var results = new SaveResults();
            var now = DateTimeHelper.Now();
            var notification = new Notification()
            {
                Created = now,
                CreatedBy = createdBy,
                From = from,
                IsActive = true,
                IsRead = false,
                Message = message,
                UserName = username
            };
            _repository.Create(notification);
            await _UnitOfWork.CommitAsync();
            results.IsSuccess = true;
            return results;
        }
        public async Task<Notification> GetNotification(Guid id)
        {
            var notification = await _repository.FindAsync(id);
            if(notification != null)
            {
                notification.IsRead = true;
                _repository.Update(notification);
                await _UnitOfWork.CommitAsync();
            }
            return notification;
        }
    }
}
