﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmManager.Notifications
{
    public interface INotificationService
    {
        Task<int> GetUnReadNotificationCount(string username);
        Task<List<Notification>> GetAllNotifications(string username);
        Task<Notification> GetNotification(Guid id);
        Task<SaveResults> AddNotification(string username, string message, string from, string createdBy);
    }
}
